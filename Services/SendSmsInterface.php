<?php

namespace App\Services;


use phpDocumentor\Reflection\Types\Integer;

/**
 * Interface For Sending Sms
 */
interface SendSmsInterface{

    /**
     * @param string $message
     * @param string $phone
     *
     * @return mixed $status
     */

     public function send(string $message ,string $phone );

}
