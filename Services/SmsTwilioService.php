<?php


namespace App\Services;
use App\Services\SendSmsInterface;
use Twilio\Rest\Client;

class SmsTwilioService  implements SendSmsInterface
{

    protected $client;
    protected $twillo_number;

    function __construct(Client $client,string $twillo_number ){
        $this->client = $client;
        $this->twillo_number = $twillo_number;
    }

    public function send(string $message,string $phone)
    {
        $response = $this->client->messages->create(
            $phone,
            [
                'from' => $this->twillo_number,
                'body' => $message
            ]
        );

        dd($response->status);
    }

}
