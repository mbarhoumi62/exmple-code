<?php

namespace App\Services;

use App\Services\SendSmsInterface;
use Illuminate\Support\Facades\Http;


class SmsMoviderService extends Http implements SendSmsInterface
{

    protected $data;
    protected $url;

    function __construct($data,$url)
    {
        $this->data = $data;
        $this->url  = $url;
    }

    /**
     * @param string $message
     * @param string $phone
     *
     * @return int  $status
     */
    public function send($message, $phone)
    {
        $data = [
            'text'  => $message,
            'to'    => $phone,
            'from'  => "MOVIDER"

        ];
        $data = array_merge($this->data,$data);
        $headers = [
            "Content-Type"  => "application/x-www-form-urlencoded",
            "cache-control" => "no-cache"
        ];
        $response = static::asForm()->withHeaders($headers)->post($this->url, $data);

        return $response->status();
    }
}
