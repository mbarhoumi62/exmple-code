<?php

namespace App\Modules\Meeting\Controllers;

use App\Modules\Category\Models\Category;
use App\Modules\Category\Models\SubCategory;
use App\Modules\Meeting\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Mail;

use App\Modules\Place\Models\City;
use App\Modules\Place\Models\Delegation;

use App\Modules\Meeting\Models\Meeting;
use App\Modules\Meeting\Models\UserParticipation;

use App\Modules\Meeting\Models\Session;
use Carbon\Carbon;
use Auth;
use UxWeb\SweetAlert\SweetAlert;

class MeetingController extends Controller
{

    public function showDetailMeeting($id)
    {
        $hasParticipated = null;
        $meeting = Meeting::where('id', $id)
            ->with('category')
            ->with('subCategory')
            ->with('creator')
            ->with('sessions')
            ->first();

        if(!$meeting){
            SweetAlert::error('Oops', "Le RDV demander n'existe pas !")->persistent('Fermer');
            return back();
        }

        $sessionsIds = [];

        foreach ($meeting->sessions as $session){
            $sessionsIds[] = $session->id;
        }
        $pts = UserParticipation::whereIn('session_id',$sessionsIds)->first();

        if($pts){
            $hasParticipated = true;
        }

        $pNbr =  $meeting->sessions->first()->participiants_nbr;

        $places = $pNbr - count(UserParticipation::where('session_id',$meeting->sessions->first()->id)->get());


        \Carbon\Carbon::setLocale('fr'); setlocale(2, 'fr', 'fr_FR');
        foreach ($meeting->sessions as $session){
        $dates[] = \Carbon\Carbon::parse($session->start_date)->formatLocalized('%A %d %b %Y');

        }
        $categoryId = $meeting->category->id;

        $recomandedMetings = [];

        $meetings = Meeting::where('id', '!=', $id)->get();

        foreach ($meetings as $key => $m) {
            $sim = similar_text($meeting->title, $m->title, $percent);
            if ($percent > 10) {
                $recomandedMetings[] = $m;
            }
        }
        /** Get Current Date in a string format */
        $completeDate = Carbon::now();
        $today = $completeDate->toDateString();

        /** Get end date of the meeting in a string format */
        $endDate = $meeting->sessions->first()->end_date->toDateString();
        $checkParticipation = UserParticipation::where('session_id', $meeting->sessions->first()->id)
            ->first();

        return view("Meeting::detailMeeting", [
            'today' => $today,
            'endDate' => $endDate,
            'meeting' => $meeting,
            'meetings' => array_slice($recomandedMetings, 0, 3),
            'checkParticipation' => $checkParticipation,
            'places' => $places,
            'dates' => $dates,
            'hasParticipated' => $hasParticipated
        ]);

    }

    public function apiFilterByCity(Request $request)
    {
        $city = $request->get('city');
        $offset = 0;
        if ($request->get('offset')) {
            $offset = $request->get('offset');
        }
        $meetings = Meeting::where('status', 1)->orderBy('created_at', 'desc')->whereHas(
            'sessions.place.city',
            function ($query) use ($city) {
                $query->where('name', $city);
            }
        )->skip($offset * 3)->take(3)->get();

        if ($offset > (ceil(count(Meeting::whereHas(
                    'sessions.place.city',
                    function ($query) use ($city) {
                        $query->where('name', $city);
                    }
                )->get())) / 3)) {

            return response()->json(['status' => '201']);
        }

        $count = count(Meeting::where('status', 1)->orderBy('created_at', 'desc')->whereHas(
            'sessions.place.city',
            function ($query) use ($city) {
                $query->where('name', $city);
            }
        )->get());

        $view = view('General::load.meetings', [
            'meetings' => $meetings
        ])->render();

        return response()->json(['html' => $view , 'count' => $count]);
    }

    public function apiFilterByDelegation(Request $request)
    {
        $offset = 0;
        $type = $request->get('type');
        if ($request->get('offset')) {
            $offset = $request->get('offset');
        }
        $delegation = $request->get('delegation');
        $meetings = Meeting::where('status', 1)->orderBy('created_at', 'desc')->whereHas('sessions.place.delegation', function ($query) use ($delegation) {
            $query->where('id', $delegation);
        })->skip($offset * 3)->take(3)->get();

        if ($offset > (ceil(count(Meeting::whereHas('sessions.place.delegation', function ($query) use ($delegation) {
                    $query->where('id', $delegation);
                })->get())) / 3)) {

            return response()->json(['status' => '201']);
        }

        $count = count(Meeting::where('status', 1)->orderBy('created_at', 'desc')->whereHas('sessions.place.delegation', function ($query) use ($delegation) {
            $query->where('id', $delegation);
        })->get());

        $view = view('General::load.meetings', [
            'meetings' => $meetings
        ])->render();

        return response()->json(['html' => $view , 'count' => $count]);

    }

    public function apiSortMeetings(Request $request)
    {
        $offset = 0;
        $type = $request->get('type');
        if ($request->get('offset')) {
            $offset = $request->get('offset');
        }

        $allMeetings = Meeting::where('status', 1)->get();

        if ($offset > (ceil(count($allMeetings)) / 3)) {

            return response()->json(['status' => '201']);
        }

        switch ($type) {
            case 1 :
                $meetings = Meeting::where('status', 1)->orderByDesc('created_at')->skip($offset * 3)->take(3)->get();
                break;
            case 2 :

                $meetings = Meeting::join('sessions as session', 'session.meeting_id', '=', 'meetings.id')->orderBy('session.price', 'desc')->select('meetings.*')->where('meetings.status', 1)->with('sessions')->skip($offset * 3)->take(3)->get();

                break;
            case 3 :
                $meetings = Meeting::join('sessions as session', 'session.meeting_id', '=', 'meetings.id')->orderBy('session.price', 'asc')->select('meetings.*')->where('meetings.status', 1)->with('sessions')->skip($offset * 3)->take(3)->get();


        }
        $view = view('General::load.meetings', [
            'meetings' => $meetings
        ])->render();

        return response()->json(['html' => $view]);
    }

    public function apiFilterBySubCategory(Request $request)
    {

        $offset = 0;
        if ($request->get('offset')) {
            $offset = $request->get('offset');
        }
        $subCategory = $request->get('subCategory');
        $meetings = Meeting::where('status', 1)->orderBy('created_at', 'desc')->whereHas('subCategory', function ($query) use ($subCategory) {
            $query->where('id', $subCategory);
        })->skip($offset * 3)->take(3)->get();

        if ($offset > (ceil(count(Meeting::whereHas('subCategory', function ($query) use ($subCategory) {
                    $query->where('id', $subCategory);
                })->get())) / 3)) {

            return response()->json(['status' => '201']);
        }

        $count = count(Meeting::where('status', 1)->orderBy('created_at', 'desc')->whereHas('subCategory', function ($query) use ($subCategory) {
            $query->where('id', $subCategory);
        })->get());

        $view = view('General::load.meetings', [
            'meetings' => $meetings
        ])->render();

        return response()->json(['html' => $view , 'count' => $count]);
    }

    public function getDelegationsByCity($city)
    {

        $city = City::where('name', $city)->first();
        $delegations = Delegation::where('city_id', $city->id)->get();

        return response()->json(['delegations' => $delegations]);
    }

    public function handleAddParticipant($id, Request $request)
    {
        $user = Auth::user();
        if (!$request->sessionId) {
            SweetAlert::error('Oups !', 'Vous devez choisir une session de ce RDV !')->persistent('Fermer');
            return redirect()->back();


        }

        $meeting = Meeting::find($id);

        if (!$meeting) {
            SweetAlert::error('Oops !', 'La réunion que vous cherché n\'existe pas !')->persistent('Fermer');
        }

        UserParticipation::create([

            'ticket_key' => bin2hex(random_bytes(24)),
            'presence' => 0,
            'session_id' => $request->sessionId,
            'user_id' => $user->id

        ]);
        $session = Session::find($request->sessionId);
        if ($session) {
            $session->save();
        } else {
            SweetAlert::error('Oups !', 'Aucune session trouvée!')->persistent('Fermer');
            return redirect()->back();

        }

        $creator = $meeting->creator;

        //mail to creator

        $content = ['user' => $user, 'creator' => $creator, 'meeting' => $meeting];
        Mail::send('User::mail.participation', $content, function ($message) use ($creator,$user,$meeting) {
            $message->to($creator->email);
            $message->subject('Nouvelle participation');
        });
        return redirect()->route('showPaymentPage');
    }

    public function handleVerifySession(Request $request)
    {
        $completeDate = Carbon::now();
        $today = $completeDate->toDateString();
        $participate = 0;

        $session = Session::find($request->sessionId);
        $endDate = $session->end_date->toDateString();
        $userParticipate = UserParticipation::where('session_id', $session->id)
            ->where('user_id', Auth::id())
            ->first();
        if ($userParticipate) {
            $participate = 1;
        }


        if ($today > $endDate) {
            return response()->json(['status' => 0, 'session' => $session, 'participate' => $participate]);


        } else {
            return response()->json(['status' => 1, 'session' => $session, 'participate' => $participate]);

        }
    }

    public function getCategories()
    {
        $categories = Category::all();
        return response()->json(['status' => 200, 'categories' => $categories]);

    }

    public function getSubCategories($id)
    {
        $subCategories = SubCategory::where('category_id', $id)->get();
        return response()->json(['status' => 200, 'subCategories' => $subCategories]);

    }

    public function handleStayInformed(){
        SweetAlert::success('Bien !', 'Votre e-mail a bien été enregistré, vous serez informés dès qu’une nouvelle session sera lancée  !')->persistent('Fermer');
        return back();
    }


    public function showPaymentPage(){

        $participations = Auth::user()->participations;
        $sessions = [];

        foreach ($participations as $participation){
            $sessions[] =  $participation->session;
        }


        return view('Meeting::payment',[
            'sessions' => $sessions
        ]);
    }

    public function handleDeleteParticipation($id){

         $part = UserParticipation::where('session_id',$id)->first();

         if(!$part){
             SweetAlert::error('Oops', "Le RDV demander n'existe pas !")->persistent('Fermer');
             return back();
         }

         $part->delete();


        $participations = Auth::user()->participations;
        $sessions = [];

        foreach ($participations as $participation){
            $sessions[] =  $participation->session;
        }


        return view('Meeting::payment',[
            'sessions' => $sessions
        ]);
    }

    public function handleSubmitComment(Request $request){

        $review = Review::create([
            'comment' => ($request->comment)? $request->comment : '' ,
            'note' =>($request->note)? $request->note : 0,
            'user_id' => Auth::id(),
            'meeting_id' => $request->meetingId
        ]);

        return back();
    }

    public function handleSearch(Request $request){
        $key = $request->get('key');
        $offset = 0;
        if ($request->get('offset')) {
            $offset = $request->get('offset');
        }
        $meetings = Meeting::where('status', 1)
            ->where('title','like','%'.$key.'%')
            ->orWhere('descreption','like','%'.$key.'%')
            ->orderBy('created_at', 'desc')
            ->skip($offset * 3)->take(3)->get();

        if ($offset > (ceil(count(Meeting::where('status', 1)
                    ->where('title','like','%'.$key.'%')
                    ->orWhere('descreption','like','%'.$key.'%')
                    ->orderBy('created_at', 'desc')->get())) / 3)) {

            return response()->json(['status' => '201']);
        }

        $count = count(Meeting::where('status', 1)
            ->where('title','like','%'.$key.'%')
            ->orWhere('descreption','like','%'.$key.'%')
            ->orderBy('created_at', 'desc')->get());

        $view = view('General::load.meetings', [
            'meetings' => $meetings
        ])->render();

        return response()->json(['html' => $view , 'count' => $count]);
    }


}
