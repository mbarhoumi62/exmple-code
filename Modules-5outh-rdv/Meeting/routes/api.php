<?php

Route::group(['module' => 'Meeting', 'middleware' => ['api'], 'namespace' => 'App\Modules\Meeting\Controllers'], function() {

    Route::resource('Meeting', 'MeetingController');

    Route::post('/filter/city','MeetingController@apiFilterByCity');
    Route::post('/sort/{type}','MeetingController@apiSortMeetings');
    Route::post('/filter/category','MeetingController@apiFilterByCategory');

});
