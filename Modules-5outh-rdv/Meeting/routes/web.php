<?php

Route::group(['module' => 'Meeting', 'middleware' => ['web'], 'namespace' => 'App\Modules\Meeting\Controllers'], function () {

    Route::resource('Meeting', 'MeetingController');

    Route::get('/detailMeeting/{id}', 'MeetingController@showDetailMeeting')->name('showDetailMeeting');

    Route::get('/delegations/{city}', 'MeetingController@getDelegationsByCity')->name('getDelegationsByCity');

    Route::get('/meetings/city', 'MeetingController@apiFilterByCity')->name('apiFilterByCity');
    Route::get('/meetings/subCategories', 'MeetingController@apiFilterBySubCategory')->name('apiFilterBySubCategory');
    Route::get('/session/verifiy', 'MeetingController@handleVerifySession')->name('handleVerifySession');

    Route::get('/meeting', 'MeetingController@apiFilterByDelegation')->name('apiFilterByDelegation');
    Route::get('/sort', 'MeetingController@apiSortMeetings')->name('apiSortMeetings');
    Route::get('/categories', 'MeetingController@getCategories')->name('getCategories');
    Route::get('/subCategories/{id}', 'MeetingController@getSubCategories')->name('getSubCategories');
    Route::get('/addParticipant/{id}', 'MeetingController@handleAddParticipant')->name('handleAddParticipant')->middleware('user');
    Route::get('/payment', 'MeetingController@showPaymentPage')->name('showPaymentPage')->middleware('user');
    Route::get('/deletePart/{id}', 'MeetingController@handleDeleteParticipation')->name('handleDeleteParticipation')->middleware('user');
    Route::post('/stayInformad', 'MeetingController@handleStayInformed')->name('handleStayInformed')->middleware('user');
    Route::post('/submitComment', 'MeetingController@handleSubmitComment')->name('handleSubmitComment')->middleware('user');
    Route::get('/search', 'MeetingController@handleSearch')->name('handleSearch');


});
