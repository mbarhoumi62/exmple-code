@extends('frontOffice.layout')
@section('css')



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<style>
  .str-shine{
          color: #ffa500;
      }
  .note-star{
          cursor: pointer;
      }
  #imgRdv {
          box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.8), 0 2px 10px 0 rgba(0, 0, 0, 0.8);
          -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.8), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
          -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
          background-image: url("{{asset($meeting->picture)}}");
          width: auto;
          height: 250px;
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
}
span.badge-warning {
          padding: 5px 15px;
}


      </style>
@endsection

@section('content')
<!-- Page Content -->
<div class="container mt-1">
  <!-- BreadCrumb-->
  <div class="row brdcrumb">
    <ul class="list-inline">
      <li class="list-inline-item"><a class="text-dark" href="">Acceuil</a></li>
      <li class="list-inline-item"><i class="fas fa-angle-right"></i></li>
      <li class="list-inline-item"><a class="text-dark" href="">{{$meeting->category->name}}</a></li>
      <li class="list-inline-item"><i class="fas fa-angle-right"></i></li>
      <li class="list-inline-item"><a class="text-dark" href="">{{$meeting->subCategory->name}}</a></li>
      <li class="list-inline-item"><i class="fas fa-angle-right"></i></li>
      <li class="list-inline-item text-secondary">{{$meeting->title}}</li>
    </ul>
  </div>
  <div class="row">
    <!-- Post Content Column -->
    <div class="col-lg-8">
      <!-- Title -->
      <h1 class="mt-4 rdv-annoce-title">{{$meeting->title}}</h1>
      <!-- Author -->
      <div class="media">
        <img class="mr-3 rounded-circle" src="{{asset($meeting->creator->picture)}}" width="64" height="64" alt="Generic placeholder image">
        <div class="media-body" style="margin-top: 15px;">
          <p class="mt-0">Publié par <i class="fa fa-user"></i> <a class="profile-link"  href="{{route('showUserPublicProfile',$meeting->creator->username)}}" target="_blank"><strong>{{$meeting->creator->username}}</strong></a>&nbsple {{$meeting->created_at->format('Y/m/d')}} à
            {{$meeting->created_at->format('H:i')}}</p>
        </div>
      </div>
      <br>
      <!-- Preview Image -->
      <div id="imgRdv"> </div>
      <br>
      <div class="row">

        <div class="col-md-4"> <p class="pull-right">Partager sur:</p></div>
        <div class="col-md-8">

          <a style="margin-right: 20px;background-color: #3b5998!important;border: none!important;color: white" class="btn btn-warning" href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}">Facebook</a>
          <a style="margin-right: 20px;background-color: #0077B5!important;border: none!important;color: white" class="btn btn-warning" href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{url()->current()}}&amp;title=my share text&amp;summary=dit is de linkedin summary">LinkedIn</a>

        </div>

      </div>
        <hr>


      <!-- Post Content -->
      <b style="font-size: larger">A propos de ce RDV</b>
      <p>{{$meeting->descreption}}</p>
      <hr>

@foreach($meeting->sessions as $session)
      <div class="card my-4 bg-light">
          <h5 class="card-header" style="font-size: larger">Lieux et dates du RDV</h5>
            <div class="card-body">
              <h5>Session :

                @if($session->end_date   > date("Y-m-d H:i:s"))  <span style="color:green">Active</span> @else <span style="color:red">Dépassé </span>@endif
              </h5>
              <h5>Lieu : {{($session->place)? $session->place->city->name : 'A distance'}}</h5>
              <h5>Date : {{$session->start_date->format('Y/m/d')}}</h5>
            </div>
      </div>
@endforeach

      <b style="font-size: larger">Mots clés</b>
      <br>
      <div class="my-2">
        <span class="badge badge-warning">{{($meeting->sessions->first()->place)? $meeting->sessions->first()->place->city->slug : ''}}</span>
        <span class="badge badge-warning">{{($meeting->sessions->first()->place)? $meeting->sessions->first()->place->delegation->slug : ''}}</span>
      </div>
      <hr>
    </div>
    <!-- Sidebar Widgets Column -->
    <div class="col-md-4">
      <!-- Side Widget -->
      <div class="card my-4 bg-light">
        <h5 class="card-header">Pour participer a ce RDV</h5>
        <div class="card-body">
          <p><i class="fas fa-battery-empty" style="color: red"></i>&nbspPublic cible  : @if($meeting->level == 1) Tout le monde @elseif($meeting->level == 2) Femmes uniquement  @else Hommes uniquement @endif</p>
          <p><i class="fas fa-map-marker-alt"></i>&nbsp {{($meeting->sessions->first()->place)? $meeting->sessions->first()->place->city->name : 'A distance'}} , {{($meeting->sessions->first()->place)? $meeting->sessions->first()->place->delegation->name : $meeting->sessions->first()->meeting_type}}</p>
          <p><i class="fas fa-clock"></i>&nbsp Durée : {{$meeting->sessions->first()->end_date->diffInHours($meeting->sessions->first()->start_date) }} heures</p>
          <div class="input-group mb-3">

          <p>  <i class="fas fa-calendar"></i>
           <select name="sessionId" class="custom-select">
          <option selected>Choissisez une date</option>

          @foreach($meeting->sessions as $key => $session)


          <option value="{{$session->id}}"> {{utf8_encode($dates[$key])}} de {{$session->start_date->format('H:i')}} à {{$session->end_date->format('H:i')}}</option>
          @endforeach
        </select>  </p>
          </div>
          <p><i class="fas fa-users"></i>&nbsp<b></b>&nbsp<span>Il reste</span> <span id="number_participants">{{$places}} </span> places</p>

          <p><i class="fas fa-dollar-sign"></i>&nbsp <span id="price">{{($meeting->sessions->first()->price == 0)? 'Gratuit' : $meeting->sessions->first()->price.' '.'TND'}} </span>  </p>

          <div  id="inactive" style="display:none">
            <p><i class="fas fa-history"></i>&nbsp<span class="h5">
            <span id="active">Le RDV est terminé</span>
          </div>


        </span></p>
          <div class="row">
            <div class="clo-lg-4"></div>
  <div id="participate" style="display:none">

    <button id="btn_participer" class="btn btn-warning  mt-2" style="width: 203%"  @if (Auth::check()) data-toggle="modal" data-target="#exampleModal" @else data-toggle="modal" data-target="#notConnected" @endif>J'achète mon PASS</button>
           <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Participer à " {{$meeting->title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
Voulez vous participer à ce RDV ?
      </div>
      <div class="modal-footer">
      <form action="{{route('handleAddParticipant',$meeting->id)}}" type="GET">
      {{csrf_field()}}
      <input type="hidden" name="sessionId" class="session_val" value="">
      <input type="submit" class="btn btn-primary" value="confirmer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
      </form>
      </div>
    </div>
  </div>
</div>

  </div>


<span id="verif" style="display:none"> &nbsp;&nbsp;&nbsp;<i class="fas fa-user-check"></i>&nbsp  Vous etes déja participant !</span><br>
<button id="btn_informer"  style="display:none"class="btn btn-warning mt-2 btn-block"  data-toggle="modal" data-target="#resteInfoModal">Restez informé</button>


            <div class="clo-lg-4"></div>
          </div>

        </div>
      </div>

    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="resteInfoModal" tabindex="-1" role="dialog" aria-labelledby="resteInfoModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title " id="resteInfoModalLabel">Restez informé</h5>
        </div>
        <div class="modal-body">
          <form action="{{route('handleStayInformed')}}" method="post">
            {{csrf_field()}}

            <p class="text-center">Inscrivez-vous dès maintenant</p>
          <div class="input-group mb-3">
            <input required type="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="btn btn-warning" type="submit" >Restez informé</button>
            </div>

          </div>
          </form>

        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

  <!-- Comments Form -->

  @if($hasParticipated)
  <div class="card my-4">
    <h5 class="card-header">Notez ce RDV
      <div class="float-right font-weight-bold">
        <span class="fa fa-star note-star" data-index="1"></span>
        <span class="fa fa-star note-star" data-index="2"></span>
        <span class="fa fa-star note-star" data-index="3"></span>
        <span class="fa fa-star note-star" data-index="4"></span>
        <span class="fa fa-star note-star" data-index="5"></span>
      </div>
    </h5>
    <div class="card-body">
      <form action="{{route('handleSubmitComment')}}" method="post">

        {{csrf_field()}}        <div class="form-group">
          <textarea class="form-control" name="comment" rows="3" placeholder="Laissez un commentaire"></textarea>
        </div>
        <input type="hidden" name="note" id="note">
        <input type="hidden" name="meetingId" value="{{$meeting->id}}">
        <button type="submit" class="btn btn-warning">Envoyer</button>
      </form>
    </div>
  </div>
  <!-- Comment with nested comments -->

  @foreach($meeting->reviews as $review)
  <div class="media mb-4">
    <img class="d-flex mr-3 rounded-circle" src="{{asset($review->commenter->picture)}}" width="50" height="50" alt="">
    <div class="media-body">
      <h5 class="mt-0">{{$review->commenter->username}}</h5>
      @if($review->note == 0)
        <span class="fa fa-star "></span>
        <span class="fa fa-star "></span>
        <span class="fa fa-star "></span>
        <span class="fa fa-star  "></span>
        <span class="fa fa-star  "></span>
        @elseif($review->note == 1)
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star "></span>
        <span class="fa fa-star "></span>
        <span class="fa fa-star  "></span>
        <span class="fa fa-star  "></span>
      @elseif($review->note == 2)
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star "></span>
        <span class="fa fa-star  "></span>
        <span class="fa fa-star  "></span>
      @elseif($review->note == 3)
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star  "></span>
        <span class="fa fa-star  "></span>
      @elseif($review->note == 4)
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star  checked"></span>
        <span class="fa fa-star  "></span>
      @else
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star  checked"></span>
        <span class="fa fa-star  checked"></span>
      @endif
      <br>
      {{$review->comment}}
      <p class="blockquote-footer">{{$review->created_at->format('m/Y')}}</p>
    </div>
  </div>
    @endforeach
  @else
    @endif



  <br>
  <br>



  <div class="cont-autre-rdv">
    <p class="h4">Les RDV qui peuvent vous interesser</p>
    <div class="row">

@foreach($meetings as $meeting)

      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <a href="{{route('showDetailMeeting',$meeting->id)}}"><img class="card-img-top" src="{{asset($meeting->picture)}}" alt=""></a>
          <div class="card-body">
            <h4 class="card-title">
              <a class="rdv-annoce-title" href="{{route('showDetailMeeting',$meeting->id)}}">{{$meeting->title}}</a>
            </h4>
            <h5>${{$meeting->sessions->first()->price}}</h5>
            <span><i class="fas fa-map-marker-alt"></i>&nbsp {{($meeting->sessions->first()->place)?$meeting->sessions->first()->place->city->name :'A distance'}}</span><br>
            <span><i class="far fa-clock"></i>&nbsp {{$meeting->sessions->first()->start_date->format('Y/m/d')}}</span>
          </div>
          <div class="card-footer">
            <small class="text-muted">
              <span class="fa fa-star" data-index="1"></span>
              <span class="fa fa-star" data-index="2"></span>
              <span class="fa fa-star" data-index="3"></span>
              <span class="fa fa-star" data-index="4"></span>
              <span class="fa fa-star" data-index="5"></span>
            </small>
          </div>
        </div>
      </div>

      @endforeach



    </div>
  </div>
</div>

  <!-- /.container -->
  @endsection
  @section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function(){
      var date_input=$('input[name="date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })
</script>


  <script type="text/javascript">
    $(document).ready(function() {
      $("ul[id*='item-rdv-']").hide();
      $(document).on("click", ".rdv-item-list", function() {
        $(".rdv-item-list").removeClass("active-rdv");
        $(this).addClass("active-rdv");
        $("ul[id*='item-rdv-']").hide();
        var rdvId = $(this).attr("id");
        $("#" + "item-" + rdvId).toggle();
      });

      //noter un rdv
      $(document).on('click', '.note-star', function() {
        $(".note-star").removeClass('checked');
        var selected_star = $(this).index();
        $(".note-star").each(function(index, el) {
          if (index <= selected_star) {
            $(el).addClass('checked');
          }
        });
      });
      //fin note RDV

      $(".note-star").hover(function() {
        var selected_star = $(this).index();
        $(".note-star").each(function(index, el) {
          if (index <= selected_star) {
            $(el).addClass('str-shine');
          }
        });
      }, function() {
        $(".note-star").removeClass('str-shine');

      });
      $( ".custom-select" ).change(function() {
        $('.session_val').val($('.custom-select').val());

        var sessionId=$('.custom-select').val();
        $.ajax({

      url: '{{ route('handleVerifySession') }}',
      type: 'GET',
      dataType : "html",
      data: {sessionId },
      success: function(data){
        data = JSON.parse(data);
        console.log(data);
if(data.status==0){

  $('#btn_informer').show();
  if(data.session.price == 0){
  $('#price').text('Gratuit');
  }else{
      $('#price').text(data.session.price+" TND");
  }
  $('#number_participants').text(data.session.participiants_nbr);
  $('#inactive').show();
  $('#participate').hide();
  $('#verif').hide();
}

if(data.status==1){

  if (data.participate==0) {
    $('#participate').show();
    $('#verif').hide();
  }else {
    $('#participate').hide();
    $('#verif').show();
  }

  $('#number_participants').text(data.session.participiants_nbr);
  $('#price').text(data.session.price);



}


      }

    });


});
    });
  </script>

<script>

  $('.note-star').click(function () {
      $('#note').val($(this).data('index'));
  })


</script>

  @endsection
