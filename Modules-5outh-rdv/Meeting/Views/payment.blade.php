@extends('frontOffice.layout')
@section('css')
    <style>
        .activeActionrdv:hover {
            color: #ffd900;
            cursor: pointer;
        }

        .stepNav {
            margin: 30px 20px;
            height: 43px;
            padding-right: 10px;
            position: relative;
            z-index: 0;
        }

        /* z-index to make sure the buttons stack from left to right */

        .stepNav li {
            float: left;
            position: relative;
            z-index: 3;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.12);
            -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.12);
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.12);
        }

        .stepNav li:first-child {
            -webkit-border-radius: 5px 0 0 5px;
            -moz-border-radius: 5px 0 0 5px;
            border-radius: 5px 0 0 5px;
        }

        .stepNav li:nth-child(2) {
            z-index: 2;
        }

        .stepNav li:nth-child(3) {
            z-index: 1;
        }

        .stepNav li:nth-child(4) {
            z-index: 0;
        }

        /* different widths */
        .stepNav.threeWide li {
            width: 30%;
        }

        /* step links */

        .stepNav a, .stepNav a:visited {
            width: 100%;
            height: 43px;
            padding: 0 0 0 25px;
            color: #717171;
            text-align: center;
            text-shadow: 0 1px 0 #fff;
            line-height: 43px;
            white-space: nowrap;
            border: 1px solid #cbcbcb;
            text-decoration: none;
            border-top-color: #dddddd;
            border-right: 0;
            background-color: #fbfbfb;
            background-image: -webkit-gradient(linear, left top, left bottom, from(rgb(251, 251, 251)), to(rgb(233, 233, 233)));
            background-image: -webkit-linear-gradient(top, rgb(251, 251, 251), rgb(233, 233, 233));
            background-image: -moz-linear-gradient(top, rgb(251, 251, 251), rgb(233, 233, 233));
            background-image: -o-linear-gradient(top, rgb(251, 251, 251), rgb(233, 233, 233));
            background-image: -ms-linear-gradient(top, rgb(251, 251, 251), rgb(233, 233, 233));
            background-image: linear-gradient(top, rgb(251, 251, 251), rgb(233, 233, 233));
            float: left;
            position: relative;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .stepNav li:first-child a {
            padding-left: 12px;
            -webkit-border-radius: 5px 0 0 5px;
            -moz-border-radius: 5px 0 0 5px;
            border-radius: 5px 0 0 5px;
        }

        .stepNav a:before {
            content: "";
            width: 29px;
            height: 29px;
            border-right: 1px solid #dddddd;
            border-bottom: 1px solid #cbcbcb;
            background-image: -webkit-gradient(linear, right top, left bottom, from(rgb(251, 251, 251)), to(rgb(233, 233, 233)));
            background-image: -webkit-linear-gradient(right top, rgb(251, 251, 251), rgb(233, 233, 233));
            background-image: -moz-linear-gradient(right top, rgb(251, 251, 251), rgb(233, 233, 233));
            background-image: -o-linear-gradient(right top, rgb(251, 251, 251), rgb(233, 233, 233));
            background-image: -ms-linear-gradient(right top, rgb(251, 251, 251), rgb(233, 233, 233));
            background-image: linear-gradient(right top, rgb(251, 251, 251), rgb(233, 233, 233));
            display: block;
            position: absolute;
            top: 6px;
            right: -16px;
            z-index: -1;
            -webkit-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            -o-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }

        .stepNav a:hover {
            color: #2e2e2e;
            background-color: #f5f5f5;
            background-image: -webkit-gradient(linear, left top, left bottom, from(rgb(242, 242, 242)), to(rgb(217, 217, 217)));
            background-image: -webkit-linear-gradient(top, rgb(242, 242, 242), rgb(217, 217, 217));
            background-image: -moz-linear-gradient(top, rgb(242, 242, 242), rgb(217, 217, 217));
            background-image: -o-linear-gradient(top, rgb(242, 242, 242), rgb(217, 217, 217));
            background-image: -ms-linear-gradient(top, rgb(242, 242, 242), rgb(217, 217, 217));
            background-image: linear-gradient(top, rgb(242, 242, 242), rgb(217, 217, 217));
        }

        .stepNav a:hover:before {
            background-image: -webkit-gradient(linear, right top, left bottom, from(rgb(242, 242, 242)), to(rgb(217, 217, 217)));
            background-image: -webkit-linear-gradient(right top, rgb(242, 242, 242), rgb(217, 217, 217));
            background-image: -moz-linear-gradient(right top, rgb(242, 242, 242), rgb(217, 217, 217));
            background-image: -o-linear-gradient(right top, rgb(242, 242, 242), rgb(217, 217, 217));
            background-image: -ms-linear-gradient(right top, rgb(242, 242, 242), rgb(217, 217, 217));
            background-image: linear-gradient(right top, rgb(242, 242, 242), rgb(217, 217, 217));
        }

        /* selected */

        .stepNav li.selected {
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            font-weight: 700;
        }

        .stepNav li.selected a, .stepNav li.selected a:before {
            background: #ebebeb;
        }

        .stepNav li.selected a {
            border-top-color: #bebebe;
            -webkit-box-shadow: inset 2px 1px 2px rgba(0, 0, 0, 0.12);
            -moz-box-shadow: inset 2px 1px 2px rgba(0, 0, 0, 0.12);
            box-shadow: inset 2px 1px 2px rgba(0, 0, 0, 0.12);
        }

        .stepNav li.selected a:before {
            border-right: 1px solid #bebebe;
            border-bottom: 1px solid #cbcbcb;
            -webkit-box-shadow: inset -1px -1px 1px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: inset -1px -1px 1px rgba(0, 0, 0, 0.1);
            box-shadow: inset -1px -1px 1px rgba(0, 0, 0, 0.1);
        }
    </style>

@endsection

@section('content')

    <div class="container mt-1">
        <!-- BreadCrumb-->
        <ul class="stepNav threeWide">
            <li class="selected"><a href="" title="">Récapitulatif</a></li>
            <li><a href="" title="">Paiement</a></li>
            <li><a href="" title="">Confirmation</a></li>
        </ul>
        <div class="table-responsive">
            <table class="table">
                <thead class="bg-light">
                <tr>
                    <th scope="col">RDV</th>
                    <th scope="col">Désignation RDV</th>
                    <th scope="col">Disponibilité</th>
                    <th scope="col">Prix Pass</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
               @foreach($sessions as $session)

                <tr>
                    <td>
                        <img src="{{asset($session->meeting->picture)}}" width="75" height="75"
                             class="rounded" alt="...">
                    </td>
                    <td><a href="{{route('showDetailMeeting',$session->meeting->id)}}"><span>{{$session->meeting->id}}</span> - <span>{{$session->meeting->title}}</span></a></td>
                    <td>
                        <span class="badge badge-success">Disponible</span>
                    </td>
                    <td>
                        <span>{{$session->price}} <sup> DT</sup></span>
                    </td>
                    <td>
                        <a href="{{route('handleDeleteParticipation',$session->id)}}" class="far fa-trash-alt mx-1 activeActionrdv" data-toggle="tooltip" data-placement="right"
                           title="Supprimer"></a>
                    </td>
                </tr>

               @endforeach
                </tbody>
            </table>
        </div>
        <div class="container">
            <div class="row my-3">
                <a href="{{route('showHome')}}" class="col-lg-2 btn" style="background-color: #e2e6ea"><i class="fas fa-angle-double-left" ></i>&nbspContinuer mes achats</a>
                <a  disabled=" " class="col-lg-2 offset-lg-8 btn pull-right" style="background-color: #e2e6ea ;   margin-left: 740px;
">Valider commande&nbsp<i class="fas fa-angle-double-right"></i></a>
            </div>
        </div>
    </div>

@endsection