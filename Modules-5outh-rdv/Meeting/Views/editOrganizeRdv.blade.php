@extends('frontOffice.layout')
@section('css')

    <style>
        .cross-keywrd:hover, .local-rdv:hover {
            cursor: pointer;
        }

        .local-rdv.invalid {
            color: red;
        }

        .local-rdv.valid {
            color: green;
        }

        .keywrdspan {
            font-size: smaller !important;
            font-weight: 400 !important;
        }

        .invalidputrdv {
            border: solid 1px red;
        }

        @-webkit-keyframes blinker {
            from {
                opacity: 1.0;
            }
            to {
                opacity: 0.0;
            }
        }

        .blink {
            text-decoration: blink;
            -webkit-animation-name: blinker;
            -webkit-animation-duration: 0.6s;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: ease-in-out;
            -webkit-animation-direction: alternate;
        }

        #delete-date-rdv {
            display: none;
        }

        #delete-date-rdv:hover, #add-date-rdv:hover {
            cursor: pointer;
            border-bottom: 2px solid #e3c136;
        }

        .oblig-star {
            color: red;
        }
        .arrow-up {
            width: 0;
            height: 0;
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-bottom: 5px solid #ffc107;
        }
        #cart-inf-title .arrow-up {
            position: relative;
            margin-left: 116px;
        }
        #cart-inf-keywrd .arrow-up {
            position: relative;
            margin-left: 75px;
        }
        #cart-inf-desc .arrow-up {
            position: relative;
            margin-left: 187px;
        }
        .arr-cartinf{
            display: none;
            margin-bottom: 4px;
        }
        .cartid-info {
            -webkit-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
            -moz-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
            box-shadow: 0px 0px 7px 1px rgba(255, 193, 7, 1);
        }
    </style>

@endsection
@section('content')
    <div class="container" style="margin-top:50px;">
        <h4>Publiez votre annonce RDV en 3 étapes:</h4>
        <div class="progress my-4" role="tablist">
            <div class="progress-bar bg-warning prg-bar-1 font-weight-bold" role="progressbar" style="width: 33.33%;" aria-valuenow="25"
                 aria-valuemin="0" aria-valuemax="100">étape1
            </div>
            <div class="progress-bar bg-secondary prg-bar-2 font-weight-bold" role="progressbar" style="width: 33.33%;" aria-valuenow="25"
                 aria-valuemin="0" aria-valuemax="100">étape2
            </div>
            <div class="progress-bar bg-secondary prg-bar-3 font-weight-bold" role="progressbar" style="width: 33.33%;" aria-valuenow="25"
                 aria-valuemin="0" aria-valuemax="100">étape3
            </div>
        </div>
        <form action="{{route('handleEditAnnounecement' , $meeting->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="rdvFormInfo1 jumbotron bg-light mt-4">

                        <div class="form-group">
                            <label for="inputPseudo">Intitulé du RDV <i data-info="cart-inf-title" class="fas fa-info-circle cart-ifo"></i><sup class="oblig-star">*</sup></label>
                            <div id="cart-inf-title" class="arr-cartinf">
                                <div class="arrow-up"></div>
                                <div class="cartid-info">
                                    <small class="form-text">Le titre du Rdv doit etre clair, concis et accrocheur pour capter l'attention</small>
                                </div>
                            </div>
                            <input type="text" class="form-control in-step1" id="inputPseudo" placeholder="{{$meeting->title}}" value="{{$meeting->title}}" name = "title">
                        </div>
                        <div class="form-group">
                            <label for="sectionFormRdvSelect">Sélectionner une catégorie<sup
                                        class="oblig-star">*</sup></label>
                            <select class="form-control in-step1" id="sectionFormRdvSelect" name="categoryId">

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="subsectionFormRdvSelect">Sélectionner une sous catégorie <sup
                                        class="oblig-star">*</sup></label>
                            <select class="form-control in-step1" id="subsectionFormRdvSelect" name="subCategoryId">

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Mots clés <i data-info="cart-inf-keywrd" class="fas fa-info-circle cart-ifo"></i><sup class="oblig-star">*</sup></label>
                            <div id="cart-inf-keywrd" class="arr-cartinf">
                                <div class="arrow-up"></div>
                                <div class="cartid-info">
                                    <small class="form-text">Les mots clés permettent de meiux référencer votre RDV pour plus de visibilité</small>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" id="keywrdRdvId" class="form-control" placeholder="android">
                                <div class="input-group-append">
                                    <button id="addKeywrdRdvId" class="btn btn-warning" type="button">Ajouter</button>
                                </div>
                            </div>
                            <div id="keywordRdvBlk" class="input-group in-step1"></div>
                        </div>
                        <div class="form-group">
                            <label for="describeFormRdvTextarea">Description de votre RDV <i data-info="cart-inf-desc" class="fas fa-info-circle cart-ifo"></i><sup class="oblig-star">*</sup></label>
                            <div id="cart-inf-desc" class="arr-cartinf">
                                <div class="arrow-up"></div>
                                <div class="cartid-info">
                                    <small class="form-text">Plus vous serez précis, plus vous donnez envie aux autres membres de participer à votre RDV</small>
                                </div>
                            </div>
                            <textarea class="form-control in-step1" id="describeFormRdvTextarea" rows="3" name="descreption">{{$meeting->descreption}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="levelFormRdvSelect">Pour quel niveau ce RDV est-il conseillé <sup
                                        class="oblig-star">*</sup></label>
                            <select class="form-control in-step1" id="levelFormRdvSelect" name="level">
                                <option @if($meeting->level == 1) selected @else @endif  value="1">Tout le monde</option>
                                <option @if($meeting->level == 2) selected @else @endif value="2">Femme uniquement</option>
                                <option @if($meeting->level == 3) selected @else @endif value="3">Homme uniquement</option>
                            </select>
                        </div>
                        <button data-show="rdvFormInfo2" name="btnnextcontrol1" data-hide="rdvFormInfo1" data-line="2"
                                type="button"
                                class="btn btn-warning float-right">Suivant
                        </button>
                    </div>

                    <div class="rdvFormInfo2 jumbotron bg-light mt-4">
                        <div class="table-responsive-lg">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Jour<sup class="oblig-star">*</sup></th>
                                    <th>Heure début<sup class="oblig-star">*</sup></th>
                                    <th>Heure fin<sup class="oblig-star">*</sup></th>
                                    <th>Prix&nbsp<sup style="font-weight: 400; font-size: small">DTN</sup><sup
                                                class="oblig-star">*</sup></th>
                                    <th>Nb.participants<sup class="oblig-star">*</sup></th>
                                    <th>Lieu<sup class="oblig-star">*</sup></th>
                                </tr>
                                </thead>
                                <tbody id="tb-body-rdv">
                                <tr id="rdvTrTbId" class="rdvTrTb" data-line="0">
                                    @foreach($meeting->sessions as $key => $session)
                                    <td>
                                        <input class="form-control in-step2" type="date" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" name="sessionDay[]" data-toggle="tooltip" value="{{$session->start_date->format('Y-m-d')}}"
                                               data-placement="top" title="Tooltip on top">
                                    </td>
                                    <td>
                                        <input class="form-control in-step2 timepicker" type="text" placeholder="00:00" name="sessionStartTime[]" value="{{$session->start_date->format('H:i:s')}}">
                                    </td>
                                    <td>
                                        <input class="form-control in-step2 timepicker" type="text" placeholder="00:00" name="sessionEndTime[]" value="{{$session->end_date->format('H:i:s')}}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control " name="sessionPrice[]" value="{{$session->price}}">
                                    </td>
                                    <td>
                                        <input type="number" class="form-control" name="sessionParticipiantsNbr[]" value="{{$session->participiants_nbr}}">

                                    </td>
                                    <td>
                                        <i class="fas fa-map-marker-alt local-rdv elmnt-modal-place-0" data-toggle="modal"
                                           data-target="#formRdvModal0"></i>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        <div class="row text-center">
                            <div class="col-6">
							<span id="delete-date-rdv">
                                <i class="fas fa-minus"></i>
                                &nbspSupprimer la dernière date
							</span>
                            </div>
                            <div class="col-6">
							<span id="add-date-rdv"><i
                                        class="fas fa-plus"></i>&nbspAjouter une autre date
							</span>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-lg-6">
                            </div>
                            <div class="col-lg-6">
                                <button name="btnnextcontrol2" data-show="rdvFormInfo3" data-hide="rdvFormInfo2" data-line="3"
                                        type="button"
                                        class="btn btn-warning float-right">Suivant
                                </button>
                                <!--ici data-line est orienté pour quel le progress-->
                                <button data-show="rdvFormInfo1" data-hide="rdvFormInfo2" data-line="1" type="button"
                                        class="controlFormRdv btn btn-warning float-right mr-2">Précedent
                                </button>
                            </div>
                        </div>
                        <div class="blk-alert hide">
                            <div class="alert alert-danger" role="alert">
                                <span class="blink">Compléter les champs nécessaires!</span>
                            </div>
                        </div>
                    </div>
                    <div class="rdvFormInfo3 jumbotron bg-light mt-4">
                        <h5>Image d'annonce de RDV</h5>
                        <div class="card" style="width: 18rem;">
                            <img id="rdvControlImg" class="card-img-top"
                                 src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_165d2478cd9%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_165d2478cd9%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22106.3984375%22%20y%3D%2295.4%22%3E286x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                 width="80" height="80" alt="Card image cap">
                            <div class="card-body">
                                <div class="form-group">
                                    <input type="file" name="picture" class="form-control-file" id="rdvControlFile" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="condition1Check1" required>
                            <label class="form-check-label" for="condition1Check1">
                                Je confirme l’exactitude de toutes les informations déclarées dans cette annonce de Rdv
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="condition1Check2" required>
                            <label class="form-check-label" for="condition1Check2">
                                Je m’engage à respecter tous les élements déclarés dans cette annonce et à assumer mon entière responsabilité en cas de non réalisation (selon les CGU)
                            </label>
                        </div>

                        <input type="hidden" id="city0" name="city0">
                        <input type="hidden" name="delegation0" id="delegation0">
                        <input type="hidden" name="address0" id="address0">


                        <div class="row my-3">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <button name="btnvalidcontrol" type="submit" class="btn btn-warning float-right">Publier
                                </button>
                                <button data-show="rdvFormInfo2" data-hide="rdvFormInfo3" data-line="2" type="button"
                                        class="controlFormRdv btn btn-warning float-right mr-2">Précedent
                                </button>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="col-1"></div>
            </div>
        </form>
    </div>




    <!--modal lieu-->
    @include('Meeting::inc.placeModal')



@endsection

@section('script')

@section('script')

    <script type="text/javascript">
        $('.timepicker').datetimepicker({

            format: 'HH:mm'

        });
        $(document).ready(function () {
            $(".blk-alert").hide();
            /*$(document).on("change", "input[name='inputDay']", function () {
            if
            ($("input[name='inputDay']").val()) {
            console.log("ok");
            } else {
            console.log("no right");
            }
            })*/
            $(".rdvFormInfo2").hide();
            $(".rdvFormInfo3").hide();
            $("ul[id*='item-rdv-']").hide();
            $(document).on("click", ".rdv-item-list", function () {
                $(".rdv-item-list").removeClass("active-rdv");
                $(this).addClass("active-rdv");
                $("ul[id*='item-rdv-']").hide();
                var rdvId = $(this).attr("id");
                $("#" + "item-" + rdvId).toggle();
            });
            // <!-- partie ajout des mots clé -->
            $(document).on("click", "#addKeywrdRdvId", function () {
                if (!($("#keywrdRdvId").val() === '')) {
                    $("#keywordRdvBlk").append('<span class="badge badge-warning keywrdspan mx-1">' + $("#keywrdRdvId").val() + '&nbsp<i class="far fa-times-circle cross-keywrd"></i></span>');
                    $("#keywrdRdvId").val('');
                }
            });
            // <!--partie de suppression de mot clé-->
            $(document).on("click", ".cross-keywrd", function () {
                $(this).parent(".keywrdspan").remove();
            });

            function showProgressBar(btnNext) {
                var mydivToHide = btnNext.data("hide");
                var mydivToShow = btnNext.data("show");
                $("." + mydivToHide).hide();
                $("." + mydivToShow).show();
                $(document).scrollTop($("." + mydivToShow).offset().top);
                $(".progress-bar").removeClass("bg-warning");
                $(".progress-bar").addClass("bg-secondary");
                $(".prg-bar-" + btnNext.data("line")).addClass("bg-warning");
            }
            // <!-- /.partie bouton suivant/précedent de formulaire de rdv-->
            // <!-- partie bouton d'ajout d'un autre date-->
            var i = 1;
            $(document).on("click", "#add-date-rdv", function () {

                i++;
                $("#delete-date-rdv").show();
                var nbreTrTb = $(".rdvTrTb").length;
                $("#tb-body-rdv").append('<tr id="rdvTrTbId' + nbreTrTb + '" class="rdvTrTb" data-line="' + nbreTrTb + '">\n' +
                    '                            <td>\n' +
                    '                                <input class="form-control in-step2" type="date"  min="{{Carbon\Carbon::now()->format('Y-m-d')}}" name="sessionDay[]">\n' +
                    '                            </td>\n' +
                    '                            <td>\n' +
                    '                                <input class="form-control in-step2 timepicker" type="text" placeholder="00:00" name="sessionStartTime[]">\n' +
                    '                            </td>\n' +
                    '                            <td>\n' +
                    '                                <input type="text" class="form-control in-step2 timepicker" placeholder="00:00" name="sessionEndTime[]">\n' +
                    '        <input type="hidden" id="city' + nbreTrTb + '" name="city' + nbreTrTb + '">\n' +
                    '<input type="hidden" name="delegation' + nbreTrTb + '" id="delegation' + nbreTrTb + '">\n' +
                    '<input type="hidden" name="address' + nbreTrTb + '" id="address' + nbreTrTb + '">\n' +
                    '<input type="hidden" name="platforme' + nbreTrTb + '" id="platforme' + nbreTrTb + '">\n' +
                    '                            </td>\n' +
                    '                            <td>\n' +
                    '                                <input type="text" class="form-control" placeholder="vide si gratuit" name="sessionPrice[]">\n' +
                    '                            </td>\n' +
                    '                            <td>\n' +
                    '                                   <input type="number" class="form-control in-step2" name="sessionParticipiantsNbr[]">\n' +
                    '                            </td>\n' +
                    '                            <td>\n' +
                    '                                <i class="fas fa-map-marker-alt local-rdv elmnt-modal-place-' + nbreTrTb + '" data-toggle="modal"\n' +
                    '                                   data-target="#formRdvModal' + nbreTrTb + '" data-row="' + nbreTrTb + '"></i>\n' +
                    '                            </td>\n' +
                    '                        </tr>');
                // <!-- /.partie bouton d'ajout d'un autre date-->
                // <!-- partie l'hors de clique sur le bouton de lieu pour l'ajout de nouvelle modal-->
                $("#divOfModal").append('<div class="modal fade modal-place" id="formRdvModal' + nbreTrTb + '" data-line="' + nbreTrTb + '" tabindex="-1" role="dialog" aria-labelledby="localModalLabel' + nbreTrTb + '"\n' +
                    '         aria-hidden="true">\n' +
                    '        <div class="modal-dialog" role="document">\n' +
                    '            <div class="modal-content">\n' +
                    '                <div class="modal-header">\n' +
                    '                    <h5 class="modal-title" id="formRdvModalLabel' + nbreTrTb + '">Lieu</h5>\n' +
                    '<select class="form-control" id="typeSelect' + nbreTrTb + '" style="margin-left: 25px;">\n' +
                    '<option value="0">Emplacement physique</option>\n' +
                    '<option value="1">RDV a distance</option>\n' +
                    '</select>\n' +
                    '                </div>\n' +
                    '                <div class="modal-body">\n' +
                    '                                    <div id="modalBody1' + nbreTrTb + '">    \n' +
                    '                        <div class="form-group">\n' +
                    '                            <label for="villeSelect' + nbreTrTb + '">Ville</label>\n' +
                    '                            <select class="form-control in-step2 elmnt-modal-place sel" data-line="' + nbreTrTb + '" name="addedCity' + nbreTrTb + '" id="addedCity' + nbreTrTb + '">\n' +
                    '                          <option value="0">Select City</option>\n' +
                    '                                <option value="1">Tunis</option>\n' +
                    '                                <option value="2">Ariana</option>\n' +
                    '                                <option value="3">Ben Arous</option>\n' +
                    '                                <option value="4">Mannouba</option>\n' +
                    '                                <option value="5">Nabeul</option>\n' +
                    '                                <option value="6">Zaghoun</option>\n' +
                    '                                <option value="7">Bizert</option>\n' +
                    '                                <option value="8">Beja</option>\n' +
                    '                                <option value="9">Jendouba</option>\n' +
                    '                                <option value="10">Elkef</option>\n' +
                    '                                <option value="11">Siliana</option>\n' +
                    '                                <option value="12">Sousse</option>\n' +
                    '                                <option value="13">Monastir</option>\n' +
                    '                                <option value="14">Mahdia</option>\n' +
                    '                                <option value="15">Sfax</option>\n' +
                    '                                <option value="16">Kairouan</option>\n' +
                    '                                <option value="17">Kasserine</option>\n' +
                    '                                <option value="18">Sidi Bouzid</option>\n' +
                    '                                <option value="19">Gabes</option>\n' +
                    '                                <option value="20">Mednine</option>\n' +
                    '                                <option value="21">Tataouine</option>\n' +
                    '                                <option value="22">Gafsa</option>\n' +
                    '                                <option value="23">Tozeur</option>\n' +
                    '                                <option value="24">Kebili</option> ' +
                    ' </select>\n' +
                    '                        </div>\n' +
                    '                        <div class="form-group">\n' +
                    '                            <label for="delegSelect' + nbreTrTb + '">Délégation</label>\n' +
                    '                            <select class="form-control in-step2 elmnt-modal-place sel2" data-line="' + nbreTrTb + '" name="addedDelegation' + nbreTrTb + '" id="addedDelegation' + nbreTrTb + '">\n' +
                    '                            </select>\n' +
                    '                        </div>\n' +
                    '                        <div class="form-group">\n' +
                    '                            <label for="addressTxtArea' + nbreTrTb + '">Adresse</label>\n' +
                    '                            <textarea class="form-control in-step2 elmnt-modal-place" data-line="' + nbreTrTb + '" name="addedAddress ' + nbreTrTb + '" id="addedAddress' + nbreTrTb + '" rows="3"></textarea>\n' +
                    '                        </div>\n' +
                    '                        </div>\n' +
                    ' <div id="modalBody2' + nbreTrTb + '" class="hideModal">\n' +
                    '<div class="form-group">\n' +
                    '<label for="platformeSelect">Platforme</label>\n' +
                    '<select class="form-control platformeSelect" id="platforme' + nbreTrTb + '" name="platforme' + nbreTrTb + '">\n' +
                    ' <option >Select platforme</option>\n' +
                    '  <option value="Skype">Skype</option>\n' +
                    '  <option value="Zoom">Zoom</option>\n' +
                    ' <option value="Autre">Autre</option>\n' +
                    '</select>\n' +
                    ' </div>\n' +
                    ' </div>\n' +
                    '                </div>\n' +
                    '                <div class="modal-footer">\n' +
                    '                    <button type="button" class="btn btn-warning btnValidplace" data-dismiss="modal">Ok</button>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '    </div>');
                // <!-- /.partie l'hors de clique sur le bouton de lieu pour l'ajout de nouvelle modal-->
            });


            // <!-- clique bouton suivant1-->
            $(document).on("click", "button[name='btnnextcontrol1']", function () {

                $("#keywrdRdvId").removeClass("invalidputrdv");
                $(".in-step1").removeClass("invalidputrdv");
                var verifyForm1 = true;
                $(".in-step1").each(function (i, elm) {
                    if ($(elm).attr("id") === "keywordRdvBlk") {
                        if ($(elm).children(".keywrdspan").length === 0) {

                            $("#keywrdRdvId").addClass("invalidputrdv");
                            verifyForm1 = false;
                        }
                    } else {
                        if (!$(elm).val()) {
                            $(elm).addClass("invalidputrdv");
                            verifyForm1 = false;
                        }

                    }
                });
                if (verifyForm1 === true)
                {
                    showProgressBar($(this));
                }
            });

            <!-- btn precedent formulaire-->
            $(document).on("click", ".controlFormRdv", function () {
                showProgressBar($(this));
            });

            <!-- clique bouton suivant2-->
            $(document).on("click", "button[name='btnnextcontrol2']", function () {
                // var verifyForm2 = true;
                // $("i.local-rdv").removeClass("invalid");
                // $(".in-step2").each(function (i, elm) {
                //     $(elm).removeClass("invalidputrdv");
                //     $(".blk-alert").hide();
                //     if (!$(elm).val()) {
                //         if ($(elm).hasClass("elmnt-modal-place")) {
                //             $("tr.rdvTrTb").each(function () {
                //                 if ($(this).data("line") === $(elm).parents().eq(5).data("line")) {
                //                     $(this).children("td").children("i").addClass("invalid");
                //                 }
                //             })
                //         }
                //         $(elm).addClass("invalidputrdv");
                //         verifyForm2 = false;
                //         $(".blk-alert").show();
                //         return false;
                //     } else {
                //         verifyForm2 = true;
                //     }
                // });

                showProgressBar($(this));

                // });
            });


            // <!-- partie clique ok modal lieu-->
            $(document).on("click", ".btnValidplace", function () {
                var i = 1 ;
                var j = 0;


                for(i;i<10;i++){

                    $("#city"+i).val( $("#addedCity"+i).val());
                    $("#delegation"+i).val( $("#addedDelegation"+i).val());
                    $("#address"+i).val( $("#addedAddress"+i).val());
                    $("#platforme"+i).val( $("#platforme"+i).val());
                }
                $("i.local-rdv").removeClass("invalid");
                $(".blk-alert").hide();
                if(!$('#modalBody1'+j).hasClass('hideModal')){


                    $(".elmnt-modal-place").each(function (i, elm) {
                        if (!$(elm).val()) {
                            $("i.elmnt-modal-place-" + $(elm).data("line")).addClass("invalid");
                            $(".blk-alert").show();
                            return false;
                        }
                    });

                }

                j++;
            });



            // <!-- Bouton supprimer derniere RDV-->
            $(document).on("click", "#delete-date-rdv", function () {
                var nbreTrTb = $(".rdvTrTb").length;
                var lastTrTb = $(".rdvTrTb:last");
                var dataLine = lastTrTb.data("line");
                $(".modal").each(function (i, elm) {
                    if ($(elm).data("line") === dataLine)
                    {
                        $(elm).remove();
                    }
                });
                lastTrTb.remove();
                if (nbreTrTb === 2) {

                    $("#delete-date-rdv").hide();
                }
            });

            // <!--partie de formulaire3 coté image-->
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#rdvControlImg').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#rdvControlFile").change(function () {
                readURL(this);
            });
            $(".cart-ifo").hover(function () {
                $("#"+$(this).data("info")).toggle("slow");
            });
        });
        $(document).on("click", ".rdv-item", function () {
            $(this).children("a").css('border-bottom', 'solid 2px #ffd900');
        })
    </script>


    <script type="text/javascript">
        $(document).on('change','select.sel',function() {


            var selectedCity = $(this).children("option:selected").text();
            $('select.sel2').children().remove();

            $.get("{{ route('showHome')}}/delegations/"+selectedCity).done(function (res) {

                $.each(res.delegations, function(j, d) {

                    $('select.sel2').append('<option value="' + d.id + '">' + d.name + '</option>');
                });

            });


        });

        $.get("{{ route('showHome')}}/categories").done(function (res) {

            $.each(res.categories, function(j, d) {
                var selected = "";
                var catName ='{{$meeting->category->name}}';

                if(d.name == catName){
                    selected = "selected";
                }

                $('#sectionFormRdvSelect').append('<option '+selected+' value="' + d.id + '">' + d.name + '</option>');
            });


            var catId ='{{$meeting->category->id}}';
            $.get("{{ route('showHome')}}/subCategories/"+catId).done(function (res) {

                $.each(res.subCategories, function(j, d) {
                    var selected = "";
                    var subName = '{{$meeting->subCategory->name}}';
                    if(d.name == subName){
                        selected = "selected";
                    }

                    $('#subsectionFormRdvSelect').append('<option '+selected+' value="' + d.id + '">' + d.name + '</option>');
                });

            });



        });






        $('#sectionFormRdvSelect').change(function(){

            var selectedCategory = $(this).children("option:selected").val();
            $('#subsectionFormRdvSelect').children().remove();

            $.get("{{ route('showHome')}}/subCategories/"+selectedCategory).done(function (res) {

                $.each(res.subCategories, function(j, d) {

                    $('#subsectionFormRdvSelect').append('<option value="' + d.id + '">' + d.name + '</option>');
                });

            });

        });

        $('#formRdvModal0').on('hide.bs.modal', function () {
            $("#city0").val( $("#city").val());
            $("#delegation0").val( $("#delegation").val());
            $("#address0").val( $("#address").val());
            $('#platforme0').val($('#platforme').val());

        });



        $('#typeSelect0').change(function(){

            if($('#modalBody10').hasClass('hideModal')){
                $('#modalBody10').removeClass('hideModal');
            }else {
                $('#modalBody10').addClass('hideModal');
                $('#city').removeClass('in-step2');
                $('#delegation').removeClass('in-step2');
                $('#address').removeClass('in-step2');

            }

            if($('#modalBody20').hasClass('hideModal')){
                $('#modalBody20').removeClass('hideModal');
            }else {
                $('#modalBody20').addClass('hideModal');
            }

        });


        $(document).on('click','.local-rdv',function () {
            var i = $(this).data('row');
            $(document).on('change','#typeSelect'+i,function () {



                if($('#modalBody1'+i).hasClass('hideModal')){

                    $('#modalBody1'+i).removeClass('hideModal');
                }else {
                    $('#modalBody1'+i).addClass('hideModal');
                    $('#city').removeClass('in-step2');
                    $('#delegation').removeClass('in-step2');
                    $('#address').removeClass('in-step2');

                }

                if($('#modalBody2'+i).hasClass('hideModal')){
                    $('#modalBody2'+i).removeClass('hideModal');
                }else {
                    $('#modalBody2'+i).addClass('hideModal');
                }
            });
        });




    </script>


@endsection
