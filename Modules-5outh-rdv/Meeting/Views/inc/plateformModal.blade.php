<div id="plateformModal">
    <div class="modal fade modal-place" id="plateformModal0" data-line="0" tabindex="-1" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-labelledby="formRdvModalLabel0"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formRdvModalLabel0">RDV a distance</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="villeSelect">Platforme</label>
                        <select class="form-control in-step2 elmnt-modal-place sel" data-line="0" id="platforme" name="platforme[]">
                            <option value="0">Select platforme</option>
                            <option value="Skype">Skype</option>
                            <option value="Zoom">Zoom</option>
                            <option value="Autre">Autre</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning btnValidplatforme" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
</div>
