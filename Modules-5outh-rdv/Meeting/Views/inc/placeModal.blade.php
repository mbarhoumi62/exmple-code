<style>
    .hideModal{
        display: none;
    }
</style>

<div id="divOfModal">
    <div class="modal fade modal-place" id="formRdvModal0" data-line="0" tabindex="-1" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-labelledby="formRdvModalLabel0"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formRdvModalLabel0">Lieu</h5>
                        <select class="form-control" id="typeSelect0" style="margin-left: 25px;">
                            <option value="0">Emplacement physique</option>
                            <option value="1">RDV a distance</option>
                        </select>
                </div>
                <div class="modal-body">
                    <div id="modalBody10">
                        <div class="form-group">
                            <label for="villeSelect">Ville</label>
                            <select class="form-control in-step2 elmnt-modal-place sel" data-line="0" id="city" name="sessionCityId[]">
                                <option value="0">Select City</option>
                                <option value="1">Tunis</option>
                                <option value="2">Ariana</option>
                                <option value="3">Ben Arous</option>
                                <option value="4">Mannouba</option>
                                <option value="5">Nabeul</option>
                                <option value="6">Zaghoun</option>
                                <option value="7">Bizert</option>
                                <option value="8">Beja</option>
                                <option value="9">Jendouba</option>
                                <option value="10">Elkef</option>
                                <option value="11">Siliana</option>
                                <option value="12">Sousse</option>
                                <option value="13">Monastir</option>
                                <option value="14">Mahdia</option>
                                <option value="15">Sfax</option>
                                <option value="16">Kairouan</option>
                                <option value="17">Kasserine</option>
                                <option value="18">Sidi Bouzid</option>
                                <option value="19">Gabes</option>
                                <option value="20">Mednine</option>
                                <option value="21">Tataouine</option>
                                <option value="22">Gafsa</option>
                                <option value="23">Tozeur</option>
                                <option value="24">Kebili</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="delegSelect">Délégation</label>
                            <select class="form-control in-step2 elmnt-modal-place sel2" data-line="0" id="delegation" name="sessionDelegationId[]">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="addressTxtArea[]">Adresse</label>
                            <textarea class="form-control in-step2 elmnt-modal-place" data-line="0" id="address" name="sessionAddress" placeholder=""
                                      rows="3"></textarea>
                        </div>

                </div>
                    <div id="modalBody20" class="hideModal">

                        <div class="form-group">
                            <label for="platformeSelect">Platforme</label>
                            <select class="form-control platformeSelect" id="platforme" name="platforme">
                                <option >Select platforme</option>
                                <option value="Skype">Skype</option>
                                <option value="Zoom">Zoom</option>
                                <option value="Autre">Autre</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning btnValidplace" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
</div>


