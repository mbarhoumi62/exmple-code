<?php

namespace App\Modules\Meeting\Models;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model {


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'meetings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'descreption',
        'level',
        'picture',
        'status',
        'user_id',
        'category_id',
        'sub_category_id'
    ];

    public function creator()
    {
     return $this->belongsTo('App\Modules\User\Models\User','user_id');

    }

    public function category()
    {
    return $this->hasOne('App\Modules\Category\Models\Category','id','category_id');

    }
    public function subCategory()
    {
     return $this->hasOne('App\Modules\Category\Models\SubCategory','id','sub_category_id');

    }

     public function sessions()
    {
     return $this->hasMany('App\Modules\Meeting\Models\Session');

    }

    public function notify()
    {
        return $this->hasMany('App\Modules\User\Models\NotifyList');

    }

    public function cities(){

        $sessions = $this->sessions;
        $cities = [];

        foreach ($sessions as $key => $session) {
           $cities = $session->place->city->name;
        }

        return $cities;

    }

    public function reviews(){
        return $this->hasMany('App\Modules\Meeting\Models\Review');

    }




}
