<?php

namespace App\Modules\Meeting\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model {


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sessions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
'price',
'participiants_nbr',
'meeting_type',
'place_id',
'meeting_id',
        'start_date',
        'end_date'
    ];

    protected $dates = ['start_date',
'end_date'
];


    public function place()
    {
     return $this->hasOne('App\Modules\Place\Models\Place','id','place_id');

    }

    public function meeting()
    {
     return $this->hasOne('App\Modules\Meeting\Models\Meeting','id','meeting_id');

    }

    public function participation()
    {
        return $this->hasOne('App\Modules\Meeting\Models\UserParticipation','session_id','id');

    }




}
