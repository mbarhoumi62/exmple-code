<?php

namespace App\Modules\Meeting\Models;

use Illuminate\Database\Eloquent\Model;

class UserParticipation extends Model {


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_participations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
     'ticket_key',
'presence',
'session_id',
'user_id'
    ];

    public function session()
    {
     return $this->hasOne('App\Modules\Meeting\Models\Session','id','session_id');

    }

    public function user()
    {
     return $this->belongsTo('App\Modules\User\Models\User');

    }



}
