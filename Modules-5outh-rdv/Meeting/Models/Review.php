<?php

namespace App\Modules\Meeting\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment',
        'note',
        'user_id',
        'meeting_id'
    ];

    public function commenter()
    {
        return $this->hasOne('App\Modules\User\Models\User','id','user_id');

    }

    public function commented()
    {
        return $this->hasOne('App\Modules\Meeting\Models\Meeting','id','meeting_id');

    }



}
