<?php

Route::group(['module' => 'General', 'middleware' => ['web'], 'namespace' => 'App\Modules\General\Controllers'], function() {


    Route::get('reset', 'GeneralController@showResetPassword')->name('showResetPassword');
    Route::post('reset', 'GeneralController@handleResetPassword')->name('handleResetPassword');




});
