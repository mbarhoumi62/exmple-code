<?php

namespace App\Modules\General\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\User\Models\User;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;
use Illuminate\Support\Facades\Mail;

class GeneralController extends Controller
{

    public function showResetPassword(){

        return view('User::frontOffice.passwordReset');
    }


    public function handleResetPassword(Request $request){


        //TODO Sweet alert
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $newPassword = '';
        for ($i = 0; $i < 10; $i++) {
            $newPassword .= $characters[rand(0, $charactersLength - 1)];
        }

        $user = User::where('email', $request->email)->first();

        if(!$user){

            SweetAlert::error('Oops', 'Il n\'y a pas de compte enregistré avec l\'email que vous avez entré')->persistent('Fermer');
            return back();
        }

        $user->update([
            'password' => bcrypt($newPassword)
        ]);


        $content = ['user' => $user , 'newPassword' => $newPassword];
        Mail::send('User::mail.reset', $content, function ($message) use ($user) {
            $message->to($user->email);
            $message->subject('Nouveau mot de passe');
        });

        SweetAlert::success('Bien', 'Nous vous avons envoyé un mail contenant votre nouveau mot de passe')->persistent('Fermer');

        return redirect(route('showHome'));



    }



}