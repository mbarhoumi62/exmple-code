  <div id="meetings">
    <div class="row main">

      @foreach ($meetings as $meeting)
        

      <div class="col-lg-4 col-md-6 mb-4 ">
        <div class="card h-100">
          <a href="{{route('showDetailMeeting',$meeting->id)}}"><img class="card-img-top mh-100" src="{{asset($meeting->picture)}}" alt=""></a>
          <div class="card-body">
            <h4 class="card-title">
              <a class="rdv-annoce-title" href="{{route('showDetailMeeting',$meeting->id)}}">
                {{$meeting->title}} @if($meeting->sessions->first()->price == 0) <span class="badge badge-success">Gratuit !</span> @endif
              </a>
            </h4>
            <span><i class="fas fa-map-marker-alt"></i>&nbsp<span>{{($meeting->sessions->first()->place)?$meeting->sessions->first()->place->city->name : 'A distance'}}</span>&nbsp-&nbsp<span>{{($meeting->sessions->first()->place)?$meeting->sessions->first()->place->delegation->name : $meeting->sessions->first()->meeting_type}}</span></span><br>
            <span><i class="far fa-clock"></i>&nbsp<span>{{$meeting->sessions->first()->start_date->format('Y/m/d')}}</span>&nbspà&nbsp<span>{{$meeting->sessions->first()->start_date->format('H:i')}}</span></span>

          </div>
          <div class="card-footer">
            <small class="text-muted">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </small>
            <div class="float-lg-right font-weight-bold">
              <span>{{$meeting->sessions->first()->price}}</span>
              <span>TND</span>
            </div>
          </div>
        </div>
      </div>

      @endforeach

    </div>

  </div>





  <!-- /.row -->
