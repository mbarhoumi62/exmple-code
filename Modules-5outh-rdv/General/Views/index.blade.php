@extends('frontOffice.layout')
@section('css')
    <style>
        .valid-put {
            -webkit-box-shadow: 0px 0px 7px 1px rgba(0, 255, 0, 1);
            -moz-box-shadow: 0px 0px 7px 1px rgba(0, 255, 0, 1);
            box-shadow: 0px 0px 7px 1px rgba(0, 255, 0, 1);
        }

        .invalid-put {
            -webkit-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
            -moz-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
            box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
        }

        .oblig-put {
            color: red;
        }

        .arrow-up {
            margin-left: 177px;
            width: 0;
            height: 0;
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-bottom: 5px solid #ffc107;
        }

        .arr-cartinf {
            display: none;
            margin-bottom: 4px;
        }

        .cartid-info {
            -webkit-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
            -moz-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
            box-shadow: 0px 0px 7px 1px rgba(255, 193, 7, 1);
        }

        .page-item.active .page-link {
            background-color: #ffc107;
            border-color: #ffc107;
        }

        .page-link:hover {
            background-color: #eeefe9;
            color: #ffc107;
        }

        .load-more-button {
            background-color: #FFC107;
            color: #fff;
            display: inline-block;
            padding: 15px 35px;
            border-radius: 30px;
            margin-top: 50px;
            position: relative;
            box-shadow: 0px 0px 0px 7px rgba(204, 204, 204, 0.2);
        }

        .load-more-button i {
            float: right;
            margin-left: 10px;
            top: 2px;
            position: relative;
            animation-name: spin;
            animation-duration: 900ms;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes spin {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }

        .hide {
            display: none;
        }

    </style>
@endsection

@section('content')




    <section class="pt-5 pb-5 mt-0 align-items-center d-flex bg-dark" style="height:500px; background-size: cover; background-image: url(https://trello-attachments.s3.amazonaws.com/5b804f522a0b4623bfbe6cbb/5c72bee5c00ca71d36340cf8/b15b7319671df6afb85ebf55e590b1bd/rawpixel-310778-unsplash.jpg);">

        <div class="container-fluid">
            <div class="row  justify-content-center align-items-center d-flex text-center h-100">
                <div class="col-12 col-md-8  h-50 ">
                    <h1 style="color: #6F0081!important;" class="display-2  text-light mb-2 mt-5"><strong></strong> </h1>
                    <p style="color: #6F0081!important;font-size: 36px" class="lead  text-light mb-5">Trouvez DES MILLIERS de RDV en ligne ou prés de Chez-vous sur toute la Tunisie</p>

                    <div class="row">
                        <div class="col-lg-6">
                            <select class="form-control cities" id="cities">
                                <option selected>Toute la Tunisie</option>
                                @foreach($cities as $city)
                                    <option>{{$city->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <select id="delegations" class="delegations form-control">
                                <option selected>Toutes les villes</option>

                            </select>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>



    <!-- Page Content -->
    <div id="contentToScroll" class="container mt-5">

        <div class="row">
            <div class="col-lg-3">

                <h5 class="my-4">Catégories des RDV</h5>
                <ul class="list-group list-group-flush">
                    <li>
                        <div id="rdv-1" class="row p-3 rdv-item-list">
                            <div class="col-10"><i class="fas fa-laptop"></i>&nbsp<span>Les RDV Technologie</span></div>
                            <div class="col-2"><i class="fas fa-angle-down"></i></div>
                        </div>

                        <ul id="item-rdv-1">
                            <li class="rdv-item">
                                <a class="subs" data-id="1" href="">Développement informatique</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="2" href="">Marketing Digital</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="3" href="">Bureautique</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="4" href="">Infographie Multimédia</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="5" href="">EnjeuxTechnologiques</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <div id="rdv-2" class="row p-3 rdv-item-list">
                            <div class="col-10"><i class="fas fa-chart-line"></i>&nbsp<span>Les RDV Business</span>
                            </div>
                            <div class="col-2"><i class="fas fa-angle-down"></i></div>
                        </div>
                        <ul id="item-rdv-2">
                            <li class="rdv-item">
                                <a class="subs" data-id="6" href="">Droit et législation</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="7" href="">Ressources Humaines</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="8" href="">Gestion Comptabilité Finance</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="9" href="">Achats et Supply Chain</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="10" href="">Qualité, sécurité,et développement durable</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="11" href="">Entrepreneuriat et stratégies d'entreprises</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <div id="rdv-3" class="row p-3 rdv-item-list">
                            <div class="col-10"><i class="fas fa-book"></i>&nbsp<span>Les RDV Coaching</span></div>
                            <div class="col-2"><i class="fas fa-angle-down"></i></div>
                        </div>
                        <ul id="item-rdv-3">
                            <li class="rdv-item">
                                <a class="subs" data-id="12" href="">Religion et spiritualité</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="13" href="">Etudes, emploi et carrière</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="14" href="">Santé, beauté et bien être</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="15" href="">Développement personnel</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <div id="rdv-4" class="row p-3 rdv-item-list">
                            <div class="col-10"><i class="fas fa-globe-americas"></i>&nbsp<span>Les RDV Langues</span>
                            </div>
                            <div class="col-2"><i class="fas fa-angle-down"></i></div>
                        </div>
                        <ul id="item-rdv-4">
                            <li class="rdv-item">
                                <a class="subs" data-id="16" href="">Français</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="17" href="">Anglais</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="18" href="">Autres langues</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <div id="rdv-5" class="row p-3 rdv-item-list">
                            <div class="col-10"><i
                                        class="fas fa-drafting-compass"></i>&nbsp<span>Les RDV Créativité</span></div>
                            <div class="col-2"><i class="fas fa-angle-down"></i></div>
                        </div>
                        <ul id="item-rdv-5">
                            <li class="rdv-item">
                                <a class="subs" data-id="19" href="">Cuisine et arts culinaires</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="20" href="">Couture, bijoux et créations artisanales</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="21" href="">Bricolage et travaux divers</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="22" href="">Ecriture, dessin et peinture</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <div id="rdv-6" class="row p-3 rdv-item-list">
                            <div class="col-10"><i class="fas fa-headphones"></i>&nbsp<span>Les RDV Loisirs</span></div>
                            <div class="col-2"><i class="fas fa-angle-down"></i></div>
                        </div>
                        <ul id="item-rdv-6">
                            <li class="rdv-item">
                                <a class="subs" data-id="23" href="">Musique</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="24" href="">Théatre</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="25" href="">Photographie, cinéma, et arts divers</a>
                            </li>
                            <li class="rdv-item">
                                <a class="subs" data-id="26" href="">Activités sportives et sorties</a>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
            <!-- /.col-lg-3 -->

            <div class="col-lg-9">
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <label id="toHideWhen0" style="font-size: larger">Il y'a <span id="nbrRdv"></span> annonces RDV</label>

                            </div>
                            <div class="col-lg-3"></div>
                            <div class="col-lg-3">
                                <label style="font-size: larger">Trier par:</label>
                                <select class="form-control type" id="price">
                                    <option value="1">le plus récent</option>
                                    <option value="2">le plus cher</option>
                                    <option value="3">le moins cher</option>
                                    <option value="4">Meilleure note</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>


                <div id="meetings">
                    <div class="row main">

                        @foreach ($meetings as $meeting)


                            <div class="col-lg-4 col-md-6 mb-4 ">
                                <div class="card h-100">
                                    <a href="{{route('showDetailMeeting',$meeting->id)}}"><img
                                                class="card-img-top mh-100" src="{{asset($meeting->picture)}}"
                                                alt=""></a>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <a class="rdv-annoce-title"
                                               href="{{route('showDetailMeeting',$meeting->id)}}">
                                                {{$meeting->title }} @if($meeting->sessions->first()->price == 0) <span
                                                        class="badge badge-success">Gratuit !</span> @endif
                                            </a>
                                        </h4>
                                        <span><i class="fas fa-map-marker-alt"></i>&nbsp<span>{{($meeting->sessions->first()->place)? $meeting->sessions->first()->place->city->name : 'A distance'}}</span>&nbsp-&nbsp<span>{{($meeting->sessions->first()->place)? $meeting->sessions->first()->place->delegation->name : $meeting->sessions->first()->meeting_type}}</span></span><br>
                                        <span><i class="far fa-clock"></i>&nbsp<span>{{$meeting->sessions->first()->start_date->format('Y/m/d')}}</span>&nbspà&nbsp<span>{{$meeting->sessions->first()->start_date->format('H:i')}}</span></span>

                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </small>
                                        <div class="float-lg-right font-weight-bold">
                                            <span>{{($meeting->sessions->first()->price)}}</span>
                                            <span>TND</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach

                    </div>

                </div>
                <div class="row" id="load-more">


                    <div>

                        <a style="margin-left: 350px;
    margin-bottom: 20px;" class="load-more-button" id="load">Afficher plus <i class="fa fa-circle-o-notch"></i> </a>

                    </div>


                </div>
            @include('General::load.btnLoad')


            <!-- /.row -->

            </div>
            <!-- /.col-lg-9 -->
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
@endsection
@section('script')

    <script type="text/javascript">


        var offsetCity = 0;
        var offsetType = 0;
        var offsetDelegation = 0;
        var offsetSubs = 0;
        var offsetSearch = 0;
        $(document).ready(function () {

            var count = {{ $count }}

            $('#nbrRdv').text(count);
            var offset = 0;
            $('#load').click(function (e) {
                offset++;
                e.preventDefault();
                $('#load-more').hide();
                $.get("{{ route('showHomeOffset')}}?offset=" + offset).done(function (res) {
                    if (res.status == "201") {
                        $('#load-more').hide();
                    } else {
                        $('#load-more').show();
                        $('#meetings').append(res.html);
                    }

                });
            });

        });


        $(document).ready(function () {
            offsetDelegation = 0;
            offsetCity = 0;
            offsetType = 0;
            offsetSubs = 0;
            offsetSearch = 0;
            $("select.type").change(function () {
                $('#load-type').removeClass('hide');
                offsetType = 0;
                var selectedtype = $('.type').children("option:selected").val();

                $('#meetings').html(' ');
                $.get("{{ route('apiSortMeetings')}}?type=" + selectedtype).done(function (res) {
                    $('#load-more').hide();
                    $('#load-more-type').removeClass('hide');
                    $('#load-more-delegation').addClass('hide');
                    $('#load-more-city').addClass('hide');
                    $('#meetings').append(res.html);

                });
            });

            $('#load-type').click(function (e) {
                offsetCity = 0;
                offsetDelegation = 0;
                offsetType++;
                offsetSubs = 0;
                offsetSearch = 0;
                e.preventDefault();
                $('#load-more-type').addClass('hide');
                var selectedtype = $("#price").val();
                $.get("{{ route('apiSortMeetings')}}?type=" + selectedtype + "&offset=" + offsetType).done(function (res) {
                    if (res.status == "201") {
                        $('#load-type').addClass('hide');
                    }
                    $('#load-more-type').removeClass('hide');
                    $('#meetings').append(res.html);

                });
            });

        });
        $(document).ready(function () {
            $("select.cities").change(function () {
                $('#load-subs').addClass('hide');
                $('#load-city').removeClass('hide');
                $('#load-delegation').addClass('hide');
                offsetCity = 0;
                offsetDelegation = 0;
                offsetType = 0;
                offsetSubs = 0;
                offsetSearch = 0;
                var selectedCity = $(this).children("option:selected").val();
                var selectedDelegation = $('#delegations').children("option:selected").val();
                $('#delegations').children().remove();
                $.get("{{ route('showHome')}}/delegations/" + selectedCity).done(function (res) {
                    $('#delegations').append('<option value="">Toutes les villes</option>');
                    $.each(res.delegations, function (i, d) {

                        $('#delegations').append('<option value="' + d.id + '">' + d.name + '</option>');
                    });

                });
                $('#meetings').html(' ');
                $.get("{{ route('apiFilterByCity')}}?city=" + selectedCity).done(function (res) {

                  var count = res['count'];
                  $('#nbrRdv').text(count);
                    $('#load-more').hide();
                    $('#load-more-type').addClass('hide');
                    $('#load-more-city').removeClass('hide');
                    $('#meetings').append(res.html);

                });
            });

            $('#load-city').click(function (e) {
                offsetCity++;
                offsetDelegation = 0;
                offsetType = 0;
                offsetSubs = 0;
                offsetSearch = 0;
                var selectedCity = $('#cities').children("option:selected").val();
                e.preventDefault();
                $('#load-delegation').addClass('hide');
                $.get("{{ route('apiFilterByCity')}}?city=" + selectedCity + "&offset=" + offsetCity).done(function (res) {
                    if (res.status == "201") {
                        $('#load-city').addClass('hide');
                    }
                    $('#load-more-city').removeClass('hide');
                    $('#meetings').append(res.html);

                });
            });


        });

        $(document).ready(function () {
            offsetDelegation = 0;
            offsetCity = 0;
            offsetType = 0;
            offsetSubs = 0;
            offsetSearch = 0;
            $("ul[id*='item-rdv-']").hide();
            $(document).on("click", ".rdv-item-list", function () {
                $(".rdv-item-list").removeClass("active-rdv");
                $(this).addClass("active-rdv");
                $("ul[id*='item-rdv-']").hide();
                var rdvId = $(this).attr("id");
                $("#" + "item-" + rdvId).toggle();
            });

            $(document).on("click", ".rdv-item", function () {
                $(this).children("a").css('border-bottom', 'solid 2px #ffd900');
            });


            $("select.delegations").change(function () {
                $('#load-delegation').removeClass('hide');
                $('#load-subs').addClass('hide');
                offsetCity = 0;
                offsetDelegation = 0;
                offsetType = 0;
                offsetSubs = 0;
                offsetSearch = 0;
                var selectedDelegation = $(this).children("option:selected").val();
                $('#meetings').html(' ');
                $.get("{{ route('apiFilterByDelegation')}}?delegation=" + selectedDelegation).done(function (res) {

                    var count = res['count'];
                    $('#nbrRdv').text(count);
                    $('#load-more').hide();
                    $('#load-more-type').addClass('hide');
                    $('#load-more-city').addClass('hide');
                    $('#load-more-delegation').removeClass('hide')
                    $('#meetings').append(res.html);

                });


            });

            $('#load-delegation').click(function (e) {
                offsetCity = 0;
                offsetDelegation++;
                offsetType = 0;
                offsetSubs = 0;
                offsetSearch = 0;
                var selectedDelegation = $('#delegations').val();
                e.preventDefault();
                $('#load-more-delegation').addClass('hide');
                var selectedtype = $("#cities").val();
                $.get("{{ route('apiFilterByDelegation')}}?delegation=" + selectedDelegation + "&offset=" + offsetDelegation).done(function (res) {
                    if (res.status == "201") {
                        $('#load-delegation').addClass('hide');
                    }
                    $('#load-more-delegation').removeClass('hide');
                    $('#meetings').append(res.html);

                });
            });

            $('.subs').click(function (e) {

                $('#load').addClass('hide');
                $('#load-more-subs').show();

                e.preventDefault();
                var i = $(this).data('id');
                $('#load-subs').data('id', i);
                offsetCity = 0;
                offsetDelegation = 0;
                offsetType = 0;
                offsetSearch = 0;
                $('#meetings').html(' ');
                $.get("{{ route('apiFilterBySubCategory')}}?subCategory=" + i).done(function (res) {
                   var count = res['count'];
                   $('#nbrRdv').text(count);
                    $('#meetings').append(res.html);
                });
            });
            $('#load-subs').click(function (e) {
                offsetCity = 0;
                offsetDelegation = 0;
                offsetType = 0;
                offsetSubs++;
                offsetSearch = 0;
                e.preventDefault();
                $('#load-more-subs').addClass('hide');
                var i = $(this).data('id');
                $.get("{{ route('apiFilterBySubCategory')}}?subCategory=" + i + "&offset=" + offsetSubs).done(function (res) {
                    if (res.status == "201") {
                        $('#load-subs').addClass('hide');
                    }
                    $('#load-more-subs').removeClass('hide');
                    $('#meetings').append(res.html);

                });
            });


        });


        $(document).ready(function () {
            $("#searchBtn").click(function () {
                $('html, body').animate({
                    scrollTop: $("#contentToScroll").offset().top
                }, 1000);

                $('#load-subs').addClass('hide');
                $('#load-city').addClass('hide');
                $('#load-type').addClass('hide');
                $('#load-search').removeClass('hide');
                offsetCity = 0;
                offsetDelegation = 0;
                offsetType = 0;
                offsetSubs = 0;
                offsetSearch = 0;
                var key = $('#searchKey').val();

                $('#meetings').html(' ');
                $.get("{{ route('handleSearch')}}?key=" + key).done(function (res) {

                    var count = res['count'];
                    if(count == 0){
                        $('#toHideWhen0').text('Il y a 0 annonces RDV , modifiez vos critères de recherche ou contactez-nous.');
                        $('#load-search').addClass('hide');
                        $('#nbrRdv').addClass('hide');
                    }else {
                        $('#nbrRdv').text(count);
                    }

                    $('#load-more').hide();
                    $('#load-more-type').addClass('hide');
                    $('#load-more-search').removeClass('hide');
                    $('#meetings').append(res.html);

                });
            });

            $('#load-search').click(function (e) {
                offsetSearch++;
                offsetDelegation = 0;
                offsetType = 0;
                offsetSubs = 0;
                offsetCity = 0;
                var key = $('#searchKey').val();
                e.preventDefault();
                $('#load-delegation').addClass('hide');
                $.get("{{ route('handleSearch')}}?key=" + key + "&offset=" + offsetSearch).done(function (res) {
                    if (res.status == "201") {
                        $('#load-search').addClass('hide');
                    }
                    $('#load-more-search').removeClass('hide');
                    $('#meetings').append(res.html);

                });
            });


        });

    </script>

@endsection
