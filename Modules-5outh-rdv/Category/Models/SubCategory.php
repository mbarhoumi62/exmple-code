<?php

namespace App\Modules\Category\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sub_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'categoty_id'
    ];


    public function category()
    {
     return $this->belongsTo('App\Modules\Category\Models\Category');

    }

}
