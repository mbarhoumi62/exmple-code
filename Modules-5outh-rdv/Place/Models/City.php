<?php

namespace App\Modules\Place\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug'
    ];

     public function delegations()
    {
     return $this->hasMany('App\Modules\Place\Models\Delegation','city_id','id');

    }

      public function places()
    {
     return $this->hasMany('App\Modules\Place\Models\Place');

    }

}
