<?php

namespace App\Modules\Place\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'places';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address',
'city_id',
'delegation_id'
    ];

     public function city()
    {
     return $this->hasOne('App\Modules\Place\Models\City','id','city_id');

    }

     public function delegation()
    {
    return $this->hasOne('App\Modules\Place\Models\Delegation','id','delegation_id');

    }

      public function session()
    {
     return $this->belongsTo('App\Modules\Meeting\Models\Session','place_id','id');

    }

}
