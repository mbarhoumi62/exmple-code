<?php

namespace App\Modules\Place\Models;

use Illuminate\Database\Eloquent\Model;

class Delegation extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'delegations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'city_id'
    ];


    public function city()
    {
     return $this->belongsTo('App\Modules\Place\Models\City','city_id','id');

    }
     public function places()
    {
     return $this->hasMany('App\Modules\Place\Models\Place');

    }

}
