<?php

Route::group(['module' => 'Place', 'middleware' => ['web'], 'namespace' => 'App\Modules\Place\Controllers'], function() {

    Route::resource('Place', 'PlaceController');

});
