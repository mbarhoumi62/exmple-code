<?php

Route::group(['module' => 'Place', 'middleware' => ['api'], 'namespace' => 'App\Modules\Place\Controllers'], function() {

    Route::resource('Place', 'PlaceController');

});
