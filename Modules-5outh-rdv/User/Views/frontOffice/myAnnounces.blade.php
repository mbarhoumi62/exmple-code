@extends('frontOffice.layout')
@section('css')


<style media="screen">
.view-account{
  background:#f9f9fb
}
 .valid-put {
     -webkit-box-shadow: 0px 0px 7px 1px rgba(0, 255, 0, 1);
     -moz-box-shadow: 0px 0px 7px 1px rgba(0, 255, 0, 1);
     box-shadow: 0px 0px 7px 1px rgba(0, 255, 0, 1);
 }
.invalid-put {
    -webkit-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
    -moz-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
    box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
}
.oblig-put {
    color: red;
}
.arrow-up {
    margin-left: 177px;
    width: 0;
    height: 0;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-bottom: 5px solid #ffc107;
}
.arr-cartinf {
    display: none;
    margin-bottom: 4px;
}
.cartid-info {
    -webkit-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
    -moz-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
    box-shadow: 0px 0px 7px 1px rgba(255, 193, 7, 1);
}
.activeActionrdv:hover {
    color: #ffd900;
    cursor: pointer;
}
.fa-toggle-on{
    color: green;
}
.fa-toggle-off{
    color: red;
}
.blk-down{
    margin-top: -16px;
}
.blk-up{
    margin-top: -3px;
}
</style>
@endsection

@section('content')
<!-- Page Content -->
@include('User::frontOffice.inc.userInformation')
@include('User::frontOffice.inc.navUserProfile')


<div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mon profil <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Mes annonces RDV </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Commandes sur RDV</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mes participations aux RDV</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mes révenus</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container bg-light rounded my-3">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <div class="rdvList">
                        <p class="h5 font-weight-bold my-4">Mes annonces</p>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Réf
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Désignation
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Publié le
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Nombre participants
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Statut
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Action
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>


                                   @foreach($meetings as $meeting)
                                    <tr id="{{$meeting->id}}">
                                        <th  scope="row">{{$meeting->id}}</th>
                                        <td>{{$meeting->title}}</td>
                                        <td>{{$meeting->created_at->format('d/m/Y')}}</td>
                                        <td>{{$meeting->sessions->first()->participiants_nbr}}</td>
                                        <td id="status{{$meeting->id}}">@if($meeting->status == 1) Publié  @else Brouillon @endif</td>
                                        <td>
                                            <div class="d-flex justify-content-center">
                                                <a href="{{ route('showEditAnnounecement',$meeting->id) }}" class="far fa-edit mx-1 activeActionrdv" data-toggle="tooltip" data-placement="right" title="Modifier" ></a>
                                                <i class="far fa-trash-alt mx-1 activeActionrdv" data-placement="right" title="Supprimer" data-toggle="modal" data-target="#delete{{ $meeting->id }}"></i>
                                                <i class="fas @if($meeting->status == 1) fa-toggle-on @else fa-toggle-off @endif mx-1 editStatus" data-toggle="tooltip" data-placement="right" title="Publier/Dépublier"></i>
                                            </div>
                                            <div id="delete{{ $meeting->id }}" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Confirmation</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p> Are you sure you want to delete this meeting ' {{  $meeting->title  }} ' ?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Non</button>
                                                            <a href="{{ route('handleDeleteAnnounecement', $meeting->id) }}" type="button" class="btn btn-danger">Oui</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                       @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>
        </div>



  <!-- /.container -->
@endsection
@section('script')

    <script type="text/javascript">


        $(document).on("click", ".editStatus", function () {
            var trid = $(this).closest('tr').attr('id');

            $.get("{{ route('showHome')}}/user/announce/updateStatus/"+trid).done(function (res) {
                if(res.status == 200){
              $('#status'+trid).text('Publié');
                }else {
                    $('#status'+trid).text('Brouillon');
                }
            });

            if($(this).hasClass("fa-toggle-off"))
            {
                $(this).removeClass("fa-toggle-off");
                $(this).addClass("fa-toggle-on");
            } else if ($(this).hasClass("fa-toggle-on")) {
                $(this).removeClass("fa-toggle-on");
                $(this).addClass("fa-toggle-off");
            }
        });
    </script>

@endsection
