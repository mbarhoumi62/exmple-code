@extends('frontOffice.layout')
@section('css')



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<!-- Custom styles for this template -->
<link href="{{asset('css/frontOffice/css/userProfile.css')}}" rel="stylesheet"></link>
<link href="{{asset('css/frontOffice/css/circle.css')}}" rel="stylesheet"></link>

<style media="screen">
.view-account{
  background:#f9f9fb
}
</style>
@endsection

@section('content')

  <style media="screen">
  .col-form-label {
  padding-top: calc(.375rem + 1px) !important;
  padding-bottom: calc(.375rem + 1px) !important;
  margin-bottom: 0 !important;
  font-size: inherit !important;
  line-height: 1.5 !important;
}


.custom-control-label {
    position: relative !important;
    margin-bottom: 0 !important;
}

input[type=radio] {
    box-sizing: border-box;
    padding: 0;
}
  </style>
<!-- Page Content -->
@include('User::frontOffice.inc.userInformation')
@include('User::frontOffice.inc.navUserProfile')
<div class="container">

    <div class="row">
      <div class="col-lg-12">
        <p class="h5 mt-4 font-weight-bold">Vos informations personnelles</p>
        <div class="jumbotron bg-light mt-4">
          <form action="{{route('handleUpdateUserProfile')}}" method="post">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
              <label for="inputPseudo">Pseudo</label>
              <input type="text" class="form-control" id="inputPseudo"  name="username" value="{{Auth::User()->username}}">
              @if ($errors->has('username'))
                  <span class="help-block">
                      <strong>{{ $errors->first('username') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-row">
              <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} col-md-6">
                <label for="inputNom">Nom</label>
                <input type="text" class="form-control" id="inputNom" placeholder="Nom" name="first_name" value="{{Auth::User()->first_name}}">
                @if ($errors->has('first_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} col-md-6">
                <label for="inputPrenom">Prénom</label>
                <input type="text" class="form-control" id="inputPrenom" placeholder="Prénom" name="last_name" value="{{Auth::User()->last_name}}">
                @if ($errors->has('last_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="form-row">
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-md-6">
                <label for="inputEmail">Email</label>
                <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" value="{{Auth::User()->email}}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-6">
                <label for="inputPassword">Mot de passe</label>
                <input type="password" class="form-control" id="inputPassword" placeholder="Mot de passe" name="password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <fieldset class="form-group">
              <div class="row">
                <legend class="col-form-label col-sm-2 pt-0">Genre</legend>
                <div class="col-sm-10">

                  <div class="form-check">
                    <label class="form-check-label" for="radio1">
                      <input type="radio" class="form-check-input" id="radio1" name="gender" value="1"
                       @if (Auth::user()->gender == 1)
                          checked
                      @endif
                      >Homme
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label" for="radio2">
                      <input type="radio" class="form-check-input" id="radio2" name="gender" value="2"

                      @if (Auth::user()->gender == 2)
                         checked
                     @endif

                      >Femme
                    </label>
                  </div>
                </div>
              </div>
            </fieldset>
            <div class="form-row">
              <div class="col-md-4 mb-3">
                <label >Date de naissance</label>
                <input type="date" name="birthday" id="bday"
                min="1920-01-01" class="form-control">
              </div>
              <div class="form-group{{ $errors->has('cin') ? ' has-error' : '' }} col-md-4 mb-3">
                <label >Num.Carte identité</label>
                <input type="text" class="form-control" name="cin" value="{{Auth::User()->cin}}">
                @if ($errors->has('cin'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cin') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}col-md-4 mb-3">
                <label >Num.Tel</label>
                <input type="tel" class="form-control" name ="phone" value = "{{Auth::User()->phone}}">
                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
              <label for="inputAddress">Adresse</label>
              <input type="text" class="form-control" id="inputAddress" placeholder="N°5 Rue Irak" name="address" value="{{Auth::User()->address}}">
              @if ($errors->has('address'))
                  <span class="help-block">
                      <strong>{{ $errors->first('address') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-row">
              <div class="orm-group col-md-6">
                <label for="inputState">Gouvernorat</label>
                <select id="cities" class="form-control" name="city">
                    <option selected>{{Auth::User()->city}}</option>
                  @foreach ($citys as $city)

                        <option value="{{$city->name}}">{{$city->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="inputState">Délégation</label>
                <select id="delegations" class="form-control" name="delegation">
                    <option selected>{{Auth::User()->delegation}}</option>

                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="inputZip">Code postal</label>
                <input value="{{Auth::User()->postal_code}}" type="text" class="form-control" id="inputZip" name="postal_code">
              </div>
            </div>
            <button type="submit" class="btn btn-warning float-right">Valider</button>
          </form>
        </div>

          <p class="h5 mt-4 font-weight-bold">Vos comptes en social media</p>
          <div class="jumbotron bg-light mt-4">
          <form action="{{route('updateUserLinks')}}" method="post">
              {{ csrf_field() }}
          <table id="myTable" class=" table order-list">
              <thead>
              <tr>
                  <td>Platforme</td>
                  <td>Pseudo</td>
              </tr>
              </thead>
              <tbody>
              <tr>
                  <td class="">
                      <span style="text-align: center" class="fab fa-skype pull-left form-control"></span>
                  </td>

                  <td class="col-md-4">
                      <input type="text" name="Spseudo" placeholder="votre skype pseudo" value="{{( \App\Modules\User\Models\UserSocialLinks::where('user_id',AUth::id())->where('platforme','Skype')->first())?  \App\Modules\User\Models\UserSocialLinks::where('user_id',AUth::id())->where('platforme','Skype')->first()->pseudo :'' }}" class="form-control"/>
                  </td>

              </tr>
              <tr>
                  <td class="">
                    <span style="text-align: center" class="fab fa-facebook-f pull-left form-control"></span>
                  </td>

                  <td class="">
                      <input type="text" name="Fpseudo" placeholder="votre facebook pseudo " value="{{ (\App\Modules\User\Models\UserSocialLinks::where('user_id',AUth::id())->where('platforme','Facebook')->first())? \App\Modules\User\Models\UserSocialLinks::where('user_id',AUth::id())->where('platforme','Facebook')->first()->pseudo : '' }}" class="form-control"/>
                  </td>

              </tr>
              <tr>
                  <td class="">
                      <span style="text-align: center" class="fab fa-twitter pull-left form-control"></span>
                  </td>

                  <td class="">
                      <input type="text" name="Tpseudo" placeholder="votre twitter pseudo" value="{{ (\App\Modules\User\Models\UserSocialLinks::where('user_id',AUth::id())->where('platforme','Twitter')->first())? \App\Modules\User\Models\UserSocialLinks::where('user_id',AUth::id())->where('platforme','Twitter')->first()->pseudo : '' }}" class="form-control"/>
                  </td>

              </tr>

              </tbody>

          </table>

              <div class="text-right">
                  <button type="submit" class="btn btn-warning my-3">Valider</button>
              </div>
              </form>
          </div>


              <p class="h5 mt-4 font-weight-bold">Vos informations professionnelles</p>
        <div class="jumbotron bg-light mt-4">
          <form action="{{route('handleUpdateUserAbout')}}" method="post">
              {{ csrf_field() }}
            <div class="form-group">
              <label for="exampleFormControlTextarea1">A propos de vous</label>
              <textarea placeholder="{{(Auth::user()->infos)? Auth::user()->infos->about_me : ''}}" name="about_me" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="inputSituation">Votre situation actuelle</label>
                <select id="inputSituation" name="state" class="form-control">
                    @if(is_null(Auth::user()->infos))
                        <option selected >Situation actuelle</option>
                        <option  value="1">Salarié</option>
                        <option value="2">Entrepreneur</option>
                        <option value="3">Fonctionnaire de l'Etat</option>
                        <option value="4">Chef d'entreprise</option>
                        <option value="5">Enseignant</option>
                        <option value="6">Autre</option>
                    @elseif(Auth::user()->infos->state == 1)
                  <option selected value="1">Salarié</option>
                        <option value="2">Entrepreneur</option>
                        <option value="3">Fonctionnaire de l'Etat</option>
                        <option value="4">Chef d'entreprise</option>
                        <option value="5">Enseignant</option>
                        <option value="6">Autre</option>
                @elseif(Auth::user()->infos->state == 2)
                        <option  value="1">Salarié</option>
                        <option selected value="2">Entrepreneur</option>
                        <option value="3">Fonctionnaire de l'Etat</option>
                        <option value="4">Chef d'entreprise</option>
                        <option value="5">Enseignant</option>
                        <option value="6">Autre</option>

                    @elseif(Auth::user()->infos->state == 3)
                        <option  value="1">Salarié</option>
                        <option  value="2">Entrepreneur</option>
                        <option selected value="3">Fonctionnaire de l'Etat</option>
                        <option value="4">Fonctionnaire de l'Etat</option>
                        <option value="5">Chef d'entreprise</option>
                        <option value="6">Enseignant</option>
                        <option value="7">Autre</option>
                    @elseif(Auth::user()->infos->state == 4)
                        <option  value="1">Salarié</option>
                        <option  value="2">Entrepreneur</option>
                        <option  value="3">Fonctionnaire de l'Etat</option>
                        <option selected value="4">Chef d'entreprise</option>
                        <option value="5">Enseignant</option>
                        <option value="6">Autre</option>
                    @elseif(Auth::user()->infos->state == 5)
                        <option  value="1">Salarié</option>
                        <option  value="2">Entrepreneur</option>
                        <option  value="3">Fonctionnaire de l'Etat</option>
                        <option  value="4">Chef d'entreprise</option>
                        <option selected value="5">Enseignant</option>
                        <option value="6">Autre</option>
                        @else
                        <option  value="1">Salarié</option>
                        <option  value="2">Entrepreneur</option>
                        <option  value="3">Fonctionnaire de l'Etat</option>
                        <option  value="4">Chef d'entreprise</option>
                        <option  value="5">Enseignant</option>
                        <option selected value="6">Autre</option>

                    @endif
                </select>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="inputSchoolLevel">Votre niveau scolaire</label>
                <select id="inputSchoolLevel" name="study_level" class="form-control">
                    @if(is_null(Auth::user()->infos))
                        <option selected >Niveau scolaire</option>
                        <option  value="1">Baccalauréat</option>
                        <option value="2">Licence</option>
                        <option value="3">Master</option>
                        <option value="4">Doctorat</option>
                        <option value="5">Autre</option>

                 @elseif(Auth::user()->infos->study_level == 1)
                  <option selected value="1">Baccalauréat</option>
                  <option value="2">Licence</option>
                  <option value="3">Master</option>
                  <option value="4">Doctorat</option>
                  <option value="5">Autre</option>
                     @elseif(Auth::user()->infos->study_level == 2)
                        <option  value="1">Baccalauréat</option>
                        <option selected value="2">Licence</option>
                        <option value="3">Master</option>
                        <option value="4">Doctorat</option>
                        <option value="5">Autre</option>
                    @elseif(Auth::user()->infos->study_level == 3)
                        <option  value="1">Baccalauréat</option>
                        <option  value="2">Licence</option>
                        <option selected value="3">Master</option>
                        <option value="4">Doctorat</option>
                        <option value="5">Autre</option>
                    @elseif(Auth::user()->infos->study_level == 4)
                        <option  value="1">Baccalauréat</option>
                        <option  value="2">Licence</option>
                        <option  value="3">Master</option>
                        <option selected value="4">Doctorat</option>
                        <option value="5">Autre</option>
                    @else
                        <option  value="1">Baccalauréat</option>
                        <option  value="2">Licence</option>
                        <option  value="3">Master</option>
                        <option  value="4">Doctorat</option>
                        <option selected value="5">Autre</option>

                    @endif
                </select>
              </div>
            </div>
            <div class="text-right">
              <button type="submit" class="btn btn-warning my-3">Valider</button>
            </div>
          </form>
            <br>
            <br>
          <div class="card mb-4">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-11">
                  <span class="h-5 font-weight-bold">Diplômes</span>
                </div>
                <div class="col-lg-1">
                  <button type="button" name="button" class="btn" data-toggle="modal" data-target="#diplomeModal" style="background-color: #ffc33d54" >
                  <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
            </div>
            @foreach (Auth::user()->educations as $education)

                  <div class="card-body" >
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-11">
                        <h5>{{$education->school}}</h5>
                        <input type="hidden" id="school{{$education->id}}" value="{{$education->school}}">
                        </div>
                        <div class="col-lg-1">
                          <button type="button" name="button" class="btn edit-education" data-education = {{$education->id}} style="background-color: #ffc33d54">
                          <i class="fas fa-pencil-alt"></i>
                          <a href="#" style="background-color: #ffc33d54" class="edit-education"></a>
                          </button>
                        </div>
                      </div>
                      <h6>Diplome  : {{$education->diploma}}</h6>
                      <input type="hidden" id="diploma{{$education->id}}" value="{{$education->diploma}}">
                      <h6>Domaine d'étude:  {{$education->study_field}}</h6>
                      <input type="hidden" id="study{{$education->id}}" value="{{$education->study_field}}">
                      <small class="text-muted"><span>{{$education->start_year}}</span><span> - </span><span>{{$education->end_year}}</span></small>
                    </div>
                    <hr>
                  </div>

          @endforeach
          </div>
          <div class="card mb-4">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-11">
                  <span class="h-5 font-weight-bold">Expérience professionnelles</span>
                </div>
                <div class="col-lg-1">
                  <button type="button" name="button" class="btn" data-toggle="modal" data-target="#experienceModal" style="background-color: #ffc33d54">
                  <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
            </div>
            @foreach (Auth::user()->experiences as $experience)

              <div class="card-body">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-11">
                      <h5>{{$experience->title}}</h5>
                      <input type="hidden" id="title{{$experience->id}}" value="{{$experience->title}}">

                    </div>
                    <div class="col-lg-1">
                      <button type="button" name="button"  data-experience = {{$experience->id}} class="btn edit-experience" style="background-color: #ffc33d54">
                      <i class="fas fa-pencil-alt"></i>
                      </button>
                    </div>
                  </div>
                  <h6>Entreprise: {{$experience->entreprise}}</h6>
                  <input type="hidden" id="entreprise{{$experience->id}}" value="{{$experience->entreprise}}">
                  <h6>Lieu : {{$experience->place}}</h6>
                  <input type="hidden" id="place{{$experience->id}}" value="{{$experience->place}}">
                  <p>{{$experience->descreption}}</p>
                  <input type="hidden" id="descreption{{$experience->id}}" value="{{$experience->descreption}}">
                  <small class="text-muted"><span>{{$experience->start_year}}</span><span> - </span><span>{{$experience->end_year}}</span></small>
                </div>
                <hr>
              </div>

          @endforeach
          </div>
          <div class="card mb-4">
            <div class="card-header">
              <div class="row">
                <div class="col-lg-11">
                  <span class="h-5 font-weight-bold">Centres d'intérêt</span>
                </div>

              </div>
            </div>
            <div class="card-body">
              <div class="container">
          <div class="row" style="margin-bottom: 20px">
            <button id="interest1" style="margin-right: 5px" type="button" class="btn  {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Art et culture')->first())? 'btn-primary' : ''}}" class="add-interest">Art et culture</button>
            <button id="interest2" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Lecture')->first())? 'btn-primary' : ''}}" class="add-interest">Lecture</button>
            <button id="interest3" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Economie')->first())? 'btn-primary' : ''}}" class="add-interest">Economie</button>
            <button id="interest4" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Cuisine')->first())? 'btn-primary' : ''}}" class="add-interest">Cuisine</button>
            <button id="interest5" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Nouvelles technologies')->first())? 'btn-primary' : ''}}" class="add-interest">Nouvelles technologies</button>

          </div>
          <div class="row" style="margin-bottom: 20px">
            <button id="interest6" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Communication médias')->first())? 'btn-primary' : ''}}" class="add-interest">Communication médias</button>

            <button id="interest7" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Mode')->first())? 'btn-primary' : ''}}" class="add-interest">Mode</button>
            <button id="interest8" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Politique')->first())? 'btn-primary' : ''}}" class="add-interest">Politique</button>
            <button id="interest9" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Santé et bien etre')->first())? 'btn-primary' : ''}}" class="add-interest">Santé et bien etre</button>


          </div>
          <div class="row" style="margin-bottom: 20px">

            <button id="interest10" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Ecologie et développement durable')->first())? 'btn-primary' : ''}}" class="add-interest">Ecologie et développement durable</button>
            <button id="interest11" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Musique')->first())? 'btn-primary' : ''}}" class="add-interest">Musique</button>
            <button id="interest12" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Sciences')->first())? 'btn-primary' : ''}}" class="add-interest">Sciences</button>
            <button id="interest13" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Sport')->first())? 'btn-primary' : ''}}" class="add-interest">Sport</button>

          </div>
            <div class="row" style="margin-bottom: 20px">

            <button id="interest14" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Ativités manuelles')->first())? 'btn-primary' : ''}}" class="add-interest">Ativités manuelles</button>
            <button id="interest15" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Théatre')->first())? 'btn-primary' : ''}}" class="add-interest">Théatre</button>
            <button id="interest16" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Spectacles')->first())? 'btn-primary' : ''}}" class="add-interest">Spectacles</button>
            <button id="interest17" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Photo, video')->first())? 'btn-primary' : ''}}" class="add-interest">Photo, video</button>
            </div>

          <div class="row">
            <button id="interest18" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Peche , activités nautiques')->first())? 'btn-primary' : ''}}" class="add-interest" value="Peche , activités nautiques">Peche , activités nautiques</button>
            <button id="interest19" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Jardinage, bricolage')->first())? 'btn-primary' : ''}}" class="add-interest" value="Jardinage, bricolage">Jardinage, bricolage</button>
            <button  id="interest20" style="margin-right: 5px" type="button" class="btn {{(\App\Modules\User\Models\UserInterests::where('user_id',Auth::id())->where('name','Tourisme et voyages')->first())? 'btn-primary' : ''}}" class="add-interest" value="Tourisme et voyages">Tourisme et voyages</button>


          </div>
              </div>
              <hr>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-1"></div>
    </div>
  </div>


@include('User::frontOffice.modals.educationModal')
@include('User::frontOffice.modals.updateEducationModal')
@include('User::frontOffice.modals.experienceModal')
@include('User::frontOffice.modals.updateExperienceModal')


  <!-- /.container -->
@endsection

@section('script')

  <script type="text/javascript">

    $( ".edit-education" ).click(function() {
});

  $(document).on('click', '.edit-education', function() {
            var id = $(this).data('education');
            $('#education_id').val(id);
            $('#school').val($('#school'+id).val());
            $('#diploma').val($('#diploma'+id).val());
            $('#study').val($('#study'+id).val());
            $('#upadteEducatioModal').modal('show');
      });

      $(document).on('click', '.edit-experience', function() {
                var id = $(this).data('experience');

                console.log($('#descreption'+id).val());
                $('#experience_id').val(id);
                console.log($('#description'+id).val());
                $('#title').val($('#title'+id).val());
                $('#entreprise').val($('#entreprise'+id).val());
                $('#place').val($('#place'+id).val());
                $('#descreption').val($('#descreption'+id).val());
                $('#updateExperienceModal').modal('show');
          });

      for(var i = 1;i<22;i++){
    $(document).on('click','#interest'+i, function() {
        var interest = $(this).text();

        if($(this).hasClass('btn-primary')){
            $(this).removeClass('btn-primary');

            $.get("{{ route('showHome')}}/user/remove-interest/"+interest).done();

        }else{
        $(this).addClass('btn-primary');

        $.get("{{ route('showHome')}}/user/add-interest/"+interest).done();

        }
    });
      }
    $("#cities").change(function () {
        var selectedCity = $(this).children("option:selected").val();
        $('#delegations').children().remove();

        $.get("{{ route('showHome')}}/delegations/" + selectedCity).done(function (res) {
        $.each(res.delegations, function (i, d) {

            $('#delegations').append('<option value="' + d.name + '">' + d.name + '</option>');
        });

    });
    });
    document.getElementById("bday").defaultValue = "{{(Auth::user()->birthday)? Auth::user()->birthday->format('Y-m-d') : "dd/mm/yyyy"}}";




  </script>









@endsection
