<div class="modal fade" id="updateExperienceModal" tabindex="-1" role="dialog" aria-labelledby="experienceModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="experienceModalLabel">Update une expérience</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action = "{{route('hundelUpdateExperience')}}" method="post">
          {{ csrf_field() }}

          <input type="hidden" id="experience_id" name="id">
          <div class="form-group">
            <label for="experienceInputPoste">Intitulé du poste</label>
            <input type="text" class="form-control" id="title" name="title" required="">
          </div>
          <div class="form-group">
            <label for="experienceInputPassword">Entreprise</label>
            <input type="text" class="form-control" id="entreprise" name="entreprise" required="">
          </div>
          <div class="form-group">
            <label for="experienceInputDE">Lieu</label>
            <input type="text" class="form-control" id="place" name="place" required="">
          </div>
          <div class="form-row">
            <div class="col-md-6 mb-3">
              <label >De l'année</label>
              <input type="date" name="start_year" max="{{Carbon\Carbon::now()->format('Y-m-d')}}"
              min="1920-01-01" class="form-control" required="">
            </div>
            <div class="col-md-6 mb-3">
              <label >A l'année</label>
              <input type="date" name="end_year" max="{{Carbon\Carbon::now()->format('Y-m-d')}}"
              min="1920-01-01" class="form-control" required="">
            </div>
          </div>
          <div class="form-group">
            <label for="experienceFormControlDescription">Description</label>
            <textarea class="form-control" id="descreption" rows="3" name="descreption" required=""></textarea>
          </div>

          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Enregistrer</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>
