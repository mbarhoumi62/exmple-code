<div class="modal fade" id="diplomeModal" tabindex="-1" role="dialog" aria-labelledby="diplomeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="diplomeModalLabel">Ajouter diplôme</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action = "{{route('hundelAddEducation')}}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="diplomeInputEcole">Ecole</label>
            <input type="text" class="form-control" id="diplomeInputEcole" name="school" required="">
          </div>
          <div class="form-group">
            <label for="diplomeInputPassword">Diplome</label>
            <input type="text" class="form-control" id="diplomeInputPassword" name="diploma" required="">
          </div>
          <div class="form-group">
            <label for="diplomeInputDE">Domaine d'étude</label>
            <input type="text" class="form-control" id="diplomeInputDE" name="study_field" required="">
          </div>
          <div class="form-row">
            <div class="col-md-6 mb-3">
              <label >De l'année</label>
              <input type="date" name="start_year" max="{{Carbon\Carbon::now()->format('Y-m-d')}}"
              min="1920-01-01" class="form-control" required="">
            </div>
            <div class="col-md-6 mb-3">
              <label >A l'année</label>
              <input type="date" name="end_year" max="{{Carbon\Carbon::now()->format('Y-m-d')}}"
              min="1920-01-01" class="form-control" required="" >
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Enregistrer</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>
