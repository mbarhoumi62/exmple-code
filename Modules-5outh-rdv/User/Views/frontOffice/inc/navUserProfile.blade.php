<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-2">
  <div class="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
    aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'showProfile' ))  active @endif"   href="{{route('showProfile')}}" >Mon profil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link  @if(Route::currentRouteName() == 'showMyAnnounces' ))  active @endif"   href="{{route('showMyAnnounces')}}">Mes Annonces </a>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'showMyCommandes' ))  active @endif"   href="{{route('showMyCommandes')}}" ">Mes Vends</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link @if(Route::currentRouteName() == 'showMyParticipations' ))  active @endif"   href="{{route('showMyParticipations')}}" ">Mes Achats</a>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'showMyIncomes' ))  active @endif"   href="{{route('showMyIncomes')}}" >Mes révenus</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
