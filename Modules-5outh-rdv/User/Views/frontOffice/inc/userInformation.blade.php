<div class="container mt-1">
  <!-- BreadCrumb-->
  <div class="row brdcrumb">
    <ul class="list-inline">
      <li class="list-inline-item"><a class="text-dark" href="">Acceuil</a></li>
      <li class="list-inline-item"><i class="fas fa-angle-right"></i></li>
      <li class="list-inline-item"><a class="text-dark" href="">Mon Profile</a></li>
    </ul>
  </div>
  <!-- partie mon-compte-->
  <div class="row mb-2">
    <div class="col-lg-3">
      @if (Auth::user()->picture != null)
         <img src="{{asset(Auth::user()->picture)}}" width="200" height="200" alt="..." class="img-thumbnail">
          @else
          <img src="{{asset('img/unknown.png')}}" width="200" height="200" alt="..." class="img-thumbnail">
      @endif
      <form id="form" action="{{route('handleUpdateUserProfilePicture')}}" method="post" enctype="multipart/form-data">

        {{ csrf_field() }}
          <label for="">Télécharger une photo de profil</label>
      <input id="file" name="picture" type="file" />
     </form>
    </div>
    <div class="col-lg-9 border rounded">
        <div class="row">
    <div class="col-lg-4">
        <div class="card">

            <div class="card-block px-2">
                <h4 class="card-title">{{Auth::User()->first_name}} {{Auth::User()->last_name}}</h4>
                <p class="card-text">inscrit le&nbsp<span>{{Auth::User()->created_at->format('m/d/Y')}}&nbsp<span>à&nbsp</span>{{Auth::User()->created_at->format('H')}}h</span></p>
                <p class="card-text">Votre ID&nbsp : <span>{{Auth::User()->cin}}</span></p>
            </div>
            <div class="w-100"></div>

        </div>
    </div>
      <div class="col-lg-4">


          <div class="card">

              <div class="card-block px-2">
                  <h4 class="card-title">Votre Contribution</h4>
                  <p class="card-text">Vous avez participé à&nbsp<span>{{Auth::User()->participations->count()}}&nbsp</span><span>RDV</span></p>
                  <p class="card-text">Vous avez publié &nbsp<span>{{Auth::User()->meetings->count()}}&nbsp</span><span>RDV</span></p>
              </div>
              <div class="w-100"></div>

          </div>
      </div>
      <div class="col-lg-4">
          <div class="card">

              <div class="card-block px-2">
                  <h4 class="card-title">Status du Profil</h4>

                  <p class="card-text">Profil complet à<span>&nbsp{{$pc}}%</span></p>

                  <div class="progress card-text">
                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="15"aria-valuemin="0" aria-valuemax="100" style="width:{{$pc}}%;height: 23px">{{$pc}}%</div>
                  </div>
              </div>

          </div>
      </div>
  </div>
    </div>
</div>
<!-- /.partie mon-compte-->
</div>
<script>
    document.getElementById("file").onchange = function() {
        document.getElementById("form").submit();
    };
</script>

