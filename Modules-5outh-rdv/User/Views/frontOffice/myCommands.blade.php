@extends('frontOffice.layout')
@section('css')



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<!-- Custom styles for this template -->
<link href="{{asset('css/frontOffice/css/userProfile.css')}}" rel="stylesheet"></link>
<link href="{{asset('css/frontOffice/css/circle.css')}}" rel="stylesheet"></link>

<style media="screen">
.view-account{
  background:#f9f9fb
}


 .invalid {
     color: red;
 }
.valid {
    color: green;
}
[disabled]{
    cursor: no-drop;
}
.blk-down{
    margin-top: -16px;
}
.blk-up{
    margin-top: -3px;
}
</style>
@endsection

@section('content')
<!-- Page Content -->
@include('User::frontOffice.inc.userInformation')
@include('User::frontOffice.inc.navUserProfile')


<div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mon profil <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mes annonces RDV </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Commandes sur RDV</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mes participations aux RDV</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mes révenus</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container bg-light rounded my-3">
            <form>
                <div class="form-row">
                    <!--affiche seullement les catégories des rdv publiés et non terminés-->
                    <div class="form-group col-md-4">
                        <label for="sectionInPublicRdv">Catégorie</label>
                        <select id="sectionInPublicRdv" class="form-control" >
                            <option value="" selected>Select Category</option>

                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="subsectionInPublicRdv">Sous catégorie</label>
                        <select id="subsectionInPublicRdv" class="form-control" >
                            <option value="" selected>Select SubCategory</option>

                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inPublicRdv">Titre de RDV</label>
                        <select id="inPublicRdv" class="form-control" >
                            <option value="" selected>Select Title</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
<div class="container bg-light rounded my-3">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <p class="h5 my-4">Les commandes sur <a href="">&nbsp<span id="itemCmdRdv"></span></a>&nbsp<span class="badge badge-success">validé</span></p>
            <div class="rdvList">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="container-fluid">
                        <tr>
                            <th scope="col">
                                <div>
                                                <span>
                                                    Faite le
                                                </span>
                                    <div class="float-right">
                                        <div class="blk-up">
                                            <i class="fas fa-caret-up"></i>
                                        </div>
                                        <div class="blk-down">
                                            <i class="fas fa-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th scope="col">
                                <div>
                                                <span>
                                                    Participant
                                                </span>
                                    <div class="float-right">
                                        <div class="blk-up">
                                            <i class="fas fa-caret-up"></i>
                                        </div>
                                        <div class="blk-down">
                                            <i class="fas fa-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th scope="col">
                                <div>
                                                <span>
                                                    Statut
                                                </span>
                                    <div class="float-right">
                                        <div class="blk-up">
                                            <i class="fas fa-caret-up"></i>
                                        </div>
                                        <div class="blk-down">
                                            <i class="fas fa-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th scope="col">
                                <div>
                                                <span>
                                                    Action
                                                </span>
                                    <div class="float-right">
                                        <div class="blk-up">
                                            <i class="fas fa-caret-up"></i>
                                        </div>
                                        <div class="blk-down">
                                            <i class="fas fa-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th scope="col">
                                <div>
                                                <span>
                                                    Participation
                                                </span>
                                    <div class="float-right">
                                        <div class="blk-up">
                                            <i class="fas fa-caret-up"></i>
                                        </div>
                                        <div class="blk-down">
                                            <i class="fas fa-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody id="divContainer">
                   @foreach($participations as $participation)

                        <tr ><th scope="row">{{explode('-', explode(' ',$participation->created_at)[0])[2].'/'.explode('-', explode(' ',$participation->created_at)[0])[1].'/'.explode('-', explode(' ',$participation->created_at)[0])[0]}}</th>

                            <td id="user{{$participation->id}}">{{$participation->user->id}}</td>
                            <td id="validationClass{{$participation->id}}" class="{{($participation->presence == 0)? 'invalid' : 'valid'}}">{{($participation->presence == 0)? 'Non Confirmé' : 'Confirmé'}}</td>
                            <td>
                                <a href="" style="color: black" id="confirmationAction" data-id="{{$participation->id}}">{{($participation->presence == 0)? 'Confirmé' : ''}}</a>
                            </td>
                            <td class="text-center">
                                <button  class="btn btn-warning disabling{{$participation->id}}" {{($participation->presence == 0 || $participation->presence == 3)? 'disabled' : 'enabled'}} id="showModal">Valider</button>
                            </td>
                        </tr>

                       @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>

</div>
<!-- Modal validation commande RDV-->
        <div class="modal fade" id="cmdModal" tabindex="-1" role="dialog" aria-labelledby="cmdModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="cmdModalLabel"></h5>
                    </div>
                    <div class="modal-body">
                        <p>Vous etes sur le point de valider la participation de membre suivants: <p id="userName"></p></p>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="font-weight-bold" for="inputJeton">Clé participant</label>
                                    <input type="text" class="form-control" id="key" placeholder="Clé">
                                </div>
                            </div>
                            <div  class="form-group col-md-12">
                                <label for="" class="invalid" id="errorMsg"></label>
                                <input type="hidden" id="participationId">
                            </div>
                            <button  id="validateParticipation" class="btn btn-warning float-right" >Valider</button>
                    </div>
                </div>
            </div>
        </div>



@endsection
@section('script')

    <script>

       var meetingName = "";
        $(document).ready(function () {
            $.get("{{ route('showHome')}}/user/meetings/categories").done(function (res) {

               console.log(res);
                $.each(res.categories, function(j, d) {

                    $('select#sectionInPublicRdv').append('<option value="' + d.id + '">' + d.name + '</option>');
                });

            });

        });

        $('#sectionInPublicRdv').on('change',function(){

            var catId = $(this).children("option:selected").val();
            $('#subsectionInPublicRdv').find('.forRemove').remove();
            $('#divContainer').children().remove();

            $.get("{{ route('showHome')}}/user/meetings/subCategories/"+catId).done(function (res) {

                $.each(res.subCategories, function(j, d) {

                    $('#subsectionInPublicRdv').append('<option class="forRemove" value="' + d.id + '">' + d.name + '</option>');
                });

            });

        });

        $('#subsectionInPublicRdv').on('change',function(){

            var catId = $('#sectionInPublicRdv').children("option:selected").val();
            var subCatId = $(this).children("option:selected").val();
            $('#inPublicRdv').find('.titlesForRemove').remove();
            $.get("{{ route('showHome')}}/user/meetings/"+catId+"/"+subCatId).done(function (res) {

                $.each(res.meetings, function(j, d) {

                    $('#inPublicRdv').append('<option class="titlesForRemove" value="' + d.id + '">' + d.title + '</option>');


                });

            });

        });

        $('#inPublicRdv').on('change',function(){

            $('#divContainer').children().remove();
            var meetingId = $('#inPublicRdv').children("option:selected").val();
            var meetingTitle = $('#inPublicRdv').children("option:selected").text();
            meetingName = meetingTitle;
            $('#itemCmdRdv').text(meetingTitle);
               $.get("{{ route('showHome')}}/user/participations/"+meetingId).done(function (res) {
                   console.log(res);
                $.each(res.participations, function(j, d) {

                    var presence = '';
                    var action = "";
                    var confirmationClass = "";
                    var validation ='enabled';

                    if(d.presence == 0){
                        presence = "Non Confirmé";
                        action = "Confirmé";
                        confirmationClass = 'invalid';
                        validation = 'disabled';

                    }else if(d.presence == 1 || d.presence == 3){
                        presence = "Confirmé";
                        confirmationClass = 'valid';

                    }

                    if(d.presence == 3){
                        validation = 'disabled';
                    }
                    var day = d.created_at.split(' ')[0].split('-')[2];
                    var month = d.created_at.split(' ')[0].split('-')[1];
                    var year = d.created_at.split(' ')[0].split('-')[0];


                    $('#divContainer').append('  <tr ><th scope="row">'+day+'/'+month+'/'+year+'</th>\n' +
                        '                            <td id="user'+d.id+'" >'+d.user.username+'</td>\n' +
                        '                            <td id="validationClass'+d.id+'" class="'+confirmationClass+'">'+presence+'</td>\n' +
                        '                            <td>\n' +
                        '                                <a href="" style="color: black" id="confirmationAction" data-id="'+d.id+'">'+action+'</a>\n' +
                        '                            </td>\n' +
                        '                            <td class="text-center">\n' +
                        '                                <button  class="btn btn-warning disabling'+d.id+'" id="showModal" '+validation+' data-id="'+d.id+'">Valider</button>\n' +
                        '                            </td>   </tr>');




                });



            });

        });

        $(document).on('click','#showModal', function () {
         $('#participationId').val($(this).data('id'));
         $('#cmdModalLabel').text(meetingName);
         var id = $(this).data('id');
         var username = $("#user"+id).text();
         $('#userName').text(username);
         $('#cmdModal').modal('show');
        });

        $(document).on('click','#validateParticipation', function () {

            var key = $('#key').val();
            var participationId = $('#participationId').val();
            $.get("{{ route('showHome')}}/user/participation/validate/"+participationId+"/"+key).done(function (res) {

                if(res.status == 200){
                    $('#cmdModal').modal('hide');
                    $('.disabling'+participationId).prop("disabled",true);


                }else {
                    $('#errorMsg').text('Verifier le clé SVP !!')
                }

            });
        });

        $(document).on('click','#confirmationAction', function (e) {
            e.preventDefault();
            $(this).text('');
            var id = $(this).data('id');
            $('.disabling'+id).prop("disabled",false);
            $("#validationClass"+id).removeClass("invalid");
            $("#validationClass"+id).addClass("valid");
            $("#validationClass"+id).text("Confirmé");
            $.get("{{ route('showHome')}}/user/participation/confirme/"+id).done();

        });






    </script>
    @endsection