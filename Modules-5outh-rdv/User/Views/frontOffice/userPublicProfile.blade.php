<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style type="text/css">
    body{
  margin-top: auto;
    background-color: #f1f1f1;
  }
  .border{
    border-bottom:1px solid #F1F1F1;
    margin-bottom:10px;
  }

  .image-section{
    padding: 0px;
  }
  .image-section img{
    width: 100%;
    height:250px;
    position: relative;
  }
  .user-image{
    position: absolute;
    margin-top:-50px;
  }

  .user-image img{
    width:150px;
    height:auto;
  }
  .user-profil-part{
    padding-bottom:30px;
    background-color:#FAFAFA;
  }
  .follow{    
        background-color: #FFC107!important;
    border-color: #FFC107!important;
    margin-top:70px;   
  }
  .user-detail-row{
    margin:0px; 
  }
  .user-detail-section2 p{
    font-size:12px;
    padding: 0px;
    margin: 0px;
  }
  .user-detail-section2{
    margin-top:10px;
  }
  .user-detail-section2 span{
    color:#7CBBC3;
    font-size: 20px;
  }
  .user-detail-section2 small{
    font-size:12px;
    color:#D3A86A;
  }
  .profile-right-section{
    padding: 20px 0px 10px 15px;
    background-color: #FFFFFF;  
  }
  .profile-right-section-row{
    margin: 0px;
  }
  .profile-header-section1 h1{
    font-size: 25px;
    margin: 0px;
  }
  .profile-header-section1 h5{
    color: #0062cc;
  }
  .req-btn{
    height:30px;
    font-size:12px;
  }
  .profile-tag{
    padding: 10px;
    border:1px solid #F6F6F6;
  }
  .profile-tag p{
    font-size: 12px;
    color:black;
  }
  .profile-tag i{
    color:#ADADAD;
    font-size: 20px;
  }
  .image-right-part{
    background-color: #FCFCFC;
    margin: 0px;
    padding: 5px;
  }
  .img-main-rightPart{
    background-color: #FCFCFC;
    margin-top: auto;
  }
  .image-right-detail{
    padding: 0px;
  }
  .image-right-detail p{
    font-size: 12px;
  }
  .image-right-detail a:hover{
    text-decoration: none;
  }
  .image-right img{
    width: 100%;
  }
  .image-right-detail-section2{
    margin: 0px;
  }
  .image-right-detail-section2 p{
    color:#38ACDF;
    margin:0px;
  }
  .image-right-detail-section2 span{
    color:#7F7F7F;
  }

  .nav-link{
    font-size: 1.2em;    
  }
  
</style>
@extends('frontOffice.layout')
@section('css')

  <style>
    .cross-keywrd:hover, .local-rdv:hover {
        cursor: pointer;
    }

    .local-rdv.invalid {
        color: red;
    }

    .local-rdv.valid {
        color: green;
    }

    .keywrdspan {
        font-size: smaller !important;
        font-weight: 400 !important;
    }

    .invalidputrdv {
        border: solid 1px red;
    }

    @-webkit-keyframes blinker {
        from {
            opacity: 1.0;
        }
        to {
            opacity: 0.0;
        }
    }

    .blink {
        text-decoration: blink;
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 0.6s;
        -webkit-animation-iteration-count: infinite;
        -webkit-animation-timing-function: ease-in-out;
        -webkit-animation-direction: alternate;
    }

    #delete-date-rdv {
        display: none;
    }

    #delete-date-rdv:hover, #add-date-rdv:hover {
        cursor: pointer;
        border-bottom: 2px solid #e3c136;
    }

    .oblig-star {
        color: red;
    }
    .arrow-up {
        width: 0;
        height: 0;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-bottom: 5px solid #ffc107;
    }
    #cart-inf-title .arrow-up {
        position: relative;
        margin-left: 116px;
    }
    #cart-inf-keywrd .arrow-up {
        position: relative;
        margin-left: 75px;
    }
    #cart-inf-desc .arrow-up {
        position: relative;
        margin-left: 187px;
    }
    .arr-cartinf{
        display: none;
        margin-bottom: 4px;
    }
    .cartid-info {
        -webkit-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
        -moz-box-shadow: 0px 0px 7px 1px rgba(255, 0, 0, 1);
        box-shadow: 0px 0px 7px 1px rgba(255, 193, 7, 1);
    }
</style>

@endsection
@section('content')
<body>
   <div class="col-md-12 col-sm-12 col-xs-12 image-section">
                <img style="height: 90px!important" src="{{asset('img/banner.png')}}">
            </div>
    <div class="container main-secction">

        <div class="row">
           
                <div class="col-md-4 user-profil-part pull-left" style="margin-left: 30px">
                    <div class="row ">
                        <div class="col-md-12 col-md-12-sm-12 col-xs-12 user-image text-center">
                            <img src="{{asset($user->picture)}}" class="rounded-circle">
                        </div>
                        <div class="col-md-12 col-xs-12  text-center">
                            <br>
                            <p>{{$user->first_name}}  {{$user->last_name}}</p>
                            <hr style="width : 30%">
                            <p>@if(is_null($user->infos->state)) @elseif($user->infos->state == 1) Salarié
                                @elseif($user->infos->state == 2) Entrepreneur
                                @elseif($user->infos->state == 3) Fonctionnaire de l'Etat
                                @elseif($user->infos->state == 4) Chef d'entreprise
                                @elseif($user->infos->state == 5) Enseignant
                                @else Autre

                                @endif</p>
                        </div>


                        <div class="cont-autre-rdv">

                            <hr>
                            <br>
                            <p style="font-size: 1.36rem!important;" class="h6">Mes derniers RDV organisés
                            </p>
                            <hr>
                            <br>
                            <div class="row">

                                @foreach($user->meetings->take(3) as $meeting)

                                    <div class="col-lg-12 mb-4">
                                        <div class="card">
                                            <a href="{{route('showDetailMeeting',$meeting->id)}}"><img class="card-img-top" src="{{asset($meeting->picture)}}" alt=""></a>
                                            <div class="card-body">
                                                <h4 class="card-title">
                                                    <a class="rdv-annoce-title" href="{{route('showDetailMeeting',$meeting->id)}}">{{$meeting->title}}</a>
                                                </h4>
                                                <h5>${{$meeting->sessions->first()->price}}</h5>
                                                <span><i class="fas fa-map-marker-alt"></i>&nbsp {{($meeting->sessions->first()->place)?$meeting->sessions->first()->place->city->name : 'A distance'}}</span><br>
                                                <span><i class="far fa-clock"></i>&nbsp {{$meeting->sessions->first()->start_date->format('Y/m/d')}}</span>
                                            </div>

                                        </div>
                                    </div>

                                @endforeach



                            </div>
                        </div>

            
                    </div>
                </div>
            <br>
            <div class="col-md-7 user-profil-part pull-left" style="margin-left: 30px; margin-top: 10px">

                <div class="card mb-4">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-11">
                                <span class="h-5 font-weight-bold">A propos de moi</span>
                            </div>

                        </div>
                    </div>

                        <div class="card-body" >
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <h6 style="    line-height: 25px;
">{{($user->infos->about_me)? $user->infos->about_me : ''}}</h6>
                                    </div>

                                </div>
                            </div>
                            <hr>
                        </div>

                </div>


                    <div class="card mb-4">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-11">
                                    <span class="h-5 font-weight-bold">Mon Cursus Académique</span>
                                </div>

                            </div>
                        </div>
                        @foreach ($user->educations as $education)

                            <div class="card-body" >
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-11">
                                            <h5>{{$education->school}}</h5>
                                            <input type="hidden" id="school{{$education->id}}" value="{{$education->school}}">
                                        </div>

                                    </div>
                                    <h6>Diplome  : {{$education->diploma}}</h6>
                                    <h6>Domaine d'étude:  {{$education->study_field}}</h6>
                                    <small class="text-muted"><span>{{$education->start_year}}</span><span> - </span><span>{{$education->end_year}}</span></small>
                                </div>
                                <hr>
                            </div>

                        @endforeach
            </div>
                <div class="card mb-4">
                    <div class="card-header">
                            <div class="col-lg-11">
                                <span class="h-5 font-weight-bold">Mon Parcours Professionnel</span>
                            </div>

                        </div>

                    @foreach ($user->experiences as $experience)

                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <h5>{{$experience->title}}</h5>
                                        <input type="hidden" id="title{{$experience->id}}" value="{{$experience->title}}">

                                    </div>

                                </div>
                                <h6>Entreprise: {{$experience->entreprise}}</h6>
                                <h6>Lieu : {{$experience->place}}</h6>
                                <p>{{$experience->descreption}}</p>
                                <small class="text-muted"><span>{{$experience->start_year}}</span><span> - </span><span>{{$experience->end_year}}</span></small>
                            </div>
                            <hr>
                        </div>

                    @endforeach
                </div>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-11">
                                <span class="h-5 font-weight-bold">Mes Centres D’intérêt</span>
                            </div>

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                             @foreach($user->interests as $interest)
                            <button  style="margin-right: 5px; margin-bottom: 20px" type="button" class="btn btn-primary">{{$interest->name}}</button>
@endforeach

                        </div>
                        </div>

                    </div>
                </div>


        </div>
    </div>
    </div>

</body>
@endsection