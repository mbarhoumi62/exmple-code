@extends('frontOffice.layout')
@section('css')



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<!-- Custom styles for this template -->
<link href="{{asset('css/frontOffice/css/userProfile.css')}}" rel="stylesheet"></link>
<link href="{{asset('css/frontOffice/css/circle.css')}}" rel="stylesheet"></link>

<style media="screen">
.view-account{
  background:#f9f9fb
}
</style>
@endsection

@section('content')
<!-- Page Content -->
@include('User::frontOffice.inc.userInformation')
@include('User::frontOffice.inc.navUserProfile')


<div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mon profil <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mes annonces RDV </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Commandes sur RDV</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Mes participations aux RDV</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mes révenus</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container bg-light rounded my-3">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="rdvList mt-3">
                        <div class="table-responsive">
                            <p class="h5 font-weight-bold my-4">Mes participations</p>
                            <table class="table">
                                <thead class="container-fluid">
                                    <tr>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Faite le
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Désignation
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Statut
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Mon Pass-RDV
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div>
                                                <span>
                                                    Note sur RDV
                                                </span>
                                                <div class="float-right">
                                                    <div class="blk-up">
                                                        <i class="fas fa-caret-up"></i>
                                                    </div>
                                                    <div class="blk-down">
                                                        <i class="fas fa-caret-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                  @foreach ($participations as $participation)

                                    <tr>
                                        <th scope="row">{{$participation->session->start_date->format('m/d/Y')}}</th>
                                        <td>{{$participation->session->meeting->title}}</td>
                                        <td class="valid">Confirmée</td>
                                        <td>
                                            <a href="{{route('pdfDownload',$participation->session->id)}}">Télécharge</a>
                                        </td>
                                        <td class="text-center">
                                            <a href="" class="btn btn-warning disabled">Noter</a>
                                        </td>
                                    </tr>

                                  @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>


@endsection
