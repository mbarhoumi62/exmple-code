@extends('frontOffice.layout')
@section('css')



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<!-- Custom styles for this template -->
<link href="{{asset('css/frontOffice/css/userProfile.css')}}" rel="stylesheet"></link>
<link href="{{asset('css/frontOffice/css/circle.css')}}" rel="stylesheet"></link>

<style media="screen">
.view-account{
  background:#f9f9fb
}
</style>
@endsection

@section('content')
<!-- Page Content -->
@include('User::frontOffice.inc.userInformation')
@include('User::frontOffice.inc.navUserProfile')

<div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Mon profil <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Mes annonces RDV </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Commandes sur RDV</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Mes participations aux RDV</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="#">Mes révenus</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container bg-light rounded my-3">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="rdvList mt-3">
                <div class="table-responsive">
                    <p class="h5 font-weight-bold my-4">Mes révenus</p>
                    <table class="table">
                        <thead class="container-fluid">
                        <tr>
                            <th scope="col">
                                <div>
                                                <span>
                                                    Date
                                                </span>
                                    <div class="float-right">
                                        <div class="blk-up">
                                            <i class="fas fa-caret-up"></i>
                                        </div>
                                        <div class="blk-down">
                                            <i class="fas fa-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th scope="col">
                                <div>
                                                <span>
                                                    Type de l'action
                                                </span>
                                </div>
                            </th>
                            <th scope="col">
                                <div>
                                                <span>
                                                    Gain en TND
                                                </span>
                                </div>
                            </th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($pc == 100)
                        <tr>
                            <td >{{now()->format('m/d/Y')}}</td>
                            <td><a href="">Profil 100%</a></td>
                            <td data-tnd="5" class="valid font-weight-bold tnd">(+5) TND</td>
                        </tr>
                      @endif
                        @foreach($commands as $command)
                        <tr>
                            <td>{{$command->participation->created_at->format('m/d/Y')}}</td>
                            <td><a href="#">Validation de la participation du {{$command->participation->user->username}}</a></td>
                            <td data-tnd="{{$command->price}}" class=" tnd valid font-weight-bold">(+{{$command->price}}) </td>
                        </tr>
@endforeach
                        @foreach($sells as $sell)
                            <tr>
                                <td>{{$sell->participation->created_at->format('m/d/Y')}}</td>
                                <td><a href="#">participation au {{$sell->meeting->title}}</a></td>
                                <td data-tnd="{{0-$sell->price}}" class=" tnd valid font-weight-bold">(-{{$sell->price}}) </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row mt-0 ">
                    <div class="col-6 offset-6">
                        <table class="table  ">
                            <thead style="background-color: #fbde85a3">
                            <tr>
                                <th scope="col">Total:</th>
                                <th id="total" scope="col">{{$total}} TND</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 offset-9">
                        <button class="btn btn-warning" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-hand-holding-usd"></i>&nbspVerser mon solde</button>
                        <!--Modal si solde insuffisant-->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <p>
                                            Votre solde est insuffisant <br>
                                            Vous pouvez faire une demande de paiement a partir de <b>100 TND</b> le premier de chaque mois.
                                        </p>

                                        <button type="button" class="btn btn-warning float-right" data-dismiss="modal">Ok</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2"></div>


    </div>
</div>

@endsection
