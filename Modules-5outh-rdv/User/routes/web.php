<?php

Route::group(['module' => 'User', 'middleware' => ['web'], 'namespace' => 'App\Modules\User\Controllers'], function() {

  Route::get('admin/dashboard', 'UserController@showAdminDashboard')->name('showAdminDashboard');
  Route::get('/', 'UserController@showHome')->name('showHome');
  Route::get('/logout', 'UserController@handleLogout')->name('handleLogout');
  Route::get('/mail/{code}', 'UserController@handleUserMailValidation')->name('handleUserMailValidation');
  Route::post('/login', 'UserController@handleLogin')->name('handleLogin');
  Route::post('/register', 'UserController@handleRegister')->name('handleRegister');

  Route::get('/offset', 'UserController@showHome')->name('showHomeOffset');

Route::group(['prefix' => 'user', 'middleware' => ['user']],function(){

  Route::get('profile', 'UserController@showProfile')->name('showProfile');
  Route::get('profile/myAnnounces', 'UserController@showMyAnnounces')->name('showMyAnnounces');
  Route::get('profile/myCommands', 'UserController@showMyCommandes')->name('showMyCommandes');
  Route::get('profile/myParticipations', 'UserController@showMyParticipations')->name('showMyParticipations');
  Route::get('profile/myIncomes', 'UserController@showMyIncomes')->name('showMyIncomes');
  Route::get('announcement/showAdd', 'UserController@showAddAnnouncement')->name('showAddAnnouncement');
  Route::get('announce/showUpdate/{id}', 'UserController@showEditAnnounecement')->name('showEditAnnounecement');
  Route::get('announce/delete/{id}', 'UserController@handleDeleteAnnounecement')->name('handleDeleteAnnounecement');
  Route::get('announce/updateStatus/{id}', 'UserController@handleEditAnnounecementStatus')->name('handleEditAnnounecementStatus');
  Route::get('/downloadInv/{session}', 'UserController@pdfDownload')->name('pdfDownload');
  Route::post('profile/update', 'UserController@handleUpdateUserProfile')->name('handleUpdateUserProfile');
  Route::post('profile/update/infos', 'UserController@handleUpdateUserAbout')->name('handleUpdateUserAbout');
  Route::post('profile/update/links', 'UserController@updateUserLinks')->name('updateUserLinks');
  Route::post('profile/addEducation', 'UserController@hundelAddEducation')->name('hundelAddEducation');
  Route::post('profile/addExperience', 'UserController@hundelAddExperience')->name('hundelAddExperience');
  Route::post('profile/UpdateEducation', 'UserController@hundelUpdateEducation')->name('hundelUpdateEducation');
  Route::post('profile/UpdateExperience', 'UserController@hundelUpdateExperience')->name('hundelUpdateExperience');
  Route::post('profile/updatePicture', 'UserController@handleUpdateUserProfilePicture')->name('handleUpdateUserProfilePicture');
  Route::post('announce/add', 'UserController@handleAddAnnounecement')->name('handleAddAnnounecement');
  Route::post('announce/update/{id}', 'UserController@handleEditAnnounecement')->name('handleEditAnnounecement');
  Route::get('meetings/categories', 'UserController@getUserMeetingsCategories')->name('getUserMeetingsCategories');
  Route::get('meetings/subCategories/{categoryId}', 'UserController@getUserMeetingsSubCategories')->name('getUserMeetingsSubCategories');
  Route::get('meetings/{categoryId}/{subCatId}', 'UserController@getUserMeetings')->name('getUserMeetings');
  Route::get('participations/{meetingId}', 'UserController@getMeetingParticipiants')->name('getMeetingParticipiants');
  Route::get('participation/validate/{id}/{key}', 'UserController@validateParticipation')->name('validateParticipation');
  Route::get('participation/confirme/{id}', 'UserController@confirmeParticipation')->name('confirmeParticipation');
  Route::get('pdf', 'UserController@showPdf')->name('showPdf');

    Route::get('public_profile/{username}', 'UserController@showUserPublicProfile')->name('showUserPublicProfile');
    Route::get('remove-interest/{name}', 'UserController@removeUserInterest')->name('removeUserInterest');
    Route::get('add-interest/{name}', 'UserController@addUserInterest')->name('addUserInterest');

});
});
