<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;


class UserSocialLinks extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_social_links';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'platforme',
        'pseudo',
        'user_id'
    ];


}
