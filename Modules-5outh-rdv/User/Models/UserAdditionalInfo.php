<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;


class UserAdditionalInfo extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_additional_info';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'about_me',
        'state',
        'study_level',
        'user_id'
    ];


}
