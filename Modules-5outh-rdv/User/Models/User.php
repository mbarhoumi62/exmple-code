<?php

namespace App\Modules\User\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'picture',
        'status',
        'password',
        'validation',
        'phone',
        'gender',
        'address',
        'username',
'cin',
'is_pro',
'profile_status',
'balance',
'city',
'delegation',
'postal_code',
        'birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['birthday'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            'App\Modules\User\Models\Role',
            'user_roles',
            'user_id',
            'role_id'
        )->withTimestamps();
    }

    public function assignRole($role){
        if(is_string($role)){
            $role = Role::where('title',$role)->get();
        }
        if(!$role) return false;
        $this->roles()->attach($role);
    }

    public function experiences(){

      return  $this->hasMany('App\Modules\User\Models\UserExperience');
    }

    public function educations(){

      return  $this->hasMany('App\Modules\User\Models\UserEducation');
    }

    public function links(){

        return  $this->hasMany('App\Modules\User\Models\UserSocialLinks');
    }

    public function infos(){

        return  $this->hasOne('App\Modules\User\Models\UserAdditionalInfo');
    }

    public function interests(){

        return  $this->hasMany('App\Modules\User\Models\UserInterests');
    }

        public function participations()
    {
     return $this->hasMany('App\Modules\Meeting\Models\UserParticipation');

    }

    public function meetings()
    {
     return $this->hasMany('App\Modules\Meeting\Models\Meeting');

     }




}
