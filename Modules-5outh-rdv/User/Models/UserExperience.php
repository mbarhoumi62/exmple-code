<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserExperience extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_experience';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
'title',
'entreprise',
'start_year',
'end_year',
'place',
'descreption',
'user_id'
    ];


}
