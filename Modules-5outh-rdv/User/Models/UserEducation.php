<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;


class UserEducation extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_educations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
  'school',
'diploma',
'study_field',
'start_year',
'end_year',
'user_id'
    ];


}
