<?php

namespace App\Modules\User\Controllers;

use App\Modules\User\Models\UserInterests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserEducation;
use App\Modules\User\Models\UserSocialLinks;
use App\Modules\User\Models\UserAdditionalInfo;
use App\Modules\User\Models\UserExperience;
use App\Modules\Meeting\Models\UserParticipation;
use App\Modules\Place\Models\City;
use App\Modules\Meeting\Models\Meeting;
use App\Modules\Place\Models\Place;
use App\Modules\Meeting\Models\Session;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;
use Illuminate\Support\Facades\Mail;

use PDF;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAdminDashboard()
    {
        return view("User::backOffice.dashboard");
    }

    public function showHome(Request $request)
    {

        $cities = City::all();
        $count = Meeting::All()->count();
        $meetings = Meeting::where('status', 1)->orderBy('created_at', 'desc')
            ->with('category')
            ->with('subCategory')
            ->with('creator')
            ->with('sessions')
            ->take(3)->get();

        if ($request->ajax()) {
            $offset = $request->get('offset');

            if ($offset > (ceil($count) / 3)) {

                return response()->json(['status' => '201']);
            }

            $meetings = Meeting::where('status', 1)->orderBy('created_at', 'desc')->with('category')
                ->with('subCategory')
                ->with('creator')
                ->with('sessions')
                ->skip($offset * 3)->take(3)->get();

            $view = view('General::load.meetings', [
                'cities' => $cities,
                'meetings' => $meetings
            ])->render();

            return response()->json(['html' => $view]);
        }

        return view("General::index", [
            'cities' => $cities,
            'meetings' => $meetings,
            'count' => $count
        ]);
    }

    public function handleLogin()
    {

        $data = Input::all();

        $email = $data['email'];
        $password = $data['password'];

        if (isset($email))
            $user = User::where('email', $email)->get();

        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        if (Auth::attempt($credentials)) {
            if (Auth::user()->status == 1) {
                $user = Auth::user();
                Auth::login($user);

                if (checkAdministratorRole($user))
                    return redirect(route('showAdministration'));
                elseif (checkUserRole($user))
                    return redirect(route('showHome'));
            } else {
                SweetAlert::error('Oops', 'Votre compte est suspendu !')->persistent('Fermer');
                return back();
            }
        }

        SweetAlert::error('Oops', 'Vérifiez votre email et mot de passe !')->persistent('Fermer');
        return back();
    }


    public function handleLogout()
    {
        Auth::logout();
        return redirect(route('showHome'));
    }


    public function handleRegister(Request $request)
    {

        if (Auth::user()) {
            SweetAlert::error('Oops', 'Vous ete deja connecter !')->persistent('Fermer');
            return back();
        }


        $userByMail = User::where('email', $request->email)->first();

        if ($userByMail) {
            SweetAlert::error('Oops', 'Email deja existe !')->persistent('Fermer');
            return back();

        }

        $userByUsername = User::where('username', $request->username)->first();

        if ($userByUsername) {
            SweetAlert::error('Oops', 'Pseudo deja existe !')->persistent('Fermer');
            return back();

        }


        $validation = str_random(30);
        $picture = 'storage/avatar/avatar.png';

        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'validation' => $validation,
            'picture' => $picture,
            'username' => $request->username,
            'cin' => $request->cin,
            'profile_status' => 40
        ]);

        $user->roles()->attach(2);

        if ($user) {

            $content = ['user' => $user, 'validation' => $validation];
            Mail::send('User::mail.activation', $content, function ($message) use ($user) {
                $message->to($user->email);
                $message->subject('Activation de mail');
            });

            SweetAlert::success('Bien', 'Un email vous a été envoyé. Vous pouvez maintenant  activer votre compte')->persistent('Fermer');
        }

        return back();

    }

    public function handleUserMailValidation($code)
    {

        $user = User::where('validation', '=', $code)->first();
        if ($user) {

            Auth::login($user);
            $user->status = 1;
            $user->save();

            SweetAlert::success('Bien !', 'Complétez votre profil à 100% et gagnez 5 dinars dans votre e-wallet !')->persistent('Fermer');

            return redirect(route('showHome')); //Todo show profile
        } else {
            SweetAlert::warning('Oops', 'Aucun utilisateur trouvé')->persistent('Fermer');
            return redirect(route('showHome'));
        }
    }

    public function showProfile()
    {


        return view("User::frontOffice.userProfile", [
            'user' => Auth::user()->with('experiences')->with('educations')
                ->with('participations')->get(),
            'citys' => City::All(),
            'pc' => getProfileCompletetion()
        ]);
    }

    public function handleUpdateUserProfile(Request $request)
    {

        $user = Auth::user();

        $this->validate($request, [
            'email' => 'required|email|unique:users,email,' . $user->id,
            'username' => 'required',
            'cin' => 'required',
        ],
            [
                'email.email' => 'Veuillez saisir un email valide',
                'email.required' => 'Le champ email est obligatoire',
                'email.unique' => 'L\'email indiqué est déjà utilisé',
                'username.required' => 'Le champ Pseudo est obligatoire',
                'cin.required' => 'Le champ CIN est obligatoire',
            ]);

        $user->update([
            'username' => $request->username,
            'cin' => $request->cin,
            'email' => $request->email,
            'password' => ($request->password) ? bcrypt($request->password) : $user->password,
            'gender' => ($request->gender) ? $request->gender : $user->gender,
            'address' => ($request->address) ? $request->address : $user->address,
            'first_name' => ($request->first_name) ? $request->first_name : $user->first_name,
            'last_name' => ($request->last_name) ? $request->last_name : $user->last_name,
            'city' => ($request->city) ? $request->city : $user->city,
            'delegation' => ($request->delegation) ? $request->delegation : $user->delegation,
            'postal_code' => ($request->postal_code) ? $request->postal_code : $user->postal_code,
            'birthday' => ($request->birthday) ? $request->birthday : $user->birthday,
            'phone' => ($request->phone) ? $request->phone : $user->phone,
        ]);

        SweetAlert::success('Bien !', 'Profil Modifié avec succés !')->persistent('Fermer');
        return redirect()->route('showProfile');


    }

    public function handleUpdateUserAbout(Request $request)
    {

        $userAdditionalInfo = UserAdditionalInfo::where('user_id', Auth::id())->first();
        if ($userAdditionalInfo) {
            $userAdditionalInfo->update([
                'about_me' => ($request->about_me) ? $request->about_me : Auth::user()->infos->about_me,
                'state' => ($request->state) ? $request->state : Auth::user()->infos->state,
                'study_level' => ($request->study_level) ? $request->study_level : Auth::user()->infos->study_level
            ]);
            SweetAlert::success('Bien !', 'Profil Modifié avec succés !')->persistent('Fermer');
            return redirect()->route('showProfile');

        } else {
            UserAdditionalInfo::create([
                'about_me' => $request->about_me,
                'state' => $request->state,
                'study_level' => $request->study_level,
                'user_id' => Auth::id()
            ]);

            SweetAlert::success('Bien !', 'Profil Modifié avec succés !')->persistent('Fermer');
            return redirect()->route('showProfile');
        }
    }

    public function updateUserLinks(Request $request)
    {
        $flink = UserSocialLinks::where('user_id', Auth::id())->where('platforme', 'Facebook')->first();
        $slink = UserSocialLinks::where('user_id', Auth::id())->where('platforme', 'Skype')->first();
        $tlink = UserSocialLinks::where('user_id', Auth::id())->where('platforme', 'Twitter')->first();

        if ($flink) {
            $flink->update([
                'pseudo' => ($request->Fpseudo) ? $request->Fpseudo : $flink->pseudo
            ]);
        } else {
            UserSocialLinks::create([
                'platforme' => 'Facebook',
                'pseudo' => $request->Fpseudo,
                'user_id' => Auth::id(),
            ]);
        }
        if ($slink) {
            $slink->update([
                'pseudo' => ($request->Spseudo) ? $request->Spseudo : $slink->pseudo
            ]);
        } else {
            UserSocialLinks::create([
                'platforme' => 'Skype',
                'pseudo' => $request->Spseudo,
                'user_id' => Auth::id(),
            ]);
        }
        if ($tlink) {
            $tlink->update([
                'pseudo' => ($request->Tpseudo) ? $request->Tpseudo : $tlink->pseudo
            ]);
        } else {
            UserSocialLinks::create([
                'platforme' => 'Twitter',
                'pseudo' => $request->Tpseudo,
                'user_id' => Auth::id(),
            ]);
        }

        SweetAlert::success('Bien !', 'Profil Modifié avec succés !')->persistent('Fermer');
        return redirect()->route('showProfile');

    }

    public function hundelAddEducation(Request $request)
    {


        $user = Auth::user();

        $this->validate($request, [
            'school' => 'required',
            'diploma' => 'required',
            'study_field' => 'required',
            'start_year' => 'required|date',
            'end_year' => 'required|date|after:start_year'
        ],
            [
                'email.required' => 'Le champ ecole est obligatoire',
                'diploma.required' => 'Le champ diplome est obligatoire',
                'study_field.required' => 'Le champ domaine d\'étude est obligatoire',
                'start_year.required' => 'Le champ date de début d\'étude est obligatoire',
                'end_year.required' => 'Le champ date de fin d\'étude est obligatoire',
                'end_year.after' => 'Vérifiez la date de début et la date de fin'
            ]);

        UserEducation::Create([

            'school' => $request->school,
            'diploma' => $request->diploma,
            'study_field' => $request->study_field,
            'start_year' => $request->start_year,
            'end_year' => $request->end_year,
            'user_id' => $user->id
        ]);

        SweetAlert::success('Bien !', 'Education Ajouté avec succès !')->persistent('Fermer');
        return redirect()->route('showProfile');
    }

    public function hundelAddExperience(Request $request)
    {

        $user = Auth::user();

        $this->validate($request, [
            'title' => 'required',
            'entreprise' => 'required',
            'place' => 'required',
            'start_year' => 'required|date',
            'end_year' => 'required|date|after:start_year',
            'descreption' => 'required',
        ],
            [
                'title.required' => 'Le champ Intitulé du poste est obligatoire',
                'entreprise.required' => 'Le champ Entreprise est obligatoire',
                'place.required' => 'Le champ Lieu est obligatoire',
                'start_year.required' => 'Le champ date de début d\'étude est obligatoire',
                'end_year.required' => 'Le champ date de fin d\'étude est obligatoire',
                'end_year.after' => 'Vérifiez la date de début et la date de fin',
                'descreption.required' => 'Le champ description est obligatoire'
            ]);

        UserExperience::Create([

            'title' => $request->title,
            'entreprise' => $request->entreprise,
            'place' => $request->place,
            'descreption' => $request->descreption,
            'start_year' => $request->start_year,
            'end_year' => $request->end_year,
            'user_id' => $user->id
        ]);

        SweetAlert::success('Bien !', 'Experience Ajouté avec succès !')->persistent('Fermer');
        return redirect()->route('showProfile');
    }

    public function hundelUpdateEducation(Request $request)
    {

        $user = Auth::user();
        $id = $request->id;
        $education = UserEducation::find($id);

        if (empty($education)) {
            SweetAlert::error('Oops !', ' Veuillez choisir une valide Education !')->persistent('Fermer');
            return back();
        }

        $this->validate($request, [
            'school' => 'required',
            'diploma' => 'required',
            'study_field' => 'required',
            'start_year' => 'required|date',
            'end_year' => 'required|date|after:start_year'
        ],
            [
                'email.required' => 'Le champ ecole est obligatoire',
                'diploma.required' => 'Le champ diplome est obligatoire',
                'study_field.required' => 'Le champ domaine d\'étude est obligatoire',
                'start_year.required' => 'Le champ date de début d\'étude est obligatoire',
                'end_year.required' => 'Le champ date de fin d\'étude est obligatoire',
                'end_year.after' => 'Vérifiez la date de début et la date de fin'
            ]);

        $education->update([

            'school' => $request->school,
            'diploma' => $request->diploma,
            'study_field' => $request->study_field,
            'start_year' => $request->start_year,
            'end_year' => $request->end_year,
            'user_id' => $user->id
        ]);

        SweetAlert::success('Bien !', 'Education Modifié avec succès !')->persistent('Fermer');
        return redirect()->route('showProfile');
    }

    public function hundelUpdateExperience(Request $request)
    {
        $user = Auth::user();

        $id = $request->id;

        $experience = UserExperience::find($id);
        if (empty($experience)) {
            SweetAlert::error('Oops !', ' Veuillez choisir une valide Experience !')->persistent('Fermer');
            return back();
        }
        $this->validate($request, [
            'title' => 'required',
            'entreprise' => 'required',
            'place' => 'required',
            'descreption' => 'required',
            'start_year' => 'required|date',
            'end_year' => 'required|date|after:start_year'
        ],
            [
                'title.required' => 'Le champ Intitulé du poste est obligatoire',
                'entreprise.required' => 'Le champ Entreprise est obligatoire',
                'place.required' => 'Le champ Lieu est obligatoire',
                'descreption.required' => 'Le champ description est obligatoire',
                'start_year.required' => 'Le champ date de début d\'étude est obligatoire',
                'end_year.required' => 'Le champ date de fin d\'étude est obligatoire',
                'end_year.after' => 'Vérifiez la date de début et la date de fin'
            ]);

        $experience->update([

            'title' => $request->title,
            'entreprise' => $request->entreprise,
            'place' => $request->place,
            'descreption' => $request->descreption,
            'start_year' => $request->start_year,
            'end_year' => $request->end_year,
            'user_id' => $user->id
        ]);

        SweetAlert::success('Bien !', 'Experience Modifié avec succès !')->persistent('Fermer');
        return redirect()->route('showProfile');
    }

    public function showMyCommandes()
    {

        $user = Auth::user();

        $participations = [];
        $userMeetings = $user->meetings;
        $meetingSessionsIds = [];

             if($userMeetings){

                 foreach ($userMeetings as $userMeeting){


                     foreach ($userMeeting->sessions as $session) {
                         $meetingSessionsIds[] = $session->id;
                     }
                     $participations = UserParticipation::whereIn('session_id', $meetingSessionsIds)->with('user')->get();

                 }

             }


        return view("User::frontOffice.myCommands",[
            'participations' => $participations,
            'pc' => getProfileCompletetion()
        ]);
    }

    public function showMyParticipations()
    {

        return view("User::frontOffice.myParticipations",
            [
                'participations' => Auth::user()->participations,
                'pc' => getProfileCompletetion()
            ]
        );
    }

    public function showMyIncomes()
    {

        $total = 0;

        if(getProfileCompletetion() == 100){
            $total += 5;

        }

        $meetings = Auth::user()->meetings;
      foreach ($meetings as $meeting){
       $sessions = $meeting->sessions;

      }
      $commands = [];
      $sells = [];
      foreach ($sessions as $session){
          if($session->participation){
              $commands[] = $session;
              $total += $session->price;
          }
      }

        foreach (Auth::user()->participations as $participation){
            $sells[] = $participation->session;
            $total -= $participation->session->price;

        }

        return view("User::frontOffice.myIncome",[
            'pc' =>getProfileCompletetion(),
            'commands' => $commands,
            'sells' => $sells,
            'total' => $total
        ]);
    }

    public function handleUpdateUserProfilePicture(Request $request)
    {
        $user = Auth::user();

        $file = $request->picture;
        $imagePath = 'storage/uploads/avatar/';
        $filename = 'avatar' . $user->id . '.' . $file->getClientOriginalExtension();
        $file->move(public_path($imagePath), $filename);
        $user->update([
            'picture' => $imagePath . '' . $filename
        ]);

        SweetAlert::success('Bien !', 'Image De Profil Modifié avec succés !')->persistent('Fermer');
        return redirect()->route('showProfile');

    }


    public function showMyAnnounces()
    {

        return view("User::frontOffice.myAnnounces", [

            'meetings' => Auth::user()->meetings()->where('status', '!=', 2)->get(),
            'pc' => getProfileCompletetion()

        ]);
    }

    public function handleAddAnnounecement(Request $request)
    {


        //image upload
        $place = null;
        $file = $request->picture;
        $imagePath = 'storage/uploads/mettings/';
        $filename = 'meetingImage' . Auth::user()->id . '.' . time() . $file->getClientOriginalExtension();
        $file->move(public_path($imagePath), $filename);

        //meeting creation
        $meeting = Meeting::create([
            'title' => $request->title,
            'descreption' => $request->descreption,
            'level' => $request->level,
            'picture' => $imagePath . '' . $filename,
            'status' => 1,
            'user_id' => Auth::user()->id,
            'category_id' => $request->categoryId,
            'sub_category_id' => $request->subCategoryId
        ]);
        //creation des sessions

        foreach ($request->sessionPrice as $key => $sessionPrice) {

            $sessionDay = $request->sessionDay[$key];
            $sessionStartTime = $request->sessionStartTime[$key];
            $sessionEndTime = $request->sessionEndTime[$key];

            $sessionStartDate[] = date('Y-m-d H:i:s',
                strtotime("$sessionDay $sessionStartTime"));


            $sessionEndDate[] = date('Y-m-d H:i:s',
                strtotime("$sessionDay $sessionEndTime"));

            if ($request->input('address' . $key) != null) {
                $place = Place::create([
                    'address' => $request->input('address' . $key),
                    'city_id' => $request->input('city' . $key),
                    'delegation_id' => $request->input('delegation' . $key),
                ]);
            }
            $session = Session::create([
                'start_date' => $sessionStartDate[$key],
                'end_date' => $sessionEndDate[$key],
                'price' => ($request->sessionPrice[$key]) ? $request->sessionPrice[$key] : 0,
                'participiants_nbr' => $request->sessionParticipiantsNbr[$key],
                'place_id' => ($place) ? $place->id : null,
                'meeting_id' => $meeting->id,
                'meeting_type' => ($request->input('platforme' . $key)) ? $request->input('platforme' . $key) : null
            ]);

        }

        if ($meeting) {

            SweetAlert::success('Bien !', 'Rendez-vous publié avec succés !')->persistent('Fermer');
            return redirect()->route('showMyAnnounces');
        } else {
            SweetAlert::danger('Ouups !', 'Verifier les coordonneé svp !')->persistent('Fermer');
            return back();
        }
    }

    public function handleDeleteAnnounecement($id)
    {

        $meeting = Meeting::find($id);
        if ($meeting) {
            $meeting->update([
                'status' => 2
            ]);

            SweetAlert::success('Bien !', 'Annonce supprimé avec succés !')->persistent('Fermer');

            return back();
        }

        SweetAlert::warning('Ouups !', 'Announce non trouvé')->persistent('Fermer');
        return back();
    }

    public function handleEditAnnounecement(Request $request, $id)
    {

        $meeting = Meeting::find($id);
        if(!$meeting){
            SweetAlert::error('Ouups !', 'Meeting not found !')->persistent('Fermer');
            return back();
        }


        //image upload
        $place = null;
        $file = $request->picture;
        $imagePath = 'storage/uploads/mettings/';
        $filename = 'meetingImageUpdated' . Auth::user()->id . '.' . time() . $file->getClientOriginalExtension();
        $file->move(public_path($imagePath), $filename);

        //meeting creation
        $meeting->update([
            'title' => $request->title,
            'descreption' => $request->descreption,
            'level' => $request->level,
            'picture' => $imagePath . '' . $filename,
            'status' => 1,
            'user_id' => Auth::user()->id,
            'category_id' => $request->categoryId,
            'sub_category_id' => $request->subCategoryId
        ]);
        //creation des sessions

        foreach ($meeting->sessions as $key => $session) {

            $sessionDay = $request->sessionDay[$key];
            $sessionStartTime = $request->sessionStartTime[$key];
            $sessionEndTime = $request->sessionEndTime[$key];

            $sessionStartDate[] = date('Y-m-d H:i:s',
                strtotime("$sessionDay $sessionStartTime"));


            $sessionEndDate[] = date('Y-m-d H:i:s',
                strtotime("$sessionDay $sessionEndTime"));

            if ($request->input('address' . $key) != null) {
                $place = Place::create([
                    'address' => $request->input('address' . $key),
                    'city_id' => $request->input('city' . $key),
                    'delegation_id' => $request->input('delegation' . $key),
                ]);
            }
            $session->update([
                'start_date' => $sessionStartDate[$key],
                'end_date' => $sessionEndDate[$key],
                'price' => ($request->sessionPrice[$key]) ? $request->sessionPrice[$key] : 0,
                'participiants_nbr' => $request->sessionParticipiantsNbr[$key],
                'place_id' => ($place) ? $place->id : null,
                'meeting_id' => $meeting->id,
                'meeting_type' => ($request->input('platforme' . $key)) ? $request->input('platforme' . $key) : null
            ]);

        }


            SweetAlert::success('Bien !', 'Rendez-vous modifier avec succés !')->persistent('Fermer');
            return redirect()->route('showMyAnnounces');


    }

    public function handleEditAnnounecementStatus($id)
    {

        $meeting = Meeting::find($id);
        if ($meeting->status == 0) {
            $meeting->update([
                'status' => 1
            ]);
            return response()->json(['status' => 200]);

        } elseif ($meeting->status == 1) {
            $meeting->update([
                'status' => 0
            ]);
            return response()->json(['status' => 201]);
        }


    }

    public function pdfDownload($id)
    {

        $user = Auth::user();
        $name = Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $session = Session::find($id);


        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('User::frontOffice.inc.pdf',
            [
                'name' => $name,
                'rdv' => $session->meeting->title,
                'owner' => $session->meeting->creator->first_name,
                'pass' => UserParticipation::where('session_id', $id)->where('user_id', $user->id)->first()->ticket_key
            ]
        );
        return $pdf->download('pdfview.pdf');


    }

    public function showPdf()
    {
        return view('User::frontOffice.inc.pdf');
    }

    public function showAddAnnouncement()
    {
        return view("Meeting::organizeRdv");
    }

    public function showEditAnnounecement($id)
    {
        $meeting = Meeting::find($id);
        if(!$meeting){
            SweetAlert::error('Ouups !', 'Meeting not found !')->persistent('Fermer');
            return back();
        }

        return view("Meeting::editOrganizeRdv", [
            'meeting' => $meeting
        ]);
    }

    public function getUserMeetingsCategories()
    {
        $user = Auth::user();
        $userMeetings = $user->meetings;
        $categories = [];

        foreach ($userMeetings as $userMeeting) {

            if (in_array($userMeeting->category, $categories) == false) {
                $categories[] = $userMeeting->category;
            }
        }
        return response()->json(['categories' => $categories]);
    }

    public function getUserMeetingsSubCategories($catId)
    {
        $user = Auth::user();
        $userMeetings = $user->meetings()->where('category_id', $catId)->get();
        $subCategories = [];

        foreach ($userMeetings as $userMeeting) {

            if (in_array($userMeeting->subCategory, $subCategories) == false) {
                $subCategories[] = $userMeeting->subCategory;
            }
        }
        return response()->json(['subCategories' => $subCategories]);
    }

    public function getUserMeetings($catId, $subCatId)
    {

        $userMeetings = Auth::user()->meetings()->where('category_id', $catId)->where('sub_category_id', $subCatId)->get();


        return response()->json(['meetings' => $userMeetings]);
    }

    public function getMeetingParticipiants($meetingId)
    {

        $meeting = Meeting::find($meetingId);

        if (!$meeting) {
            return response()->json(['status' => 404]);

        }

        $meetingSessionsIds = [];

        foreach ($meeting->sessions as $session) {
            $meetingSessionsIds[] = $session->id;
        }
        $participations = UserParticipation::whereIn('session_id', $meetingSessionsIds)->with('user')->get();

        return response()->json(['participations' => $participations]);

    }

    public function validateParticipation($id, $key)
    {

        $participation = UserParticipation::find($id);
        if ($participation->ticket_key == $key) {
            $participation->update([
                'presence' => 3
            ]);
            return response()->json(['status' => 200]);
        } else
            return response()->json(['status' => 404]);

    }

    public function confirmeParticipation($id)
    {
        $participation = UserParticipation::find($id);

        $participation->update([
            'presence' => 1
        ]);
        return response()->json(['status' => 200]);

    }

    public function showUserPublicProfile($userName)
    {
        $user = User::where('username', $userName)->first();

        return view("User::frontOffice.userPublicProfile", [
            'user' => $user
        ]);
    }

    public function addUserInterest($name)
    {

        UserInterests::create([
            'name' => $name,
            'user_id' => Auth::user()->id
        ]);

        return response()->json(['status' => 200]);


    }

    public function removeUserInterest($name)
    {

        $interest = UserInterests::where('name', $name)->first();
        $interest->delete();

        return response()->json(['status' => 200]);


    }



}
