<?php

function checkAdministratorRole($user)
    {
        foreach ($user->roles as $role) {
            if (($role->id == 1)) {
                return true;
            }
        }
        return false;
    }

if (!function_exists('checkUserRole')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function checkUserRole($user)
    {
        foreach ($user->roles as $role) {
            if ($role->id == 2) {
                return true;
            }
        }
        return false;
    }


    function getProfileCompletetion(){
        $user = \Illuminate\Support\Facades\Auth::user();

        $pC = 0;

        if($user->first_name  != null){
            $pC += 5;
        }

        if($user->last_name  != null){
            $pC += 5;
        }

        if($user->gender  != null){
            $pC += 5;
        }

        if($user->birthday  != null){
            $pC += 5;
        }

        if($user->phone  != null){
            $pC += 5;
        }
        if($user->address  != null){
            $pC += 5;
        }

        if($user->city  != null){
            $pC += 5;
        }

        if($user->delegation  != null){
            $pC += 5;
        }

        if($user->postal_code  != null){
            $pC += 5;
        }

        if($user->links->first()  != null){
            $pC += 10;
        }

        if($user->infos  != null){
            $pC += 20;
        }

        if($user->educations->first()  != null){
            $pC += 10;
        }

        if($user->experiences->first()  != null){
            $pC += 10;
        }

        if($user->interests->first()  != null){
            $pC += 5;
        }


        return $pC;

    }
}