<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeBookmarked;

class Job extends Model {
    use CanBeBookmarked;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'category_id',
        'type',
        'description',
        'max_salary',
        'min_salary',
        'user_id',
        'address_id',
        'company_id',
        'category_id'
    ];


    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Modules\User\Models\Company');
    }

    public function category()
    {
        return $this->hasOne('App\Modules\General\Models\JobCategory');
    }

    public function medias() {
        return $this->hasMany('App\Modules\User\Models\Media');
    }

    public function tags() {
        return $this->hasMany('App\Modules\Hired\Models\JobTag','job_id','id');
    }

    public function address() {
        return $this->hasOne('App\Modules\User\Models\Address','id','address_id');
    }

    public function applications() {
        return $this->hasMany('App\Modules\Hired\Models\Application','job_id','id');
    }

       public function interviews() {
        return $this->hasMany('App\Modules\Hired\Models\Interview','job_id','id');
    }

    public function getJobParamTitle() {
        $title = str_replace(' ', '-', $this->title);
        $title = str_replace('?', '', $title);
        $title = str_replace('/', '&', $title);

        $id = $this->id * 33 + 9371;
        return $title.'-'.$id;
    }

}
