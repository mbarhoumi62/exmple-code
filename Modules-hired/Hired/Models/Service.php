<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeBookmarked;
class Service extends Model {

    use CanBeBookmarked;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'category',
        'description',
        'requirements',
        'delay',
        'status',
        'min',
        'max',
        'user_id',
        'category_id'
    ];


    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }


    public function medias() {
        return $this->hasMany('App\Modules\User\Models\Media');
    }

    public function skills() {
        return $this->hasMany('App\Modules\User\Models\Skill');
    }


    public function category()
    {
        return $this->hasOne('App\Modules\General\Models\TaskCategory','id','category_id');
    }

    public function bookings() {
        return $this->hasMany('App\Modules\Hired\Models\Booking','service_id','id');
    }

    public function getServiceParamName() {
        $name = str_replace(' ', '-', $this->name);
        $name = str_replace('?', '', $name);
        $name = str_replace('/', '&', $name);
        $id = $this->id * 33 + 9371;
        return $name.'-'.$id;
    }





}
