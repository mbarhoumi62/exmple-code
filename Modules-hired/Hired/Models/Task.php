<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeBookmarked;
class Task extends Model {

    use CanBeBookmarked;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tasks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'category',
        'description',
        'status',
        'user_id',
        'budget_id',
        'address_id',
        'company_id',
        'category_id'
    ];


    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Modules\User\Models\Company');
    }

    public function medias() {
        return $this->hasMany('App\Modules\User\Models\Media');
    }

    public function skills() {
        return $this->hasMany('App\Modules\User\Models\Skill');
    }

    public function budget() {
        return $this->hasOne('App\Modules\Hired\Models\Budget','id','budget_id');
    }

    public function address() {
        return $this->hasOne('App\Modules\User\Models\Address','id','address_id');
    }

    public function bids() {
        return $this->hasMany('App\Modules\Hired\Models\Bid');
    }

    public function accepted() {
        return $this->hasOne('App\Modules\Hired\Models\AcceptedBid');

    }

    public function category()
    {
        return $this->hasOne('App\Modules\General\Models\TaskCategory','id','category_id');
    }

    public function getTaskParamName() {
        $name = str_replace(' ', '-', $this->name);
        $name = str_replace('?', '', $name);
        $name = str_replace('/', '&', $name);
        $id = $this->id * 33 + 9371;
        return $name.'-'.$id;
    }




}
