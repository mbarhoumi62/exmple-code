<?php

namespace App\Modules\Hired\Controllers;

use App\Events\HiredEvents;
use App\Events\ScheduleInterview;
use App\Modules\Hired\Models\Interview;
use App\Modules\User\Models\Company;
use App\Modules\User\Models\Portfolio;
use App\Modules\User\Models\User;
use App\Modules\Hired\Models\AcceptedBid;
use App\Modules\Hired\Models\Booking;
use App\Modules\Hired\Models\Budget;
use App\Modules\Hired\Models\Service;
use App\Modules\Hired\Models\Task;
use App\Modules\Hired\Models\Bid;
use App\Modules\Hired\Models\Job;
use App\Modules\Hired\Models\JobTag;
use App\Modules\Hired\Models\Application;
use App\Modules\User\Models\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class HiredController extends Controller
{

    /**
     *@desc  Handle add new post
     *
     * @route /tasks/add
     */
    public function handleAddTask()
    {
        $data = Input::all();

        $rules = [
            'name' => 'required|min:10|max:50',
            'category' => 'required',
            'min' => 'required|numeric|lt:'.$data['max'],
            'max'  => 'required|numeric|gt:'.$data['min'],
            'skills' => 'required',
            'description' => 'required',
            'attachments' => 'max:3',
            'attachments.*' => 'mimes:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf|max:10240'
        ];

        $messages = [
            'name.required'          => 'Project name is required !',
            'name.min'             => 'Project name must contains at least 10 characters !',
            'min.lt'             => 'Min must be less than max!',
            'max.gt'             => 'Max must be greater than min!',
            'name.max'       => 'Project name is too long !',
            'skills.required'   => 'Please specify the needed skills for this task !',
            'description.required'   => 'Describing your project will help you find the perfect expert you need !',
            'attachments.max'   => 'You can only update 3 files !',
            'attachments.*.mimes'   => 'Files accepted are doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf !',
            'attachments.*.max'   => 'File is too large !',
        ];

          if(isset($data['address'])) {
            $rules['address'] = 'required';
            $rules['lon'] = 'required';
            $messages['address.required'] = 'Please pick your address from the suggestions or from the map below!';
            $messages['lon.required'] = 'Please pick your address from the suggestions or from the map below!';
           }

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }


        if($data['address']) {
            $address = Address::create([
                'address'      => $data['address'],
                'city'         => $data['city'],
                'province'     => $data['province'] ? $data['province']: $data['city'],
                'lat'          => $data['lat'],
                'lon'          => $data['lon'],
            ]);
        }

        $budget = Budget::create([
            'min' => $data['min'],
            'max' => $data['max'],
            'type' => $data['budgetType']
        ]);

        $task = Task::create([
            'name' => $data['name'],
            'category_id' => $data['category'],
            'description' => $data['description'],
            'user_id' => Auth::id(),
            'budget_id' => $budget->id,
            'address_id' => $address->id ?? null,
            'company_id' => $data['company'] ?? null
        ]);

        $skills = explode(',', $data['skills']);
        foreach ($skills as $skill) {
            $task->skills()->create([
                'label' => $skill
            ]);
        }

            if(isset($data['attachments'])) {
                foreach ($data['attachments'] as $file) {
                    $link = uploadFile($file, 'taskFiles');
                        $task->medias()->create([
                            'link' => $link,
                            'label' => $file->getClientOriginalName(),
                            'task_id' => $task->id,
                            'type' => 3
                        ]);
                    }
            }


            Session::flash('message', 'Task created Successfully!');
            Session::flash('color', '#5f9025');
            return redirect()->route('showManageTask');
    }

    /**
     *@desc  show update new post
     *
     * @route /tasks/update
     */

    public function showUpdateTask($id){
        try {
            $taskId = decrypt($id);
        } catch (DecryptException $e) {
            return back();
        }
            $task = Task::find($taskId);

            if(!$task) {
                //not found
            }

            return view('User::frontOffice.dashboard.updateTask', [
                'task' => $task
            ]);
    }

    /**
     *@desc  handle update new post
     *
     * @route /tasks/update
     */

    public function handleUpdateTask($id) {
        $data = Input::all();
        $taskId = explode(";", explode(":", Crypt::decryptString($id))[1])[0];

        $task = Task::find($taskId);

        if(!$task) {
            
        }

        $rules = [
            'name' => 'required|min:10|max:50',
            'category' => 'required',
            'min' => 'required|numeric',
            'max'  => 'required|numeric',
            'skills' => 'required',
            'description' => 'required',
            'attachments' => 'max:3',
            'attachments.*' => 'mimes:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf|max:10240'
        ];

        $messages = [
            'name.required'          => 'Project name is required !',
            'name.min'             => 'Project name must contains at least 10 characters !',
            'name.max'       => 'Project name is too long !',
            'skills.required'   => 'Please specify the needed skills for this task !',
            'description.required'   => 'Describing your project will help you find the perfect expert you need !',
            'attachments.max'   => 'You can only update 3 files !',
            'attachments.*.mimes'   => 'Files accepted are doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf !',
            'attachments.*.max'   => 'File is too large !',
        ];
     if(isset($data['address'])) {
            $rules['address'] = 'required';
            $rules['lon'] = 'required';
            $messages['address.required'] = 'Please pick your address from the suggestions or from the map below!';
            $messages['lon.required'] = 'Please pick your address from the suggestions or from the map below!';
           }

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }


         $newAddress = null;

      if($data['address']) {
          if($task->address) {
             $task->address()->update([
              'address'      => $data['address'],
              'city'         => $data['city'],
              'province'     => $data['province'] ? $data['province']: $data['city'],
              'lat'          => $data['lat'],
              'lon'          => $data['lon'],
          ]);
           }else {
               $newAddress = Address::create([
                'address'      => $data['address'],
                'city'         => $data['city'],
                'province'     => $data['province'] ? $data['province']: $data['city'],
                'lat'          => $data['lat'],
                'lon'          => $data['lon'],
            ]);
           }
      }else {
         if($task->address) {
           $task->update([
          'address_id' => null
        
          ]);
              $task->address()->delete();
         }
      }

          if($newAddress) {
         $task->update([
          'address_id' => $newAddress->id
        
      ]);
      }

        $task->budget()->update([
            'min' => $data['min'],
            'max' => $data['max'],
            'type' => $data['budgetType']
        ]);

        $task->update([
            'name' => $data['name'],
            'category_id' => $data['category'],
            'description' => $data['description']
        ]);



        DB::table('skills')->where('task_id', '=', $task->id)->delete();

        $skills = explode(',', $data['skills']);
        foreach ($skills as $skill) {
             $task->skills()->create([
                    'label' => $skill
                ]);
        }

        if(isset($data['attachments'])) {
            foreach ($data['attachments'] as $file) {
                $link = uploadFile($file, 'taskFiles');
                $task->medias()->where('type',3)->first()->update([
                    'link' => $link,
                    'label' => $file->getClientOriginalName()
                ]);
            }
        }


        Session::flash('message', 'Task updated Successfully!');
        Session::flash('color', '#5f9025');
        return redirect()->route('showManageTask');
    }

    public function HandleDeleteTask()
    {
        $data   =  Input::all();

        $taskId =  $data['taskId'];

        $task = Task::find($taskId);

        if(!$task) {
          Session::flash('message', 'Some things went wrong, please try again!');
          Session::flash('color', '#de5959');
          return back();
        }

          $task->delete();

          Session::flash('message', 'Task deleted Successfully!');
          Session::flash('color', '#5f9025');
          return back();
    }

    public function HandleDeleteBid()
    {
        $data   =  Input::all();

        $bidId =  $data['bidId'];

        $bid = Bid::find($bidId);

        if(!$bid) {
          Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
           return back();
        }

          $bid->delete();
          Session::flash('message', 'Bid deleted Successfully!');
          Session::flash('color', '#5f9025');

          return back();
    }

    public function handleUpdateBid()
    {
        $data   =  Input::all();

        $bidId =  $data['id'];

        $bid = Bid::find($bidId); 

        if(!$bid) {
          Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
           return back();
        }

        $bid->update([
          'rate'   => $data['rate'],
          'delivery_time'  => $data['delivery_time'] ,
          'type'   => $data['type']
        ]);
    event(new HiredEvents(Auth::user(),'bidUpdate',$bid->task->name,$bid->task->user->id,$bid->task->id));
    saveNotification(Auth::id(), $bid->task->user->id, 0, $bid->task->name, 'Updated his bid on', route('showManageBidders', Crypt::encrypt($bid->task->id)));
          Session::flash('message', 'Bid Updated Successfully!');
          Session::flash('color', '#5f9025');

        return back();
    }


    public function handleAcceptBidderOffer() {
            $data = Input::all();

            AcceptedBid::create([
            'bid_id' => $data['bid_id'],
            'task_id' => $data['task_id']
            ]);

            $bid = Bid::find($data['bid_id']);
            $task = Task::find($data['task_id']);
            $task->status = 2;
            $task->save();


        event(new HiredEvents(Auth::user(),'acceptOffer',$task->name,$bid->bidder->id,''));
        saveNotification(Auth::id(), $bid->bidder->id, 0, $task->name, 'Accepted your bid on', route('showInProgressTasks'));

        sendMail('Hired::mails.acceptBid',
            $bid->bidder->email,
            ['user' => $bid->bidder , 'task' => $task] ,
            'New Bid Acceptance');
        Session::flash('message', 'Bid accepted!');
        Session::flash('color', '#5f9025');
        return redirect()->route('showManageInProgressTasks');

    }


    public function handlePostJob()
    {
        $data = Input::all();


        $rules = [
            'title' => 'required|max:100',
            'category' => 'required',
            'type' => 'required',
            'company' => 'required',
            'min_salary' => 'nullable|numeric|lt:'.$data['max_salary'],
            'max_salary'  => 'nullable|numeric|gt:'.$data['min_salary'],
            'description' => 'required',
            'attachments' => 'max:3',
            'attachments.*' => 'mimes:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf,png,jpg,jpeg,gif|max:10240'
        ];

        $messages = [
            'title.required'          => 'Job Title is required!',
            'title.max'       => 'Job Title is too long!',
            'type.required'       => 'Job type is required!',
            'category.required'       => 'Job category is required!',
            'company.required'       => 'You must specify the company!',
            'description.required'   => 'Describing your job will help you find the perfect expert you need!',
            'attachments.max'   => 'You can only update 3 files !',
            'attachments.*.mimes'   => 'Files accepted are doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf!',
            'attachments.*.max'   => 'File is too large!',
            'min_salary.lt'             => 'Min must be less than max!',
            'max_salary.gt'             => 'Max must be greater than min!'
        ];

        if(isset($data['address'])) {
            $rules['address'] = 'required';
            $rules['lon'] = 'required';
            $messages['address.required'] = 'Please pick your address from the suggestions or from the map below!';
            $messages['lon.required'] = 'Please pick your address from the suggestions or from the map below!';
           }


        $validation = Validator::make($data, $rules, $messages);
      
        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        if(isset($data['address'])){
            $address = Address::create([
                'address'      => $data['address'],
                'city'         => $data['city'],
                'province'     => $data['province'] ? $data['province']: $data['city'],
                'lat'          => $data['lat'],
                'lon'          => $data['lon']
            ]);
        }

        $job = Job::create([
            'title' => $data['title'],
            'category_id' => $data['category'],
            'type' => $data['type'],
            'description' => $data['description'],
            'min_salary' => $data['min_salary'] ?? 0,
            'max_salary' => $data['max_salary'] ?? 0,
            'user_id' => Auth::id(),
            'address_id' => $address->id ?? null,
            'company_id' => $data['company']
        ]);

        $tags = explode(',', $data['tags']);
        foreach ($tags as $tag) {
            if($tag !== '') {
                $job->tags()->create([
                    'label' => $tag
                ]);
            }
        }

        if(isset($data['attachments'])) {
            foreach ($data['attachments'] as $file) {
                $link = uploadFile($file, 'jobFiles');
                $job->medias()->create([
                    'link' => $link,
                    'label' => $file->getClientOriginalName(),
                    'type' => 4
                ]);
            }
        }

          Session::flash('message', 'Job Published Successfully!');
          Session::flash('color', '#5f9025');

        return redirect()->route('showManageJobs');
    }

    public function handleDeleteJob()
    {
        $data   =  Input::all();
        $jobId =  $data['jobId'];

        $job = Job::find($jobId);

        if(!$job) {
          Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
            return back();
        }
        if($job->address) {
          $job->address->delete();
        }
        $job->delete();
        Session::flash('message', 'Job deleted Successfully!');
          Session::flash('color', '#5f9025');

        return back();
    }

    public function showUpdateJob($id){
        try {
            $jobId = decrypt($id);
        } catch (DecryptException $e) {
            return back();
        }
        $job = Job::find($jobId);

        if(!$job) {
             Session::flash('message', 'Something went wrong, please try again!');
             Session::flash('color', '#de5959');
             return back();
        }

        return view('User::frontOffice.dashboard.updateJob', [
            'job' => $job
        ]);
    }

    public function handleUpdateJob($id) {
      $data = Input::all();


      $job = Job::find(Crypt::decrypt($id));

      if(!$job) {
          Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
                return back();
      }
      $rules = [
          'title' => 'required|max:100',
          'category' => 'required',
          'type' => 'required',
          'company' => 'required',
          'min_salary' => 'nullable|numeric',
          'max_salary'  => 'nullable|numeric',
          'description' => 'required',
          'attachments' => 'max:3',
          'attachments.*' => 'mimes:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf,png,jpg,jpeg,gif|max:10240'
      ];

      $messages = [
          'title.required'          => 'Job Title is required!',
          'title.max'       => 'Job Title is too long!',
          'type.required'       => 'Job type is required!',
          'category.required'       => 'Job category is required!',
          'company.required'       => 'You must specify the company!',
          'description.required'   => 'Describing your job will help you find the perfect expert you need!',
          'attachments.max'   => 'You can only update 3 files!',
          'attachments.*.mimes'   => 'Files accepted are doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf!',
          'attachments.*.max'   => 'File is too large!',
      ];
       if(isset($data['address'])) {
            $rules['address'] = 'required';
            $rules['lon'] = 'required';
            $messages['address.required'] = 'Please pick your address from the suggestions or from the map below!';
            $messages['lon.required'] = 'Please pick your address from the suggestions or from the map below!';
           }


      $validation = Validator::make($data, $rules, $messages);

      if ($validation->fails()) {
          return redirect()->back()->withErrors($validation->errors())->withInput();
      }

      $newAddress = null;

      if($data['address']) {
          if($job->address) {
             $job->address()->update([
              'address'      => $data['address'],
              'city'         => $data['city'],
              'province'     => $data['province'] ? $data['province']: $data['city'],
              'lat'          => $data['lat'],
              'lon'          => $data['lon'],
          ]);
           }else {
               $newAddress = Address::create([
                'address'      => $data['address'],
                'city'         => $data['city'],
                'province'     => $data['province'] ? $data['province']: $data['city'],
                'lat'          => $data['lat'],
                'lon'          => $data['lon'],
            ]);
           }
      }else {
         if($job->address) {
           $job->update([
          'address_id' => null
        
          ]);
              $job->address()->delete();
         }
      }

      $job->update([
          'title' => $data['title'],
          'category_id' => $data['category'],
          'type' => $data['type'],
          'description' => $data['description'],
          'min_salary' => $data['min_salary'] ?? 0,
          'max_salary' => $data['max_salary'] ?? 0,
          'company_id' => $data['company']
      ]);

      if($newAddress) {
         $job->update([
          'address_id' => $newAddress->id
        
      ]);
      }

      DB::table('job_tags')->where('job_id', '=', $job->id)->delete();

      $tags = explode(',', $data['tags']);

      foreach ($tags as $tag) {
            if($tag !== '') {
                $job->tags()->create([
                    'label' => $tag
                ]);
            }
      }

      if(isset($data['attachments'])) {
        foreach ($job->medias()->where('type',4)->get() as $media) {
            unlink(public_path($media->link));
            $media->delete();
        }
        foreach ($data['attachments'] as $file) {
            $link = uploadFile($file, 'jobFiles');
            $job->medias()->create([
                'link' => $link,
                'label' => $file->getClientOriginalName(),
                'type' => 4
            ]);
        }
      }

          Session::flash('message', 'Job updated successfully!');
          Session::flash('color', '#5f9025');
                return redirect()->route('showManageJobs');

    }

    public function handleApplyForJob(Request $request) {

        $job = Job::find( $request->jobId);

        if(!$job) {
          return response()->json(['status' => 404]);
        }

        $media_id = null;
        if(isset($request->cv)) {
          $file = $request->cv;
          $link = uploadFile($file, 'applicationCV');
          $cv = Auth::user()->medias()->create([
              'link' => $link,
              'label' => $file->getClientOriginalName(),
              'type' => 6
          ]);

          $media_id = $cv->id;
        }
        if(isset($request->oldCv)) {
            $media_id = $request->oldCv;
        }
        $application = Application::create([
          'note' => $request->note ?? null,
          'media_id' => $media_id,
          'user_id' => Auth::id(),
          'job_id' => $job->id
        ]);

        event(new HiredEvents(Auth::user(),'application',$application->job->title,$application->job->user->id,$application->job->id));
        saveNotification(Auth::id(), $application->job->user->id, 0, $application->job->title, 'Applied for a job', route('showManageCandidates', Crypt::encrypt($application->job->id)));
        $user = User::where('id',$application->job->user->id)->first();
        sendMail('Hired::mails.apply',
            $user->email,
            ['user' => $user , 'title' => $application->job->title , 'id' => $application->job->id, 'params' => $application->job->getJobParamTitle()] ,
            'New Job Application');
        return response()->json(['status' => 200]);
    }

    public function handleDeleteApplication() {
      $data   =  Input::all();
      $applicationId =  $data['applicationId'];

      $application = Application::find($applicationId);

      if(!$applicationId) {
          Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
                    return back();
      }
      $application->delete();
          Session::flash('message', 'Application delete successfully!');
          Session::flash('color', '#de5959');
                return back();
    }

    public function showJobDetails($title) {
        $param = explode("-",$title);
        $id = ((int)end($param) - 9371) / 33 ;
        $job = Job::find($id);
        if(!$job) {
           Session::flash('message', 'Something went wrong, please try again!');
           Session::flash('color', '#de5959');
            return redirect()->route('showJobs');
        }

        return view('Hired::jobDetails',[
                'job' => $job
            ]);
    }


    public function showTaskDetails($name) {
        $param = explode("-",$name);
        $id = ((int)end($param) - 9371) / 33 ;
        $task = Task::find($id);
        if(!$task) {
           Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
            return redirect()->route('showTasks');
        }

        return view('Hired::taskDetails',[
            'task' => $task
        ]);
    }


    public function apiHandleBidOnTask(Request $request) {

        $task = Task::find($request->data['taskId']);

        if(!$task || $task->status == 2) {
            return response()->json(['status' => 404]);
        }

       $bid =  Auth::user()->bids()->create([
            'rate' => $request->data['rate'],
            'delivery_time' => $request->data['deliveryTime'],
            'type' => $request->data['type'],
            'task_id' => $request->data['taskId']
        ]);

       $time = $bid->type == 0 ? $bid->delivery_time.' days' : $bid->delivery_time.' hours';
       $src = Auth::user()->medias()->where('type',0)->first() ? asset(Auth::user()->medias()->where('type',0)->first()->link) : '';


        event(new HiredEvents(Auth::user(),'bid',$bid->task->name,$bid->task->user->id,$bid->task->id));
        saveNotification(Auth::id(), $bid->task->user->id, 0, $bid->task->name, 'Placed a bid on', route('showManageBidders', Crypt::encrypt($bid->task->id)));

        $user = User::where('id',$bid->task->user->id)->first();
        sendMail('Hired::mails.bid',
            $user->email,
            ['user' => $user , 'name' => $bid->task->name , 'id' => $bid->task->id, 'params' => $bid->task->getTaskParamName()] ,
            'New Bid Submission');
        if(count($bid->bidder->getAllRatings($bid->bidder->id,'desc','App\Modules\User\Models\User')) < 3) {

            $userRating = '';
            $userRatingClass = 'company-not-rated';
            $userRatingText = 'Minimum of 3 votes required';
        }else {

            $userRating = $bid->bidder->averageRating(1)[0];
            $userRatingClass = 'star-rating';
            $userRatingText = '';
        }
        return response()->json(['rate' => $bid->rate.' TND', 'time' => $time, 'name' => Auth::user()->getFullName(), 'url' => route('showFreelancerDetails', Auth::user()->getFreelancerUsername()), 'src' => $src, 'userRating' => $userRating , 'userRatingClass' => $userRatingClass , 'userRatingText' => $userRatingText]);
    }

    public function showManageServices() {
        return view('User::frontOffice.dashboard.services.list');
    }

    public function showPostService() {
        return view('User::frontOffice.dashboard.services.postService');
    }


    public function handlePostService() {

        $data = Input::all();

        $rules = [
            'name' => 'required|min:10|max:100',
            'category' => 'required',
            'min' => 'required|numeric|lt:'.$data['max'],
            'max'  => 'required|numeric|gt:'.$data['min'],
            'skills' => 'required',
            'description' => 'required',
            'requirements' => 'required',
            'delay' => 'required|numeric',
            'attachments' => 'max:3',
            'attachments.*' => 'mimes:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf|max:10240'
        ];

        $messages = [
            'name.required'          => 'Service name is required !',
            'name.min'             => 'Service name must contains at least 10 characters !',
            'name.max'       => 'Service name is too long !',
            'skills.required'   => 'Please specify the involved skills for this service !',
            'requirements.required'   => 'Please specify what information you will need before starting!',
            'description.required'   => 'Describing your service will help others find it!',
            'attachments.max'   => 'You can only update 3 files !',
            'attachments.*.mimes'   => 'Files accepted are doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf !',
            'attachments.*.max'   => 'File is too large !',
            'min.lt'             => 'Min must be less than max!',
            'max.gt'             => 'Max must be greater than min!'
        ];


        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }


        $service = Service::create([
            'name' => $data['name'],
            'category_id' => $data['category'],
            'description' => $data['description'],
            'requirements' => $data['requirements'],
            'delay' => $data['delay'],
            'min' => $data['min'],
            'max' => $data['max'],
            'user_id' => Auth::id()
        ]);

        $skills = explode(',', $data['skills']);
        foreach ($skills as $skill) {
            $service->skills()->create([
                'label' => $skill
            ]);
        }

        if(isset($data['attachments'])) {
            foreach ($data['attachments'] as $file) {
                $link = uploadFile($file, 'serviceFiles');
                $service->medias()->create([
                    'link' => $link,
                    'label' => $file->getClientOriginalName(),
                    'type' => 7
                ]);
            }
        }


          Session::flash('message', 'Service Published Successfully!');
          Session::flash('color', '#5f9025');
                return redirect()->route('showManageServices');
    }

    public function showUpdateService($id){
        try {
            $serviceId = decrypt($id);
        } catch (DecryptException $e) {
            return back();
        }
        $service = Service::find($serviceId);

        if(!$service) {
           Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
           return back();
        }

        return view('User::frontOffice.dashboard.services.updateService', [
            'service' => $service
        ]);
    }

    public function handleUpdateService($id) {

        $data = Input::all();

        $service = Service::find(Crypt::decrypt($id));

        if(!$service) {
            return back();
        }

        $rules = [
            'name' => 'required|min:10|max:50',
            'category' => 'required',
            'min' => 'required|numeric',
            'max'  => 'required|numeric',
            'skills' => 'required',
            'description' => 'required',
            'requirements' => 'required',
            'delay' => 'required|numeric',
            'attachments' => 'max:3',
            'attachments.*' => 'mimes:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf|max:10240'
        ];

        $messages = [
            'name.required'          => 'Service name is required !',
            'name.min'             => 'Service name must contains at least 10 characters !',
            'name.max'       => 'Service name is too long !',
            'skills.required'   => 'Please specify the involved skills for this service !',
            'requirements.required'   => 'Please specify what information you will need before starting!',
            'description.required'   => 'Describing your service will help others find it!',
            'attachments.max'   => 'You can only update 3 files !',
            'attachments.*.mimes'   => 'Files accepted are doc,csv,xlsx,xls,docx,ppt,odt,ods,odp,pdf !',
            'attachments.*.max'   => 'File is too large !',
        ];


        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }


        $service->update([
            'name' => $data['name'],
            'category_id' => $data['category'],
            'description' => $data['description'],
            'requirements' => $data['requirements'],
            'delay' => $data['delay'],
            'min' => $data['min'],
            'max' => $data['max']
        ]);

        DB::table('skills')->where('service_id', '=', $service->id)->delete();

        $skills = explode(',', $data['skills']);
        foreach ($skills as $skill) {
            $service->skills()->create([
                'label' => $skill
            ]);
        }

        if(isset($data['attachments'])) {
            foreach ($data['attachments'] as $file) {
                $link = uploadFile($file, 'serviceFiles');
                $service->medias()->where('type',7)->first()->update([
                    'link' => $link,
                    'label' => $file->getClientOriginalName()
                ]);
            }
        }


          Session::flash('message', 'Service updated successfully!');
          Session::flash('color', '#5f9025');
                  return redirect()->route('showManageServices');
    }

    public function handleDeleteService()
    {
        $data   =  Input::all();

        $serviceId =  $data['serviceId'];

        $service = Auth::user()->services()->where('id',$serviceId)->first();

        if(!$service) {
          Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
            return back();
        }

        $service->delete();
         Session::flash('message', 'Service deleted successfully!');
          Session::flash('color', '#5f9025');
        return back();
    }

    public function showServiceDetails($name) {
        $param = explode("-",$name);
        $id = ((int)end($param) - 9371) / 33 ;
        $service = Service::find($id);
        if(!$service) {
           Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
            return redirect()->route('showServices');
        }

        return view('Hired::serviceDetails',[
            'service' => $service
        ]);
    }

    public function handleBookService(Request $request) {

        $service = Service::find($request->serviceId);
        if(!$service) {
           return response()->json(['status' => 404]);
        }
        $media_id = null;
        if(isset($request->file)) {
            $file = $request->file;
            $link = uploadFile($file, 'bookServiceFiles');
            $bookFile = Auth::user()->medias()->create([
                'link' => $link,
                'label' => $file->getClientOriginalName(),
                'type' => 8
            ]);

            $media_id = $bookFile->id;
        }

        $booking = Booking::create([
            'note' => $request->note ?? null,
            'user_id' => Auth::id(),
            'media_id' =>  $media_id,
            'service_id' => $service->id
        ]);

        event(new HiredEvents(Auth::user(),'booking',$booking->service->name,$booking->service->user->id,$booking->service->id));
        saveNotification(Auth::id(), $booking->service->user->id, 0, $booking->service->name, 'Booked your service', route('showManageServices'));

        $user = User::where('id',$booking->service->user->id)->first();
        sendMail('Hired::mails.book',
            $user->email,
            ['user' => $user , 'name' => $booking->service->name , 'id' => $booking->service->id, 'params' =>$booking->service->getServiceParamName()] ,
            'New Service Booking');
        return response()->json(['status' => 200]);
    }

    public function handleDeleteBooking()
    {
        $data   =  Input::all();

        $bookingId =  $data['bookingId'];

        $booking = Booking::find($bookingId);

        if(!$booking) {
          Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
                      return back();
        }

        $booking->delete();
          Session::flash('message', 'Booking deleted successfully!');
          Session::flash('color', '#5f9025');
        return back();
    }

    public function showManageBookings($id = null)
    {
        if($id != null) {
            try {
                $serviceId = decrypt($id);
            } catch (DecryptException $e) {
              Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
                return back();
            }
            $service = Service::find($serviceId);
            if(!$service) {
              Session::flash('message', 'Something went wrong, please try again!');
          Session::flash('color', '#de5959');
                return back();
            }
            return view("User::frontOffice.dashboard.services.bookings",[
                'service' => $service
            ]);
        }else {

            if(count(Auth::user()->services) == 0) {
              Session::flash('message', "You don't have any active service yet! create one.");
                 Session::flash('color', '#de5959');
                return back();
            }
            return view("User::frontOffice.dashboard.services.bookings",[
                'service' => Auth::user()->services()->latest()->first()
            ]);
        }

    }

    public function handleSubmitReview(Request $request) { 

       if($request->has('fId')) {
           $freelancer = User::find($request->get('fId'));

          $rating =  $freelancer->rating([
               'title' => $request->get('title'),
               'body' => $request->get('message'),
               'rating' => (int)$request->get('rating'),
               'approved' => true, 
           ], Auth::user());

           return response()->json(['title'=> $rating->title , 'name' => $rating->author->getFullName(), 'rating' => $rating->rating , 'date' => Carbon::parse($rating->created_at)->format('d F Y') , 'body' => $rating->body ]);
       }else {
           $company = Company::find($request->get('cId'));
           if(!$company) {
            return response()->json(['status' => 404]);
           }
           $rating =  $company->rating([
               'title' => $request->get('title'),
               'body' => $request->get('message'),
               'rating' => (int)$request->get('rating'),
               'approved' => true, 
           ], Auth::user());

           return response()->json(['title'=> $rating->title , 'name' => $rating->author->getFullName(), 'rating' => $rating->rating , 'date' => Carbon::parse($rating->created_at)->format('d F Y') , 'body' => $rating->body ]);

       }
    }

    //portfolio managements

    public function showPortfolio() {
        return view('User::frontOffice.dashboard.portfolio');
    }

    public function handleAddProject() {
        $data = Input::all();
        

        $rules = [
            'title' => 'required',
            'realized_at'  => 'required|date|before:today',
            'type' => 'required'
        ];

        $messages = [
            'title.required'          => 'Project title is required!',
            'desc.required'             => 'Project description is required!',
            'realized_at.date'       => 'Please enter a valid date',
            'realized_at.before'       => 'Please enter a valid date',
            'type.required'   => 'Please specify the project category!'
        ];

        if(isset($data['link'])) {

            if (!filter_var($data['link'], FILTER_VALIDATE_URL)) {
                $rules['link'] = 'url';
                $messages['link.url'] = 'Please enter a valid url!';
        }}

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

         $portfolio =  Auth::user()->portfolios()->create([
            'project_title' => $data['title'],
            'project_description' => $data['desc'],
            'realized_at' => $data['realized_at'],
            'type' => $data['type'],
            'link' => $data['link'] ?? null
        ]);

        if(isset($data['images'])) {
            foreach ($data['images'] as $file) {
            $portfolio->medias()->create([
                'link' =>   json_decode(urldecode($file))->url,
                'label' => json_decode(urldecode($file))->name,
                'type' => 9
            ]);
        }
        }

        Session::flash('message', 'Project published successfully!');
        Session::flash('color', '#5f9025');
        return back();

    }


    public function handleUploadMiltupleImages(Request $request)
    {
        $file = $request->file('file');
         $link = uploadFile($file, 'portfolio');
        return response()->json([
            'link' => $link,
            'name' => $file->getClientOriginalName(),
        ]);
    }

    public function apiGetProjectMedias(Request $request) {

        $medias = [];
        if(Portfolio::where('id', $request->get('id'))->first()->medias) {
            $medias = Portfolio::where('id', $request->get('id'))->first()->medias;
        }

        $view = view('User::frontOffice.dashboard.loaders.projects', [
            'medias' => $medias
        ])->render();
        return response()->json(['html' => $view]);
    }

    public function handleDeleteProject(Request $request) {
        $project = Portfolio::find($request->pId);

        if(!$project) {
            Session::flash('message', 'Something went wrong, please try again!');
            Session::flash('color', '#de5959');
             return back();
        }

        $project->delete();
        Session::flash('message', 'Project deleted!');
        Session::flash('color', '#5f9025');
        return back();
    }


    public function handleScheduleInterview(Request $request) {

        $interview = Interview::create([
            'note' => $request->data['note'] ?? null,
            'type' => $request->data['type'],
            'status' => 0,
            'interviewer' => Auth::id(),
            'interviewed' => $request->data['uId'],
            'job_id' => $request->data['jId']
        ]);

        foreach ($request->data['dates'] as $date) {

            $interview->dates()->create([
                'date' => Carbon::parse($date),
                'status' => 0
            ]);
        }

        event(new ScheduleInterview(Auth::user(),$request->data['uId']));
        saveNotification(Auth::id(), $request->data['uId'], 0, 'See Details', 'Has scheduled an interview with you!', route('showCalendar'));

        sendMail('Hired::mails.scheduleInterview',
            $interview->interviewedUser()->email,
            ['job' => $interview->job, 'user' => $interview->interviewedUser() ] ,
            'New Interview Invitation');
        return response()->json(['status' => 200 , 'url' => route('showCalendar')]);
    }

    public function sendMassMailsFr($n = '') {
            $users =  json_decode(file_get_contents(storage_path().'/settings/employers'.$n.'.json'), true);
            foreach ($users as $user) {
            sendMail('Hired::mails.massEmployersFr',
            $user,
            ['name' => ''] ,
            'Le HR Simplifiée');
            }

            return 'All mails have been sent';
    }

      public function sendMassMailsEn($n = '') {
            $users =  json_decode(file_get_contents(storage_path().'/settings/users'.$n.'.json'), true);

            foreach ($users as $user) {

            sendMail('Hired::mails.massEn',
            $user['email'],
            ['name' => $user['fullname']] ,
            'Work In Tunisia');
            }

            return 'All mails have been sent';
    }

    public function finishProfileMassMails($n) {
            $users = User::where('type',1)->where('status',1)->skip( 7 + $n)->take(10)->get();

            foreach ($users as $user) {
            sendMail('Hired::mails.finishProfile',
            $user->email,
            ['name' => $user->getFullName()] ,
            'Publish your profile');
            }

            return 'All mails have been sent';
    }

}
