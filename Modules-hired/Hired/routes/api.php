<?php

Route::group(['module' => 'Hired', 'middleware' => ['api'], 'namespace' => 'App\Modules\Hired\Controllers'], function() {

    Route::resource('Hired', 'HiredController');

});
