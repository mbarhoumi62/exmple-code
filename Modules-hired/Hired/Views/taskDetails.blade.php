@extends('frontOffice.layout',  ['title' => 'Task | '.$task->name])
@section('meta')
    <meta property="og:url" content="{{ route('showTaskDetails', $task->getTaskParamName())}}">
    <meta property="og:description" content="{{$task->name}}">
    <meta property="og:site_name" content="Hired.tn">
    <meta property="og:image" content="{{asset($task->company ? $task->company->medias()->where('type',0)->first()->link : '')}}">
    <meta property="og:image:type" content="image/png">
    <meta property="twitter:creator" content="{{$task->company ? $task->company->name : $task->name}}">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="{{$task->company ? $task->company->name : $task->name }}">
    <meta property="twitter:description" content="{{$task->description}}">
    <meta property="twitter:image:src" content="{{asset($task->company ? $task->company->medias()->where('type',0)->first()->link : '')}}">
    <meta property="twitter:image:width" content="1291">
    <meta property="twitter:image:height" content="315">
    <meta name="msapplication-TileColor" content="#2A5977">
    <meta name="theme-color" content="#2A5977">

    <meta name="description" content="{{ $task->description }}">
    <meta name="keywords" content="{{ $task->description.','.$task->name }}">
@endsection
@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')

    <div class="single-page-header" data-background-image="{{asset('frontOffice/images/single-task.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            @php
                                $company = $task->company ?? null;
                            @endphp
                           @if($company)
                                <div class="header-image"><a href="{{ route('showCompanyDetails' , $task->company->getCompanyParamName()) }}"><img src="{{ asset($company->medias()->where('type',0)->first() ? $company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png') }}" alt=""></a></div>
                               @endif
                            <div class="header-details">
                                <h3>{{$task->name }}</h3>

                              @if($company)
                                    <h5>{{ $company->tagline }}</h5>
                                  @endif
                                <ul>
                                    @if($company)
                                        <li><a href="{{ route('showCompanyDetails' , $task->company->getCompanyParamName()) }}"><i class="icon-material-outline-business"></i>{{ $company->name }}</a></li>
                                        @if(count($company->getAllRatings($company->id,'desc','App\Modules\User\Models\Company')) < 3)
                                            <li><span class="company-not-rated">{{__('Minimum of 3 votes required')}}</span></li>
                                        @else
                                            <li><div class="star-rating" data-rating="{{ $company->averageRating(1)[0] }}"></div></li>
                                        @endif
                                        @else
                                        <li><a href="{{ route('showFreelancerDetails', $task->user->getFreelancerUsername()) }}"><i class="icon-feather-user"></i>{{ $task->user->getFullName() }}</a></li>
                                        @if(count($task->user->getAllRatings($task->user->id,'desc','App\Modules\User\Models\User')) < 3)
                                            <li><span class="company-not-rated">{{__('Minimum of 3 votes required')}}</span></li>
                                        @else
                                            <li><div class="star-rating" data-rating="{{ $task->user->averageRating(1)[0] }}"></div></li>
                                        @endif

                                        @endif
                                      @if($company)      
                                    <li><img class="flag" src="{{ asset('frontOffice/images/flags/'.strtolower($company->country).'.svg') }}" alt="{{ $company->country }}"> {{ $company->country }}</li>
                                        @else
                                              <li>{{ $task->address->address ?? '' }}</li>
                                        @endif

                                </ul>
                            </div>
                        </div>
                        <div class="right-side">
                            <div class="salary-box">
                                <div class="salary-type">{{__('Project Budget')}}  <strong>{{ $task->budget->type == 0 ? '(Fixed rate)' : '(Hourly rate)' }}</strong></div>
                                <div class="salary-amount">{{ $task->budget->min.' TND' }} - {{ $task->budget->max.' TND' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">

                <!-- Description -->
                <div class="single-page-section">
                    <h3 class="margin-bottom-25">{{__('Project Description')}}</h3>
                    <p>{{ $task->description }}</p>


                </div>

                <!-- Atachments -->
                @if(count($task->medias()->where('type',3)->get()) != 0)
                    <div class="single-page-section">
                        <h3>Attachments</h3>
                            <div class="attachments-container">
                                @foreach($task->medias()->where('type',3)->get() as $file)
                                <a href="{{ asset($file->link) }}" class="attachment-box ripple-effect" download><span>{{ $file->label }}</span></a>
                                @endforeach
                            </div>
                    </div>

                @endif
                <!-- Skills -->
                @if(count($task->skills) != 0)
                    <div class="single-page-section">
                        <h3>Skills Required</h3>
                        <div class="task-tags">
                            @foreach($task->skills as $skill)
                                <span>{{$skill->label}}</span>
                            @endforeach
                        </div>
                    </div>
                    @endif
                <div class="clearfix"></div>

                <!-- Freelancers Bidding -->
                <div class="boxed-list margin-bottom-60">
                    <div class="boxed-list-headline">
                        <h3><i class="icon-material-outline-group"></i> {{__('Candidates Bidding')}}</h3>
                    </div>
                    <ul class="boxed-list-ul bidsList">
                        @if(count($task->bids) !=0)
                            @foreach($task->bids as $bid)
                        <li>
                            <div class="bid">
                                <!-- Avatar -->
                                <div class="bids-avatar">
                                    <div class="freelancer-avatar">  

                                        <a href="{{ route('showFreelancerDetails', $bid->bidder->getFreelancerUsername()) }}"><img src="{{ asset($bid->bidder->medias()->where('type',0)->first() ? $bid->bidder->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png') }}" alt="{{ $bid->bidder->getFullName() }}"></a>
                                    </div>
                                </div>

                                <!-- Content -->
                                <div class="bids-content">
                                    <!-- Name -->
                                    <div class="freelancer-name">
                                        <h4><a href="{{ route('showFreelancerDetails', $bid->bidder->getFreelancerUsername()) }}">{{ $bid->bidder->getFullName()  }}</a></h4>

                                @if(count($bid->bidder->getAllRatings($bid->bidder->id,'desc','App\Modules\User\Models\User')) < 3)
                                 <div class="company-not-rated">{{ __('Minimum of 3 votes required') }}</div>
                                @else
                                    <div class="star-rating" data-rating="{{ $bid->bidder->averageRating(1)[0] }}"></div>
                                @endif
                                    </div>
                                </div>

                                <!-- Bid -->
                                <div class="bids-bid">
                                    <div class="bid-rate">
                                        <div class="rate">{{ $bid->rate.' TND'  }}</div>
                                        <span>in {{ $bid->type == 0 ? $bid->delivery_time.' days' : $bid->delivery_time.' hours' }}</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                            @endforeach
                            @else
                            <li class="noBids">
                                <div class="bid">

                                    <!-- Content -->
                                    <div class="bids-content">
                                        <!-- Name -->
                                        <div class="freelancer-name">
                                            <h4>{{__('No biddings on this task yet!')}}</h4>
                                        </div>
                                    </div>

                                </div>
                            </li>
                            @endif

                    </ul>
                </div>

            </div>


            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <div class="sidebar-container">
                    @php

                        $endDate = Carbon\Carbon::parse($task->created_at)->addDays(20);
                        $left = $endDate->diffInDays(Carbon\Carbon::now());
                        $leftInHours = null;
                        if($left == 0 ) {
                             $leftInHours = $endDate->diffInHours(Carbon\Carbon::now());
                        }
                    @endphp
                    <div class="countdown green margin-bottom-35">{{ $leftInHours ? $leftInHours." ".  __('Hours Left') : $left." ".  __('Days Left') }}</div>

                    @if($task->user_id != Auth::id())
                    @if($task->bids()->where('bidder_id',Auth::id())->first())

                        <div class="countdown green ">Bid placed {{ \Carbon\Carbon::parse($task->bids()->where('bidder_id',Auth::id())->first()->created_at)->diffForHumans() }}</div>
                        <div class="countdown margin-bottom-35"><a href="{{ route('showActiveBids') }}">{{__('Manage Bids')}}</a></div>

                    @else
                        <div class="bidPlaced" style="display: none">
                              <div class="countdown green">{{__('Your bid has been placed!')}}</div>
                            <div class="countdown margin-bottom-35"><a href="{{ route('showActiveBids') }}">{{__('Manage Bids')}}</a></div>

                        </div>

                    <div class="sidebar-widget placeBidWidget">
                        <div class="bidding-widget">
                            <div class="bidding-headline"><h3>{{__('Bid on this job!')}}</h3></div>
                            <div class="bidding-inner">

                                <!-- Headline -->
                                <span class="bidding-detail">{{__('Set your')}} <strong>{{__('minimal rate')}}</strong></span>

                                <!-- Price Slider -->
                                <div class="bidding-value">TND <span id="biddingVal"></span> <small style="color: black">({{ $task->budget->type == 0 ? __('Fixed rate') :__('Hourly rate') }})</small></div>
                                <input class="bidding-slider" type="text" value="" data-slider-handle="custom" data-slider-currency=TND" data-slider-min="{{ $task->budget->min }}" data-slider-max="{{ $task->budget->max }}" data-slider-value="auto" data-slider-step="{{ $task->budget->type == 0 ? '5' :'1' }}" data-slider-tooltip="hide" />

                                <!-- Headline -->
                                <span class="bidding-detail margin-top-30">{{__('Set your')}} <strong>{{__('delivery time')}}</strong></span>

                                <!-- Fields -->
                                <div class="bidding-fields">
                                    <div class="bidding-field">
                                        <!-- Quantity Buttons -->
                                        <div class="qtyButtons">
                                            <div class="qtyDec"></div>
                                            <input type="text" id="deliveryTime" name="qtyInput" value="1">
                                            <div class="qtyInc"></div>
                                        </div>
                                    </div>
                                    <div class="bidding-field">
                                        <select id="type" class="selectpicker default">
                                            <option value="0" selected>{{__('Days')}}</option>
                                            <option value="1">{{__('Hours')}}</option>
                                        </select>
                                    </div>
                                </div>

                               @if(Auth::check()) 

                                        @if(eligibleCandidate(Auth::user()))
                                <button id="place-bid" class="button ripple-effect move-on-hover full-width margin-top-30"><span>{{__('Place a Bid')}}</span></button>
                                         @else
 <button id="notEligible" class="button ripple-effect move-on-hover full-width margin-top-30"><span>{{__('Place a Bid')}}</span></button>
                                                <div style="display: none" class="countdown yellow margin-bottom-35 notEligible">{{__('You must update your profile information in order to be able to bid on tasks.')}} <br> <a href="{{ route('showProfile') }}">{{ __('Update Profile Now') }}</a> </div>
                                         @endif
                                @else
                                        <a  href="#sign-in-dialog" class="button ripple-effect move-on-hover popup-with-zoom-anim full-width margin-top-30"><span>{{__('Place a Bid')}}</span></a>
                                @endif

                            </div>
                        </div>
                    </div>
                    @endif
                    @endif

                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <h3>{{__('Bookmark or Share')}}</h3>

                        <!-- Bookmark Button -->
                        @if(Auth::check() && Auth::id() != $task->user->id)
                        <button class="bookmark-button margin-bottom-25 {{ Auth::user()->hasBookmarked($task) ? 'bookmarked' : '' }}">
                            <span class="bookmark-icon"></span>
                            <span class="bookmark-text">{{__('Bookmark')}}</span>
                            <span class="bookmarked-text">{{__('Bookmarked')}}</span>
                        </button>
                        @endif
                        <!-- Copy URL -->
                        <div class="copy-url">
                            <input id="copy-url" type="text" value="" class="with-border">
                            <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
                        </div>

                        <!-- Share Buttons -->
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>{{__('Interesting?')}} <strong>{{__('Share It!')}}</strong></span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"
                                           data-sharer="facebook" data-title="{{$task->name}}" data-url="{{route('showTaskDetails', $task->getTaskParamName())}}"
                                        ><i class="icon-brand-facebook-f"></i></a></li>

                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"
                                           data-sharer="twitter"  data-title="{{$task->name}}" data-url="{{route('showTaskDetails', $task->getTaskParamName())}}"
                                        ><i class="icon-brand-twitter"></i></a></li>

                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"
                                           data-sharer="linkedin"  data-url="{{route('showTaskDetails', $task->getTaskParamName())}}"
                                        ><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


    <!-- Spacer -->
    <div class="margin-top-15"></div>
    @include('User::frontOffice.auth.modals.login')

    <script type="text/javascript">
        $('.bookmark-button').on('click', function () {
            $.get('{{ route('handleBookmark') }}?type=task&id='+{{ $task->id }}).done(function(res) {
                if(res.bookmarked) {
                    $('.bookmark-button').addClass('bookmarked');
                }else {
                    $('.bookmark-button').removeClass('bookmarked');
                }
            }).fail(function(error) {

            });
        });

        $('#place-bid').on('click', function () {
            $(this).html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
            $(this).css('opacity','0.7');
            $(this).css('cursor','not-allowed');
            $(this).attr("disabled", true);
            var data = {
                 rate : $('.bidding-slider').val(),
                deliveryTime : $('#deliveryTime').val(),
                 type : $('#type').val(),
                 taskId : '{{ $task->id }}'
            }

            $.post('{{route('apiHandleBidOnTask')}}',
                {
                    '_token': $('meta[name=csrf-token]').attr('content'), data : data

                })
                .done(function (res) {

                    if(res.status == 404) {
                        $('.placeBidWidget').hide();
                        Snackbar.show({
                            text: '{{ __('This task has been removed or closed by the employer!') }}',
                            pos: 'bottom-center',
                            showAction: false,
                            actionText: "Dismiss",
                            duration: 3000,
                            textColor: '#fff',
                            backgroundColor: '#f00000'
                        });
                    }else {
                         $('.noBids').remove();
                    $('.bidsList').append(' <li>\n' +
                        '                            <div class="bid">\n' +
                        '                                <!-- Avatar -->\n' +
                        '                                <div class="bids-avatar">\n' +
                        '                                    <div class="freelancer-avatar">\n' +
                        '                                        <a href="'+ res.url +'"><img src="'+ res.src +'"></a>\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                                <!-- Content -->\n' +
                        '                                <div class="bids-content">\n' +
                        '                                    <!-- Name -->\n' +
                        '                                    <div class="freelancer-name">\n' +
                        '                                        <h4><a href="'+ res.url +'"></a> '+ res.name +' </h4>\n' +
                        '                                        <div class="' + res.userRatingClass + '" data-rating="' + res.userRating + '"> ' + res.userRatingText + ' </div>\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                                <!-- Bid -->\n' +
                        '                                <div class="bids-bid">\n' +
                        '                                    <div class="bid-rate">\n' +
                        '                                        <div class="rate">'+ res.rate +'</div>\n' +
                        '                                        <span>in '+ res.time +'</span>\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                        </li>');

                    $('.placeBidWidget').hide();
                    $('.bidPlaced').show();
                    }
                   
                })
                .fail(function (res) {

                    $('#place-bid').html('<span>Place a Bid</span>')
                    $('#place-bid').css('opacity','1');
                    $('#place-bid').css('cursor','pointer');
                    $('#place-bid').attr("disabled", false);
                    Snackbar.show({
                        text: 'Something went wrong, please try again!',
                        pos: 'bottom-center',
                        showAction: false,
                        actionText: "Dismiss",
                        duration: 3000,
                        textColor: '#fff',
                        backgroundColor: '#f00000'
                    });
                })

        });


        $('#notEligible').on('click' , function (e) {
            e.preventDefault();
                $('.notEligible').show();
        });
    </script>

@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection
