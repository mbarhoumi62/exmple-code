<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">{{__('Book a Service')}}</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>{{__('Book')}} {{ $service->name }}</h3>
                </div>

                <!-- Form -->
                <form action="javascript:void(0)"  id="booking-form" method="post" enctype="multipart/form-data">

                    <textarea name="textarea" id="notes" cols="10" placeholder="{{__('Notes... (Optional)')}}" class="with-border"></textarea>

                    <div class="uploadButton margin-top-25">
                        <input class="uploadButton-input" type="file" accept="image/*, application/pdf" id="upload" />
                        <label class="uploadButton-button ripple-effect attach" for="upload">{{__('Add Attachment')}}</label>
                        <span class="uploadButton-file-name">{{__('Allowed file type: pdf, png, jpg, gif')}} <br> {{__('Max. file size: 10 MB.')}}</span>
                        <small id="fileErrors"></small>
                    </div>

                </form>

                <!-- Button -->
                <button class="button margin-top-35 full-width button-sliding-icon ripple-effect bookBtn" form="booking-form" type="submit">{{__('Book')}} <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>


        </div>
    </div>
</div>

<script>
    //  $(document).on('click','.attach', function () {
    //          $('.popup-tab-content').css('padding','50px 0px');

    // });


    var files = null;
    $(document).on('change','#upload', function () {
        files = $(this)[0].files;
        if((files[0].size / 1048576).toFixed(2) > 10 ) {
            $('#fileErrors').text('Maximum file size is 10 MB').fadeIn('slow').delay(2000).fadeOut();
        }else {
            $('.uploadButton-file-name').html(files[0].name);
        }
    });

    $('#booking-form').submit( function (event) {
        event.preventDefault();
        $('.bookBtn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $('.bookBtn').css('opacity','0.5');
        $('.bookBtn').css('cursor','not-allowed');
        $(".bookBtn").attr("disabled", true);

        var formData = new FormData();

        formData.append("serviceId", '{{ $service->id }}');

        if(files != null) {
                formData.append("file", files[0]);
                formData.append("upload_file", true);

        }

        if($('#notes').val() != '') {
            formData.append("note", $('#notes').val());
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '{{ route('handleBookService') }}',

            success: function (data) {

                if(data.status == 404) {
                    $('.book-now-button').hide();
                        Snackbar.show({
                    text: '{{__('This service has been removed by the owner!')}}',
                    pos: 'bottom-center',
                    showAction: false,
                    actionText: "Dismiss",
                    duration: 3000,
                    textColor: '#fff',
                    backgroundColor: '#5f9025'
                });
                $('.mfp-close').click()
                }else {

                               $('.book-now-button').hide();
                $('.bookedNotif').show();
                Snackbar.show({
                    text: 'Service booked successfully',
                    pos: 'bottom-center',
                    showAction: false,
                    actionText: "Dismiss",
                    duration: 3000,
                    textColor: '#fff',
                    backgroundColor: '#5f9025'
                });
                $('.mfp-close').click()
                }
     
            },
            error: function (error) {
                $('.bookBtn').html('Book<i class="icon-material-outline-arrow-right-alt"></i>')
                $('.bookBtn').css('opacity','1');
                $('.bookBtn').css('cursor','pointer');
                $(".bookBtn").attr("disabled", false);
                Snackbar.show({
                    text: 'Something went wrong, please try again!',
                    pos: 'bottom-center',
                    showAction: false,
                    actionText: "Dismiss",
                    duration: 3000,
                    textColor: '#fff',
                    backgroundColor: '#f00000'
                });
                $('.mfp-close').click()
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    });
</script>
