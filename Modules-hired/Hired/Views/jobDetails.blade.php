@extends('frontOffice.layout',['title' => 'Job | '.$job->title])
@section('meta')
    <meta property="og:url" content="{{ route('showJobDetails', $job->getJobParamTitle())}}">
    <meta property="og:description" content="{{$job->title}}">
    <meta property="og:site_name" content="Hired.tn">
    <meta property="og:image" content="{{asset($job->company->medias()->where('type',0)->first()->link ?? 'frontOffice/images/company-logo-placeholder.png')}}">
    <meta property="og:image:type" content="image/png">
    <meta property="twitter:creator" content="{{$job->user->getFullName()}}">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="{{$job->title}}">
    <meta property="twitter:description" content="{{$job->description}}">
    <meta property="twitter:image:src" content="{{asset($job->company->medias()->where('type',0)->first()->link ?? 'frontOffice/images/company-logo-placeholder.png')}}">
    <meta property="twitter:image:width" content="1291">
    <meta property="twitter:image:height" content="315">
    <meta name="msapplication-TileColor" content="#2A5977">
    <meta name="theme-color" content="#2A5977">

    <meta name="description" content="{{ $job->description }}">
    <meta name="keywords" content="{{ $job->description.','.$job->title }}">
@endsection
@section('header') 
    @include('frontOffice.inc.header')
@endsection

@section('content')
    <div class="single-page-header" data-background-image="{{ asset('frontOffice/images/single-job.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            <div class="header-image"><a href="{{ route('showCompanyDetails' , $job->company->getCompanyParamName()) }}"><img src="{{ asset($job->company->medias()->where('type',0)->first() ? $job->company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png') }}" alt="{{ $job->company->name }}"></a></div>
                            <div class="header-details">
                                <h3>{{ $job->title }}</h3>
                                <h5>{{ $job->company->info->tagline }}</h5>
                                <ul>
                                    <li><a href="{{ route('showCompanyDetails' , $job->company->getCompanyParamName()) }}"><i class="icon-material-outline-business"></i>{{ $job->company->name }}</a></li>

                                    @if(count($job->company->getAllRatings($job->company->id,'desc','App\Modules\User\Models\Company')) < 3)
                                        <li><span class="company-not-rated">{{__('Minimum of 3 votes required')}}</span></li>
                                    @else
                                        <li><div class="star-rating" data-rating="{{ $job->company->averageRating(1)[0] }}"></div></li>
                                        @endif

                                    <li><img class="flag" src="{{ asset('frontOffice/images/flags/'.strtolower($job->company->country).'.svg') }}" alt="{{ $job->company->country }}"> {{ $job->company->country }}</li>
                                </ul>
                            </div>
                        </div>
                        @if($job->max_salary > 0)
                        <div class="right-side">
                            <div class="salary-box">
                                <div class="salary-type">{{__('Salary')}}</div>
                                <div class="salary-amount" style="font-size : 25px!important">{{ $job->min_salary.' TND - '.$job->max_salary.' TND' }}</div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">

                <div class="single-page-section">
                    <h3 class="margin-bottom-25">{{__('Job Description')}}</h3>

                    <p>{{ $job->description }}</p>
                </div>
                @if(count($job->tags) > 0)
                <div class="single-page-section">
                <h3>{{ __('Post Requirements') }}</h3>
                <div class="task-tags">
                        @foreach($job->tags as $tag)
                        <span>{{ $tag->label }}</span>
                @endforeach
                </div>
               </div>
               @endif

                @if(count($job->medias()->where('type',4)->get()) != 0)
                    <div class="sidebar-widget">
                        <h3>Attachments</h3>
                        <div class="attachments-container">
                            @foreach($job->medias()->where('type',4)->get() as $file)
                                <a href="{{ asset($file->link) }}" class="attachment-box ripple-effect" download><span>{{$file->label}}</span></a>
                            @endforeach
                        </div>
                    </div>
                @endif
       
             @if($job->address)
                    <div class="single-page-section">
                        <h3 class="margin-bottom-30">{{__('Location')}}</h3>
                        <div class="col-xl-12">
                            <div id="singleMap" style="height: 400px; width: 100%" data-latitude="{{$job->address->lat}}" data-longitude="{{$job->address->lon}}"></div>
                        </div>
                    </div>
             @endif

                <div class="single-page-section">
                    <h3 class="margin-bottom-25">{{__('Similar Jobs')}}</h3>

                    <!-- Listings Container -->
                    <div class="listings-container grid-layout">

                         @foreach(getSimilarJobs($job) as $sJob)
                             <!-- Job Listing -->
                                 <a href="{{ route('showJobDetails', $sJob->getJobParamTitle()) }}" class="job-listing">

                                     <!-- Job Listing Details -->
                                     <div class="job-listing-details">
                                         <!-- Logo -->
                                         <div class="job-listing-company-logo">
                                             <img src="{{ asset($sJob->company->medias()->where('type',0)->first() ? $sJob->company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png') }}" alt="{{ $sJob->company->name }}">
                                         </div>

                                         <!-- Details -->
                                         <div class="job-listing-description">
                                             <h4 class="job-listing-company">{{ $sJob->company->name }}</h4>
                                             <h3 class="job-listing-title">{{ $sJob->title }}</h3>
                                         </div>
                                     </div>

                                     <!-- Job Listing Footer -->
                                     <div class="job-listing-footer">
                                         <ul>
                                             <li><i class="icon-material-outline-location-{{ $sJob->address ? 'on' :'off' }}"></i> {{$sJob->address ? $sJob->address->address : 'Remote'}}</li>
                                             <li><i class="icon-material-outline-business-center"></i> {{ jobTypeToString($sJob->type) }}</li>

                                             <li><i class="icon-material-outline-account-balance-wallet"></i> {{ $sJob->min_salary.' TND - '.$sJob->max_salary.' TND' }}</li>

                                             <li><i class="icon-material-outline-access-time"></i> {{ \Carbon\Carbon::parse($sJob->created_at)->diffForHumans() }}</li>
                                         </ul>
                                     </div>
                                 </a>
                        @endforeach

                    </div>
                    <!-- Listings Container / End -->

                </div>
            </div>


            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <div class="sidebar-container">
                      @if(Auth::check())

                      @if(Auth::user()->type == 0)

                          @elseif(applied($job))
                          <div class="countdown green margin-bottom-35 appliedNotif">{{__('Applied')}} {{applied($job)}}</div>
                           @else

                            @if(eligibleCandidate(Auth::user()))
                          <div style="display : none" class="countdown green margin-bottom-35 appliedNotif">{{__('Application submitted successfully')}}</div>
                          <a  href="#small-dialog" class="apply-now-button popup-with-zoom-anim">{{__('Apply Now')}} <i class="icon-material-outline-arrow-right-alt"></i></a>
                            @else
                                <a href="#" id="notEligible"  class="apply-now-button popup-with-zoom-anim">{{__('Apply Now')}} <i class="icon-material-outline-arrow-right-alt"></i></a>
                                <div style="display: none" class="countdown yellow margin-bottom-35 notEligible">{{__('You must update your profile information in order to be able to apply on jobs.')}} <br> <a href="{{ route('showProfile') }}">{{ __('Update Profile Now') }}</a> </div>
                            @endif

                        @endif

                      @else
                        <a href="#sign-in-dialog"  class="apply-now-button popup-with-zoom-anim">{{__('Apply Now')}} <i class="icon-material-outline-arrow-right-alt"></i></a>
                      @endif


                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <div class="job-overview">
                            <div class="job-overview-headline">{{__('Job Summary')}}</div>
                            <div class="job-overview-inner">
                                <ul>
                                    <li>
                                        <i class="icon-material-outline-location-{{ $job->address ? 'on' :'off' }}"></i>
                                        <span>{{__('Location')}}</span>
                                        <h5>{{$job->address ? $job->address->address : 'Remote'}}</h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-business-center"></i>
                                        <span>{{__('Job Type')}}</span>
                                        <h5>{{ jobTypeToString($job->type) }}</h5>
                                    </li>

                                    @if($job->max_salary > 0)
                                    <li>
                                        <i class="icon-material-outline-local-atm"></i>
                                        <span>Salary</span>
                                        <h5>{{ $job->min_salary.' TND - '.$job->max_salary.' TND' }}</h5>
                                    </li>
                                    @endif
                                    <li>
                                        <i class="icon-material-outline-access-time"></i>
                                        <span>Date Posted</span>
                                        <h5>{{ \Carbon\Carbon::parse($job->created_at)->diffForHumans() }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <h3>{{__('Bookmark or Share')}}</h3>

                        <!-- Bookmark Button -->
                        @if(Auth::check() && $job->user->id != Auth::id())
                        <button class="bookmark-button margin-bottom-25  {{ Auth::user()->hasBookmarked($job) ? 'bookmarked' : '' }}">
                            <span class="bookmark-icon"></span>
                            <span class="bookmark-text">{{__('Bookmark')}}</span>
                            <span class="bookmarked-text">{{__('Bookmarked')}}</span>
                        </button>
                        @endif
                        <!-- Copy URL -->
                        <div class="copy-url">
                            <input id="copy-url" type="text" value="" class="with-border">
                            <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>

                        </div>

                        <!-- Share Buttons -->
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>{{__('Interesting?')}} <strong>{{__('Share It!')}}</strong></span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"
                                           data-sharer="facebook" data-title="{{$job->title}}" data-url="{{route('showJobDetails', $job->getJobParamTitle())}}"
                                        ><i class="icon-brand-facebook-f"></i></a></li>

                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"
                                           data-sharer="twitter"  data-title="{{$job->title}}" data-url="{{route('showJobDetails', $job->getJobParamTitle())}}"
                                        ><i class="icon-brand-twitter"></i></a></li>

                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"
                                           data-sharer="linkedin"  data-url="{{route('showJobDetails', $job->getJobParamTitle())}}"
                                        ><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        </div>
    </div>
    @include('User::frontOffice.auth.modals.login')

                <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp"></script>

                <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

                <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>
    <script>
        $('#authCheckPopup').on('click' , function () {
            $.confirm({
                icon: 'fa fa-warning',
                title: 'Authentication is required!',
                type: 'orange',
                columnClass: 'col-xl-6 col-xl-offset-4',
                content: '       <h3 style="margin-top : 20px; margin-bottom: 10px"> You must be logged in to apply for a job.</h3>\n' +
                    '                            <a href="{{ route('showLogin') }}"> Already have an account? </a>\n' +
                    '                            <a href="{{ route('showRegister') }}"> Register now! </a>',
                buttons: {
                    cancel: function () {
                    }
                }
            });
        })


        function singleMap() {
            var myLatLng = {
                lng: $('#singleMap').data('longitude'),
                lat: $('#singleMap').data('latitude'),
            };

            var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                zoom: 18,
                center: myLatLng,
                scrollwheel: false,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                panControl: false,
                navigationControl: false,
                streetViewControl: false,
                styles: [{
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }]
                }]
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                draggable: true,
                map: single_map,
                title: 'Your location'
            });
            var zoomControlDiv = document.createElement('div');
            var zoomControl = new ZoomControl(zoomControlDiv, single_map);


            function ZoomControl(controlDiv, single_map) {
                zoomControlDiv.index = 1;
                single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
                controlDiv.style.padding = '5px';
                var controlWrapper = document.createElement('div');
                controlDiv.appendChild(controlWrapper);
                var zoomInButton = document.createElement('div');
                zoomInButton.className = "mapzoom-in";
                controlWrapper.appendChild(zoomInButton);
                var zoomOutButton = document.createElement('div');
                zoomOutButton.className = "mapzoom-out";
                controlWrapper.appendChild(zoomOutButton);
                google.maps.event.addDomListener(zoomInButton, 'click', function() {
                    single_map.setZoom(single_map.getZoom() + 1);
                });
                google.maps.event.addDomListener(zoomOutButton, 'click', function() {
                    single_map.setZoom(single_map.getZoom() - 1);
                });
            }
            google.maps.event.addListener(marker, 'dragend', function(event) {
                document.getElementById("lat").value = event.latLng.lat();
                document.getElementById("long").value = event.latLng.lng();
            });

        }

        var single_map = document.getElementById('singleMap');
        if (typeof(single_map) != 'undefined' && single_map != null) {
            google.maps.event.addDomListener(window, 'load', singleMap);
        }

    </script>
    <script type="text/javascript">
        $('.bookmark-button').on('click', function () {
            $.get('{{ route('handleBookmark') }}?type=job&id='+{{ $job->id }}).done(function(res) {
                if(res.bookmarked) {
                    $('.bookmark-button').addClass('bookmarked');
                }else {
                    $('.bookmark-button').removeClass('bookmarked');
                }
            }).fail(function(error) {

            });
        });

        $('#notEligible').on('click' , function (e) {
            e.preventDefault();
                $('.notEligible').show();
        });
    </script>

    @if(Auth::check())
    @include('Hired::modals.apply')
  @endif
@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection
