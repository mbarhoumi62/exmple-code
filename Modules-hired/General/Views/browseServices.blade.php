@extends('frontOffice.layout',['class' => 'gray','title' => __('Browse Services')])

@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'not-sticky'])
@endsection

@section('content')

    <!-- Page Content
  ================================================== -->
    <div class="full-page-container">

        <div class="full-page-sidebar">
            <div class="full-page-sidebar-inner" data-simplebar>
                <div class="sidebar-container">


                    <!-- Keywords -->
                    <div class="sidebar-widget">
                        <h3>{{__('Keyword')}}</h3>
                        <div>
                            <div class="keyword-input-container">
                                <input type="text" id="key"  placeholder="{{__('e.g. service name')}}"/>
                            </div>
                            <div class="keywords-list"><!-- keywords go here --></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>


                    <div class="sidebar-widget">
                        <h3>{{__('Category')}}</h3>
                        <select class="selectpicker default" id="categories" multiple data-selected-text-format="count" data-size="7" title="All Categories" >
                            @foreach (\App\Modules\General\Models\TaskCategory::all() as $category)
                                <option value="{{$category->id}}">{{$category->label}}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Hourly Rate -->
            

                    <!-- Tags -->
                    <div class="sidebar-widget">
                        <h3>{{__('Skills')}}</h3>

                        <div class="tags-container">
                             @foreach(\App\Modules\User\Models\Skill::whereNotNull('service_id')->groupBy('label')->inRandomOrder()->take(6)->get() as $key => $skill)
                            <div class="tag">
                                <input value="{{$skill->label}}" type="checkbox" id="tag{{$key+1}}"/>
                                <label for="tag{{$key+1}}">{{$skill->label}}</label>
                            </div>
                           @endforeach
                        </div>
                        <div class="clearfix"></div>

                        <!-- More Skills -->
                        <div class="keywords-container margin-top-20">
                            <div class="keyword-input-container">
                                <input type="text" class="keyword-input" placeholder="{{__('add more skills')}}"/>
                                <button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
                            </div>
                            <div class="keywords-list"><!-- keywords go here --></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                            <div class="sidebar-widget">
                        <h3>{{__('Service Cost')}}</h3>
                         <span>{{ __('This rang, represent the max and the min costs that are currently present and specified by owners.') }}</span>
                        <div class="margin-top-55"></div>

                        <!-- Range Slider -->
                        <input class="range-slider" id="costRange" type="text" value="" data-slider-currency="TND " data-slider-min="{{ \App\Modules\Hired\Models\Service::select('min')->min('min') }}" data-slider-max="{{  \App\Modules\Hired\Models\Service::select('max')->max('max') }}" data-slider-step="5" data-slider-value="{{ '['.\App\Modules\Hired\Models\Service::select('min')->min('min').','.\App\Modules\Hired\Models\Service::select('max')->max('max').']' }}"/>
                    </div>
                    <div class="clearfix"></div>

                    <div class="margin-bottom-40"></div>

                </div>
                <!-- Sidebar Container / End -->

                <!-- Search Button -->
                <div class="sidebar-search-button-container">
                    <button class="button ripple-effect handleFilterServices">{{__('Search')}}</button>
                </div>
                <!-- Search Button / End-->

            </div>
        </div>
        <!-- Full Page Sidebar / End -->

        <!-- Full Page Content -->
        <div class="full-page-content-container" data-simplebar>
            <div class="full-page-content-inner">

                <h3 class="page-title">{{__('Search Results')}}</h3>

                <div class="notify-box margin-top-15">
                    <div class="switch-container">
                        @if(Auth::check())
                            <label class="switch"><input type="checkbox" id="emailAlerts"><span class="switch-button"></span><span class="switch-text">
                               {{__('Turn on email alerts for this search')}}</span></label>
                        @else
                            <label class="switch"><input type="checkbox" disabled><span class="switch-button"></span><span class="switch-text">
                                    <a href="#sign-in-dialog" class="popup-with-zoom-anim">{{__('Log in')}}</a> {{__('to be able to turn on email alerts for your searchs.')}}</span></label>
                        @endif
                    </div>

                </div>


                    <div class="tasks-list-container listings-container tasks-grid-layout margin-top-35">

                    @foreach($services as $service)
                        <!-- Task -->
                            <a href="{{ route('showServiceDetails', $service->getServiceParamName()) }}" class="task-listing">

                                <!-- Job Listing Details -->
                                <div class="task-listing-details">

                                    <!-- Details -->
                                    <div class="task-listing-description">
                                        <h3 class="task-listing-title">{{ $service->name }}</h3>
                                        <ul class="task-icons">
                                            <li><i class="icon-feather-user"></i> {{ $service->user->getFullName() }} </li>
                                            <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($service->created_at)->diffForHumans() }}</li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="task-listing-bid">
                                    <div class="task-listing-bid-inner">
                                        <div class="task-offers">
                                            <strong>{{ $service->min.' TND' }} - {{ $service->max.' TND' }}</strong>
                                        </div>
                                        <span class="button button-sliding-icon ripple-effect">Book Now <i class="icon-material-outline-arrow-right-alt"></i></span>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                <!-- Pagination -->
                <div class="pagination-container margin-top-20 margin-bottom-20 ">
                    <nav class="pagination">
                        <ul>
                            <button class="button ripple-effect resetFilters" style="display: none;">{{__('Reset filters')}}</button>
                        </ul>
                    </nav>
                </div>
                <div class="clearfix"></div>
                @if($count > json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Services Items'])
                    <div class="pagination-container margin-top-20 margin-bottom-20 ">
                        <nav class="pagination">
                            <ul>
                                <button class="button ripple-effect loadMore">{{__('Load more')}}</button>
                            </ul>
                        </nav>
                    </div>
                @endif
                <div class="clearfix"></div>
                <!-- Pagination / End -->



            </div>
        </div>
        <!-- Full Page Content / End -->

    </div>

    <script>

        $(".keywords-container").each(function() {
            var keywordInput = $(this).find(".keyword-input");
            var keywordsList = $(this).find(".keywords-list");

            var count = $('.tags-container').find('.tag').length;

            function addKeyword() {
                count++;
                var $newKeyword = $('\t<div class="tag">\n' +
                    '  \t\t\t\t\t\t\t<input value="'+ keywordInput.val() +'" type="checkbox" checked id="tag'+ count +'"/>\n' +
                    '  \t\t\t\t\t\t\t<label for="tag'+ count +'"> '+ keywordInput.val() +' </label>\n' +
                    '  \t\t\t\t\t\t</div>');
                $('.tags-container').append($newKeyword).trigger('resizeContainer');
                keywordInput.val("");

            }
            keywordInput.on('keyup', function(e) {
                if ((e.keyCode == 13) && (keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $('.keyword-input-button').on('click', function() {
                if ((keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $(document).on("click", ".keyword-remove", function() {
                $(this).parent().addClass('keyword-removed');
                function removeFromMarkup() {

                    $(".keyword-removed").remove();

                }

                setTimeout(removeFromMarkup, 500);
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            keywordsList.on('resizeContainer', function() {
                var heightnow = $(this).height();
                var heightfull = $(this).css({
                    'max-height': 'auto',
                    'height': 'auto'
                }).height();
                $(this).css({
                    'height': heightnow
                }).animate({
                    'height': heightfull
                }, 200);
            });
            $(window).on('resize', function() {
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            $(window).on('load', function() {
                var keywordCount = $('.keywords-list').children("span").length;
                if (keywordCount > 0) {
                    keywordsList.css({
                        'height': 'auto'
                    }).height();
                }
            });
        });
        var offset = 0;
        $('.handleFilterServices').on('click', function () {
            $('.listings-container').html('<div class="loader"></div>');
            $('.loadMore').hide();
            $('.resetFilters').hide();
            offset = 0;
            var skills = [];
            var categories = [];
            var costRange = $('#costRange').val();
            var key = $('#key').val();
            $("#categories option:selected").each(function (i,item) {
                categories[i]  = item.value;
            });
            $('.tag input:checkbox:checked').each( function (i,item) {
                skills[i]  = item.value;
            });

            var  url = '{{ route('showServices') }}?offset='+offset+'&costRange='+costRange;
            var query = {
                'costRange=' :costRange
            }

            if(key != '') {
                url += '&key='+key;
                query.key = key;
            }

            if(skills.length != 0) {
                url += '&skills='+skills;
                query.skiils = skills;
            }


            if(categories.length != 0) {
                url += '&cat='+categories;
                query.cat = categories;
            }

            if($('#emailAlerts').is(':checked')) {
                $.get('{{ route('handleSubscribeForEmailAlerts') }}?type=service&query='+JSON.stringify(query)).done(function (res) {
                    console.log(res);
                });
            }

           $.get(url).done( function (res) {
                if(res.html.length !=0) {
                    $('.listings-container').html(res.html);
                    if(res.count == '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Services Items'] }}') {
                        $('.loadMore').show();
                    }
                }else {
                    $('.listings-container').html('<p> No services were found, please try to change the filters or reset them by clicking the button below. </p>');
                    $('.resetFilters').show();
                }


            }).fail(function (err) {
                $('.listings-container').html('<p> No services were found, please try to change the filters or reset them by clicking the button below. </p>');
                $('.resetFilters').show();
            });

            $("#emailAlerts").prop("checked", false);



        });


         $('.loadMore').on('click', function () {
         $('.loadMore').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>');
            offset++;
              var skills = [];
            var categories = [];
            var costRange = $('#costRange').val();
            var key = $('#key').val();
            $("#categories option:selected").each(function (i,item) {
                categories[i]  = item.value;
            });
            $('.tag input:checkbox:checked').each( function (i,item) {
                skills[i]  = item.value;
            });

            var  url = '{{ route('showServices') }}?offset='+offset+'&costRange='+costRange;
            var query = {
                'costRange=' :costRange
            }

            if(key != '') {
                url += '&key='+key;
                query.key = key;
            }

            if(skills.length != 0) {
                url += '&skills='+skills;
                query.skiils = skills;
            }


            if(categories.length != 0) {
                url += '&cat='+categories;
                query.cat = categories;
            }

            $.get(url).done( function (res) {
                $('.listings-container').append(res.html);
                if(res.count < '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Services Items'] }}') {
                    $('.loadMore').hide();
                }
                $('.loadMore').html('Load more');

            }).fail(function (err) {
                console.log(err);
            })
        });

        $('.resetFilters').on('click', function () {
            $('.listings-container').html('<div class="loader"></div>');
            $('.loadMore').hide();
            $('.resetFilters').hide();



            $('#key').val('');
            $("#categories").val('').change();
            $(".checkedType:checkbox").prop('checked',false);
            $(".tag input:checkbox").prop('checked',false);

            var  url = '{{ route('showServices') }}?offset=0&costRange=0,10000';
            $.get(url).done( function (res) {
                $('.listings-container').html(res.html);
                if(res.count == '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Services Items'] }}') {
                    $('.loadMore').show();
                }
            })

        })



    </script>
@endsection

