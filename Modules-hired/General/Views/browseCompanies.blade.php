@extends('frontOffice.layout',['class' => 'gray','title' => __('Browse Companies')])

@section('header')
@include('frontOffice.inc.header')
@endsection

@section('content')

  <div id="titlebar" class="gradient">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12">

  				<h2>{{ __('Browse Companies') }}</h2>

  				<!-- Breadcrumbs -->
  				<nav id="breadcrumbs" class="dark">
  					<ul>
  						<li><a href="{{ route('showHomePage') }}">{{ __('Home')}}</a></li>
  						<li><a href="#">{{ __('Find Work') }}</a></li>
  						<li>{{ __('Browse Companies') }}</li>
  					</ul>
  				</nav>

  			</div>
  		</div>
  	</div>
  </div>


  <!-- Page Content
  ================================================== -->
  <div class="container">
  	<div class="row">
  		<div class="col-xl-12">
  			<div class="letters-list">
  				<a href="#">A</a>
  				<a href="#">B</a>
  				<a href="#">C</a>
  				<a href="#">D</a>
  				<a href="#">E</a>
  				<a href="#">F</a>
  				<a href="#">G</a>
  				<a href="#">H</a>
  				<a href="#">I</a>
  				<a href="#">J</a>
  				<a href="#">K</a>
  				<a href="#">L</a>
  				<a href="#">M</a>
  				<a href="#">N</a>
  				<a href="#">O</a>
  				<a href="#">P</a>
  				<a href="#">Q</a>
  				<a href="#">R</a>
  				<a href="#">S</a>
  				<a href="#">T</a>
  				<a href="#">U</a>
  				<a href="#">V</a>
  				<a href="#">W</a>
  				<a href="#">X</a>
  				<a href="#">Y</a>
  				<a href="#">Z</a>
  			</div>
  		</div>
  		<div class="col-xl-12">
  			<div class="companies-list">
  		    	@foreach($companies as $company)
                    <a href="{{ route('showCompanyDetails',$company->getCompanyParamName()) }}" class="company">
                        <div class="company-inner-alignment">
                            <span class="company-logo"><img src="{{asset($company->medias()->where('type',0)->first() ? $company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png' )}}"></span>
                            <h4>{{ $company->name }}</h4>
                            @if(count($company->getAllRatings($company->id,'desc','App\Modules\User\Models\Company')) < 3)
                                <span class="company-not-rated">Minimum of 3 votes required</span>
                            @else
                                <div class="star-rating" data-rating="{{ $company->averageRating(1)[0] }}"></div>
                            @endif
                        </div>
                    </a>
                @endforeach
  			</div>
  		</div>
  	</div>
  </div>


  <!-- Spacer -->
  <div class="margin-top-70"></div>
  <!-- Spacer / End-->
  <script>
      $('.letters-list a').on('click', function (e) {
          e.preventDefault();
          $('.companies-list').html('<div class="loader"></div>');
              var letter =  $(this).text();
           $.get('{{ route('showBrowseCompanies') }}?letter='+letter).done( function (res) {
               if(res.html.length != 0) {
                   $('.companies-list').html(res.html);
               }else {
                   $('.companies-list').html('      <a href="#" class="company">\n' +
                       '                        <div class="company-inner-alignment">\n' +
                       '                            <h4>{{ __('No companies were found!') }}</h4>\n' +
                       '                        </div>\n' +
                       '                    </a>')
               }

           })
      })
  </script>

@endsection


@section('footer')
  @include('frontOffice.inc.footer')
@endsection

