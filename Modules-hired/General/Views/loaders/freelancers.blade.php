@foreach($freelancers as $freelancer)
    <!--Freelancer -->
    <div class="freelancer">

        <!-- Overview -->
        <div class="freelancer-overview">
            <div class="freelancer-overview-inner">

                <!-- Bookmark Icon -->
                @if(Auth::check() && Auth::id() != $freelancer->id)

                    <span class="bookmark-icon  {{ Auth::user()->hasBookmarked($freelancer) ? 'bookmarked' : '' }}" data-id="{{ $freelancer->id }}"></span>
            @endif
            <!-- Avatar -->
                <div class="freelancer-avatar">
                    <a href="{{  route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}"><img src="{{asset( $freelancer->medias()->where('type',0)->first() ? $freelancer->medias()->where('type',0)->first()->link :  'frontOffice/images/user-avatar-placeholder.png')}}" alt=""></a>
                </div>

                <!-- Name -->
                <div class="freelancer-name">
                    <h4><a href="{{  route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}"> {{ $freelancer->getFullName() }}</a></h4>
                    <span>{{ $freelancer->info->tagline }}</span>
                </div>

                <!-- Rating -->
                <div class="freelancer-rating">
                    @if(count($freelancer->getAllRatings($freelancer->id,'desc','App\Modules\User\Models\User')) < 3)
                       <span class="company-not-rated">{{__('Minimum of 3 votes required')}}</span>
                    @else
                        <div class="star-rating" data-rating="{{ $freelancer->averageRating(1)[0] }}"></div>
                    @endif
                </div>
            </div>
        </div>

        <!-- Details -->
        <div class="freelancer-details">
            <div class="freelancer-details-list">
                <ul>
                    <li>Address <strong><i class="icon-material-outline-location-on"></i> {{ $freelancer->address->city ?? $freelancer->address->province }}</strong></li>
                    <li>Hourly Rate <strong>{{ $freelancer->preference ? $freelancer->preference->hour_rate.' TND' : '---' }} / hr</strong></li>

                </ul>
            </div>
            <a href="{{  route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}" class="button button-sliding-icon ripple-effect">{{__('View Profile')}} <i class="icon-material-outline-arrow-right-alt"></i></a>
        </div>
    </div>
    <!-- Freelancer / End -->
@endforeach
