@foreach($companies as $company)
    <a href="{{ route('showCompanyDetails',$company->getCompanyParamName()) }}" class="company">
        <div class="company-inner-alignment">
            <span class="company-logo"><img src="{{asset($company->medias()->where('type',0)->first() ? $company->medias()->where('type',0)->first()->link :'frontOffice/images/company-logo-placeholder.png')}}" alt="{{$company->name}}"></span>
            <h4>{{ $company->name }}</h4>
            @if(count($company->getAllRatings($company->id,'desc','App\Modules\User\Models\Company')) < 3)
                <span class="company-not-rated">{{__('Minimum of 3 votes required')}}</span>
            @else
                <div class="star-rating" data-rating="{{ $company->averageRating(1)[0] }}"></div>
            @endif
        </div>
    </a>
@endforeach
