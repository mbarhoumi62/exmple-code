@extends('frontOffice.layout',['class' => 'gray','title' => __('Browse Tasks')])

@section('header')
@include('frontOffice.inc.header')
@endsection

@section('content')

    <style>
        .sliderMargin > .slider {
            margin-top: 15px;
        } 
    </style>
  <!-- Page Content
  ================================================== -->
  <div class="full-page-container">

  	<div class="full-page-sidebar">
  		<div class="full-page-sidebar-inner" data-simplebar>
  			<div class="sidebar-container">

  				<!-- Location -->
  				<div class="sidebar-widget sliderMargin">
  					<h3>{{__('Location')}}</h3>
  					<div class="input-with-icon">
  						<div id="autocomplete-container">
  							<input id="autocomplete-input" type="text" placeholder="{{__('Location')}}">
							<input type="hidden"  id="lat">
							<input type="hidden"  id="lon">
  						</div>
  						<i class="icon-material-outline-location-on"></i>
  					</div>
                    <small style="color: black">{{__('Raduis')}} (KM)</small>

                    <input class="range-slider-single" id="range" type="text"  data-slider-min="5" data-slider-max="50" data-slider-step="1" data-slider-value="20"/>

				</div>
                  <div class="sidebar-widget">

                        <div class="switches-list">

                            <div class="switch-container">
                                <label class="switch"><input value="0" class="remoteFilter" type="checkbox"><span class="switch-button"></span> {{ __('Show only remote tasks') }}</label>
                            </div>
                        </div>

                    </div>
  				<!-- Category -->
  				<div class="sidebar-widget">
  					<h3>{{__('Category')}}</h3>
  					<select class="selectpicker default" id="categories" multiple data-selected-text-format="count" data-size="7" title="All Categories" >
						@foreach (\App\Modules\General\Models\TaskCategory::all() as $category)
							<option value="{{$category->id}}">{{$category->label}}</option>
						@endforeach
  					</select>
  				</div>

  				<!-- Keywords -->
  				<div class="sidebar-widget">
  					<h3>{{__('Keyword')}}</h3>
  					<div class="keywords-container">
  						<div class="keyword-input-container">
  							<input type="text" id="key" class="keyword-input" placeholder="{{__('e.g. task title')}}"/>
  						</div>

  						<div class="clearfix"></div>
  					</div>
  				</div>

  				<!-- Budget -->
				<div class="sidebar-widget">
					<h3>{{__('Budget Type')}}</h3>

					<div class="switches-list">

						<div class="switch-container">
							<label class="switch"><input value="0" class="checkedType" type="checkbox"><span class="switch-button"></span> {{__('Fixed Price')}}</label>
						</div>

						<div class="switch-container">
							<label class="switch"><input value="1" class="checkedType" type="checkbox"><span class="switch-button"></span> {{__('Hourly Rate')}}</label>
						</div>

					</div>

				</div>

  				<!-- Tags -->
  				<div class="sidebar-widget">

  				</div>
  				<div class="clearfix"></div>

  				<div class="margin-bottom-40"></div>

  			</div>
  			<!-- Sidebar Container / End -->

  			<!-- Search Button -->
  			<div class="sidebar-search-button-container">
  				<button class="button ripple-effect handleFilterTasks">{{__('Search')}}</button>
  			</div>
  			<!-- Search Button / End-->

  		</div>
  	</div>
  	<!-- Full Page Sidebar / End -->

  	<!-- Full Page Content -->
  	<div class="full-page-content-container" data-simplebar>
  		<div class="full-page-content-inner">

  			<h3 class="page-title">{{__('Search Results')}}</h3>

  			<div class="notify-box margin-top-15">
                <div class="switch-container">
                    @if(Auth::check())
                        <label class="switch"><input type="checkbox" id="emailAlerts"><span class="switch-button"></span><span class="switch-text">
                                {{__('Turn on email alerts for this search')}}</span></label>
                    @else
                        <label class="switch"><input type="checkbox" disabled><span class="switch-button"></span><span class="switch-text">
                                    <a href="#sign-in-dialog" class="popup-with-zoom-anim">{{__('Log in')}}</a> {{__('to be able to turn on email alerts for your searchs.')}}</span></label>
                    @endif
                </div>

  			</div>

  			<!-- Tasks Container -->
  			<div class="tasks-list-container listings-container tasks-grid-layout margin-top-35">
                 @if(count($tasks) == 0)   
                     <p> {{ __('No tasks were found, please try to change the filters or reset them by clicking the button below.')}} </p>
                        @else
                @foreach($tasks as $task)
  				<!-- Task -->
  				<a href="{{ route('showTaskDetails', $task->getTaskParamName()) }}" class="task-listing">

  					<!-- Job Listing Details -->
  					<div class="task-listing-details">

  						<!-- Details -->
  						<div class="task-listing-description">
  							<h3 class="task-listing-title">{{ $task->name }}</h3>
  							<ul class="task-icons">
  								<li><i class="icon-material-outline-location-on"></i> {{ $task->address ? $task->address->address :'Remote'  }} </li>
  								<li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($task->created_at)->diffForHumans() }}</li>
  							</ul>
  						</div>

  					</div>

  					<div class="task-listing-bid">
  						<div class="task-listing-bid-inner">
  							<div class="task-offers">
  								<strong>{{ $task->budget->min.' TND' }} - {{ $task->budget->max.' TND' }}</strong>
  								<span>{{ $task->budget->type == 0 ? 'Fixed Price' : 'Hourly Price' }}</span>
  							</div>
  							<span class="button button-sliding-icon ripple-effect">Bid Now <i class="icon-material-outline-arrow-right-alt"></i></span>
  						</div>
  					</div>
  				</a>
                   @endforeach
                   @endif
  			</div>
  			<!-- Tasks Container / End -->
            <div class="pagination-container margin-top-20 margin-bottom-20 ">
                <nav class="pagination">
                     @if(count($tasks) ==0) 
                        <ul>
                            <button class="button ripple-effect resetFilters" >{{ __('Reset filters') }}</button>
                        </ul>
                        @else
                           <ul>
                            <button class="button ripple-effect resetFilters" style="display: none;">{{ __('Reset filters') }}</button>
                        </ul>
                        @endif
                </nav>
            </div>
  			<!-- Pagination -->
  			<div class="clearfix"></div>
			@if($count > json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Tasks Items'])
				<div class="pagination-container margin-top-20 margin-bottom-20 ">
					<nav class="pagination">
						<ul>
							<button class="button ripple-effect loadMore">{{__('Load more')}}</button>
						</ul>
					</nav>
				</div>
			@endif
  			<div class="clearfix"></div>
  			<!-- Pagination / End -->


  		</div>
  	</div>
  	<!-- Full Page Content / End -->

  </div>
  <script>

            $("#autocomplete-input").keyup(function() {

    if (!this.value) {
        $('#lat').val();
        $('#lon').val();
    }

});
      var offset = 0;

      $('.handleFilterTasks').on('click', function () {
          $('.listings-container').html('<div class="loader"></div>');
          $('.loadMore').hide();
          $('.resetFilters').hide();
          offset = 0;
          var categories = [];
          var budgetTypes = [];

          var key = $('#key').val();
          $('.checkedType:checkbox:checked').each( function (i,item) {
              budgetTypes[i]  = item.value;
          });
          var range = $('#range').val();
          $("#categories option:selected").each(function (i,item) {
              categories[i]  = item.value;
          });
          var lat = $('#lat').val();
          var lon =  $('#lon').val();

          var  url = '{{ route('showTasks') }}?offset='+offset;
          var query = {};


          if(lat != '') {
              url += '&lat='+lat+'&lon='+lon+'&range='+range;
              query.lat = lat;
              query.lon = lat;
              query.range = range;
          }


          if(key != '') {
              url += '&key='+key;
              query.key = key;
          }

          if(categories.length != 0) {
              url += '&cat='+categories;
              query.cat = categories;
          }

          if(budgetTypes.length != 0) {
              url += '&type='+budgetTypes;
              query.type = budgetTypes;
          }

             if($('.remoteFilter').is(':checked')) {
                    url += '&remote=yes'; 
            }

          if($('#emailAlerts').is(':checked')) {
              $.get('{{ route('handleSubscribeForEmailAlerts') }}?type=task&query='+JSON.stringify(query)).done(function (res) {
              });
          }

          $.get(url).done( function (res) {
              if(res.html.length !=0) {
                  $('.listings-container').html(res.html);
                  if(res.count == '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Tasks Items'] }}') {
                      $('.loadMore').show();
                  }
              }else {
                  $('.listings-container').html('<p> No tasks were found, please try to change the filters or reset them by clicking the button below. </p>');
                  $('.resetFilters').show();
              }


          }).fail(function (err) {
              $('.listings-container').html('<p> No tasks were found, please try to change the filters or reset them by clicking the button below. </p>');
              $('.resetFilters').show();
          })

      })

      $('.loadMore').on('click', function () {
          $('.loadMore').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>');
          offset++;
          var categories = [];
          var budgetTypes = [];

          var key = $('#key').val();
          $('.checkedType:checkbox:checked').each( function (i,item) {
              budgetTypes[i]  = item.value;
          });
          var range = $('#range').val();
          $("#categories option:selected").each(function (i,item) {
              categories[i]  = item.value;
          });
          var lat = $('#lat').val();
          var lon =  $('#lon').val();

          var  url = '{{ route('showTasks') }}?offset='+offset;


          if(lat != '') {
              url += '&lat='+lat+'&lon='+lon+'&range='+range;
          }

          if(key != '') {
              url += '&key='+key
          }

          if(categories.length != 0) {
              url += '&cat='+categories;
          }

          if(budgetTypes.length != 0) {
              url += '&type='+budgetTypes;
          }

             if($('.remoteFilter').is(':checked')) {
                    url += '&remote=yes'; 
            }

          $.get(url).done( function (res) {
              $('.listings-container').append(res.html);
              if(res.count < '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Tasks Items'] }}') {
                  $('.loadMore').hide();
              }
              $('.loadMore').html('Load more');

          }).fail(function (err) {
              console.log(err);
          });
          $("#emailAlerts").prop("checked", false);
      });

      $('.resetFilters').on('click', function () {
          $('.listings-container').html('<div class="loader"></div>');
          $('.loadMore').hide();
          $('.resetFilters').hide();



          $('#key').val('');
          $("#categories").val('').change();
          $(".checkedType:checkbox").prop('checked',false);
          $(".remoteFilter:checkbox").prop('checked',false);
          $('#lat').val('');
          $('#lon').val('');
          $('#autocomplete-input').val('');

          var  url = '{{ route('showTasks') }}?offset=0';
          $.get(url).done( function (res) {
              $('.listings-container').html(res.html);
              if(res.count == '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Tasks Items'] }}') {
                  $('.loadMore').show();
              }
          })

      })
  </script>

  <script type="text/javascript">
      function initAutocomplete() {
          var input = document.getElementById('autocomplete-input');
          var autocomplete = new google.maps.places.Autocomplete(input);
          autocomplete.addListener('place_changed', function() {
              var placeUser = autocomplete.getPlace();
              var lat = placeUser.geometry.location.lat();
              var lng = placeUser.geometry.location.lng();
              $('#lat').val(lat);
              $('#lon').val(lng);
          });
      }
  </script>

  <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>

@endsection


