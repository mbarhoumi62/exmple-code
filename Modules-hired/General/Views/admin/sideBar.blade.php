<!-- Dashboard Sidebar
================================================== -->
<div class="dashboard-sidebar">
    <div class="dashboard-sidebar-inner" data-simplebar>
        <div class="dashboard-nav-container">

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger">
        <span class="hamburger hamburger--collapse">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </span>
                <span class="trigger-title">Dashboard Navigation</span>
            </a>

            <!-- Navigation -->
            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    <ul data-submenu-title="Start">
                        <li ><a href="{{route('showAdminDashboard')}}"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
                    </ul>

                    <ul data-submenu-title="Blog">
                            <li><a href="#"><i class="icon-material-outline-speaker-notes"></i> Posts</a>
                                <ul>
                                    <li><a href="{{ route('showManageCategories') }}">Manage Categories </a></li>
                                    <li><a href="{{ route('showManagePosts') }}">Manage Posts</a></li>
                                    <li><a href="{{ route('showAddArticle') }}">Add New Post</a></li>
                                </ul>
                            </li>

                    </ul>
                     <ul data-submenu-title="Genaral">
                              <li><a href="{{route('showGeneralSettings')}}"><i class="icon-material-outline-settings"></i> General Settings</a></li>

                            <li><a href="#"><i class="icon-material-outline-speaker-notes"></i> Categories</a>

                                <ul>
                                    <li><a href="{{ route('showJobCategories') }}">Job Categories </a></li>
                                    <li><a href="{{ route('showTaskCategories') }}">Task Categories</a></li>
                                </ul>
                            </li>

                    </ul>
                     <ul data-submenu-title="Translation">
                            <li><a href="{{ url('translations') }}"><i class="icon-line-awesome-language"></i> Translation</a>
                                                          </li>

                    </ul>
                    <ul data-submenu-title="Account">

                        <li><a href="{{route('showSettings')}}"><i class="icon-material-outline-settings"></i> Settings</a></li>
                        <li><a href="#"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
            <!-- Navigation / End -->

        </div>
    </div>
</div>
<!-- Dashboard Sidebar / End -->


