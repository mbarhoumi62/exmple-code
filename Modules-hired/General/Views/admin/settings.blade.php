@extends('frontOffice.layout',['class' => 'gray','title' => 'General Settings'])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')

<style type="text/css">
  #reset-this-root {
    all: initial;
    * {
        all: unset;
    }

}
</style>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/jsoneditor/8.6.3/jsoneditor.min.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jsoneditor/8.6.3/jsoneditor-minimalist.js"></script>

    <div class="dashboard-container">
        @include('General::admin.sideBar')
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

                <div class="dashboard-headline">
                     <h3>{{ __('General Settings') }}</h3>

                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{__('Home')}}</a></li>
                            <li>{{ __('General Settings') }}</li>
                        </ul>
                    </nav>
                </div>

                <div class="row" style="padding-bottom: 75px">
                	<span id="status"></span>
                	<div id="jsoneditor" style="width: 100%; height: 300px;"></div>
                	<button style="margin-top: 20px" id="saveJson" class="button button-sliding-icon ripple-effect">Save <i class="icon-material-outline-arrow-right-alt"></i> </button>
                </div>
        <form action="{{ route('handleUpdateLogos') }}" method="post" enctype="multipart/form-data">
          @csrf
                <div class="row" style="padding-bottom: 75px">

                       <div class="col-xl-4">

                         <img  style="max-width: 50%; margin-bottom: 15px" src="{{ asset(\App\Modules\User\Models\Media::where('type' , 100)->first()?  \App\Modules\User\Models\Media::where('type' , 100)->first()->link : "") }}" alt="{{ __('Hired Logo') }}" />
                         <input type="file" accept="image/*" id="reset-this-root" name="headerLogo" />

                    </div>
                    <div class="col-xl-4">
                       <img style="max-width: 50%; margin-bottom: 15px"  src="{{ asset(\App\Modules\User\Models\Media::where('type' , 101)->first()?  \App\Modules\User\Models\Media::where('type' , 100)->first()->link : "") }}" alt="{{ __('Hired Logo') }}" />
                         <input type="file" accept="image/*" id="reset-this-root" name="footerLogo" />
                    </div>
                    <div class="col-xl-4">
                       <img style="max-width: 17%; margin-bottom: 15px"  src="{{ asset(\App\Modules\User\Models\Media::where('type' , 102)->first() ?  \App\Modules\User\Models\Media::where('type' , 100)->first()->link : "") }}" alt="{{ __('Hired Logo') }}" />
                         <input type="file" accept="image/*" id="reset-this-root" name="svg" />
                    </div>


                 <button type="submit" style="margin-top: 20px" class="button button-sliding-icon ripple-effect">Save changes</button>
                </div>
              </form>
    </div>
    </div>
    </div>

      <script>


      	var updatedSettings = {};
		const container = document.getElementById("jsoneditor")
         const options = {
    mode: 'tree',
    modes: ['code', 'form', 'text', 'tree', 'view', 'preview'], // allowed modes
    onError: function (err) {
      alert(err.toString())
    },
    onModeChange: function (newMode, oldMode) {
      console.log('Mode switched from', oldMode, 'to', newMode)
    }
  }
        const editor = new JSONEditor(container, options)

      	$.get('{{ route('apiGetSettings') }}').done(function (settings) {
        editor.set(settings);

      	}).fail(function (err) {
      		console.log(err);
      	});

        $('#saveJson').on('click', function () {
        	var btn = $(this);
        $(btn).html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $(btn).css('opacity','0.5');
        $(btn).css('cursor','not-allowed');
        $(btn).attr("disabled", true);
        var updatedSettings = JSON.stringify(editor.get());

        $.get('{{ route('apiSaveSettings') }}?settings='+updatedSettings).done(function (res) {
       	 $(btn).html('Save <i class="icon-material-outline-arrow-right-alt"></i> ')
        $(btn).css('opacity','1');
        $(btn).css('cursor','pointer');
        $(btn).attr("disabled", false);

        $('#status').html('{{ __('Setting updated successfully') }}').fadeIn('slow').delay(2000).fadeOut();;

      	}).fail(function (err) {
      		$('#status').html('{{ __('Something went wrong, please try again') }}').fadeIn('slow').delay(2000).fadeOut();;
      	});

        })

    </script>
@endsection
