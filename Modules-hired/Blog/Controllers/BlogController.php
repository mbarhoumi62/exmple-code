<?php

namespace App\Modules\Blog\Controllers;

use App\Modules\Blog\Models\Comment;
use App\Modules\Blog\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Modules\Blog\Models\Category;
use App\Modules\Blog\Models\Article;
use Auth;
class BlogController extends Controller
{

    public function showBlog(Request $request)
    {

           if ($request->has('key')) { 

               $featured = Article::where('status',0)->get();
               $standard =  Article::where('status' , 1 , function($q) use($request) {
                $q->where('title', 'like', '%' . $request->key . '%')->orWhere('content', 'like', '%' . $request->key . '%');
               })->paginate(4);
               return view("Blog::index", [
                   'featured' => $featured,
                   'standard' => $standard
               ]);

           }



           if($request->has('tag')) {

               $featured = Article::where('status',0)->get();
               $tag = Tag::where('name',$request->tag)->first();
               $standard = $tag->articles()->where('status' , 1)->paginate(4);
               return view("Blog::index", [
                   'featured' => $featured,
                   'standard' => $standard
               ]);



           }



        $featured = Article::where('status',0)->get();
        $standard = Article::where('status',1)->paginate(4);
        return view("Blog::index", [
            'featured' => $featured,
            'standard' => $standard
        ]);
    }

    public function showPost($title)
    {
        $param = explode("-",$title);
        $id = ((int)end($param) - 9371) / 33 ;
        $post = Article::find($id);
        if(!$post) {
            Session::flash('message', 'Something went wrong, please try again!');
            Session::flash('color', '#de5959');
            return back();
        }

        return view("Blog::post",[
            'post' => $post,
            'related' => Article::where('id','!=',$id)->where('category_id',$post->category_id)->get()
        ]);
    }

    public function showManageCategories() {

        return view("Blog::backOffice.categories", [
            'categories' => Category::all()
        ]);
    }

    public function handleAddCategory() {
        $data = Input::all();

        $rules = [
            'name' => 'required'
        ];

        $messages = [
            'name.required' => 'Category name is required'
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        Category::create([
            'name' => $data['name']
        ]);
        Session::flash('message', 'Catgeory created');
        Session::flash('color', '#5f9025');
        return back();
    }

    public function handleDeleteCategory(Request $request) {
        $category = Category::find($request->cId);

        if(!$category) {
            Session::flash('message', 'Something went wrong, please try again!');
            Session::flash('color', '#de5959');
        }

        $category->delete();
        Session::flash('message', 'Category deleted!');
        Session::flash('color', '#5f9025');
        return back();
    }

    public function showAddArticle()
    {
        return view('Blog::backOffice.newPost',[
            'categories' => Category::All()
        ]);
    }

    public function handleAddPost(){
        $data = Input::all();
        $rules = [
            'title'       => 'required',
            'content'     => 'required',
            'category'      => 'required',
            'featuredImage'      => 'required',
        ];

        $messages = [
            'title.required'        => 'Title Field Is Required !',
            'content.required'      => 'Content Field Is Required !',
            'category.required'       => 'Category Field Is Required !',
            'featuredImage.required'       => 'Featured Image Is Required !'
        ];
        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }


        $content = $data['content'];

        $dom = new \domdocument();
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');

        foreach($images as $k => $img){

            $dataImg = $img->getattribute('src');

            list(, $dataImg) = explode(';', $dataImg);
            list(, $dataImg)      = explode(',', $dataImg);

            $dataImg = base64_decode($dataImg);
            $image_name= time().$k.'.png';
            $path = public_path('storage/uploads/blog/content') .'/'. $image_name;

            file_put_contents($path, $dataImg);

            $img->removeattribute('src');
            $img->setattribute('src', url('storage/uploads/blog/content') .'/'. $image_name);
        }

        $content = $dom->savehtml();

        $status = 3;

        if(isset($data['featured']) && isset($data['published'])) {
            $status = 0;
        }

        if(isset($data['featured']) && !isset($data['published'])) {
            $status = 2;
        }

        if(!isset($data['featured']) && isset($data['published'])) {
            $status = 1;
        }

        $article = Article::create([
            'title' => $data['title'],
            'content' => $content,
            'status' => $status,
            'user_id' => Auth::id(),
            'category_id' => $data['category']
        ]);
        $featuredLink  = uploadFile($data['featuredImage'], 'featuredPostImage');

        $article->medias()->create([
            'type' => 10,
            'label' => $data['featuredImage']->getClientOriginalName(),
            'link' => $featuredLink
        ]);

        $tags = explode(',', $data['tags']);

            foreach ($tags as $tag) {

                $article->addTag($tag);

            }
            Session::flash('message', 'Post created');
        Session::flash('color', '#5f9025');
        return redirect()->route('showManagePosts');


    }

    public function showManagePosts() {

        return view('Blog::backOffice.posts',[
            'posts' => Article::all()
        ]);
    }

    public function showUpdatePost($param) {
        try {
            $postId = decrypt($param);
        } catch (DecryptException $e) {
            return back();
        }
        $post = Article::find($postId);

        if(!$post) {
            return back();
        }

        return view('Blog::backOffice.editPost', [
            'post' => $post,
            'categories' => Category::all()
        ]);
    }

    public function handleUpdatePost($param) {
        $post = Article::find($param);

        if(!$post) {
            return back();
        }

        $data = Input::all();

        $rules = [
            'title'       => 'required',
            'content'     => 'required',
            'category'      => 'required'
        ];

        $messages = [
            'title.required'        => 'Title Field Is Required !',
            'content.required'      => 'Content Field Is Required !',
            'category.required'       => 'Category Field Is Required !'
        ];
        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            dd($validation->errors());
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }


        $content = $data['content'];

        $dom = new \domdocument();
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');

        foreach($images as $k => $img){

            $dataImg = $img->getattribute('src');

            if($dataImg[0] != '.') {

                list(,$dataImg) = explode(';', $dataImg);
                list(,$dataImg)      = explode(',', $dataImg);

                $dataImg = base64_decode($dataImg);
                $image_name= time().$k.'.png';
                $path = public_path('storage/uploads/blog/content') .'/'. $image_name;

                file_put_contents($path, $dataImg);

                $img->removeattribute('src');
                $img->setattribute('src', url('storage/uploads/blog/content') .'/'. $image_name);
            }

        }

        $content = $dom->savehtml();

        $status = 3;

        if(isset($data['featured']) && isset($data['published'])) {
            $status = 0;
        }

        if(isset($data['featured']) && !isset($data['published'])) {
            $status = 2;
        }

        if(!isset($data['featured']) && isset($data['published'])) {
            $status = 1;
        }

        $post->update([
            'title' => $data['title'],
            'content' => $content,
            'status' => $status,
            'user_id' => Auth::id(),
            'category_id' => $data['category']
        ]);

        if(isset($data['featuredImage'])) {
            $featuredLink  = uploadFile($data['featuredImage'], 'featuredPostImage');

            $post->medias()->create([
                'type' => 10,
                'label' => $data['featuredImage']->getClientOriginalName(),
                'link' => $featuredLink
            ]);
        }
        $post->tags()->detach();

        $tags = explode(',', $data['tags']);

        foreach ($tags as $tag) {
                    if($tag != '') {
                        $post->addTag($tag);
                    }

        }

        Session::flash('message', 'Post updated');
        Session::flash('color', '#5f9025');
        return redirect()->route('showManagePosts');
    }

    public function apiUpdatePostStatus(Request $request) {
        $post = Article::find($request->get('id'));
        if(!$post) {
            return response()->json(['status' => 404]);
        }

        switch ($post->status) {
            case 0 :
                $post->update([
                    'status' => 2
                ]);
                return response()->json(['unpublished' => 200]);
            case 1 :
                $post->update([
                    'status' => 3
                ]);
                return response()->json(['unpublished' => 200]);
            case 2 :
                $post->update([
                    'status' => 0
                ]);
                return response()->json(['published' => 200]);
            case 3 :
                $post->update([
                    'status' => 1
                ]);
                return response()->json(['published' => 200]);
        }

    }

    public function apiUpdatePostFeaturedStatus(Request $request) {
        $post = Article::find($request->get('id'));
        if(!$post) {
            return response()->json(['status' => 404]);
        }

        switch ($post->status) {
            case 0 :
                $post->update([
                    'status' => 1
                ]);
                return response()->json(['standard' => 200]);
            case 1 :
                $post->update([
                    'status' => 0
                ]);
                return response()->json(['featured' => 200]);
            case 2 :
                $post->update([
                    'status' => 3
                ]);
                return response()->json(['standard' => 200]);
            case 3 :
                $post->update([
                    'status' => 2
                ]);
                return response()->json(['featured' => 200]);
        }

    }

    public function apiHandlePostComment(Request $request) {

        $comment = Comment::create([
            'full_name' => isset($request->data['name']) ? $request->data['name'] : null,
            'email' => isset($request->data['email']) ? $request->data['email'] : null,
            'content' => $request->data['commentContent'],
            'user_id' => Auth::check() ? Auth::id() : null,
            'article_id' => $request->data['post'],
            'comment_id' => isset($request->data['comment']) ? $request->data['comment'] : null
        ]);

        $res = [
                'img' => asset( $comment->user ? $comment->user->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png'),
                'name' => $comment->user ? $comment->user->getFullName() : $comment->full_name,
                'date' => $comment->created_at->format('d M, Y'),
                'content' => $comment->content,
                'id' => isset($request->data['comment']) ? $request->data['comment'] : null
        ];

        return response()->json(['status' => 200 , 'comment' => $res,'level' => $comment->comment_id != null ? 'two' : 'one', 'fC' => $comment->comment_id ?? '']);

    }


}
