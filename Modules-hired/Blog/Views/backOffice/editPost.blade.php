@extends('frontOffice.layout',['class' => 'gray','title' => 'Update '.$post->title])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
    <script src="https://cdn.tiny.cloud/1/893s7vt8btcy1m4zri10cir98u12ch4h4ttunu2pvsk9btt0/tinymce/5/tinymce.min.js"></script>
    <script>tinymce.init({
            selector: 'textarea#full-featured',
            height: 500,
            menubar: true,
            paste_data_images: false,
            file_picker_types: 'image',
            plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            automatic_uploads: true,
            /*
              URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
              images_upload_url: 'postAcceptor.php',
              here we add custom filepicker only to Image dialog
            */
            file_picker_types: 'image',
            /* and here's our custom image picker*/
            file_picker_callback: function (cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                /*
                  Note: In modern browsers input[type="file"] is functional without
                  even adding it to the DOM, but that might not be the case in some older
                  or quirky browsers like IE, so you might want to add it to the DOM
                  just in case, and visually hide it. And do not forget do remove it
                  once you do not need it anymore.
                */

                input.onchange = function () {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function () {
                        /*
                          Note: Now we need to register the blob in TinyMCEs image blob
                          registry. In the next release this part hopefully won't be
                          necessary, as we are looking to handle it internally.
                        */
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        /* call the callback and populate the Title field with the file name */
                        cb(blobInfo.blobUri(), { title: file.name });
                    };
                    reader.readAsDataURL(file);
                };

                input.click();
            }
        });</script>
    <div class="dashboard-container">
        @include('General::admin.sideBar')
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

                <div class="dashboard-headline">
                    <h3>Add New Post</h3>

                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Blog</a></li>
                            <li>New Post</li>
                        </ul>
                    </nav>
                </div>

                <div class="row">
                    <form action="{{ route('handleUpdatePost',$post->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <!-- Dashboard Box -->
                        <div class="col-xl-12">
                            <div class="dashboard-box margin-top-0">

                                <div class="headline">
                                    <h3><i class="icon-feather-folder-plus"></i> Update : {{ $post->title }}</h3>
                                </div>

                                <div class="content with-padding padding-bottom-10">
                                    <div class="row">
                                        <div class="col-xl-4" >
                                            <div class="avatar-wrapper" style="width: auto; height: auto" data-tippy-placement="bottom" title="Featured Image">
                                                <img class="profile-pic" src="{{ asset($post->medias()->where('type',10)->first()->link)}}" alt="{{ $post->title }}" />
                                                <div class="upload-button"></div>
                                                <input name="featuredImage" class="file-upload" type="file" accept="image/*" />

                                            </div>
                                            @if ($errors->has('image'))
                                                <small>{{ $errors->first('image') }}</small>
                                            @endif
                                        </div>
                                        <div class="col-xl-8">
                                            <div class="submit-field">
                                                <h5>Post Title</h5>
                                                <input name="title" type="text" value="{{ $post->title }}" class="with-border" placeholder="Title">
                                                @if ($errors->has('title'))
                                                    <small>{{ $errors->first('title') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>Category</h5>
                                                <select name="category" class="selectpicker with-border" data-size="7" title="Select Category">
                                                    @foreach ($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('category'))
                                                    <small>{{ $errors->first('category') }}</small>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>Tags <i class="help-icon" data-tippy-placement="right" title="Up to 6 tags"></i> <small id="maxSkills"></small></h5>
                                                <div class="keywords-container">
                                                    <div class="keyword-input-container">
                                                        <input  type="text" id="skillsInput" class="keyword-input with-border" placeholder="Add Tags"/>
                                                        @if ($errors->has('tags'))
                                                            <small>{{ $errors->first('tags') }}</small>
                                                        @endif
                                                        @php
                                                            $oldSkills = '';
                                                            foreach($post->tags as $tag)
                                                            $oldSkills .=$tag->name.',';
                                                        @endphp
                                                        <input value="{{  substr($oldSkills, 0, -1) }}" type="hidden" id="taskSkills" name="tags">
                                                        <a class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></a>

                                                    </div>
                                                    @php
                                                        $oldSkills = null;
                                                        if(old('tags'))  $oldSkills = explode(',',old('tags'));
                                                    @endphp
                                                    <div class="keywords-list">
                                                        @foreach($post->tags as $tag)
                                                            <span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>{{ $tag->name }}</span></span>
                                                        @endforeach
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-xl-12">
                                            <div class="submit-field">
                                                <h5>Content</h5>
                                                <textarea name="content" id="full-featured" cols="30" rows="3" class="with-border">{!! $post->content  !!}</textarea>
                                                @if ($errors->has('content'))
                                                    <small>{{ $errors->first('content') }}</small>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="col-xl-12">
                                            <div class="switches-list">
                                                <div class="switch-container">
                                                    <label class="switch"><input name="featured" value="1" type="checkbox" @if($post->status == 0 || $post->status == 2) checked @endif ><span class="switch-button"></span>Featured ?</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-12">
                                            <div class="switches-list">
                                                <div class="switch-container">
                                                    <label class="switch"><input name="published" value="1" type="checkbox" @if($post->status == 0 || $post->status == 1) checked @endif><span class="switch-button"></span>Publish ?</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12">
                            <button type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function() {
            const oldCategory = '{{ $post->category_id   }}';

            if(oldCategory !== '') {
                $('select').val(oldCategory).change();
            }
        });

        $('#skillsInput').focus(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

        });

        $('#skillsInput').focusout(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    $('form').submit();

                }
            });
        });
        $(".keywords-container").each(function() {
            var skills = []  ;
            skills = $('#taskSkills').val().split(',');


            var keywordInput = $(this).find(".keyword-input");
            var keywordsList = $(this).find(".keywords-list");

            function addKeyword() {
                if(skills.length < 6 ) {
                    var $newKeyword = $("<span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>" + keywordInput.val() + "</span></span>");
                    keywordsList.append($newKeyword).trigger('resizeContainer');
                    skills.push(keywordInput.val());
                    $('#taskSkills').val(skills);
                }else {
                    $('#maxSkills').text('You can just add 6 tags!').fadeIn('slow').delay(2000).fadeOut();
                }
                keywordInput.val("");
            }
            keywordInput.on('keyup', function(e) {
                if ((e.keyCode == 13) && (keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $('.keyword-input-button').on('click', function() {
                if ((keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $(document).on("click", ".keyword-remove", function() {
                $(this).parent().addClass('keyword-removed');
                function removeFromMarkup() {
                    skills = $.grep(skills, function(value) {
                        return value !=  $(".keyword-removed").text();
                    });
                    $('#taskSkills').val(skills);
                    $(".keyword-removed").remove()
                }
                setTimeout(removeFromMarkup, 500);
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            keywordsList.on('resizeContainer', function() {
                var heightnow = $(this).height();
                var heightfull = $(this).css({
                    'max-height': 'auto',
                    'height': 'auto'
                }).height();
                $(this).css({
                    'height': heightnow
                }).animate({
                    'height': heightfull
                }, 200);
            });
            $(window).on('resize', function() {
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            $(window).on('load', function() {
                var keywordCount = $('.keywords-list').children("span").length;
                if (keywordCount > 0) {
                    keywordsList.css({
                        'height': 'auto'
                    }).height();
                }
            });
        });
    </script>
@endsection
