@extends('frontOffice.layout',['title' => 'Hired Blog'])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')
    <style>
        .pagination ul li span {
            color: white;
            transition: all 200ms ease-in-out;
            font-size: 14px;
            float: left;
            display: inline-block;
            border-radius: 4px;
            width: 44px;
            height: 44px;
            padding: 0;
            line-height: 44px;
            box-shadow: 0 2px 8px rgba(42,65,232,.25);
            background-color: #2a41e8;
            font-weight: 700;
            margin: 0;
            border-bottom: none;
        }
        .pagination ul .disabled span {
            background-color: black!important;
        }
    </style>
    <div id="titlebar" class="white margin-bottom-30">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Blog</h2>
                    <span>Featured Posts</span>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">Home</a></li>
                            <li>Blog</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!-- Recent Blog Posts -->
    <div class="section white padding-top-0 padding-bottom-60 full-width-carousel-fix">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="blog-carousel">
                        @foreach($featured as $post)
                        <a href="{{ route('showPost', $post->getArticleParamTitle()) }}" class="blog-compact-item-container">
                            <div class="blog-compact-item">
                                <img src="{{ asset($post->medias()->where('type',10)->first()->link) }}" alt="{{ $post->title }}">
                                <span class="blog-item-tag">{{ $post->category->name }}</span>
                                <div class="blog-compact-item-content">
                                    <ul class="blog-post-tags">
                                        <li>{{ $post->created_at->format('d M, Y') }}</li>
                                    </ul>
                                    <h3>{{ $post->title }}</h3>
                                </div>
                            </div>
                        </a>
                            @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Recent Blog Posts / End -->


    <!-- Section -->
    <div class="section gray">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8">

                    <!-- Section Headline -->
                    <div class="section-headline margin-top-60 margin-bottom-35">
                        <h4>Recent Posts</h4>
                    </div>
                    <div class="standardPosts">
                     @foreach($standard as $post)
                    <!-- Blog Post -->
                    <a href="{{ route('showPost', $post->getArticleParamTitle()) }}" class="blog-post">
                        <!-- Blog Post Thumbnail -->
                        <div class="blog-post-thumbnail">
                            <div class="blog-post-thumbnail-inner">
                                <span class="blog-item-tag">{{ $post->category->name }}</span>
                                <img src="{{ asset($post->medias()->where('type',10)->first()->link) }}" alt="{{ $post->title }}">
                            </div>
                        </div>
                        <!-- Blog Post Content -->
                        <div class="blog-post-content">
                            <span class="blog-post-date">{{  $post->created_at->format('d M, Y') }}</span>
                            <h3>{{ $post->title }}</h3>
                        </div>
                        <!-- Icon -->
                        <div class="entry-icon"></div>
                    </a>
                @endforeach
                    </div>
                    <!-- Blog Post -->

                    <!-- Pagination -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Pagination -->
                            <div class="pagination-container margin-top-10 margin-bottom-20">
                                <nav class="pagination">
                                    {{ $standard->appends(request()->input())->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- Pagination / End -->

                </div>


                <div class="col-xl-4 col-lg-4 content-left-offset">
                    <div class="sidebar-container margin-top-65">

                        <!-- Location -->
                        <div class="sidebar-widget margin-bottom-40">
                            <div class="input-with-icon">
                                <input id="searchPosts"  type="text" placeholder="Search">
                                <i class="icon-material-outline-search"></i>
                            </div>
                        </div>

                        <!-- Widget -->
                     {{--   <div class="sidebar-widget">

                            <h3>Trending Posts</h3>
                            <ul class="widget-tabs">

                                <!-- Post #1 -->
                                <li>
                                    <a href="pages-blog-post.html" class="widget-content active">
                                        <img src="images/blog-02a.jpg" alt="">
                                        <div class="widget-text">
                                            <h5>How to "Woo" a Recruiter and Land Your Dream Job</h5>
                                            <span>29 June 2019</span>
                                        </div>
                                    </a>
                                </li>

                                <!-- Post #2 -->
                                <li>
                                    <a href="pages-blog-post.html" class="widget-content">
                                        <img src="images/blog-07a.jpg" alt="">
                                        <div class="widget-text">
                                            <h5>What It Really Takes to Make $100k Before You Turn 30</h5>
                                            <span>3 June 2019</span>
                                        </div>
                                    </a>
                                </li>
                                <!-- Post #3 -->
                                <li>
                                    <a href="pages-blog-post.html" class="widget-content">
                                        <img src="images/blog-04a.jpg" alt="">
                                        <div class="widget-text">
                                            <h5>5 Myths That Prevent Job Seekers from Overcoming Failure</h5>
                                            <span>5 June 2019</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>

                        </div>--}}
                        <!-- Widget / End-->


                        <!-- Widget -->
                        <div class="sidebar-widget">
                            <h3>Social Profiles</h3>
                            <div class="freelancer-socials margin-top-25">
                                <ul>
                                    <li><a href="{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Facebook'] }}" target="_blank" title="Facebook" data-tippy-placement="top"><i class="icon-brand-facebook"></i></a></li>
                                    <li><a href="{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Linkedin'] }}" target="_blank" title="Linkedin" data-tippy-placement="top"><i class="icon-brand-linkedin"></i></a></li>
                                   
                                </ul>
                            </div>
                        </div>

                        <!-- Widget -->
                        <div class="sidebar-widget">
                            <h3>Tags</h3>
                            <div class="task-tags">
                                @foreach(\App\Modules\Blog\Models\Tag::all() as $tag)
                                <a href="{{ route('showBlog').'?tag='.$tag->name }}"><span>{{ $tag->name }}</span></a>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <!-- Spacer -->
        <div class="padding-top-40"></div>
        <!-- Spacer -->

    </div>

    <script>



        $('#searchPosts').keyup(function(e){
            if(e.keyCode == 13)
            {
                var key = $(this).val();
                window.location.href = '{{ route('showBlog').'?key=' }}'+key;
            }
        });
    </script>

@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection
