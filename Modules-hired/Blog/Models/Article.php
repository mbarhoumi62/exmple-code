<?php

namespace App\Modules\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {
  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'articles';


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'title',
      'content',
      'status', // 0 published featured / 1 published standard / 2 unpublished featured / 3 unpublished standard
      'category_id',
      'user_id'
  ];


  public function tags()
  {
      return $this->belongsToMany(
          'App\Modules\Blog\Models\Tag',
          'article_tags',
          'article_id',
          'tag_id'
      )->withTimestamps();
  }

  public function user()
  {
      return $this->belongsTo('App\Modules\User\Models\User');
  }
  public function category()
  {
      return $this->belongsTo('App\Modules\Blog\Models\Category');
  }
  public function comments()
  {
      return $this->hasMany('App\Modules\Blog\Models\Comment');
  }

    public function medias() {
        return $this->hasMany('App\Modules\User\Models\Media'); // type 0 featured / type 1 content
    }
    public function addTag($tagParam)
    {
        if (is_string($tagParam)) {
            $tag = Tag::where('name', $tagParam)->first();
        }
        if (!$tag){
            $newTag = Tag::create([
                'name' => $tagParam
            ]);
            $this->tags()->attach($newTag);
        }else {
                   $this->tags()->attach($tag);

        }

    }

    public function getArticleParamTitle() {
        $title = str_replace(' ', '-', $this->title);
        $title = str_replace('?', '', $title);
        $id = $this->id * 33 + 9371;
        return $title.'-'.$id;
    }



}
