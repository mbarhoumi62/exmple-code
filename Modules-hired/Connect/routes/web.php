<?php

Route::group(['module' => 'Connect', 'middleware' => ['web'], 'namespace' => 'App\Modules\Connect\Controllers'], function() {

    Route::group(['module' => 'Connect', 'middleware' => ['user']], function() {
        Route::get('/channels', 'ConnectController@getUserChannels')->name('getUserChannels');
        Route::get('/chat/messages', 'ConnectController@getChannelMessages')->name('getChannelMessages');
        Route::get('/chat/status', 'ConnectController@handleUpdateMessageStatus')->name('handleUpdateMessageStatus');
        Route::get('/channel/status', 'ConnectController@handleUpdateChannelMessagesStatus')->name('handleUpdateChannelMessagesStatus');
        Route::get('/channel/search', 'ConnectController@handleSearchChannels')->name('handleSearchChannels');
        Route::post('/chat/push', 'ConnectController@handlePushMessage')->name('handlePushMessage');
        Route::post('/chat/direct', 'ConnectController@apiSendDirectMessage')->name('apiSendDirectMessage');
        Route::post('/chat/saveNotif', 'ConnectController@apiSaveMsgNotification')->name('apiSaveMsgNotification');
        Route::post('/chat/delete-convo', 'ConnectController@handleDeleteConversation')->name('handleDeleteConversation');
        Route::get('/notifications/read', 'ConnectController@apiReadNotifications')->name('apiReadNotifications');
        Route::get('/messages/read', 'ConnectController@apiRearAllMessages')->name('apiRearAllMessages');



    });

});
