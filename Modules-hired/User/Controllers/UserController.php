<?php

namespace App\Modules\User\Controllers;

use App\Events\ConfirmDate;
use App\Events\HiredEvents;
use App\Events\ProfilePublished;
use App\Events\CancelInterview;
use App\Modules\Hired\Models\Task;
use App\Modules\Hired\Models\Service;
use App\Modules\User\Models\Address;
use App\Modules\User\Models\Employment;
use App\Modules\User\Models\Info;
use App\Modules\User\Models\Preference;
use App\Modules\User\Models\Media;
use App\Modules\User\Models\Progress;
use App\Modules\User\Models\Skill;
use App\Modules\User\Models\Social;
use App\Modules\Hired\Models\Job;
use App\Modules\Hired\Models\Interview;
use App\Modules\Hired\Models\InterviewDate;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Company;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;
use Storage;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;


class UserController extends Controller
{

    /**
     * Show Detail Company.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCompanyDetails($name)
    {
        $param = explode("-", $name);
        $id = ((int)end($param) - 9371) / 33;
        $company = Company::find($id);
        if (!$company) {
            Session::flash('message', 'Something went wrong, please try again!');
            Session::flash('color', '#de5959');
            return redirect()->route('showBrowseCompanies');
        }
        return view("User::frontOffice.company", [
            'company' => $company
        ]);
    }


    public function showProfile()
    {
        return view('User::frontOffice.dashboard.profile');
    }

    /**
     * Show Detail Employer.
     *
     * @return \Illuminate\Http\Response
     */
    public function showFreelancerDetails($username)
    {
        $exp = explode('-', $username);
        $id = ((int)end($exp) - 987654321) / 77;

        $user = User::find($id);
        if (!$user) {
            return back();
        }

        return view("User::frontOffice.freelancer", [
            'user' => $user
        ]);
    }


    /**
     * Show User Dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showDashboard()
    {
        return view("User::frontOffice.dashboard.index");
    }

    /**
     * Show show Manage Bibbers.
     *
     * @return \Illuminate\Http\Response
     */
    public function showManageBidders($id = null)
    {
        if ($id != null) {
            try {
                $taskId = decrypt($id);
            } catch (DecryptException $e) {
                return back();
            }
            $task = Auth::user()->tasks()->where('id', $taskId)->first();
            if (!$task) {
                return back();
            }
            return view("User::frontOffice.dashboard.bidders", [
                'task' => $task
            ]);
        } else {

            if (count(Auth::user()->tasks) == 0) {
                return back();
            }
            return view("User::frontOffice.dashboard.bidders", [
                'task' => Auth::user()->tasks()->latest()->first()
            ]);
        }
    }

    /**
     * Show show Manage Bibbers.
     *
     * @return \Illuminate\Http\Response
     */
    public function showActiveBids()
    {
        return view("User::frontOffice.dashboard.bids");
    }

    /**
     * Show bookmarks.
     *
     * @return \Illuminate\Http\Response
     */
    public function showBookmarks()
    {
        return view("User::frontOffice.dashboard.bookmarks");
    }

    /**
     * Show Manage Condidates .
     *
     * @return \Illuminate\Http\Response
     */
    public function showManageCandidates($id = null)
    {
        if ($id != null) {
            try {
                $jobId = Crypt::decrypt($id);
            } catch (DecryptException $e) {
                return back();
            }
            $job = Job::find($jobId);
            if (!$job) {
                return back();
            }

            if (Session::has('redirected')) {

                Session::flash('message', 'The interview has been scheduled');
                Session::flash('color', '#5f9025');
            }
            Session::forget('redirected');
            return view("User::frontOffice.dashboard.candidates", [
                'job' => $job
            ]);
        } else {

            if (count(Auth::user()->jobs) == 0) {
                return back();
            }
            return view("User::frontOffice.dashboard.candidates", [
                'job' => Auth::user()->jobs()->latest()->first()
            ]);
        }
    }

    /**
     * Show Manage Jobs .
     *
     * @return \Illuminate\Http\Response
     */
    public function showManageJobs()
    {
        return view("User::frontOffice.dashboard.jobs");
    }

    /**
     * Show Messages .
     *
     * @return \Illuminate\Http\Response
     */
    public function showMessages()
    {
        foreach (Auth::user()->msgNotifications as $notif) {
            $notif->status = 1;
            $notif->save();
        }

        return view("User::frontOffice.dashboard.messages");
    }

    /**
     * Show Post Job .
     *
     * @return \Illuminate\Http\Response
     */
    public function showPostJob()
    {
        if ((Auth::user()->type == 0) && (count(Auth::user()->companies) == 0)) {
            Session::flash('NotAllowed');
            return back();
        }

        return view("User::frontOffice.dashboard.postJob");
    }

    /**
     * Show Post Task .
     *
     * @return \Illuminate\Http\Response
     */
    public function showPostTask()
    {
        return view("User::frontOffice.dashboard.postTask");
    }


    /**
     * Show Reviews .
     *
     * @return \Illuminate\Http\Response
     */
    public function showReviews()
    {
        return view("User::frontOffice.dashboard.reviews");
    }

    /**
     * Show Settings .
     *
     * @return \Illuminate\Http\Response
     */
    public function showSettings()
    {
        return view("User::frontOffice.dashboard.settings");
    }

    /**
     * Show Tasks .
     *
     * @return \Illuminate\Http\Response
     */
    public function showManageTask()
    {
        return view("User::frontOffice.dashboard.task");
    }

    /**
     * Show In Progress Tasks .
     *
     * @return \Illuminate\Http\Response
     */
    public function showInProgressTasks()
    {
        return view("User::frontOffice.dashboard.InProgressTasks");
    }

    /**
     * Show Manage In Progress Tasks .
     *
     * @return \Illuminate\Http\Response
     */
    public function showManageInProgressTasks()
    {
        return view("User::frontOffice.dashboard.manageInProgressTasks");
    }




    /**
     * @desc Login Api
     * @route /api/login
     * @return  (200 => success / 401 => not activated / 404 => fail / 400 => bad request)
     */
    public function apiHandleLogin(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ];

        $messages = [
            'email.required'          => 'Email is required !',
            'email.email'             => 'Invalid email !',
            'password.required'       => 'Password is required !',
            'password.min'            => 'Password must contain at least 6 charecters !',
        ];

        $validation = Validator::make($request->data, $rules, $messages);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()]);
        }

        $credentials = [
            'email' => $request->data['email'],
            'password' => $request->data['password'],
        ];

        if (Auth::attempt($credentials, true)) {
            $user = Auth::user();

            if ($user->status === 0) {
                Auth::logout();
                return response()->json(['status' => 401]); // account not activated
            } else {
                if ($user->type == 3) {
                    return response()->json(['status' => 200, 'url' => route('showAdminDashboard')]);
                } else {

                    if ($user->status === 1) {
                        Auth::login($user);
                        return response()->json(['status' => 200, 'url' => url()->previous()]);
                    } elseif ($user->status === 2) {
                        return response()->json(['status' => 200, 'url' => url()->previous()]);
                    } else {
                        Auth::logout();
                        return response()->json(['status' => 400]); // something went wrong, bad request
                    }
                }
            }
        }

        return response()->json(['status' => 404]); // wrong email or password
    }

    /**
     * @desc Register Api
     * @route /api/register
     * @return  (200 => success / errors => fail / 400 => bad request)
     */
    public function apiHandleRegister(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'first_name' => 'required',
            'last_name' => 'required',
        ];

        $messages = [
            'email.required'          => 'Email is required !',
            'email.email'             => 'Invalid email !',
            'email.unique'            => 'Email already in use !',
            'password.required'       => 'Password is required !',
            'password.min'            => 'Password must contain at least 6 charecters !',
            'password.confirmed'      => 'Password must be confirmed !',
            'first_name.required'      => 'Firstname is required !',
            'last_name.required'      => 'Lastname is required !',
        ];

        $validation = Validator::make($request->data, $rules, $messages);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()]);
        }

        $validationStr = str_random(30);

        sendMail(
            'User::frontOffice.mails.activation',
            $request->data['email'],
            ['email' => $request->data['email'], 'validationStr' => $validationStr],
            'Account Activation'
        );

        $user = User::create([
            'first_name'         => $request->data['first_name'],
            'last_name'         => $request->data['last_name'],
            'email'         => $request->data['email'],
            'password'      => bcrypt($request->data['password']),
            'status'        => 0,
            'progress'      => 0,
            'type'          => $request->data['type'],
            'validation'    => $validationStr
        ]);

        $user->progress()->create([]);

        Session::put('emailStatus', 0);
        return response()->json(['status' => 200, 'url' => route('showCheckMail')]);
    }


    /**
     * @desc Handle provider redirect
     * @route /social/{provider}
     */
    public function handleProviderRedirect($provider)
    {
        if (!Session::get('url.intended')) {
            Session::put('url.intended', url()->previous());
        }
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @desc Handle provider  callback
     * @route /social/{provider}/callback
     */
    public function handleProviderCallback($provider, Request $request)
    {
        if (!$request->has('code') || $request->has('denied')) {
            return redirect('/');
        }

        $providerData = Socialite::driver($provider)->stateless()->user();

        $user = User::where('provider_id', '=', $providerData->id)
            ->orWhere('email', '=', $providerData->email)
            ->first();

        if (!$user) {

            if ($provider === 'google') {
                $userData = [
                    'first_name' => $providerData->user['given_name'] ??  strtok($providerData->email, '@'),
                    'last_name' => $providerData->user['family_name'] ?? '',
                    'email' =>  $providerData->email,
                    'status' => 1,
                    'provider_id' => $providerData->id,
                    'provider' => $provider,
                    'progress' => 0,
                ];
                $imagePath = 'storage/uploads/user/avatar/' . $providerData->id . '-' . time() . '.png';
                Image::make($providerData->avatar)->resize(511, 511)->save($imagePath);
            } else {
                $userData = [
                    'first_name' => explode(' ', $providerData->name)[0],
                    'last_name' => explode(' ', $providerData->name)[1],
                    'email' =>  $providerData->email ?? null,
                    'status' => $providerData->email ? 1 : 0,
                    'provider_id' => $providerData->id,
                    'provider' => $provider,
                    'progress' => 0,
                ];
                $imagePath = 'storage/uploads/user/avatar/' . $providerData->id . '-' . time() . '.png';
                Image::make($providerData->avatar)->resize(511, 511)->save($imagePath);
            }

            Session::put('user', $userData);
            Session::put('imagePath', $imagePath);
            return redirect()->route('showRegister');
        }

        Auth::login($user);
        return redirect()->intended('/');
    }


    /**
     * @desc Handle logout
     * @route /logout
     */
    public function handleLogout()
    {
        Auth::logout();
        return redirect()->route('showHomePage');
    }

    /**
     * @desc Handle login
     * @route /login
     */
    public function handleLogin()
    {

        $data = Input::all();

        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ];

        $messages = [
            'email.required'          => 'Email is required !',
            'email.email'             => 'Invalid email !',
            'password.required'       => 'Password is required !',
            'password.min'            => 'Password must contain at least 6 charecters !',
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $credentials = [
            'email' => $data['email'],
            'password' => $data['password'],
        ];

        if (Auth::attempt($credentials, true)) {
            $user = Auth::user();

            if ($user->status === 0) {
                Auth::logout();
                return back()->withErrors('Activate you email !', 'invalidCredentials')->withInput();
            } else {
                Auth::login($user);
                if (isset($data['redirect'])) {
                    $img = Auth::user()->medias()->where('type', 0)->first() ? asset(Auth::user()->medias()->where('type', 0)->first()->link) : asset('frontOffice/images/user-avatar-placeholder.png');

                    return redirect()->to('https://guide.hired.tn/loginViaHired?email=' . Auth::user()->email . '&avatar=' . $img . '&hp=' . Auth::user()->password);
                }
                return redirect()->intended('/dashboard');
            }
        }
        return back()->withErrors('Invalid credentials', 'invalidCredentials')->withInput();
    }

    /**
     * @desc Handle register
     * @route /register
     */
    public function handleRegister()
    {

        $data = Input::all();
        
        $validationSessionUser = str_random(30);
        if (Session::has('user')) {
            $sessionUser = Session::get('user');
            if ($sessionUser['status'] == 0) {
                $sessionUser['email'] = $data['email'];
                $sessionUser['validation'] = $validationSessionUser;
            }


            $rules = [
                'email' => 'required|email|unique:users,email',
                'first_name' => 'required',
                'last_name' => 'required',
                'password' => 'required|min:6',
                'password_confirmation' => 'required|same:password'
            ];

            $messages = [
                'email.required' => 'Email is required',
                'email.email' => 'Invalid email',
                'email.unique' => 'Email already used',
                'password.required' => 'Password is required',
                'password.min' => 'Password must contain at least 6 characters',
                'password_confirmation.same' => 'Password must be confirmed !',
                'password_confirmation.required' => 'Please re-enter your password !',
                'first_name.required' => 'Firstname is required !',
                'last_name.required' => 'Lastname is required !',
            ];

            $validation = Validator::make($data, $rules, $messages);

            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation->errors())->withInput();
            }

            $user = User::create($sessionUser);

            $user->medias()->create([
                'type' => 0,
                'link' => Session::get('imagePath'),
            ]);
            $user->update([
                'password' => bcrypt($data['password']),
                'type'   => $data['account-type-radio-page'],
            ]);
            $user->progress()->create([]);
            Session::forget('user');
            Session::forget('imagePath');
            if ($user->status == 0) {
                sendMail(
                    'User::frontOffice.mails.activation',
                    $data['email'],
                    ['email' => $data['email'], 'validationStr' => $validationSessionUser],
                    'Account Activation'
                );
                return redirect()->route('showCheckMail');
            }
            Auth::login($user);
            if ($user->type === 1 && $user->status === 2) {
                return redirect()->route('showProfile');
            } else {
                return redirect()->route('showStartingInterface');
            }
        }
        $rules = [
            'email' => 'email|required|unique:users,email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            'first_name' => 'required',
            'last_name' => 'required',
        ];

        $messages = [
            'email.required' => 'Email is required',
            'email.email' => 'Invalid email',
            'email.unique' => 'Email already used',
            'password.required' => 'Password is required',
            'password.min' => 'Password must contain at least 6 characters',
            'password_confirmation.same' => 'Password must be confirmed !',
            'password_confirmation.required' => 'Please re-enter your password !',
            'first_name.required' => 'First name is required !',
            'last_name.required' => 'Last name is required !',
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $validationStr = str_random(30);

        $userData = [
            'email' => $data['email'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'password' => bcrypt($data['password']),
            'validation' => $validationStr,
            'status' => 0,
            'progress' => 0,
            'type'     => $data['account-type-radio-page'],
        ];

        sendMail(
            'User::frontOffice.mails.activation',
            $data['email'],
            ['email' => $data['email'], 'validationStr' => $validationStr],
            'Account Activation'
        );

        $user = User::create($userData);
        $user->progress()->create([]);
        $user->medias()->create([
            'link' => 'frontOffice/images/user-avatar-placeholder.png',
            'type' => 0,
        ]);


        Session::put('emailStatus', 0);
        return redirect()->route('showCheckMail');
    }


    /**
     * @desc Handle mail verification
     * @route /activate
     */
    public function handleMailValidation($code)
    {

        $user = User::where('validation', '=', $code)->first();

        if ($user) {
            if ($user->status == 1) {
                return redirect(route('showSettings'));
            }

            $user->status = 1;
            $user->save();
            Auth::login($user);
            return redirect(route('showStartingInterface'));
        } else {
            return redirect(route('showHomePage'));
        }
    }

    /**
     * @desc show login page
     * @route /login
     */
    public function showLogin()
    {

        if (!Session::has('errors')) {
            Session::put('url.intended', url()->previous());
        }
        return view('User::frontOffice.auth.login');
    }

    /**
     * @desc show login page
     * @route /register
     */
    public function showRegister()
    {
        return view('User::frontOffice.auth.register');
    }

    public function showCheckMail()
    {
        return view('User::frontOffice.auth.checkMail');
    }

    public function showStartingInterface()
    {
        return view('User::frontOffice.auth.start');
    }


    /**
     * @desc request reset password
     * @route /requestReset
     */
    public function showRequestResetPassword()
    {
        return view('User::frontOffice.auth.requestReset');
    }

    /**
     * @desc api forget password
     * @route /api/requestReset
     */
    public function apiResetPassword(Request $request)
    {
        $email = $request->data['email'];

        $rules = [
            'email' => 'required|email'
        ];

        $messages = [
            'email.required'          => 'Email is required !',
            'email.email'             => 'Invalid email !'
        ];

        $validation = Validator::make($request->data, $rules, $messages);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()]);
        }

        $user = User::where('email', $email)->first();

        if (!$user) {
            return response()->json(['notFound' => "We can't find any registered user with the entered email!"]);
        }

        $validationStr = str_random(50);

        $user->validation = $validationStr;
        $user->save();


        sendMail(
            'User::frontOffice.mails.resetPassword',
            $email,
            ['code' => $validationStr, 'user' => $user],
            'Password Reset'
        );

        return response()->json(['status' => 200]);
    }

    public function showResetPassword($code)
    {

        return view('User::frontOffice.auth.reset', [
            'userId' => User::where('validation', $code)->first()->id
        ]);
    }

    public function handleResetPassword()
    {
        $data = Input::all();
        $rules = [
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ];

        $messages = [
            'password.required' => 'Password is required',
            'password.min' => 'Password must contain at least 6 characters',
            'password_confirmation.same' => 'Password must be confirmed !',
            'password_confirmation.required' => 'Please re-enter your password !'
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $user = User::find($data['id']);
        if (!$user) {
            return back()->withErrors('Something went wrong, please try again', 'badRequest');
        }
        $user->update([
            'password' => bcrypt($data['password'])
        ]);
        Auth::login($user);
        Session::flash('message', 'Your new password has been set.');
        Session::flash('color', '#5f9025');
        return redirect()->route('showDashboard');
    }

    public function handleUpdateFreelancerInfo()
    {
        $data = Input::all();



        $rules = [
            'tagline' => 'required|max:100',
            'abt_me' => 'max:500',
            'address' => 'required',
            'country' => 'required',
            'lon' => 'required',
            'zip' => 'nullable|numeric'
        ];

        $messages = [
            'tagline.required'          => 'Tagline is required !',
            'tagline.max'       => 'Tagline is too long, consider reformatting it !',
            'abt_me.max'            => 'Introduction is too long, please consider reformatting it !',
            'address.required'            => 'Please pick your address from the suggestions or from the map below!',
            'lon.required'            => 'Please pick your address from the suggestions or from the map below!',
            'zip.numeric'            => 'Postal code must be numeric!',
            'country.required'            => 'Please select your nationality!'
        ];



        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            Session::flash('UpdateFreelancerInfosErrors');
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        if (Auth::user()->info) {
            Auth::user()->info()->update([
                'tagline' => $data['tagline'],
                'abt_me' => $data['abt_me']
            ]);
        } else {
            Info::create([
                'tagline' => $data['tagline'],
                'abt_me' => $data['abt_me'],
                'user_id' => Auth::id()
            ]);
            if (Auth::user()->progress->profile == 0) {
                Auth::user()->progress->profile = 1;
                Auth::user()->progress->save();
            }
        }

        if (Auth::user()->address) {
            Auth::user()->address()->update([
                'address' => $data['address'],
                'city' => $data['city'],
                'province' => $data['province'],
                'zip' => $data['zip'],
                'lat' => $data['lat'],
                'lon' => $data['lon'],
                'country' => $data['country']
            ]);
        } else {
            Address::create([
                'address' => $data['address'],
                'city' => $data['city'],
                'province' => $data['province'],
                'zip' => $data['zip'],
                'lat' => $data['lat'],
                'lon' => $data['lon'],
                'user_id' => Auth::id(),
                'country' => $data['country']
            ]);
        }

        if (Auth::user()->type == 1 && Auth::user()->status == 1) {
            Auth::user()->update([
                'status' => 2,
            ]);
            event(new HiredEvents(Auth::user(), 'publish', 'Profile', Auth::id(), Auth::user()->getFreelancerUsername()));
            saveNotification(Auth::id(), Auth::id(), 0, 'Profile', 'Your Profile Has Been Published', route('showFreelancerDetails', Auth::user()->getFreelancerUsername()));

            Session::flash('message', 'Your Profile Has Been Published');
            Session::flash('color', '#5f9025');
        }

        Session::flash('message', 'Profile Infos updated Successfully !');
        Session::flash('color', '#5f9025');
        return back();
    }

    public function apiHandleAddSkill(Request $request)
    {

        if ($request->has('label') && $request->get('label') != '') {

            if ($request->get('label') == 'undefined') {
                return false;
            }

            $label = $request->get('label');
            $skill = Skill::where('label', $label)->where('user_id', Auth::id())->first();

            if (!$skill) {

                if (count(Auth::user()->skills) == 6) {
                    return response()->json(['status' => 401]);  //limited
                }

                Skill::create([
                    'label' => $label,
                    'user_id' => Auth::id()
                ]);
                if (Auth::user()->progress->skills == 0) {
                    Auth::user()->progress->skills = 1;
                    Auth::user()->progress->save();
                }

                return response()->json(['status' => 200, $label]);
            }

            return response()->json(['status' => 400]); // already exists

        } else {
            return response()->json(['status' => 404]);
        }
    }

    public function apiHandleDeleteSkill(Request $request)
    {
        if ($request->has('label') && ($request->get('label') != '' || $request->get('label') != 'undefined')) {
            $label = $request->get('label');
            $skill = Skill::where('label', $label)->where('user_id', Auth::id())->first();

            if (!$skill) {
                return response()->json(['status' => 400]);
            }
            $skill->delete();
            if (count(Auth::user()->skills) == 0) {

                Auth::user()->progress->skills = 0;
                Auth::user()->progress->save();
            }
            return response()->json(['status' => 200]);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    public function apiHandleSetHourRate(Request $request)
    {
        if ($request->has('hourRate') && $request->get('hourRate') != '') {
            if (Auth::user()->preference) {
                Auth::user()->preference()->update([
                    'hour_rate' => $request->get('hourRate'),
                ]);

                return response()->json(['status' => 'updated']);
            }
            Preference::create([
                'hour_rate' => $request->get('hourRate'),
                'user_id' => Auth::id()
            ]);

            if (Auth::user()->progress->rate == 0) {
                Auth::user()->progress->rate = 1;
                Auth::user()->progress->save();
            }

            return response()->json(['status' => 'created']);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    public function apiHandleUploadFreelancerFiles()
    {

        $type = $_FILES['file']['type'];
        $size = $_FILES['file']['size'];

        if ($type != "application/pdf")
            return response()->json(['badType' => 'Only PDF files are accepted']);

        if (round($size / 1024 / 1024, 2) > 10)
            return response()->json(['largeSize' => 'Maximum file size accepted is 10 MB ']);

        $filePath = 'storage/uploads/user/files/' . time() . rand(10, 30) . '.pdf';
        file_put_contents($filePath, file_get_contents($_FILES['file']['tmp_name']));

        if (count(Auth::user()->medias()->where('type', 5)->get()) < 6) {

           $media =  Media::create([
                'link' =>  $filePath,
                'type' => 5,
                'label' => $_FILES['file']['name'],
                'user_id' => Auth::id()
            ]);

            if (Auth::user()->progress->files == 0) {
                Auth::user()->progress->files = 1;
                Auth::user()->progress->save();
            }

            return response()->json(['status' => 200, 'media' => $media ]);
        } else {
            return response()->json(['maxFiles' => 'You are not allowed to upload more files, try to delete old ones!']);
        }
    }

    public function apiHandleDeleteFreelancerFile(Request $request)
    {
        $id = $request->get('id');

        $file = Media::find($id);

        if (!$file) {
            return response()->json(['status' => 404]);
        }

        $file->delete();
        if (count(Auth::user()->medias()->where('type', 4)->get()) == 0) {
            Auth::user()->progress->files = 0;
            Auth::user()->progress->save();
        }
        return response()->json(['status' => 200]);
    }

    public function apiHandleAddSocialLink(Request $request)
    {
        $link = $request->get('link');
        $label = $request->get('label');

        $social = Auth::user()->socials()->where('label', $label)->first();

        if ($social) {
            if ($social->link == $link) {
                return response()->json(['status' => 400]); //  already exists
            } else {
                $social->update([
                    'link' => $link
                ]);

                return response()->json(['status' => 'updated']); //  updated
            }
        }

        $newLink =   Auth::user()->socials()->create([
            'label' => $label,
            'link' => $link
        ]);
        if (Auth::user()->progress->links == 0) {
            Auth::user()->progress->links = 1;
            Auth::user()->progress->save();
        }

        if ($newLink) {
            return response()->json(['status' => 'created']); //  done
        } else {
            return response()->json(['status' => 404]); //  something went wrong
        }
    }

    public function handleUpdateAccountInfos()
    {

        $data = Input::all();

        $rules = [
            'firstName' => 'required',
            'lastName' => 'required'
        ];

        $messages = [
            'birthday.before' => 'Invalid date'
        ];

        if (isset($data['phone'])) {

            $rules['phone'] = 'phone:Auto,TN';
            $messages['phone.phone'] = 'Invalid phone number!';
        }
        if (isset($data['birthday'])) {

            $rules['birthday'] = 'date|before:today';
            $messages['birthday.date'] = 'Invalid date';
            $messages['birthday.before'] = 'Invalid date';
        }


        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }


        if (isset($data['profileImage'])) {
            $media = Auth::user()->medias()->where('type', 0)->first();
            if ($media) {
                if ($media->link != 'frontOffice/images/user-avatar-placeholder.png') {
                    unlink(public_path($media->link));
                }
                $image = uploadFile($data['profileImage'], 'userImage');
                $media->update([
                    'link' => $image,
                    'label' => $data['profileImage']->getClientOriginalName()
                ]);
            } else {
                $image = uploadFile($data['profileImage'], 'userImage');
                Auth::user()->medias()->create([
                    'link' => $image,
                    'type' => 0,
                    'label' => $data['profileImage']->getClientOriginalName()
                ]);
            }
        }

        Auth::user()->update([
            'first_name' => $data['firstName'],
            'last_name' => $data['lastName'],
            'phone' => $data['phone'],
            'birthday' => $data['birthday']
        ]);

        if (Auth::user()->progress->account == 0) {
            Auth::user()->progress->account = 1;
            Auth::user()->progress->save();
        }


        Session::flash('message', 'Account Settings updated Successfully !');
        Session::flash('color', '#5f9025');
        return back();
    }


    public function handleAddCompany()
    {

        $data = Input::all();

        $rules = [
            'name'         =>  'required',
            'tagline'      =>  'required|min:10|max:100',
            'abt_me'      =>    'required|min:50|max:500',
            'industry'     =>  'required',
            'cPhone'        =>  'required|phone:AUTO,' . getCountryCode($data['country']),
            'address'      =>  'required',
            'zip_code'     =>  'required',
            'country' =>  'required',
            'lon' => 'required'

        ];
        $messages = [
            'name.required'         =>    'Company Name is required!',
            'tagline.min'           =>    'Tagline must be between 10 and 100 characters!',
            'tagline.max'           =>    'Tagline must be between 10 and 100 characters!',
            'abt_me.required'       =>     'Company Introduction is required',
            'abt_me.min'       =>     'Company Introduction is must contains at least 50 characters',
            'abt_me.max'       =>     'Company Introduction is too long',
            'cPhone.required'        =>    'Phone Number is required!',
            'cPhone.phone'         =>    'Invalid phone number',
            'address.required'      =>    'Please pick your address from the suggestions or from the map below!',
            'lon.required'      =>    'Pick address from the suggestions or from the map below!',
            'country.required' =>    'Company Nationality is required!',
            'zip_code.required' =>    'Zip code is required!',
            'industry.required' =>    'Company industry is required!'

        ];



        if (isset($data['website'])) {

            if (!filter_var($data['website'], FILTER_VALIDATE_URL)) {
                $rules['website'] = 'url';
                $messages['website.url'] = 'Please enter a valid url!';
            }
        }

        if (isset($data['fax'])) {

            $rules['fax'] = 'numeric';
            $messages['fax.numeric'] = 'Fax must be numeric!';
        }

        if (isset($data['founded_at'])) {

            $rules['founded_at'] = 'date|before:' . date('Y-m-d');
            $messages['founded_at.date'] = 'Invalid date!';
            $messages['founded_at.before'] = 'Invalid date!';
        }


        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            Session::flash('CompanyCreateErrors');

            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $company = Auth::user()->companies()->create([
            'name'         =>      $data['name'],
            'industry'     =>      $data['industry'],
            'size'         =>      $data['size'] ?? null,
            'phone'        =>      $data['cPhone'],
            'website'        =>      $data['website'] ?? null,
            'country'        =>      $data['country'],
            'fax'          =>      $data['fax'] ?? null,
            'founded_at'   =>      $data['founded_at'] ?? null
        ]);

        if (isset($data['company-logo'])) {
            $image = uploadFile($data['company-logo'], 'companyImage');
            $company->medias()->create([
                'link' => $image,
                'type' => 0,
                'label' => $data['company-logo']->getClientOriginalName()
            ]);
        }

        $info =  $company->info()->create([
            'tagline'         =>      $data['tagline'],
            'abt_me'     =>      $data['abt_me']
        ]);

        $address = $company->address()->create([
            'address'      => $data['address'],
            'city'         => $data['city'] ? $data['city'] : $data['province'],
            'province'     => $data['province'] ? $data['province'] : $data['city'],
            'zip'          => $data['zip_code'],
            'lat'          => $data['lat'],
            'lon'          => $data['lon'],
        ]);

        foreach (explode(',', $data['companySocialLinks']) as $link) {
            if ($link != '') {
                $company->socials()->create([
                    'label' => explode('.', explode('/', $link)[2])[0],
                    'link' => $link
                ]);
            }
        }

        Session::flash('message', 'Company Created !');
        Session::flash('color', '#5f9025');

        return back();
    }


    public function handleUpdatePassword()
    {
        $data = Input::all();

        $user = Auth::user();

        $rules = [
            'current' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!\Hash::check($value, $user->password)) {
                    return $fail(__('The current password is incorrect.'));
                }
            }],
            'new' => 'required|min:6|different:current',
            'new_confirmation' => 'required|same:new'
        ];

        $messages = [
            'current.required' => 'Current password is required !',
            'new.required' => 'New password is required !',
            'new.min' => 'New password must contains at least 6 characters !',
            'new.different' => 'New password must be different!',
            'new_confirmation.required' => 'Please confirm your new password !',
            'new_confirmation.same' => 'New password must be confirmed !'
        ];


        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            Session::flash('message', 'Please verify the inputs!');
            Session::flash('color', '#de5959');
            return redirect()->back()->withErrors($validation->errors());
        }

        $user->password = bcrypt($data['new']);

        $user->save();

        Session::flash('message', 'Password Updated Successfully !');
        Session::flash('color', '#5f9025');
        return back();
    }

    public function apiHandleUpdateEmail(Request $request)
    {
        $user = Auth::user();
        $rules = [
            'email' => 'required|email|unique:users,email',
            'password' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!\Hash::check($value, $user->password)) {
                    return $fail(__('The entered password is incorrect.'));
                }
            }]
        ];

        $messages = [
            'email.required'          => 'Email is required !',
            'email.email'             => 'Invalid email !',
            'email.unique'            => 'Email already in use !',
            'password.required'       => 'Password is required !'
        ];

        $validation = Validator::make($request->data, $rules, $messages);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()]);
        }


        sendMail(
            'User::frontOffice.mails.updateEmail',
            $request->data['email'],
            ['email' => Crypt::encrypt($request->data['email']), 'user' => $user],
            'Email Update'
        );

        return response()->json(['status' => 200]);
    }

    public function handleUpdateEmail($email)
    {
        $email = Crypt::decrypt($email);
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            Auth::user()->email = $email;
            Auth::logout();
            return redirect()->route('showLogin')->with(['emailUpdated' => 'Your email has been updated successfully']);
        } else {

            Auth::logout();
            return redirect()->route('showLogin')->with(['emailUpdatedError' => 'Something went wrong !']);
        }
    }

    public function handleUploadIntroVideo()
    {
        $data = Input::all();
        $link = uploadFile($data['introVideo'], 'intro');

        Auth::user()->medias()->create([
            'link' => $link,
            'label' => $data['introVideo']->getClientOriginalName(),
            'type' => 2,
        ]);

        if (Auth::user()->progress->intro == 0) {
            Auth::user()->progress->intro = 1;
            Auth::user()->progress->save();
        }

        Session::flash('message', 'Introduction video uploaded successfully !');
        Session::flash('color', '#5f9025');
        return back();
    }


    public function handleUpdateIntroVideo()
    {
        $data = Input::all();
        $media = Auth::user()->medias()->where('type', 2)->first();
        unlink(public_path($media->link));

        $link = uploadFile($data['introVideo'], 'intro');

        $media->update([
            'link' => $link,
            'label' => $data['introVideo']->getClientOriginalName()
        ]);

        Session::flash('message', 'Introduction video updated successfully !');
        Session::flash('color', '#5f9025');
        return back();
    }




    public function HandleDeleteCompany()
    {
        $data   =  Input::all();
        $companyId =  $data['companyId'];

        $company = Auth::user()->companies->where('id', $companyId)->first();
        if (!$company) {
            Session::flash('message', 'Some things went wrong !');
            Session::flash('color', '#de5959');
            return back();
        }

        $company->delete();
        Session::flash('message', 'Company Deleted !');
        Session::flash('color', '#5f9025');

        return back();
    }

    public function HandleUpdateCompany($companyId)
    {

        $company = Company::find($companyId);
        if (!$company) {
            Session::flash('message', 'Some things went wrong !');
            Session::flash('color', '#de5959');
            return back();
        }

        $data = Input::all();

        $rules = [
            'name'         =>  'required',
            'tagline'      =>  'required|min:10|max:100',
            'abt_me'      =>    'required|min:50|max:500',
            'industry'     =>  'required',
            'cPhone'        =>  'required|phone:AUTO',
            'address'      =>  'required',
            'zip_code'     =>  'required',
            'country' =>  'required',
            'lon' =>  'required'

        ];
        $messages = [
            'name.required'         =>    'Company Name is required!',
            'tagline.min'           =>    'Tagline must be between 10 and 100 characters!',
            'tagline.max'           =>    'Tagline must be between 10 and 100 characters!',
            'abt_me.required'       =>     'Company Introduction is required',
            'abt_me.min'       =>     'Company Introduction is must contains at least 50 characters',
            'abt_me.max'       =>     'Company Introduction is too long',
            'cPhone.required'        =>    'Phone Number is required!',
            'cPhone.phone'         =>    'Invalid phone number!',
            'address.required'      =>    'Please pick your address from the suggestions or from the map below!',
            'lon.required'      =>    'Please pick your address from the suggestions or from the map below!',
            'country.required' =>    'Company Nationality is required!',
            'zip_code.required' =>    'Zip code is required!',
            'industry.required' =>    'Company industry is required!'

        ];

        if (isset($data['website'])) {

            if (!filter_var($data['website'], FILTER_VALIDATE_URL)) {
                $rules['website'] = 'url';
                $messages['website.url'] = 'Please enter a valid url!';
            }
        }

        if (isset($data['fax'])) {

            $rules['fax'] = 'numeric';
            $messages['fax.numeric'] = 'Fax must be numeric!';
        }

        if (isset($data['founded_at'])) {

            $rules['founded_at'] = 'date|before:' . date('Y-m-d');
            $messages['founded_at.date'] = 'Invalid date!';
            $messages['founded_at.before'] = 'Invalid date!';
        }


        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {

            Session::flash('CompanyUpdateErrors');
            Session::flash('CompanyId', $companyId);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }


        $company->update([
            'name'         =>      $data['name'],
            'industry'     =>      $data['industry'],
            'size'         =>      $data['size'] ?? null,
            'phone'        =>      $data['cPhone'],
            'fax'          =>      $data['fax'] ?? null,
            'website'          =>      $data['website'] ?? null,
            'founded_at'   =>      $data['founded_at'] ?? null,
            'country'   =>      $data['country'],
        ]);

        if (isset($data['company-logo'])) {
            $media = $company->medias()->where('type', 0)->first();
            if ($media) {
                unlink(public_path($media->link));
                $image = uploadFile($data['company-logo'], 'companyImage');
                $company->medias()->update([
                    'link' => $image,
                    'type' => 0,
                    'label' => $data['company-logo']->getClientOriginalName()
                ]);
            }
            $image = uploadFile($data['company-logo'], 'companyImage');
            $company->medias()->create([
                'link' => $image,
                'type' => 0,
                'label' => $data['company-logo']->getClientOriginalName()
            ]);
        }

        $company->info()->update([
            'tagline'         =>  $data['tagline'],
            'abt_me'     =>      $data['abt_me']
        ]);

        $company->address()->update([
            'address'      => $data['address'],
            'city'         => $data['city'],
            'province'     => $data['province'] ? $data['province'] : $data['city'],
            'zip'          => $data['zip_code'],
            'lat'          => $data['lat'],
            'lon'          => $data['lon'],
        ]);


        Session::flash('message', 'Company Updated !');
        Session::flash('color', '#5f9025');

        return back();
    }


    public function apiHandleUpdateSocialLinkCompany(Request $request)
    {
        $link = $request->get('link');
        $label = $request->get('label');
        $id = $request->get('company');
        $company = Company::find($id);
        $social = $company->socials()->where('label', $label)->first();

        if ($social) {
            if ($social->link == $link) {
                return response()->json(['status' => 400]); //  already exists
            } else {
                $social->update([
                    'link' => $link
                ]);

                return response()->json(['status' => 'updated']); //  updated
            }
        }

        $newLink =   $company->socials()->create([
            'label' => $label,
            'link' => $link
        ]);

        if ($newLink) {
            return response()->json(['status' => 'created']); //  done
        } else {
            return response()->json(['status' => 404]); //  something went wrong
        }
    }


    public function handleBookmark(Request $request)
    {
        $type = $request->get('type');
        $id = $request->get('id');
        switch ($type) {
            case 'user':
                $target = User::find($id);
                break;

            case 'company':
                $target = Company::find($id);
                break;

            case 'task':
                $target = Task::find($id);
                break;

            case 'job':
                $target = Job::find($id);
                break;

            case 'service':
                $target = Service::find($id);
                break;
        }

        if (Auth::user()->hasBookmarked($target)) {
            Auth::user()->unbookmark($target);
            return response()->json(['unbookmarked' => 200]);
        }

        Auth::user()->bookmark($target);
        return response()->json(['bookmarked' => 200]);
    }

    public function apiHandleSaveEmployment(Request $request)
    {


        $rules = [
            'cmpName' => 'required',
            'pTitle' => 'required',
            'start' => 'required|date|before:tomorrow',
            'end' => 'date|after_or_equal:start',
            'desc' => 'nullable|max:500'
        ];

        $messages = [
            'cmpName.required'          => 'Company name is required!',
            'pTitle.required'             => 'Post title is required!',
            'start.required'       => 'Start date is required!',
            'start.date'            => 'Please use a valid date (dd/mm/yyyy)',
            'start.before'            => 'Invalid date',
            'end.date'            => 'Please use a valid date (dd/mm/yyyy)',
            'end.after_or_equal'            => 'Invalid date',
            'desc.max'            => 'Description is too long'
        ];

        $validation = Validator::make($request->data, $rules, $messages);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()]);
        }

        $emp =  Auth::user()->employments()->create([
            'position' => $request->data['pTitle'],
            'company_name' => $request->data['cmpName'],
            'start_date' => $request->data['start'],
            'end_date' => $request->data['end'] ?? null,
            'present' => $request->data['present'] ?? null,
            'description' => $request->data['desc']
        ]);

        if (Auth::user()->progress->emps == 0) {
            Auth::user()->progress->emps = 1;
            Auth::user()->progress->save();
        }


        $empToReturn = [
            'id' => $emp->id,
            'position' => $emp->position,
            'company' => $emp->company_name,
            'start' => $emp->start_date->format('d/m/Y'),
            'end' => $emp->end_date ? $emp->end_date->format('d/m/Y') : 'Present'
        ];

        return response()->json(['emp' => $empToReturn]);
    }

    public function apiHandleDeleteEmployment(Request $request)
    {

        $emp = Employment::find($request->get('id'));
        if (!$emp) {
            return response()->json(['error' => 'Employment not found']);
        }

        $emp->delete();
        if (count(Auth::user()->employments) == 0) {

            Auth::user()->progress->emps = 0;
            Auth::user()->progress->save();
        }
        return response()->json(['success' => 200]);
    }

    public function apiGetUserPortfolios(Request $request)
    {
        $view = view('User::frontOffice.dashboard.loaders.portfolios', [
            'portfolios' => User::where('id', $request->get('id'))->first()->portfolios
        ])->render();
        return response()->json(['html' => $view]);
    }

    public function showRegisterEmployer()
    {

        return view('User::frontOffice.auth.registerEmployer');
    }

    public function apiHandleRegisterEmployer(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'first_name' => 'required',
            'last_name' => 'required',
        ];

        $messages = [
            'email.required'          => 'Email is required !',
            'email.email'             => 'Invalid email !',
            'email.unique'            => 'Email already in use !',
            'password.required'       => 'Password is required !',
            'password.min'            => 'Password must contain at least 6 characters !',
            'password.confirmed'      => 'Password must be confirmed !',
            'first_name.required'      => 'First name is required !',
            'last_name.required'      => 'Last name is required !',
        ];

        $validation = Validator::make($request->data, $rules, $messages);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()]);
        }

        $pinCode = mt_rand(100000, 999999);;

        $user = User::create([
            'email'         => $request->data['email'],
            'first_name'         => $request->data['first_name'],
            'last_name'         => $request->data['last_name'],
            'password'      => bcrypt($request->data['password']),
            'status'        => 0,
            'type'          => 0,
            'validation'    => $pinCode
        ]);

        sendMail(
            'User::frontOffice.mails.activation',
            $request->data['email'],
            ['email' => $request->data['email'], 'pinCode' => $pinCode],
            'Account Activation'
        );

        return response()->json(['status' => 200, 'user' => $user]);
    }

    public function apiHandleVerifyEmployer(Request $request)
    {

        $user = User::where('email', $request->data['email'])->first();
        if (!$user) {
            return response()->json(['status' => 404]);
        }

        if ($user->validation == $request->data['pin']) {
            $user->update([
                'status' => 1
            ]);
            Auth::login($user);
            return response()->json(['status' => 200, 'url' => route('showAddCompanyAfterRegister')]);
        } else {
            return response()->json(['status' => 400]);
        }
    }

    public function showAddCompanyAfterRegister()
    {


        return view('User::frontOffice.addCompany');
    }

    public function showFreelancerSteps()
    {

        return view('User::frontOffice.freelancerSteps');
    }


    public function apiHandleCreateFirstCompany(Request $request)
    {

        $company = Auth::user()->companies()->create([
            'name'         =>      $request->name,
            'industry'     =>      $request->type,
            'size'         =>      $request->size ?? null,
            'phone'        =>      $request->phone,
            'website'        =>      $request->website ?? null,
            'country'        =>      $request->country,
            'fax'          =>      $request->fax ?? null,
            'founded_at'   =>      $request->founded_at ?? null
        ]);

        if (isset($request->logo)) {
            $image = uploadFile($request->logo, 'companyImage');
            $company->medias()->create([
                'link' => $image,
                'type' => 0,
                'label' => $request->logo->getClientOriginalName()
            ]);
        }

        $company->info()->create([
            'tagline'         =>      $request->tagline,
            'abt_me'     =>      $request->abt_me
        ]);

        $company->address()->create([
            'address'      => $request->address,
            'city'         => $request->city ? $request->city : $request->province,
            'province'     => $request->province ? $request->province : $request->city,
            'zip'          => $request->zip_code,
            'lat'          => $request->lat,
            'lon'          => $request->lon,
        ]);

        foreach (explode(',', $request->companySocialLinks) as $link) {
            if ($link != '') {
                $company->socials()->create([
                    'label' => explode('.', explode('/', $link)[2])[0],
                    'link' => $link
                ]);
            }
        }
        return response()->json(['status' => 200, 'param' => $company->getCompanyParamName()]);
    }

    public function showCompanies()
    {

        return view('User::frontOffice.dashboard.companies', [
            'companies' => Auth::user()->companies
        ]);
    }

    public function apiHandleUpdateProfile(Request $request)
    {

        Info::create([
            'tagline' => $request->data['tagline'],
            'abt_me' => $request->data['abt_me'] ?? '',
            'user_id' => Auth::id()
        ]);

        if (Auth::user()->progress->profile == 0) {
            Auth::user()->progress->profile = 1;
            Auth::user()->progress->save();
        }

        Address::create([
            'address' => $request->data['address'],
            'city' => $request->data['city'],
            'province' => $request->data['province'],
            'zip' => $request->data['zip_code'] ?? '',
            'lat' => $request->data['lat'],
            'lon' => $request->data['lon'],
            'user_id' => Auth::id(),
            'country' => $request->data['country']
        ]);

        Auth::user()->update([
            'status' => 2
        ]);

        event(new HiredEvents(Auth::user(), 'publish', 'See Profile', Auth::id(), Auth::user()->getFreelancerUsername()));
        return response()->json(['status', 200]);
    }

    public function showCalendar()
    {

        return view('User::frontOffice.dashboard.calendar.calendar', [
            'interviews' => Auth::user()->interviews,
            'interviewedInterviews' => Auth::user()->interviewedInterviews
        ]);
    }

    public function handleDeleteInterview()
    {
        $data   =  Input::all();
        $id =  $data['iId'];

        $interview = Interview::find($id);
        if (!$interview) {
            Session::flash('message', 'Some things went wrong !');
            Session::flash('color', '#de5959');
            return back();
        }

        $interview->delete();
        event(new CancelInterview(Auth::user(), $interview->interviewed));
        saveNotification(Auth::id(), $interview->interviewed, 0, '', 'Has canceled his interview with you!', route('showCalendar'));

        Session::flash('message', 'Interview Canceled!');
        Session::flash('color', '#5f9025');
        return back();
    }

    public function handleConfirmInterviewDate(Request $request)
    {
        $iid = $request->get('iid');
        $did = $request->get('did');
        $int = Interview::find($iid);
        $date = InterviewDate::find($did);

        if (!$int || !$date) {
            Session::flash('message', 'Some things went wrong !');
            Session::flash('color', '#de5959');
            return back();
        }

        $date->status = 1;
        $date->save();
        $int->status = 1;
        $int->save();
        event(new ConfirmDate(Auth::user(), $int->interviewer));
        saveNotification(Auth::id(), $int->interviewer, 0, 'See Details', 'Has confirmed the interview date!', route('showCalendar'));

        sendMail(
            'Hired::mails.confirmDate',
            $int->interviewerUser()->email,
            ['username' => Auth::user()->getFullName(), 'param' =>  Auth::user()->getFreelancerUsername(), 'user' => $int->interviewerUser()],
            'New Interview Confirmation'
        );

        Session::flash('message', 'Interview date confirmed!');
        Session::flash('color', '#5f9025');
        return back();
    }

    public function redirectAfterScheduleInterview(Request $request)
    {
        Session::put('redirected', 'redirected');
        return redirect()->route('showManageCandidates', Crypt::decrypt($request->get('job')));
    }

    public function showLoginProvider()
    {

        if (Auth::check()) {
            $img = Auth::user()->medias()->where('type', 0)->first() ? asset(Auth::user()->medias()->where('type', 0)->first()->link) : asset('frontOffice/images/user-avatar-placeholder.png');

            return redirect()->to('https://guide.hired.tn/loginViaHired?email=' . Auth::user()->email . '&avatar=' . $img . '&hp=' . Auth::user()->password);
        }

        return view('User::frontOffice.auth.provider');
    }
}
