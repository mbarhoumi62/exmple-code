<?php

use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image as Image;
use App\Modules\User\Models\Media;
/**
 *	User Helper
 */

if (! function_exists('sendMail')) {


    function sendMail($view, $email, $content, $subject)
    {
        Mail::send($view, $content, function ($message) use ($email,$subject) {
            $message->to($email);
            $message->subject($subject);
        });

    }
}

if (! function_exists('uploadFile')) {


    function uploadFile($file,$type)
    {
        $fileName = str_random(5) .time().'.'.$file->getClientOriginalExtension();

        switch($type) {
            case 'userImage' :
                $path = public_path().'/storage/uploads/user/avatar/';
                Image::make($file)->resize(511, 511)->save($path.$fileName);
              //  $file->move($path, $fileName);
                $link = 'storage/uploads/user/avatar/' . $fileName;
                break;
            case 'companyImage' :
                $path = public_path().'/storage/uploads/company/avatar/';
                Image::make($file)->save($path.$fileName);
                $link = 'storage/uploads/company/avatar/' . $fileName;
                break;
            case 'intro' :
                $path = public_path().'/storage/uploads/user/intros/';
                $file->move($path, $fileName);
                $link = 'storage/uploads/user/intros/' . $fileName;
                break;
            case 'taskFiles' :
                $path = public_path().'/storage/uploads/tasks/files/';
                $file->move($path, $fileName);
                $link = 'storage/uploads/tasks/files/' . $fileName;
                break;
            case 'jobFiles' :
                $path = public_path().'/storage/uploads/jobs/files/';
                $file->move($path, $fileName);
                $link = 'storage/uploads/jobs/files/' . $fileName;
                break;
            case 'applicationCV' :
                    $path = public_path().'/storage/uploads/user/files/cvs/';
                    $file->move($path, $fileName);
                    $link = 'storage/uploads/user/files/cvs/' . $fileName;
                    break;
            case 'serviceFiles' :
                $path = public_path().'/storage/uploads/service/files/';
                $file->move($path, $fileName);
                $link = 'storage/uploads/service/files/' . $fileName;
                break;
            case 'bookServiceFiles' :
                $path = public_path().'/storage/uploads/service/files/bookings/';
                $file->move($path, $fileName);
                $link = 'storage/uploads/service/files/bookings/' . $fileName;
                break;
            case 'portfolio' :
                $path = public_path().'/storage/uploads/user/files/portfolios/';
                $watermark = Image::make(asset(Media::where('type',100)->first()->link))->resize(200,60);
                Image::make($file)->insert($watermark, 'bottom-right', 10, 10)->save($path.$fileName);
                $link = 'storage/uploads/user/files/portfolios/' . $fileName;
                break;
            case 'featuredPostImage' :
                $path = public_path().'/storage/uploads/blog/featured/';
                Image::make($file)->resize(1366, 768)->save($path.$fileName);
                $link = '/storage/uploads/blog/featured/' . $fileName;
                break;
            case 'logo' :
                $path = public_path().'/storage/uploads/general/';
                Image::make($file)->resize(546, 218)->save($path.$fileName);
                $link = 'storage/uploads/general/' . $fileName;
                break;
            case 'svg' :
                $path = public_path().'/storage/uploads/general/';
                $file->move($path, $fileName);
                $link = 'storage/uploads/general/' . $fileName;
                break;

            default : $link = null;
        }

        return $link;

    }

    if (! function_exists('profileProgress')) {


        function profileProgress()
        {

            $prg = Auth::user()->progress;

            $progress = 0;

            if($prg->account == 1) {
                $progress += 20;
            }
            if($prg->profile == 1 ) {
                $progress += 20;
            }
            if($prg->links == 1 && $prg->rate == 1 && $prg->skills == 1 && $prg->files == 1) {
                $progress += 20;
            }
            if($prg->intro == 1) {
                $progress += 20;
            }
            if($prg->emps == 1) {
                $progress += 20;
            }

            return $progress;

        }
    }
}



if (! function_exists('getConfirmedDate')) {


    function getConfirmedDate($interview)
    {
        $confirmedDate = null;
        $dates = $interview->dates;
    
        foreach($dates as $date) {

            if($date->status == 1) {

                $confirmedDate =  $date->date->format('d M, Y').' At '.$date->date->format('H:i').'H';
            }
        }

        return $confirmedDate;

    }
}


if (! function_exists('getCountryCode')) {


    function getCountryCode($countryName)
    {
       $countrycodes = array (
  'AF' => 'Afghanistan',
  'AX' => 'Åland Islands',
  'AL' => 'Albania',
  'DZ' => 'Algeria',
  'AS' => 'American Samoa',
  'AD' => 'Andorra',
  'AO' => 'Angola',
  'AI' => 'Anguilla',
  'AQ' => 'Antarctica',
  'AG' => 'Antigua and Barbuda',
  'AR' => 'Argentina',
  'AU' => 'Australia',
  'AT' => 'Austria',
  'AZ' => 'Azerbaijan',
  'BS' => 'Bahamas',
  'BH' => 'Bahrain',
  'BD' => 'Bangladesh',
  'BB' => 'Barbados',
  'BY' => 'Belarus',
  'BE' => 'Belgium',
  'BZ' => 'Belize',
  'BJ' => 'Benin',
  'BM' => 'Bermuda',
  'BT' => 'Bhutan',
  'BO' => 'Bolivia',
  'BA' => 'Bosnia and Herzegovina',
  'BW' => 'Botswana',
  'BV' => 'Bouvet Island',
  'BR' => 'Brazil',
  'IO' => 'British Indian Ocean Territory',
  'BN' => 'Brunei Darussalam',
  'BG' => 'Bulgaria',
  'BF' => 'Burkina Faso',
  'BI' => 'Burundi',
  'KH' => 'Cambodia',
  'CM' => 'Cameroon',
  'CA' => 'Canada',
  'CV' => 'Cape Verde',
  'KY' => 'Cayman Islands',
  'CF' => 'Central African Republic',
  'TD' => 'Chad',
  'CL' => 'Chile',
  'CN' => 'China',
  'CX' => 'Christmas Island',
  'CC' => 'Cocos (Keeling) Islands',
  'CO' => 'Colombia',
  'KM' => 'Comoros',
  'CG' => 'Congo',
  'CD' => 'Zaire',
  'CK' => 'Cook Islands',
  'CR' => 'Costa Rica',
  'CI' => 'Côte D\'Ivoire',
  'HR' => 'Croatia',
  'CU' => 'Cuba',
  'CY' => 'Cyprus',
  'CZ' => 'Czech Republic',
  'DK' => 'Denmark',
  'DJ' => 'Djibouti',
  'DM' => 'Dominica',
  'DO' => 'Dominican Republic',
  'EC' => 'Ecuador',
  'EG' => 'Egypt',
  'SV' => 'El Salvador',
  'GQ' => 'Equatorial Guinea',
  'ER' => 'Eritrea',
  'EE' => 'Estonia',
  'ET' => 'Ethiopia',
  'FK' => 'Falkland Islands (Malvinas)',
  'FO' => 'Faroe Islands',
  'FJ' => 'Fiji',
  'FI' => 'Finland',
  'FR' => 'France',
  'GF' => 'French Guiana',
  'PF' => 'French Polynesia',
  'TF' => 'French Southern Territories',
  'GA' => 'Gabon',
  'GM' => 'Gambia',
  'GE' => 'Georgia',
  'DE' => 'Germany',
  'GH' => 'Ghana',
  'GI' => 'Gibraltar',
  'GR' => 'Greece',
  'GL' => 'Greenland',
  'GD' => 'Grenada',
  'GP' => 'Guadeloupe',
  'GU' => 'Guam',
  'GT' => 'Guatemala',
  'GG' => 'Guernsey',
  'GN' => 'Guinea',
  'GW' => 'Guinea-Bissau',
  'GY' => 'Guyana',
  'HT' => 'Haiti',
  'HM' => 'Heard Island and Mcdonald Islands',
  'VA' => 'Vatican City State',
  'HN' => 'Honduras',
  'HK' => 'Hong Kong',
  'HU' => 'Hungary',
  'IS' => 'Iceland',
  'IN' => 'India',
  'ID' => 'Indonesia',
  'IR' => 'Iran, Islamic Republic of',
  'IQ' => 'Iraq',
  'IE' => 'Ireland',
  'IM' => 'Isle of Man',
  'IL' => 'Israel',
  'IT' => 'Italy',
  'JM' => 'Jamaica',
  'JP' => 'Japan',
  'JE' => 'Jersey',
  'JO' => 'Jordan',
  'KZ' => 'Kazakhstan',
  'KE' => 'KENYA',
  'KI' => 'Kiribati',
  'KP' => 'Korea, Democratic People\'s Republic of',
  'KR' => 'Korea, Republic of',
  'KW' => 'Kuwait',
  'KG' => 'Kyrgyzstan',
  'LA' => 'Lao People\'s Democratic Republic',
  'LV' => 'Latvia',
  'LB' => 'Lebanon',
  'LS' => 'Lesotho',
  'LR' => 'Liberia',
  'LY' => 'Libyan Arab Jamahiriya',
  'LI' => 'Liechtenstein',
  'LT' => 'Lithuania',
  'LU' => 'Luxembourg',
  'MO' => 'Macao',
  'MK' => 'Macedonia, the Former Yugoslav Republic of',
  'MG' => 'Madagascar',
  'MW' => 'Malawi',
  'MY' => 'Malaysia',
  'MV' => 'Maldives',
  'ML' => 'Mali',
  'MT' => 'Malta',
  'MH' => 'Marshall Islands',
  'MQ' => 'Martinique',
  'MR' => 'Mauritania',
  'MU' => 'Mauritius',
  'YT' => 'Mayotte',
  'MX' => 'Mexico',
  'FM' => 'Micronesia, Federated States of',
  'MD' => 'Moldova, Republic of',
  'MC' => 'Monaco',
  'MN' => 'Mongolia',
  'ME' => 'Montenegro',
  'MS' => 'Montserrat',
  'MA' => 'Morocco',
  'MZ' => 'Mozambique',
  'MM' => 'Myanmar',
  'NA' => 'Namibia',
  'NR' => 'Nauru',
  'NP' => 'Nepal',
  'NL' => 'Netherlands',
  'AN' => 'Netherlands Antilles',
  'NC' => 'New Caledonia',
  'NZ' => 'New Zealand',
  'NI' => 'Nicaragua',
  'NE' => 'Niger',
  'NG' => 'Nigeria',
  'NU' => 'Niue',
  'NF' => 'Norfolk Island',
  'MP' => 'Northern Mariana Islands',
  'NO' => 'Norway',
  'OM' => 'Oman',
  'PK' => 'Pakistan',
  'PW' => 'Palau',
  'PS' => 'Palestinian Territory, Occupied',
  'PA' => 'Panama',
  'PG' => 'Papua New Guinea',
  'PY' => 'Paraguay',
  'PE' => 'Peru',
  'PH' => 'Philippines',
  'PN' => 'Pitcairn',
  'PL' => 'Poland',
  'PT' => 'Portugal',
  'PR' => 'Puerto Rico',
  'QA' => 'Qatar',
  'RE' => 'Réunion',
  'RO' => 'Romania',
  'RU' => 'Russian Federation',
  'RW' => 'Rwanda',
  'SH' => 'Saint Helena',
  'KN' => 'Saint Kitts and Nevis',
  'LC' => 'Saint Lucia',
  'PM' => 'Saint Pierre and Miquelon',
  'VC' => 'Saint Vincent and the Grenadines',
  'WS' => 'Samoa',
  'SM' => 'San Marino',
  'ST' => 'Sao Tome and Principe',
  'SA' => 'Saudi Arabia',
  'SN' => 'Senegal',
  'RS' => 'Serbia',
  'SC' => 'Seychelles',
  'SL' => 'Sierra Leone',
  'SG' => 'Singapore',
  'SK' => 'Slovakia',
  'SI' => 'Slovenia',
  'SB' => 'Solomon Islands',
  'SO' => 'Somalia',
  'ZA' => 'South Africa',
  'GS' => 'South Georgia and the South Sandwich Islands',
  'ES' => 'Spain',
  'LK' => 'Sri Lanka',
  'SD' => 'Sudan',
  'SR' => 'Suriname',
  'SJ' => 'Svalbard and Jan Mayen',
  'SZ' => 'Swaziland',
  'SE' => 'Sweden',
  'CH' => 'Switzerland',
  'SY' => 'Syrian Arab Republic',
  'TW' => 'Taiwan, Province of China',
  'TJ' => 'Tajikistan',
  'TZ' => 'Tanzania, United Republic of',
  'TH' => 'Thailand',
  'TL' => 'Timor-Leste',
  'TG' => 'Togo',
  'TK' => 'Tokelau',
  'TO' => 'Tonga',
  'TT' => 'Trinidad and Tobago',
  'TN' => 'Tunisia',
  'TR' => 'Turkey',
  'TM' => 'Turkmenistan',
  'TC' => 'Turks and Caicos Islands',
  'TV' => 'Tuvalu',
  'UG' => 'Uganda',
  'UA' => 'Ukraine',
  'AE' => 'United Arab Emirates',
  'GB' => 'United Kingdom',
  'US' => 'United States',
  'UM' => 'United States Minor Outlying Islands',
  'UY' => 'Uruguay',
  'UZ' => 'Uzbekistan',
  'VU' => 'Vanuatu',
  'VE' => 'Venezuela',
  'VN' => 'Viet Nam',
  'VG' => 'Virgin Islands, British',
  'VI' => 'Virgin Islands, U.S.',
  'WF' => 'Wallis and Futuna',
  'EH' => 'Western Sahara',
  'YE' => 'Yemen',
  'ZM' => 'Zambia',
  'ZW' => 'Zimbabwe',
);

$code = array_search($countryName, $countrycodes); 
return $code;
    }
}




