<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'socials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label',
        'link',
        'user_id',
        'company_id'

    ];


    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Modules\User\Models\Company');
    }

}
