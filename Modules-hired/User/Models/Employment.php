<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position',
        'company_name',
        'start_date',
        'end_date',
        'present',
        'description',
        'user_id'

    ];
    protected $dates = [
        'start_date',
        'end_date'
    ];

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

}
