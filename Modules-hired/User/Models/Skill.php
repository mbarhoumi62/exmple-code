<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'skills';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label',
        'user_id',
        'task_id'
    ];


    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

    public function task()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Task');
    }

    public function service()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Service');
    }


}
