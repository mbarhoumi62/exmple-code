<?php

namespace App\Modules\User\Models;


use App\Modules\Connect\Models\Channel;
use Illuminate\Notifications\Notifiable;
use Overtrue\LaravelFollow\Traits\CanBookmark;
use Overtrue\LaravelFollow\Traits\CanBeBookmarked;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Codebyray\ReviewRateable\Contracts\ReviewRateable;
use Codebyray\ReviewRateable\Traits\ReviewRateable as ReviewRateableTrait;

class User extends Authenticatable implements ReviewRateable
{
    use Notifiable;
    use CanBookmark, CanBeBookmarked;
    use ReviewRateableTrait;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'type',
        'password',
        'phone',
        'status', // 0 mail unactivated / 1 mail activated
        'birthday',
        'validation',
        'provider',
        'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dates = [
        'birthday'
    ];

    public function roles(){
        return $this->belongsToMany(
            'App\Modules\User\Models\Role',
            'user_roles',
            'user_id',
            'role_id'
        )->withTimestamps();
    }

    public function assignRole($role){
        if(is_string($role)){
            $role = Role::where('title',$role)->get();
        }
        if(!$role) return false;
        $this->roles()->attach($role);
    }

    public function retractRole($role){
        $this->roles()->detach($role);
    }
    public function address(){
        return $this->hasOne('App\Modules\User\Models\Address');
    }
    public function medias(){
        return $this->hasMany('App\Modules\User\Models\Media');
    }

    public function info()
    {
        return $this->hasOne('App\Modules\User\Models\Info');
    }
    public function skills()
    {
        return $this->hasMany('App\Modules\User\Models\Skill');
    }

    public function competences()
    {
        return $this->hasMany('App\Modules\User\Models\Competence');
    }

    public function socials()
    {
        return $this->hasMany('App\Modules\User\Models\Social');
    }

    public function preference()
    {
        return $this->hasOne('App\Modules\User\Models\Preference');
    }

    public function employments()
    {
        return $this->hasMany('App\Modules\User\Models\Employment');
    }

    public function companies()
    {
        return $this->hasMany('App\Modules\User\Models\Company');
    }

    public function tasks() {
        return $this->hasMany('App\Modules\Hired\Models\Task');
    }

    public function getFullName() {
        return $this->first_name.' '.$this->last_name;
    }

    public function bids() {
        return $this->hasMany('App\Modules\Hired\Models\Bid','bidder_id','id');
    }

    public function channels() {
        $channels =  Channel::where("sender_id",$this->id)->orWhere('receiver_id',$this->id)->orderBy('created_at','desc')->get();
        $channelsToReturn = [];
        foreach ($channels as $channel) {
            
            if($channel->messages()->latest()->first()) {
               $message = $channel->messages()->latest()->first();
                  }else {
                          $message = [
                            'sender_id' => $channel->sender_id,
                            'receiver_id' => $channel->receiver_id
                        ];

                        $message = (object) $message;
                  }

            $sender = User::where('id', $message->sender_id)->first();
            $senderToReturn = ['id' => $sender->id,'fullName' => $sender->getFullName(),'avatar' => $sender->medias()->where('type',0)->first() ? $sender->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png'];
            $receiver = User::where('id', $message->receiver_id)->first();
            $receiverToReturn = ['id' => $receiver->id,'fullName' => $receiver->getFullName(),'avatar' => $receiver->medias()->where('type',0)->first() ? $receiver->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png'];
            $channelsToReturn[] = ['id' => $channel->id,'sender' => $senderToReturn ,'receiver' => $receiverToReturn, 'message' => $message,'unread' => count($channel->messages()->where('status', 0)->get())];
        }
        
        return $channelsToReturn;

    }

    public function jobs() {
        return $this->hasMany('App\Modules\Hired\Models\Job');
    }

    public function candidates() {
        return $this->hasMany('App\Modules\Hired\Models\Application','user_id','id');
    }

    public function serviceBookings() {
        return $this->hasMany('App\Modules\Hired\Models\Booking','user_id','id');
    }

    public function msgNotifications() {
        return $this->hasMany('App\Modules\Connect\Models\MsgNotification','user_id','id');

    }

    public function progress()
    {
        return $this->hasOne('App\Modules\User\Models\Progress');
    }

    public function getFreelancerUsername() {
        $id = $this->id * 77 + 987654321;
        $first_name = str_replace(' ', '-', $this->first_name);
        $last_name = str_replace(' ', '-', $this->last_name);

        return $first_name.'-'.$last_name.'-'.$id;
    }

    public function notes() {
        return $this->hasMany('App\Modules\General\Models\Note');
    }

    public function emailAlerts() {
        return $this->hasMany('App\Modules\General\Models\EmailAlert');
    }

    public function acceptedBids()
    {
        return $this->bids()->whereHas('accepted')->get();
    }

    public function services() {
        return $this->hasMany('App\Modules\Hired\Models\Service');

    }

    public function portfolios() {
        return $this->hasMany('App\Modules\User\Models\Portfolio');
    }

    public function interviews() {
        return $this->hasMany('App\Modules\Hired\Models\Interview','interviewer','id');

    }

    public function interviewedInterviews() {
        return $this->hasMany('App\Modules\Hired\Models\Interview','interviewed','id');

    }

     public function notifications()
    {
        return $this->hasMany('App\Modules\Connect\Models\HiredNotifications','receiver_id','id');
    }



}
