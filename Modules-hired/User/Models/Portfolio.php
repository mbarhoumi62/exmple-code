<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'portfolios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_title',
        'project_description',
        'link',
        'realized_at',
        'type', //  dev / design / web / mobile ...
        'user_id'
    ];

    protected $dates = [
        'realized_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

    public function medias()
    {
        return $this->hasMany('App\Modules\User\Models\Media');
    }

}
