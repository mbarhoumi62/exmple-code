@extends('frontOffice.layout',['class' =>'gray','title' => __('Hired | Create Company')])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')

    <style>
        .error {
            color: red;
        }

        #singleMap {
            height: 300px;
            width: 100%;
        }

        #msform {
            text-align: center;
            position: relative;
            margin-top: 20px
        }

        #msform fieldset {
            background: white;
            border: 0 none;
            border-radius: 0.5rem;
            box-sizing: border-box;
            width: 100%;
            margin: 0;
            padding-bottom: 20px;
            position: relative
        }

        .form-card {
            text-align: left
        }

        #msform fieldset:not(:first-of-type) {
            display: none
        }

        fieldset {
            padding: 30px;
            border-radius: 5px;
            box-shadow: 0 2px 8px rgba(0, 0, 0, 0.08);
        }


        #msform .action-button {

            padding: 10px 5px;
            margin: 10px 0px 10px 5px;
            float: right;
            width: 150px;
        }

        #msform .action-button:hover,
        #msform .action-button:focus {
            background-color: #311B92
        }

        #msform .action-button-previous {
            width: 100px;
            background: #616161;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px 10px 0px;
            float: right
        }

        #msform .action-button-previous:hover,
        #msform .action-button-previous:focus {
            background-color: #000000
        }

        .card {
            z-index: 0;
            border: none;
            position: relative
        }

        .fs-title {
            font-size: 25px;
            color: #673AB7;
            margin-bottom: 15px;
            font-weight: normal;
            text-align: left
        }

        .purple-text {
            color: #673AB7;
            font-weight: normal
        }

        .steps {
            font-size: 25px;
            color: gray;
            margin-bottom: 10px;
            font-weight: normal;
            text-align: right
        }

        .fieldlabels {
            color: gray;
            text-align: left
        }

        #progressbar {
            overflow: hidden;
            color: lightgrey;
            padding: 0px;
            margin-bottom: 40px;
        }

        #progressbar .active {
            color: black
        }

        #progressbar li {
            list-style-type: none;
            font-size: 15px;
            width: 20%;
            float: left;
            position: relative;
            font-weight: 400
        }


    .socialAddBtn {
        position: absolute;
        top: 0;
        right: 0;
        height: 36px;
        width: 36px;
        padding: 0;
        color: #fff;
        background-color: #2a41e8;
        border-radius: 4px;
        margin: 6px;
        font-size: 19px;
        text-align: center;
        line-height: 36px;
    }

/*
        #progressbar #account:before {
            font-family: FontAwesome;
            content: "\f13e"
        }

        #progressbar #personal:before {
            font-family: FontAwesome;
            content: "\f007"
        }

        #progressbar #payment:before {
            font-family: FontAwesome;
            content: "\f030"
        }

        #progressbar #confirm:before {
            font-family: FontAwesome;
            content: "\f00c"
        }
*/

        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 20px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }

        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: lightgray;
            position: absolute;
            left: 0;
            top: 25px;
            z-index: -1
        }

        #progressbar li.active:before,
        #progressbar li.active:after {
            background: #2a41e8
        }

        .progress {
            height: 20px
        }

        .progress-bar {
            background-color: #2a41e8
        }

        .fit-image {
            width: 100%;
            object-fit: cover
        }

        .logo-wrapper {
            position: relative;
            width: 150px;
            height: 150px;
            border-radius: 4px;
            overflow: hidden;
            box-shadow: none;
            transition: all .3s ease;
        }
        .logo-wrapper .profile-pic-logo {
            height: 100%;
            width: 100%;
            transition: all .3s ease;
            object-fit: cover;
        }
        .logo-wrapper .upload-button-logo {
          position: absolute;
    left: 0;
    height: 35%;
    width: 100%;
        }
        .logo-wrapper .file-upload-logo {
            opacity: 0;
            pointer-events: none;
            position: absolute;
        }
        
        .upload-button-logo {
  position: absolute;
  bottom: 10px;
  right: 0;
  border-radius: 5px;
  background-color: #2a41e8;
}

.logo-wrapper .upload-button-logo {
  opacity: 0.8;
}

.icon {
  color: white;
  font-size: 25px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
    </style>

            <div class="col-xl-10 offset-xl-1" style="margin-top: 50px">

                <div class="login-register-page">
                    <div class="welcome-text">
                        <h3 style="font-size: 26px;">{{ __("Let's create your company,") }} <strong>{{ __('STEP BY STEP!') }}</strong></h3>
                        <span></span>
                    </div>
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-xl-12">
                                <div class="card px-0 pt-3 pb-0 mt-3 mb-3" style="margin-bottom: 100px">
                                    <form id="msform">
                                        <!-- progressbar -->
                                        <ul id="progressbar">
                                            <li class="active icon-feather-settings" id="account"> <strong style="font-family: nunito,helveticaneue,helvetica neue,Helvetica,Arial,sans-serif;">{{ __('Basic Details') }}</strong></li>
                                            <li id="personal" class="icon-feather-flag"><strong style="font-family: nunito,helveticaneue,helvetica neue,Helvetica,Arial,sans-serif;">{{ __('Identity') }}</strong></li>
                                             <li id="socialsTab" class="icon-feather-at-sign"><strong style="font-family: nunito,helveticaneue,helvetica neue,Helvetica,Arial,sans-serif;">{{ __('Socials') }}</strong></li>
                                             <li id="payment" class="icon-feather-map-pin"><strong style="font-family: nunito,helveticaneue,helvetica neue,Helvetica,Arial,sans-serif;">{{ __('Location') }}</strong></li>
                                            <li id="confirm" class="icon-feather-check"><strong style="font-family: nunito,helveticaneue,helvetica neue,Helvetica,Arial,sans-serif;">{{ __('Finish') }}</strong></li>
                                        </ul>
                                       <!-- fieldsets -->
                                        <fieldset>
                                            <div class="form-card">

                                                <div class="row">
                                                    <div class="col-xl-3">
                                                        <div class="logo-wrapper" id="logoTip"
                                                            title="{{ __('Upload Logo') }}">
                                                            <img class="profile-pic-logo" src="{{asset('frontOffice/images/company-logo-placeholder.png')}}" alt="{{ __('Company Picture') }}" />
                                                            <div class="upload-button-logo">  <a href="#" class="icon" title="Company logo">
    <i style="color: white" class="icon-feather-edit"></i>
  </a></div>
                                                            <input class="file-upload-logo" type="file" accept="image/*"  id="logo" name="logo" />
                                                        </div>

                                                    </div>

                                                    <div class="col-xl-9">
                                                        <div class="col-xl-12">

                                                            <div class="submit-field">
                                                                <h5>{{__('Name')}} (*)</h5>
                                                                <input type="text" class="with-border" placeholder="{{ __('Company Name') }}" id="name" name="name"  >

                                                            </div>

                                                        </div>
                                                        <div class="col-xl-12">
                                                            <div class="submit-field">
                                                                <h5>{{ __('Industry') }} (*)</h5>
                                                                <select class="selectpicker with-border typeSelect"  data-size="7" title="Select Campnay Industry" data-live-search="true" name="type" id="type">
                                                                    <option value="Admin Support" >Admin Support</option>
                                                                    <option value="Customer Service">Customer Service</option>
                                                                    <option value="Data Analytics" >Data Analytics</option>
                                                                    <option value="Design & Creative">Design & Creative</option>
                                                                    <option value="Legal" >Legal</option>
                                                                    <option value="Software Developing" >Software Developing</option>
                                                                    <option value="IT & Networking">IT & Networking</option>
                                                                    <option value="Writing">Writing</option>
                                                                    <option value="Translation">Translation</option>
                                                                    <option value="Sales & Marketing">Sales & Marketing</option>
                                                                </select>
                                                                <label for="type" generated="true" class="error"></label>

                                                            </div>
                                                        </div>

                                                        <div class="col-xl-12">

                                                            <div class="submit-field">
                                                                <h5>{{ __('Company Size') }}</h5>

                                                                        <input class="range-slider" id="size" type="text"  data-slider-currency="" data-slider-min="2" data-slider-max="100" data-slider-step="8" data-slider-value="[2,50]"/>

                                                        </div>
                                                        </div>

                                                        <div class="col-xl-12">
                                                            <div class="submit-field">
                                                                <h5>{{ __('Founded in') }}</h5>
                                                                <input type="date" class="with-border" name="founded_at" id="founded">

                                                            </div>

                                                        </div>

                                                </div>

                                            </div>
                                            </div>
                                            <button type="button" name="next" class="button next action-button button-sliding-icon ripple-effect ">{{ __('Next') }}<i class="icon-material-outline-arrow-right-alt"></i></button>

                                        </fieldset>


                                        <fieldset>
                                            <div class="form-card">
                                                    <div class="row">

                                                        <div class="col-xl-12">

                                                            <div class="submit-field">
                                                                <h5>{{ __('Tagline') }} (*)</h5>
                                                                <input type="text" class="with-border" placeholder="Tagline" name="tagline" id="tagline">

                                                            </div>

                                                        </div>
                                                        </div>
                                                    <div class="row">
                                                        <div class="col-xl-3">

                                                            <div class="submit-field">
                                                                <h5>{{ __('Phone') }} (*)</h5>
                                                                <input type="text" class="with-border" placeholder="+ (---) ________" name="phone" id="phone" >
                                                            </div>

                                                        </div>
                                                        <div class="col-xl-3">

                                                            <div class="submit-field">
                                                                <h5>Fax (Optional)</h5>
                                                                <input type="text" class="with-border" placeholder="Company Fax" name="fax" id="fax" >

                                                            </div>

                                                        </div>

                                                        <div class="col-xl-6">

                                                            <div class="submit-field">
                                                                <h5>{{ __('Website') }} ({{ __('Optional') }})</h5>
                                                                <input type="text" class="with-border" placeholder="Company Website" name="website" id="website" >
                                                            </div>

                                                        </div>
                                                    </div>
                                                <div class="row">
                                                    <div class="col-xl-12">
                                                        <div class="submit-field">
                                                            <h5>{{ __('Introduce your company') }} (*)</h5>
                                                            <textarea cols="30" rows="5"
                                                                      class="with-border" name="abt_me" id="abt_me"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                    </div>

                                            <button type="button" name="next" class="button next action-button button-sliding-icon ripple-effect ">{{ __('Next') }}<i class="icon-material-outline-arrow-right-alt"></i></button>


                                            <button type="button" name="previous" class="button previous dark action-button button-sliding-icon ripple-effect ">{{ __('previous') }}</button>
                                        </fieldset>

                                            
                                                <fieldset>  

                                                <div class="form-card">
                                                    <div class="row">
                                          <div class="col-xl-2">
                                      <div class="submit-field">
                                          <h5>{{ __('Socials') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Add your social accounts') }}"></i></h5>
                                          <select id="socials" class="selectpicker with-border">
                                                   <option value="linkedin" selected>LinkedIn</option>  
                                                <option value="facebook">Facebook</option>
                                                <option value="instagram">Instagram</option>
                                                <option value="github">Github</option>
                                                <option value="behance">Behance</option>
                                                <option value="twitter">Twitter</option>
                                                <option value="dribbble">Dribbble</option>
                                          </select>

                                      </div>
                                               </div>

                                              <div class="col-xl-4">


                                      <div class="submit-field">
                                          <h5>{{ __('Links') }} <i class="help-icon" data-tippy-placement="right" title="{{__('Add your social accounts')}}"></i> <span style="color: red" id="socialErrors"></span></h5>

                                          <!-- Skills List -->
                                          <div class="keywords-container">
                                              <div class="keyword-input-container">

                                                  <input id="socialLinks" type="text" class=" with-border" value="https://linkedin.com/"/>
                                                  <button class="socialAddBtn ripple-effect"  type="button"><i class="icon-material-outline-add"></i></button>

                                              </div>

                                                 <input type="hidden" id="companySocialLinks" name="companySocialLinks" value="">
                                              <div class="clearfix"></div>
                                          </div>
                                      </div>
                                           </div>

                                  <div class="col-xl-4">
                                      <div class="section-headline ">
                                          <h5>{{ __('Company social accounts') }} <span style="color: green" id="socialSuccess"></span></h5>
                                      </div>
                                      <div class="row">

                                          <div class="col-xl-12 col-md-12">
                                              <ul class="list-3 color">
                                                  <div class="companySocial">

                                                  </div>
                                              </ul>
                                          </div>
                                      </div>
                                  </div>   

</div>
                                                    </div>

                                            <button type="button" name="next" class="button next saveSocials action-button button-sliding-icon ripple-effect ">{{ __('Next') }}<i class="icon-material-outline-arrow-right-alt"></i></button>


                                            <button type="button" name="previous" class="button previous dark action-button button-sliding-icon ripple-effect ">{{ __('previous') }}</button>
                                        </fieldset>



                                        <fieldset>
                                            <div class="form-card">
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="submit-field">
                                                            <h5>{{ __('Location') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Type your address or pick it from the map below') }}"></i></h5>
                                                            <div class="input-with-icon">
                                                                <div id="autocomplete-container">
                                                                    <input id="autocomplete-input" autocomplete="off" type="text" placeholder="{{ __('Location') }}" class="address" name="address" >
                                                                    <input   type="hidden" name="city"     id="city">
                                                                    <input  type="hidden" name="province" id="province">
                                                                    <input type="hidden" name="lat"      id="lat">
                                                                    <input  type="hidden" name="lon"      id="lon">
                                                                </div>
                                                                <i class="icon-material-outline-location-on"></i>
                                                            </div>
                                                            <label for="autocomplete-input" generated="true" class="error"></label>
                                                            <span id="addrErr" style="display: none" class="error"> {{ __('Pick address from the suggestions or from the map below!') }} </span>
                                                        </div>

                                                    </div>
                                                    <div class="col-xl-2">
                                                        <div class="submit-field">
                                                            <h5>{{ __('Zip Code') }}</h5>
                                                            <input type="text" class="with-border" placeholder="Zip Code" name="zip_code" id="zip_code" >

                                                        </div>
                                                    </div>

                                                    <div class="col-xl-4">
                                                        <div class="submit-field">
                                                            <h5>{{ __('Nationality') }}</h5>
                                                            <select name="country" id="country" class="selectpicker with-border countrySelect" data-size="7" title="Select the company nationality" data-live-search="true">
                                                                <option value="Afghanistan">Afghanistan</option>
                                                                <option value="Åland Islands">Åland Islands</option>
                                                                <option value="Albania">Albania</option>
                                                                <option value="Algeria">Algeria</option>
                                                                <option value="American Samoa">American Samoa</option>
                                                                <option value="Andorra">Andorra</option>
                                                                <option value="Angola">Angola</option>
                                                                <option value="Anguilla">Anguilla</option>
                                                                <option value="Antarctica">Antarctica</option>
                                                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                <option value="Argentina">Argentina</option>
                                                                <option value="Armenia">Armenia</option>
                                                                <option value="Aruba">Aruba</option>
                                                                <option value="Australia">Australia</option>
                                                                <option value="Austria">Austria</option>
                                                                <option value="Azerbaijan">Azerbaijan</option>
                                                                <option value="Bahamas">Bahamas</option>
                                                                <option value="Bahrain">Bahrain</option>
                                                                <option value="Bangladesh">Bangladesh</option>
                                                                <option value="Barbados">Barbados</option>
                                                                <option value="Belarus">Belarus</option>
                                                                <option value="Belgium">Belgium</option>
                                                                <option value="Belize">Belize</option>
                                                                <option value="Benin">Benin</option>
                                                                <option value="Bermuda">Bermuda</option>
                                                                <option value="Bhutan">Bhutan</option>
                                                                <option value="Bolivia">Bolivia</option>
                                                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                                <option value="Botswana">Botswana</option>
                                                                <option value="Bouvet Island">Bouvet Island</option>
                                                                <option value="Brazil">Brazil</option>
                                                                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                <option value="Bulgaria">Bulgaria</option>
                                                                <option value="Burkina Faso">Burkina Faso</option>
                                                                <option value="Burundi">Burundi</option>
                                                                <option value="Cambodia">Cambodia</option>
                                                                <option value="Cameroon">Cameroon</option>
                                                                <option value="Canada">Canada</option>
                                                                <option value="Cape Verde">Cape Verde</option>
                                                                <option value="Cayman Islands">Cayman Islands</option>
                                                                <option value="Central African Republic">Central African Republic</option>
                                                                <option value="Chad">Chad</option>
                                                                <option value="Chile">Chile</option>
                                                                <option value="China">China</option>
                                                                <option value="Christmas Island">Christmas Island</option>
                                                                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                                <option value="Colombia">Colombia</option>
                                                                <option value="Comoros">Comoros</option>
                                                                <option value="Congo">Congo</option>
                                                                <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                                <option value="Cook Islands">Cook Islands</option>
                                                                <option value="Costa Rica">Costa Rica</option>
                                                                <option value="Cote D'ivoire">Cote D'ivoire</option>
                                                                <option value="Croatia">Croatia</option>
                                                                <option value="Cuba">Cuba</option>
                                                                <option value="Cyprus">Cyprus</option>
                                                                <option value="Czech Republic">Czech Republic</option>
                                                                <option value="Denmark">Denmark</option>
                                                                <option value="Djibouti">Djibouti</option>
                                                                <option value="Dominica">Dominica</option>
                                                                <option value="Dominican Republic">Dominican Republic</option>
                                                                <option value="Ecuador">Ecuador</option>
                                                                <option value="Egypt">Egypt</option>
                                                                <option value="El Salvador">El Salvador</option>
                                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                <option value="Eritrea">Eritrea</option>
                                                                <option value="Estonia">Estonia</option>
                                                                <option value="Ethiopia">Ethiopia</option>
                                                                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                                <option value="Faroe Islands">Faroe Islands</option>
                                                                <option value="Fiji">Fiji</option>
                                                                <option value="Finland">Finland</option>
                                                                <option value="France">France</option>
                                                                <option value="French Guiana">French Guiana</option>
                                                                <option value="French Polynesia">French Polynesia</option>
                                                                <option value="French Southern Territories">French Southern Territories</option>
                                                                <option value="Gabon">Gabon</option>
                                                                <option value="Gambia">Gambia</option>
                                                                <option value="Georgia">Georgia</option>
                                                                <option value="Germany">Germany</option>
                                                                <option value="Ghana">Ghana</option>
                                                                <option value="Gibraltar">Gibraltar</option>
                                                                <option value="Greece">Greece</option>
                                                                <option value="Greenland">Greenland</option>
                                                                <option value="Grenada">Grenada</option>
                                                                <option value="Guadeloupe">Guadeloupe</option>
                                                                <option value="Guam">Guam</option>
                                                                <option value="Guatemala">Guatemala</option>
                                                                <option value="Guernsey">Guernsey</option>
                                                                <option value="Guinea">Guinea</option>
                                                                <option value="Guinea-bissau">Guinea-bissau</option>
                                                                <option value="Guyana">Guyana</option>
                                                                <option value="Haiti">Haiti</option>
                                                                <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                                <option value="Honduras">Honduras</option>
                                                                <option value="Hong Kong">Hong Kong</option>
                                                                <option value="Hungary">Hungary</option>
                                                                <option value="Iceland">Iceland</option>
                                                                <option value="India">India</option>
                                                                <option value="Indonesia">Indonesia</option>
                                                                <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                                <option value="Iraq">Iraq</option>
                                                                <option value="Ireland">Ireland</option>
                                                                <option value="Isle of Man">Isle of Man</option>
                                                                <option value="Israel">Israel</option>
                                                                <option value="Italy">Italy</option>
                                                                <option value="Jamaica">Jamaica</option>
                                                                <option value="Japan">Japan</option>
                                                                <option value="Jersey">Jersey</option>
                                                                <option value="Jordan">Jordan</option>
                                                                <option value="Kazakhstan">Kazakhstan</option>
                                                                <option value="Kenya">Kenya</option>
                                                                <option value="Kiribati">Kiribati</option>
                                                                <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                                                <option value="Korea, Republic of">Korea, Republic of</option>
                                                                <option value="Kuwait">Kuwait</option>
                                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                                <option value="Latvia">Latvia</option>
                                                                <option value="Lebanon">Lebanon</option>
                                                                <option value="Lesotho">Lesotho</option>
                                                                <option value="Liberia">Liberia</option>
                                                                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                <option value="Liechtenstein">Liechtenstein</option>
                                                                <option value="Lithuania">Lithuania</option>
                                                                <option value="Luxembourg">Luxembourg</option>
                                                                <option value="Macao">Macao</option>
                                                                <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                                <option value="Madagascar">Madagascar</option>
                                                                <option value="Malawi">Malawi</option>
                                                                <option value="Malaysia">Malaysia</option>
                                                                <option value="Maldives">Maldives</option>
                                                                <option value="Mali">Mali</option>
                                                                <option value="Malta">Malta</option>
                                                                <option value="Marshall Islands">Marshall Islands</option>
                                                                <option value="Martinique">Martinique</option>
                                                                <option value="Mauritania">Mauritania</option>
                                                                <option value="Mauritius">Mauritius</option>
                                                                <option value="Mayotte">Mayotte</option>
                                                                <option value="Mexico">Mexico</option>
                                                                <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                                <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                                <option value="Monaco">Monaco</option>
                                                                <option value="Mongolia">Mongolia</option>
                                                                <option value="Montenegro">Montenegro</option>
                                                                <option value="Montserrat">Montserrat</option>
                                                                <option value="Morocco">Morocco</option>
                                                                <option value="Mozambique">Mozambique</option>
                                                                <option value="Myanmar">Myanmar</option>
                                                                <option value="Namibia">Namibia</option>
                                                                <option value="Nauru">Nauru</option>
                                                                <option value="Nepal">Nepal</option>
                                                                <option value="Netherlands">Netherlands</option>
                                                                <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                <option value="New Caledonia">New Caledonia</option>
                                                                <option value="New Zealand">New Zealand</option>
                                                                <option value="Nicaragua">Nicaragua</option>
                                                                <option value="Niger">Niger</option>
                                                                <option value="Nigeria">Nigeria</option>
                                                                <option value="Niue">Niue</option>
                                                                <option value="Norfolk Island">Norfolk Island</option>
                                                                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                <option value="Norway">Norway</option>
                                                                <option value="Oman">Oman</option>
                                                                <option value="Pakistan">Pakistan</option>
                                                                <option value="Palau">Palau</option>
                                                                <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                                <option value="Panama">Panama</option>
                                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                                <option value="Paraguay">Paraguay</option>
                                                                <option value="Peru">Peru</option>
                                                                <option value="Philippines">Philippines</option>
                                                                <option value="Pitcairn">Pitcairn</option>
                                                                <option value="Poland">Poland</option>
                                                                <option value="Portugal">Portugal</option>
                                                                <option value="Puerto Rico">Puerto Rico</option>
                                                                <option value="Qatar">Qatar</option>
                                                                <option value="Reunion">Reunion</option>
                                                                <option value="Romania">Romania</option>
                                                                <option value="Russian Federation">Russian Federation</option>
                                                                <option value="Rwanda">Rwanda</option>
                                                                <option value="Saint Helena">Saint Helena</option>
                                                                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                <option value="Saint Lucia">Saint Lucia</option>
                                                                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                                <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                                <option value="Samoa">Samoa</option>
                                                                <option value="San Marino">San Marino</option>
                                                                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                                <option value="Senegal">Senegal</option>
                                                                <option value="Serbia">Serbia</option>
                                                                <option value="Seychelles">Seychelles</option>
                                                                <option value="Sierra Leone">Sierra Leone</option>
                                                                <option value="Singapore">Singapore</option>
                                                                <option value="Slovakia">Slovakia</option>
                                                                <option value="Slovenia">Slovenia</option>
                                                                <option value="Solomon Islands">Solomon Islands</option>
                                                                <option value="Somalia">Somalia</option>
                                                                <option value="South Africa">South Africa</option>
                                                                <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                                <option value="Spain">Spain</option>
                                                                <option value="Sri Lanka">Sri Lanka</option>
                                                                <option value="Sudan">Sudan</option>
                                                                <option value="Suriname">Suriname</option>
                                                                <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                                <option value="Swaziland">Swaziland</option>
                                                                <option value="Sweden">Sweden</option>
                                                                <option value="Switzerland">Switzerland</option>
                                                                <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                                <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                                                <option value="Tajikistan">Tajikistan</option>
                                                                <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                                <option value="Thailand">Thailand</option>
                                                                <option value="Timor-leste">Timor-leste</option>
                                                                <option value="Togo">Togo</option>
                                                                <option value="Tokelau">Tokelau</option>
                                                                <option value="Tonga">Tonga</option>
                                                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                <option value="Tunisia" >Tunisia</option>
                                                                <option value="Turkey">Turkey</option>
                                                                <option value="Turkmenistan">Turkmenistan</option>
                                                                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                                <option value="Tuvalu">Tuvalu</option>
                                                                <option value="Uganda">Uganda</option>
                                                                <option value="Ukraine">Ukraine</option>
                                                                <option value="United Arab Emirates">United Arab Emirates</option>
                                                                <option value="United Kingdom">United Kingdom</option>
                                                                <option value="United States">United States</option>
                                                                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                <option value="Uruguay">Uruguay</option>
                                                                <option value="Uzbekistan">Uzbekistan</option>
                                                                <option value="Vanuatu">Vanuatu</option>
                                                                <option value="Venezuela">Venezuela</option>
                                                                <option value="Viet Nam">Viet Nam</option>
                                                                <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                                <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                                <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                                <option value="Western Sahara">Western Sahara</option>
                                                                <option value="Yemen">Yemen</option>
                                                                <option value="Zambia">Zambia</option>
                                                                <option value="Zimbabwe">Zimbabwe</option>
                                                            </select>
                                                            <label for="country" generated="true" class="error"></label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-xl-12">
                                                        <div id="singleMap" data-latitude="36.7948008" data-longitude="10.0031931"></div>
                                                    </div>
                                                </div>

                                            </div>

                                            <button type="button" name="next" class="button next action-button button-sliding-icon ripple-effect createCompany">{{ __('Submit') }}<i class="icon-material-outline-arrow-right-alt"></i></button>

                                            <button type="button" name="previous" class="button previous dark action-button button-sliding-icon ripple-effect ">{{ __('previous') }}</button>

                                        </fieldset>
                                        <fieldset>
                                            <div class="form-card">
                                                <div class="col-md-12">
                                                    <div class="order-confirmation-page" style="padding-bottom: 25px;">
                                                        <div class="breathing-icon"><i class="icon-feather-check"></i></div>
                                                        <h2 class="margin-top-30 companyCreated" style="font-size: 38px; line-height: 2rem">{{ __('Done, your company has been published!') }}</h2>
                                                        <p>{{ __('You can now start publishing jobs and tasks') }}</p>

                                                        <div style="display: inline-grid">
                                                            <a href="#" class="button ripple-effect-dark button-sliding-icon margin-top-30 companyProfile " style="width: 300px;">{{ __('View Company Profile') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                                                            <a href="{{ route('showPostJob') }}" class="button ripple-effect-dark button-sliding-icon margin-top-30" style="width: 300px;">{{ __('Post Your First Job') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                                                            <a href="{{ route('showPostTask') }}" class="button ripple-effect-dark button-sliding-icon margin-top-30" style="width: 300px;">{{ __('Post Your First Task') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                                                        </div>

                                                     </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                    </form>
                                    <a href="#" class="skipBtn">{{ __('Skip for now') }}</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                </div>

    <script>

        $('.skipBtn').on('click', function(e) {
            e.preventDefault();
            localStorage.removeItem('companySocialLinks');
            window.location.href = '{{ route('showSettings') }}';

        })

        var image = null;
        $(document).on('change','#logo', function () {
            image = $(this)[0].files[0];

        });
        $(document).ready(function(){

            $.validator.addMethod("maxDate", function(value, element) {
                var cdt = false;
                var curDate = new Date();
                var inputDate = new Date(value);
                if (inputDate < curDate) {
                    cdt = true;
                }

                return this.optional(element) || cdt

            }, "Invalid Date!");

       $.validator.addMethod('phone', function(value) {
var numbers = value.split(/\d/).length - 1;
return (10 <= numbers && numbers <= 20 && value.match(/^(\+){0,1}(\d|\s|\(|\)){10,20}$/)); }, 'Please enter a valid phone number');

            const p = document.getElementById('logoTip');
            tippy(p, {
                placement: 'bottom',
                animation: 'perspective',
                arrowType: 'round',
                title: 'Hello, beauty', // <--- not an option! set it on the element as an attribute
                theme: 'dark',
                arrow: true
            });
            p._tippy.show();

            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            var current = 1;
            var steps = $("fieldset").length;

        //    setProgressBar(current);

            $(".next").click(function(){
                var elm = $(this);
                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

//Add Class Active
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                $('#msform').validate({
                    rules: {
                        name: {
                            required: true,
                        },
                        type: {
                            required: true,
                        },
                        tagline: {
                            required: true,
                            minlength: 10,
                            maxlength: 50
                        },
                        phone: {
                            required: true,
                            phone: true
                        },
                        fax: {
                            minlength: 8,
                            digits: true
                        },
                        abt_me: {
                            required: true,
                            minlength: 100,
                            maxlength: 500,
                        },
                        website : {
                            url: true
                        },
                        address : {
                            required : true,
                        },
                        city : {
                            required : true,
                        },
                        province : {
                            required : true,
                        },
                        lat : {
                            required : true,
                        },
                        lon : {
                            required : true,
                        },
                        zip_code : {
                            required : true,
                            digits: true,
                            minlength: 4,
                        },
                        country : {
                            required : true,
                        },
                        founded_at : {
                            date: true,
                            maxDate: true
                        }
                    }
                });

                if ((!$('#msform').valid())) {
                    return false;
                }
                if(!elm.hasClass('createCompany')) {
                    next_fs.show();
                    current_fs.animate({opacity: 0}, {
                        step: function(now) {
                            opacity = 1 - now;

                            current_fs.css({
                                'display': 'none',
                                'position': 'relative'
                            });
                            next_fs.css({'opacity': opacity});
                        },
                        duration: 500
                    });
                 //   setProgressBar(++current);
                }


            });

            $(".previous").click(function(){

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                previous_fs.show();

                current_fs.animate({opacity: 0}, {
                    step: function(now) {
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({'opacity': opacity});
                    },
                    duration: 500
                });
              //  setProgressBar(--current);
            });

        /*    function setProgressBar(curStep){
                var percent = parseFloat(100 / steps) * curStep;
                percent = percent.toFixed();
                $(".progress-bar")
                    .css("width",percent+"%")
            }*/

            $(".createCompany").click(function(){
                var elm = $(this);
                current_fs = elm.parent();
                next_fs = $(this).parent().next();

                if ((!$('#msform').valid())) {
                    return false;
                }else if($('#lon').val() == '') {
                        $('#addrErr').fadeIn('slow').delay(2000).fadeOut();
                }
                else {
                   
                    elm.html('<span class="fa fa-spinner fa-spin fa fa-fw"></span>');
                    elm.css('opacity','0.5');
                    elm.css('cursor','not-allowed');
                    elm.attr("disabled", true);
                    $('.previous').hide();

                    var formData = new FormData();
                    formData.append("name", $('#name').val());
                    formData.append("type", $('#type').val());
                    formData.append("tagline", $('#tagline').val());
                    formData.append("phone", $('#phone').val());
                    formData.append("abt_me", $('#abt_me').val());
                    formData.append("address", $('.address').val());
                    formData.append("city", $('#city').val());
                    formData.append("province", $('#province').val());
                    formData.append("lat", $('#lat').val());
                    formData.append("lon", $('#lon').val());
                    formData.append("zip_code", $('#zip_code').val());
                    formData.append("country", $('#country').val());
                    formData.append("size", $('#size').val());
                    formData.append("companySocialLinks", $('#companySocialLinks').val());

                    if($('#founded').val().length) {
                        formData.append("founded_at", $('#founded').val());
                    }

                    if($('#fax').val().length) {
                        formData.append("fax", $('#fax').val());

                    }

                    if($('#website').val().length) {
                        formData.append("website", $('#website').val());
                    }

                    if(image != null) {
                        formData.append("logo", image);
                        formData.append("upload_file", true);
                    }

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "POST",
                        url: '{{ route('apiHandleCreateFirstCompany') }}',

                        success: function (data) {
                            $('.skipBtn').hide();
                            localStorage.removeItem('companySocialLinks');
                            var url = '{{ route('showCompanyDetails' ,':param') }}';
                            url = url.replace(':param', data.param);
                            $('.companyProfile').attr("href", url);
                            next_fs.show();
                            current_fs.animate({opacity: 0}, {
                                step: function(now) {
                                    opacity = 1 - now;

                                    current_fs.css({
                                        'display': 'none',
                                        'position': 'relative'
                                    });
                                    previous_fs.css({'opacity': opacity});
                                },
                                duration: 500
                            });

                        },
                        error: function (error) {
                            elm.html('Submit <i class="icon-material-outline-arrow-right-alt"></i>')
                            elm.css('opacity','1');
                            elm.css('cursor','pointer');
                            elm.attr("disabled", false);
                        },
                        async: true,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000
                    });


                }


            });

        });

         function initAutocomplete() {
                var city, street, place, state, code, sublocality = ' ';


                var input = document.getElementById('autocomplete-input');
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.addListener('place_changed', function() {
                    var placeUser = autocomplete.getPlace();
                    var lat = placeUser.geometry.location.lat();
                    var lng = placeUser.geometry.location.lng();
                    for (var i = 0; i < placeUser.address_components.length; i++) {
                        var addressType = placeUser.address_components[i]["types"][0];
                        switch (addressType) {
                            case 'route':
                                street = placeUser.address_components[i]['long_name'];

                                break;
                            case 'administrative_area_level_1':
                                place = placeUser.address_components[i]['long_name'];
                                break;
                            case 'locality':
                                city = placeUser.address_components[i]['long_name'];
                                break;
                            case 'sublocality_level_1':
                                sublocality = placeUser.address_components[i]['long_name'];
                                break;
                            case 'country':
                                country = placeUser.address_components[i]['long_name'];
                                break;
                        }


                    }
                    $('#city').val(city);
                    $('#province').val(sublocality);
                    $('#lat').val(lat);
                    $('#lon').val(lng);

                    var myLatLng = {
                        lng: lng,
                        lat: lat,
                    };
                    var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                        zoom: 18,
                        center: myLatLng,
                        scrollwheel: false,
                        zoomControl: true,
                        mapTypeControl: false,
                        scaleControl: false,
                        panControl: false,
                        navigationControl: false,
                        streetViewControl: false,
                        styles: [{
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [{
                                "color": "#f2f2f2"
                            }]
                        }]
                    });

                    var marker = new google.maps.Marker({
                        position: myLatLng,
                        draggable: true,
                        map: single_map,
                        title: 'Your location'
                    });

                    google.maps.event.addListener(single_map, 'click', function(event) {



                        marker.setPosition(event.latLng);
                        var geocoder	= new google.maps.Geocoder();							// create a geocoder object
                        var location	= new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());		// turn coordinates into an object

                        geocoder.geocode({'latLng': location}, function (results, status) {


                            $('#autocomplete-input').val("");
                            $('#city').val("");
                            $('#province').val("");
                            $('#zip').val("");
                            $('#lat').val("");
                            $('#lon').val("");
                            $('#autocomplete-input').val(results[1].formatted_address);

                            if (results[0].address_components[3]) {
                                $('#city').val(results[0].address_components[3].long_name);
                            }else {
                                $('#city').val(results[0].address_components[2].long_name);
                            }

                            $('#province').val(results[0].address_components[2].long_name);
                            $('#lat').val(event.latLng.lat());
                            $('#lon').val(event.latLng.lng());
                        });

                    });

                    if (!placeUser.geometry) {
                        window.alert("No details available for input: '" + place.name + "'");
                        return;
                    }
                });
            }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

    <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

    <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>

    <script>


        function singleMap() {
            var myLatLng = {
                lng: $('#singleMap').data('longitude'),
                lat: $('#singleMap').data('latitude'),
            };


            var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                zoom: 10,
                center: myLatLng,
                scrollwheel: false,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                panControl: false,
                navigationControl: false,
                streetViewControl: false,
                styles: [{
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }]
                }]
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                draggable: true,
                map: single_map,
                title: 'Your location'
            });
            var zoomControlDiv = document.createElement('div');
            var zoomControl = new ZoomControl(zoomControlDiv, single_map);


            function ZoomControl(controlDiv, single_map) {
                zoomControlDiv.index = 1;
                single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
                controlDiv.style.padding = '5px';
                var controlWrapper = document.createElement('div');
                controlDiv.appendChild(controlWrapper);
                var zoomInButton = document.createElement('div');
                zoomInButton.className = "mapzoom-in";
                controlWrapper.appendChild(zoomInButton);
                var zoomOutButton = document.createElement('div');
                zoomOutButton.className = "mapzoom-out";
                controlWrapper.appendChild(zoomOutButton);
                google.maps.event.addDomListener(zoomInButton, 'click', function() {
                    single_map.setZoom(single_map.getZoom() + 1);
                });
                google.maps.event.addDomListener(zoomOutButton, 'click', function() {
                    single_map.setZoom(single_map.getZoom() - 1);
                });
            }
            google.maps.event.addListener(marker, 'dragend', function(event) {
                document.getElementById("lat").value = event.latLng.lat();
                document.getElementById("long").value = event.latLng.lng();
            });

            google.maps.event.addListener(single_map, 'click', function(event) {



                marker.setPosition(event.latLng);
                var geocoder	= new google.maps.Geocoder();							// create a geocoder object
                var location	= new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());		// turn coordinates into an object

                geocoder.geocode({'latLng': location}, function (results, status) {


                    $('#autocomplete-input').val("");
                    $('#city').val("");
                    $('#province').val("");
                    $('#zip').val("");
                    $('#lat').val("");
                    $('#lon').val("");
                    $('#autocomplete-input').val(results[1].formatted_address);

                    if (results[0].address_components[3]) {
                        $('#city').val(results[0].address_components[3].long_name);
                    }else {
                        $('#city').val(results[0].address_components[2].long_name);
                    }

                    $('#province').val(results[0].address_components[2].long_name);
                    $('#lat').val(event.latLng.lat());
                    $('#lon').val(event.latLng.lng());
                });

            });
        }
        var single_map = document.getElementById('singleMap');
        if (typeof(single_map) != 'undefined' && single_map != null) {
            google.maps.event.addDomListener(window, 'load', singleMap);
        }


    </script>
    <script type="text/javascript">
        function initAutocomplete() {
            var city, street, place, state, code, sublocality = ' ';
            @if (Auth::user()->type === 0)
            multipleAutoComplete();
                @endif
            var input = document.getElementById('autocomplete-input');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var placeUser = autocomplete.getPlace();
                var lat = placeUser.geometry.location.lat();
                var lng = placeUser.geometry.location.lng();
                for (var i = 0; i < placeUser.address_components.length; i++) {
                    var addressType = placeUser.address_components[i]["types"][0];
                    switch (addressType) {
                        case 'route':
                            street = placeUser.address_components[i]['long_name'];

                            break;
                        case 'administrative_area_level_1':
                            place = placeUser.address_components[i]['long_name'];
                            break;
                        case 'locality':
                            city = placeUser.address_components[i]['long_name'];
                            break;
                        case 'sublocality_level_1':
                            sublocality = placeUser.address_components[i]['long_name'];
                            break;
                        case 'country':
                            country = placeUser.address_components[i]['long_name'];
                            break;
                    }


                }
                $('#city').val(city);
                $('#province').val(sublocality);
                $('#lat').val(lat);
                $('#lon').val(lng);

                var myLatLng = {
                    lng: lng,
                    lat: lat,
                };
                var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                    zoom: 18,
                    center: myLatLng,
                    scrollwheel: false,
                    zoomControl: true,
                    mapTypeControl: false,
                    scaleControl: false,
                    panControl: false,
                    navigationControl: false,
                    streetViewControl: false,
                    styles: [{
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#f2f2f2"
                        }]
                    }]
                });

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    draggable: true,
                    map: single_map,
                    title: 'Your location'
                });

                google.maps.event.addListener(single_map, 'click', function(event) {



                    marker.setPosition(event.latLng);
                    var geocoder	= new google.maps.Geocoder();							// create a geocoder object
                    var location	= new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());		// turn coordinates into an object

                    geocoder.geocode({'latLng': location}, function (results, status) {


                        $('#autocomplete-input').val("");
                        $('#city').val("");
                        $('#province').val("");
                        $('#zip').val("");
                        $('#lat').val("");
                        $('#lon').val("");
                        $('#autocomplete-input').val(results[1].formatted_address);

                        if (results[0].address_components[3]) {
                            $('#city').val(results[0].address_components[3].long_name);
                        }else {
                            $('#city').val(results[0].address_components[2].long_name);
                        }

                        $('#province').val(results[0].address_components[2].long_name);
                        $('#lat').val(event.latLng.lat());
                        $('#lon').val(event.latLng.lng());
                    });

                });

                if (!placeUser.geometry) {
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }
            });
        }
    </script>

    <script type="text/javascript">
        $('#socialLinks').focus(function() {
      $(window).keydown(function(event){
          if(event.keyCode == 13) {
              event.preventDefault();
              return false;
          }
      });

  });

        $('.saveSocials').on('click' ,function() {

                var companySocialLinks = "";
            $.each(JSON.parse(localStorage.getItem("companySocialLinks")) , function(i,socialLink) {
                if(socialLink.label != '') {
                      companySocialLinks+="," +socialLink.link;
                };
              });

                $('#companySocialLinks').val(companySocialLinks)
        });

    $('#socials').on('change' , function () {
      switch($(this).children("option:selected").val()) {

        case "linkedin":
      $('#socialLinks').val('https://linkedin.com/')

        break;
      case "github":
      $('#socialLinks').val('https://github.com/')

        break;
      case "behance":
      $('#socialLinks').val('https://behance.com/')

        break;
      case "facebook":
      $('#socialLinks').val('https://facebook.com/')

        break;
      case "dribbble":
      $('#socialLinks').val('https://dribbble.com/')

      break;
      case "twitter":
      $('#socialLinks').val('https://twitter.com/')

       break;
      case "instagram":
      $('#socialLinks').val('https://instagram.com/')

       break;
      }
    })

    $(document).ready(function () {
      if(JSON.parse(localStorage.getItem("companySocialLinks")) == null) {
        var initlsLinks = JSON.stringify([
          {
            label : '',
            link : ''
          }
        ])

          localStorage.setItem("companySocialLinks",initlsLinks );
      }else {
        lsLinks = JSON.parse(localStorage.getItem("companySocialLinks"));
        $.each(lsLinks , function(i,socialLink) {
            if(socialLink.label != '') {
              $('.list-3').append(' <li class="' + socialLink.label + '"> <span class="fa fa-' + socialLink.label + '"></span> <a style="margin-left: 5px;\n' +
                  '    margin-right: 30px;" href="' + socialLink.link + '" target="_blank">' + socialLink.label + '</a> </li>')

            };
          });
      }

    })

        $('#socialLinks').on('keyup', function(e) {
            if ((e.keyCode == 13) && ($(this).val() !== "")) {
              addSocialLink();
            }
        });

        $('.socialAddBtn').on('click', function () {
              addSocialLink();
        })

        function addSocialLink() {

            var label = $('#socials').children("option:selected").val();
            var link = $('#socialLinks').val();
            var lsLinks = JSON.parse(localStorage.getItem("companySocialLinks"));
             var status = checkSocialLinks(label,link);

                if (status == 'create') {

                  lsLinks.push({
                    label : label,
                    link : link
                  });
                    $('.list-3').append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                        '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

                          $('#socialSuccess').text('Link added successfully !').fadeIn('slow').delay(2000).fadeOut();
                            localStorage.setItem("companySocialLinks",    JSON.stringify(lsLinks) );
                    }else if (status == 'update') {
                        $('.'+label).remove();
                      var lsLinks = JSON.parse(localStorage.getItem("companySocialLinks"));
                      $.each(lsLinks , function(i,socialLink) {
                        if(socialLink.label == label) {
                            socialLink.link = link
                        }
                      });

                    $('.list-3').append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                        '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

                    $('#socialSuccess').text('Link updated successfully !').fadeIn('slow').delay(2000).fadeOut();

                }
                else if(status == 'exist') {
                    $('#socialErrors').text('Social link already exists !').fadeIn('slow').delay(2000).fadeOut();
                }

            };


function checkSocialLinks(label,link) {
  var status ='';
    $.each(JSON.parse(localStorage.getItem("companySocialLinks")) , function(i,socialLink) {
      if(socialLink.label == label) {
        if(socialLink.link == link) {
          status =  'exist';
        }else {
          status = 'update';
        }
      }else {
        status = 'create';
      }
    });
    return status;
}

  $(document).on('click','.social-delete',function() {

    var label = $(this).data('label');

    var  lsLinks = JSON.parse(localStorage.getItem("companySocialLinks"));
      $.each(lsLinks , function(i,socialLink) {
              if (socialLink.label == label) {
                  lsLinks.splice(i, 1);
                  $('.'+label).remove();
                  break;
              }
        });
        localStorage.setItem("companySocialLinks",    JSON.stringify(lsLinks) );

  });


    </script>
@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection
