@extends('frontOffice.layout',['title' => $company->name])
@section('meta')
    <meta property="og:url" content="{{ route('showCompanyDetails',$company->getCompanyParamName()) }}">
    <meta property="og:description" content="{{$company->name}}">
    <meta property="og:site_name" content="Hired.tn">
    <meta property="og:image" content="{{asset($company->medias()->where('type',0)->first()->link  ?? 'frontOffice/images/company-logo-placeholder.png')}}">
    <meta property="og:image:type" content="image/png">
    <meta property="twitter:creator" content="{{$company->name}}">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="{{$company->name}}">
    <meta property="twitter:description" content="{{$company->info->abt_me}}">
    <meta property="twitter:image:src" content="{{asset($company->medias()->where('type',0)->first()->link ?? 'frontOffice/images/company-logo-placeholder.png')}}">
    <meta property="twitter:image:width" content="1291">
    <meta property="twitter:image:height" content="315">
    <meta name="msapplication-TileColor" content="#2A5977">
    <meta name="theme-color" content="#2A5977">
    <meta name="description" content="{{ $company->info->abt_me }}">
    <meta name="keywords" content="{{ $company->name.','.$company->info->tagline.','.$company->info->abt_me }}">
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection
@section('content')
  <!-- Titlebar
  ================================================== -->
  <div class="single-page-header" data-background-image="{{asset('frontOffice/images/single-company.jpg')}}">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12">
  				<div class="single-page-header-inner">
  					<div class="left-side">
  						<div class="header-image"><img src="{{asset($company->medias()->where('type',0)->first() ? $company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png' )}}" alt="{{ $company->name }}"></div>
  						<div class="header-details">
  							<h3>{{$company->name}} <span>{{$company->info->tagline}} - <strong>{{ $company->industry }}</strong></span></h3>
  							<ul>
  								<li>
                                    @if(count($company->getAllRatings($company->id,'desc','App\Modules\User\Models\Company') ) < 3)
                                        <span class="company-not-rated">{{ __('Minimum of 3 votes required') }}</span>
                                    @else
                                        <div class="star-rating" data-rating="{{ $company->averageRating(1)[0] }}"></div>
                                    @endif
                                </li>
                                <li><img class="flag" src="{{ asset('frontOffice/images/flags/'.strtolower($company->country).'.svg') }}" alt="{{ $company->country }}"> {{ $company->country }}</li>
  							</ul>
  						</div>
  					</div>
  					<div class="right-side">
  						<!-- Breadcrumbs -->
  						<nav id="breadcrumbs" class="white">
  							<ul>
  								<li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
  								<li><a href="{{ route('showBrowseCompanies') }}">{{ __('Browse Companies') }}</a></li>
  								<li>{{$company->name}}</li>
  							</ul>
  						</nav>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>


  <!-- Page Content
  ================================================== -->
  <div class="container">
  	<div class="row">

  		<!-- Content -->
  		<div class="col-xl-8 col-lg-8 content-right-offset">

  			<div class="single-page-section">
  				<h3 class="margin-bottom-25">{{ __('About Company') }}</h3>
  				<p>{{ $company->info->abt_me }}</p>

  			</div>

  			<!-- Boxed List -->
  			<div class="boxed-list margin-bottom-60">
  				<div class="boxed-list-headline">
  					<h3><i class="icon-material-outline-business-center"></i> {{ __('Open Positions') }}</h3>
  				</div>

  				<div class="listings-container compact-list-layout">
                    @if(count($company->jobs) != 0)
  					<!-- Job Listing -->
                    @foreach($company->jobs as $job)
  					<a href="{{ route('showJobDetails', $job->getJobParamTitle()) }}" class="job-listing">

  						<!-- Job Listing Details -->
  						<div class="job-listing-details">

  							<!-- Details -->
  							<div class="job-listing-description">
  								<h3 class="job-listing-title">{{ $job->title }}</h3>

  								<!-- Job Listing Footer -->
  								<div class="job-listing-footer">
  									<ul>
  										<li><i class="icon-material-outline-location-on"></i> {{$job->address ? $job->address->city : ''}}</li>
  										<li><i class="icon-material-outline-business-center"></i> {{jobTypeToString($job->type)}} </li>
  										<li><i class="icon-material-outline-access-time"></i>{{ \Carbon\Carbon::parse($job->created_at)->diffForHumans() }}</li>
  									</ul>
  								</div>
  							</div>

  						</div>


                        @if(Auth::check() && Auth::id() != $job->user->id)
                            <span class="bookmark-icon bookmarkJob {{ Auth::user()->hasBookmarked($job) ? 'bookmarked' : '' }}" data-id="{{$job->id}}"></span>
                        @endif
  						<!-- Bookmark -->
  					</a>

                        @endforeach
                    @else

                            <ul class="boxed-list-ul ">
                                <li>{{ __('This company has no open position yet.') }}</li>
                            </ul>
                    @endif
                </div>
                <!-- Job Listing -->
  			</div>
  			<!-- Boxed List / End -->

  			<!-- Boxed List -->
  			<div class="boxed-list margin-bottom-60">
  				<div class="boxed-list-headline">
  					<h3><i class="icon-material-outline-thumb-up"></i> {{ __('Reviews') }}</h3>
  				</div>
  				<ul class="boxed-list-ul reviewsList">
                    @if(count($company->getAllRatings($company->id,'desc','App\Modules\User\Models\Company')) == 0)
                        <li class="noReviews">
                            <div class="boxed-list-item ">
                                {{ __('No reviews found!') }}
                            </div>
                        </li>
                    @else
                        @foreach($company->getAllRatings($company->id, 'desc','App\Modules\User\Models\Company') as $rating)

                            <li>
                                <div class="boxed-list-item">
                                    <!-- Content -->
                                    <div class="item-content">
                                        <h4>{{ $rating->title }} <span>{{ $rating->author->getFullName() }}</span></h4>
                                        <div class="item-details margin-top-10">
                                            <div class="star-rating" data-rating="{{ $rating->rating }}"></div>
                                            <div class="detail-item"><i class="icon-material-outline-date-range"></i> {{ Carbon\Carbon::parse($rating->created_at)->format('d F Y') }} </div>
                                        </div>
                                        <div class="item-description">
                                            <p>{{ $rating->body }}</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @endif
  				</ul>

  				<div class="centered-button margin-top-35">
                    @if(Auth::check())
                        @if(Auth::id() != $company->user->id)

                            @php
                                $authorsIds = [];
                               foreach ($company->getAllRatings($company->id,'desc','App\Modules\User\Models\Company') as $rating) {
                                        $authorsIds[] = $rating->author_id;
                                   }
                            @endphp
                            @if(Auth::id() == $company->user->id)
                            @endif



                            @if(in_array(Auth::id(), $authorsIds))
                                <a href="#small-dialog-1" class="popup-with-zoom-anim button button-sliding-icon">{{ __('Update Review') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                            @else
                                <a href="#small-dialog-1" class="popup-with-zoom-anim button button-sliding-icon leaveReviewBtn">{{ __('Leave a Review') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                            @endif
                        @else @endif

                    @else
                        <a href="#sign-in-dialog"  class="popup-with-zoom-anim button button-sliding-icon">{{ __('Leave a Review') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                    @endif
  				</div>

  			</div>
  			<!-- Boxed List / End -->

  		</div>


  		<!-- Sidebar -->
  		<div class="col-xl-4 col-lg-4">
  			<div class="sidebar-container">

  				<!-- Location -->
                <div class="single-page-section">
                    <h3 class="margin-bottom-30">{{ __('Location') }}</h3>
                    <div class="col-xl-12">

                        <div id="singleMap" style="height: 400px; width: 100%" data-latitude="{{$company->address->lat}}" data-longitude="{{$company->address->lon}}"></div>

                    </div>
                </div>

  				<!-- Widget -->
                @if(count($company->socials) != 0)
  				<div class="sidebar-widget">
  					<h3>{{ __('Social Profiles') }}</h3>
  					<div class="freelancer-socials margin-top-25">
  						<ul>
  							@foreach($company->socials as $link)
  							<li><a href="{{ $link->link }}" target="_blank" title="{{$link->label}}" data-tippy-placement="top"><i class="icon-brand-{{$link->label}}"></i></a></li>
                            @endforeach
  						</ul>
  					</div>
  				</div>
                @endif


  				<!-- Sidebar Widget -->
  				<div class="sidebar-widget">
  					<h3>{{ __('Bookmark or Share') }}</h3>

  					<!-- Bookmark Button -->
  			@if(Auth::check() && Auth::id() != $company->user->id )
                        <button class="bookmark-button margin-bottom-25  {{ Auth::user()->hasBookmarked($company) ? 'bookmarked' : '' }}">
                            <span class="bookmark-icon"></span>
                            <span class="bookmark-text">{{ __('Bookmark') }}</span>
                            <span class="bookmarked-text">{{ __('Bookmarked') }}</span>
                        </button>
                @endif

  					<!-- Copy URL -->
  					<div class="copy-url">
  						<input id="copy-url" type="text" value="" class="with-border">
  						<button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
  					</div>

  					<!-- Share Buttons -->
                    <div class="share-buttons margin-top-25">
                        <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                        <div class="share-buttons-content">
                            <span>{{ __('Interesting?') }} <strong>{{ __('Share It!') }}</strong></span>
                            <ul class="share-buttons-icons">
                                <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"
                                       data-sharer="facebook" data-title="{{$company->name}}" data-url="{{ route('showCompanyDetails',$company->getCompanyParamName())}}"
                                    ><i class="icon-brand-facebook-f"></i></a></li>

                                <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"
                                       data-sharer="twitter"  data-title="{{$company->name}}" data-url="{{ route('showCompanyDetails',$company->getCompanyParamName())}}"
                                    ><i class="icon-brand-twitter"></i></a></li>

                                <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"
                                       data-sharer="linkedin"  data-url="{{ route('showCompanyDetails',$company->getCompanyParamName())}}"
                                    ><i class="icon-brand-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
  				</div>

  			</div>
  		</div>

  	</div>
  </div>


  <!-- Spacer -->
  <div class="margin-top-15"></div>
  <!-- Spacer / End-->

  @include('User::frontOffice.auth.modals.login')


  <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>
  <script>
      $('#authCheckPopup').on('click' , function () {
          $.confirm({
              icon: 'fa fa-warning',
              title: 'Authentication is required!',
              type: 'orange',
              columnClass: 'col-xl-6 col-xl-offset-4',
              content: '       <h3 style="margin-top : 20px; margin-bottom: 10px"> You must be logged in to apply for a job.</h3>\n' +
                  '                            <a href="{{ route('showLogin') }}"> Already have an account? </a>\n' +
                  '                            <a href="{{ route('showRegister') }}"> Register now! </a>',
              buttons: {
                  cancel: function () {
                  }
              }
          });
      })


      function singleMap() {
          var myLatLng = {
              lng: $('#singleMap').data('longitude'),
              lat: $('#singleMap').data('latitude'),
          };

          var single_map = new google.maps.Map(document.getElementById('singleMap'), {
              zoom: 18,
              center: myLatLng,
              scrollwheel: false,
              zoomControl: true,
              mapTypeControl: false,
              scaleControl: false,
              panControl: false,
              navigationControl: false,
              streetViewControl: false,
              styles: [{
                  "featureType": "landscape",
                  "elementType": "all",
                  "stylers": [{
                      "color": "#f2f2f2"
                  }]
              }]
          });

          var marker = new google.maps.Marker({
              position: myLatLng,
              draggable: true,
              map: single_map,
              title: 'Your location'
          });
          var zoomControlDiv = document.createElement('div');
          var zoomControl = new ZoomControl(zoomControlDiv, single_map);


          function ZoomControl(controlDiv, single_map) {
              zoomControlDiv.index = 1;
              single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
              controlDiv.style.padding = '5px';
              var controlWrapper = document.createElement('div');
              controlDiv.appendChild(controlWrapper);
              var zoomInButton = document.createElement('div');
              zoomInButton.className = "mapzoom-in";
              controlWrapper.appendChild(zoomInButton);
              var zoomOutButton = document.createElement('div');
              zoomOutButton.className = "mapzoom-out";
              controlWrapper.appendChild(zoomOutButton);
              google.maps.event.addDomListener(zoomInButton, 'click', function() {
                  single_map.setZoom(single_map.getZoom() + 1);
              });
              google.maps.event.addDomListener(zoomOutButton, 'click', function() {
                  single_map.setZoom(single_map.getZoom() - 1);
              });
          }
          google.maps.event.addListener(marker, 'dragend', function(event) {
              document.getElementById("lat").value = event.latLng.lat();
              document.getElementById("long").value = event.latLng.lng();
          });

      }

      var single_map = document.getElementById('singleMap');
      if (typeof(single_map) != 'undefined' && single_map != null) {
          google.maps.event.addDomListener(window, 'load', singleMap);
      }

  </script>

  <script type="text/javascript">
      $('.bookmark-button').on('click', function () {
          $.get('{{ route('handleBookmark') }}?type=company&id='+{{ $company->id }}).done(function(res) {
              if(res.bookmarked) {
                  $('.bookmark-button').addClass('bookmarked');
              }else {
                  $('.bookmark-button').removeClass('bookmarked');
              }
          }).fail(function(error) {

          });
      });
  </script>

  <script type="text/javascript">
      $('.bookmarkJob').on('click', function (e) {
          var elm = $(this);
        e.preventDefault();
          $.get('{{ route('handleBookmark') }}?type=job&id='+$(this).data('id')).done(function(res) {
              if(res.bookmarked) {
                  $(elm).addClass('bookmarked');
              }else {
                  $(elm).removeClass('bookmarked');
              }
          }).fail(function(error) {

          });
      });
  </script>

  @include('User::frontOffice.modals.reviewCompany')
@endsection


@section('footer')
  @include('frontOffice.inc.footer')
@endsection
