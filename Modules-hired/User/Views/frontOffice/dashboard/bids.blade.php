@extends('frontOffice.layout',['class' => 'gray', 'title' => __('My Active Bids')])
@section('header')
      @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
  <!-- Dashboard Container -->
<div class="dashboard-container">
  <!-- Dashboard Sidebar
================================================== -->

  @include('User::frontOffice.dashboard.inc.sidebar')

<!-- Dashboard Sidebar / End -->

<!-- Dashboard Content
================================================== -->
<div class="dashboard-content-container" data-simplebar>
  <div class="dashboard-content-inner" >

    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
      <h3>{{ __('My Active Bids') }}</h3>

      <!-- Breadcrumbs -->
      <nav id="breadcrumbs" class="dark">
        <ul>
          <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
          <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
          <li>{{ __('My Active Bids') }}</li>
        </ul>
      </nav>
    </div>

    <!-- Row -->
    <div class="row">

      <!-- Dashboard Box -->
      <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">

          <!-- Headline -->
          <div class="headline">
            <h3><i class="icon-material-outline-gavel"></i> {{ __('Bids List') }}</h3>
          </div>

          <div class="content">
            <ul class="dashboard-box-list">
              @if(count(Auth::user()->bids()->doesntHave('accepted')->get()) > 0)
                @foreach(Auth::user()->bids()->doesntHave('accepted')->get() as $bid)
             
              <li>
                <!-- Job Listing -->
                <div class="job-listing width-adjustment">

                  <!-- Job Listing Details -->
                  <div class="job-listing-details">

                    <!-- Details -->
                    <div class="job-listing-description">
                      <h3 class="job-listing-title"><a href="{{ route('showTaskDetails' , $bid->task->getTaskParamName()) }}">{{$bid->task->name}}</a></h3>
                    </div>
                  </div>
                </div>

                <!-- Task Details -->
                <ul class="dashboard-task-info">
                  <li><strong>{{$bid->rate}} TND</strong><span>{{$bid->task->type == 0 ? 'Fixed Rate' : 'Hourly Rate'}}</span></li>
                  <li><strong>{{$bid->type == 0 ? $bid->delivery_time.' Days' :  $bid->delivery_time.' Hours' }}</strong><span>{{ __('Delivery Time') }}</span></li>
                </ul>

                <!-- Buttons -->
                <div class="buttons-to-right always-visible">
                  <a href="#small-dialog"  class="popup-with-zoom-anim button dark ripple-effect ico edit-bid" title="Edit Bid" data-tippy-placement="top" data-bid-type="{{$bid->task->budget->type}}" data-max="{{$bid->task->budget->max}}" data-min="{{$bid->task->budget->min}}" data-delivery_time ={{$bid->delivery_time}} data-bid={{$bid->id}}  data-rate={{$bid->rate}} data-type={{$bid->type}}><i class="icon-feather-edit"></i></a>
                  {{-- <a href="#" class="button red ripple-effect ico" title="Cancel Bid" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a> --}}
                  <a href="#!" class="popup-with-zoom-anim button gray ripple-effect ico bid-delete " title="Cancel Bid" data-tippy-placement="top" data-id = {{$bid->id}}><i class="icon-feather-trash-2" data-bid-delete = {{$bid->id}}></i></a>

                </div>
              </li>
                @endforeach
                @else
                  <li>
                    <h3>{{ __('You have no active bids,') }} <a href="{{ route('showTasks') }}">{{ __('Browse tasks') }}</a></h3>
                  </li>
              @endif
            </ul>
          </div>
        </div>
      </div>

    </div>
    <!-- Row / End -->

        @include('frontOffice.inc.small-footer')

  </div>
</div>
<!-- Dashboard Content / End -->


</div>

  <script type="text/javascript">
      $(function () {

          $('.edit-bid').on('click',function () {
              var delivery_time = $(this).data('delivery_time');
              var bid = $(this).data('bid');
              var type = $(this).data('type');
              var rate = $(this).data('rate');
              var max = $(this).data('max');
              var min = $(this).data('min');
              var bidType = $(this).data('bid-type');

              $('#delivery_time').val(delivery_time);
              $('#bid').val(bid);
              $(".selectpicker").val(type).find("option[value=" + type +"]").attr('selected', true);

              if (type == 0) {
                  $('span.filter-option.pull-left').text('Days');
                  $("ul").find("[data-original-index='1']").removeClass('selected');
                  $("ul").find("[data-original-index='0']").addClass('selected');
              }else {
                  $('span.filter-option.pull-left').text('Hours');
                  $("ul").find("[data-original-index='0']").removeClass('selected');
                  $("ul").find("[data-original-index='1']").addClass('selected');
              }

              $("#biddingVal").text(rate);
              $('#bidding-slider').slider('setAttribute', 'max', max);
              $('#bidding-slider').slider('setAttribute', 'min', min);
              $('#bidding-slider').slider('refresh');
              $('#bidding-slider').slider("setValue",rate)
              if(bidType == 0) {
                  $('#bidType strong').text('minimum fixed rate')
              }else {
                  $('#bidType strong').text('minimum hourly rate')
              }


          })

      })
  </script>
  <script>
      $(document).on("click", ".bid-delete", function(e) {
          var bidId = $(this).data('id');
          $.confirm({
              title: '{{ __('Confirm!') }}',
              content: '{{ __('Are you sure you want to delete this bid?') }}',
              buttons: {
                  cancel: function () {
                  },
                  somethingElse: {
                      text: 'Yes',
                      btnClass: 'button ripple-effect',
                      keys: ['enter', 'shift'],
                      action: function(){
                          deleteForm('{{route('HandleDeleteBid')}}', {
                              bidId : bidId
                          }, 'post');
                      }
                  }
              }
          });
      });

  </script>


@include('User::frontOffice.dashboard.modals.editBidPopup')

@endsection
