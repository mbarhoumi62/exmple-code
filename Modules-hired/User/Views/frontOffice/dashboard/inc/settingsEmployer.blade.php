
<style media="screen">

    .socialAddBtn {
        position: absolute;
        top: 0;
        right: 0;
        height: 36px;
        width: 36px;
        padding: 0;
        color: #fff;
        background-color: #2a41e8;
        border-radius: 4px;
        margin: 6px;
        font-size: 19px;
        text-align: center;
        line-height: 36px;
    }
    .socialUpdateBtn {
        position: absolute;
        top: 0;
        right: 0;
        height: 36px;
        width: 36px;
        padding: 0;
        color: #fff;
        background-color: #2a41e8;
        border-radius: 4px;
        margin: 6px;
        font-size: 19px;
        text-align: center;
        line-height: 36px;
    }

    .companyAddBtn {
      position: absolute;
    top: 10px;
    left: 180px;
    height: 36px;
    width: 110px;
    padding: 0;
    color: #fff;
    background-color: #2a41e8;
    border-radius: 4px;
    margin: 6px;
    font-size: 15px;
    text-align: center;
    line-height: 36px;
    }

        .companyAddBtn:hover {
      position: absolute;
    top: 10px;
    left: 180px;
    height: 36px;
    width: 110px;
    padding: 0;
    color: #fff;
    background-color: #2a41e8;
    border-radius: 4px;
    margin: 6px;
    font-size: 15px;
    text-align: center;
    line-height: 36px;
    }


    #singleMap {
        height: 300px;
        width: 100%;
    }

    @if (count(Auth::user()->companies)>0)
      @foreach (Auth::user()->companies as $company)
      #singleMap{{$company->id}} {
          height: 300px;
          width: 100%;
      }
      @endforeach
    @endif

    #form{
        display: none;
    }

    .company-wrapper {
        position: relative;
        width: 150px;
        height: 150px;
        border-radius: 4px;
        overflow: hidden;
        box-shadow: none;
        margin: 0 10px 30px 0;
        transition: all .3s ease
    }

    .company-wrapper:hover {
        transform: scale(1.05);
        cursor: pointer
    }
    @if (count(Auth::user()->companies)>0)
        @foreach (Auth::user()->companies as $company)
      .company-wrapper #company-profile-{{$company->id}}-pic {
        height: 100%;
        width: 100%;
        transition: all .3s ease;
        object-fit: cover
    }

    .company-wrapper #company-profile-{{$company->id}}-pic:after {
        font-family: Feather-Icons;
        content: "\e9f1";
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        line-height: 120px;
        position: absolute;
        font-size: 60px;
        background: #f0f0f0;
        color: #aaa;
        text-align: center
    }

    .company-wrapper #company-upload-button-{{$company->id}} {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%
    }

    .company-wrapper #company-file-upload-{{$company->id}} {
        opacity: 0;
        pointer-events: none;
        position: absolute
    }

    @endforeach
     @endif
    .hide{
      display: none;
    }


    .logo-wrapper {
    position: relative;
    width: 150px;
    height: 150px;
    border-radius: 4px;
    overflow: hidden;
    box-shadow: none;
    margin: 0 10px 30px 0;
    transition: all .3s ease;
}
.logo-wrapper .profile-pic-logo {
    height: 100%;
    width: 100%;
    transition: all .3s ease;
    object-fit: cover;
}
.logo-wrapper .upload-button-logo {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
}
.logo-wrapper .file-upload-logo {
    opacity: 0;
    pointer-events: none;
    position: absolute;
}
</style>


<!-- Dashboard Box -->

<div class="col-xl-12" id="company-created">

    <div class="dashboard-box">

        <!-- Headline -->
        <div class="headline">
            <h3><i class="icon-material-outline-business"></i> {{ __('My Companies') }}</h3>
            @if (count(Auth::user()->companies)>0)
                  <a href="{{ route('showAddCompanyAfterRegister') }}" class="ripple-effect companyAddBtn" ><span class="add">{{ __('Add New') }}</span></a>
            @endif

        </div>

          @if (count(Auth::user()->companies)>0)



            <div class="content" id="hideEmployerCompany">
              <ul class="dashboard-box-list">

                  @foreach (Auth::user()->companies as $company)
                    <li>
                      <!-- Job Listing -->
                      <div class="job-listing">

                        <!-- Job Listing Details -->
                        <div class="job-listing-details">

                          <!-- Logo -->
                          <a href="{{ route('showCompanyDetails', $company->getCompanyParamName()) }}" target="_blank" class="job-listing-company-logo">
                            <img src="{{asset($company->medias()->where('type',0)->first() ? $company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png')}}" alt="{{ $company->name }}">
                          </a>

                          <!-- Details -->
                          <div class="job-listing-description">
                            <h3 class="job-listing-title"><a href="{{ route('showCompanyDetails', $company->getCompanyParamName()) }}" target="_blank" >{{$company->name}}</a></h3>

                            <!-- Job Listing Footer -->
                            <div class="job-listing-footer">
                              <ul>
                                <li><i class="icon-material-outline-business"></i> {{$company->industry}}</li>
                                <li><i class="icon-material-outline-location-on"></i> {{$company->address->city ? $company->address->city : $company->address->province }}</li>
                                  @if($company->founded_at)
                                <li><i class="icon-material-outline-date-range"></i> {{ $company->founded_at->format('Y/d/m') }} - {{ __('Present') }}</li>
                                @endif
                              </ul>
                            </div>


                          </div>
                        </div>
                      </div>
                      <!-- Buttons -->
                      <div class="buttons-to-right">
                        <a href="#" class="popup-with-zoom-anim button red ripple-effect  company-delete " title="Remove" data-tippy-placement="left" data-id={{$company->id}}><i class="icon-feather-trash-2 company-delete" data-company-delete = {{$company->id}}></i></a>
                        <a href="#" class="button blue ripple-effect company-edit" data-type="{{ $company->industry }}" data-country="{{ $company->country }}" title="Edit" data-tippy-placement="right" data-company-edit = {{$company->id}}><i class="icon-feather-edit "></i></a>
                        <a href="{{ route('showCompanyDetails', $company->getCompanyParamName()) }}" target="_blank" class="button black ripple-effect" title="Details" data-tippy-placement="right"><i class="icon-feather-eye "></i></a>

                      </div>

                    </li>
                    <div id="company{{$company->id}}" class="hide toggleUpdate">
                      <form class="updateCompanyForm" action="{{route('HandleUpdateCompany',$company->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <ul class="fields-ul">
                            <li>
                                <div class="row">
                                  <div class="col-auto">
                                      <div class="company-wrapper" data-tippy-placement="bottom" title="{{ __('Upload Logo') }}">
                                          <img id="company-profile-{{$company->id}}-pic" src="{{asset($company->medias()->where('type',0)->first() ? $company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png')}}" alt="{{ $company->name }}" />
                                          <div id="company-upload-button-{{$company->id}}"></div>
                                          <input id="company-file-upload-{{$company->id}}"  type="file" accept="image/*"  name="company-logo" />

                                      </div>
                                      @if (Session::has('CompanyUpdateErrors'))
                                        @if (Session::has('CompanyId'))
                                            @if (Session::get('CompanyId') == $company->id)
                                              @if ($errors->has('company-logo'))
                                                <small>{{$errors->first('company-logo')}}</small>
                                              @endif
                                            @endif
                                        @endif
                                      @endif
                                  </div>

                                    <div class="col">
                                        <div class="row">
                                            <div class="col-xl-6">

                                                <div class="submit-field">
                                                    <h5>{{ __('Name') }}</h5>
                                                    <input type="text" class="with-border" placeholder="{{ __('Company Name') }}" name="name" value="{{$company->name}}">
                                                      @if (Session::has('CompanyUpdateErrors'))
                                                        @if (Session::has('CompanyId'))
                                                            @if (Session::get('CompanyId') == $company->id)

                                                              @if ($errors->has('name'))
                                                                <small>{{$errors->first('name')}}</small>
                                                              @endif
                                                            @endif
                                                            @endif
                                                      @endif
                                                </div>

                                            </div>

                                            <div class="col-xl-6">

                                                <div class="submit-field">
                                                    <h5>{{ __('Tagline') }}</h5>
                                                    <input type="text" class="with-border" placeholder="{{ __('Tagline') }}" name="tagline" value="{{$company->info->tagline}}">
                                                    @if (Session::has('CompanyUpdateErrors'))
                                                          @if ($errors->has('tagline'))
                                                            <small>{{$errors->first('tagline')}}</small>
                                                          @endif
                                                    @endif
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-4">
                                                <div class="submit-field">
                                                    <h5>{{ __('Industry') }}</h5>
                                                    <select class="selectpicker with-border updateTypeSelect" data-size="7" title="{{ __('Industry') }}" data-live-search="true" name="industry" id="industry">
                                                      <option value="Admin Support" >Admin Support</option>
                                                      <option value="Customer Service">Customer Service</option>
                                                      <option value="Data Analytics" >Data Analytics</option>
                                                      <option value="Design And Creative">Design And Creative</option>
                                                      <option value="Legal" >Legal</option>
                                                      <option value="Software Developing" >Software Developing</option>
                                                      <option value="IT And Networking">IT And Networking</option>
                                                      <option value="Writing">Writing</option>
                                                      <option value="Translation">Translation</option>
                                                      <option value="Sales And Marketing">Sales And Marketing</option>
                                                    </select>
                                                    @if (Session::has('CompanyUpdateErrors'))
                                                          @if ($errors->has('industry'))
                                                            <small>{{$errors->first('industry')}}</small>
                                                          @endif
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-xl-4">


                                                  <div class="submit-field">
                                                      <h5>{{ __('Company Siz') }}e</h5>
                                                      <input class="range-slider" name="size" type="text"  data-slider-currency="" data-slider-min="2" data-slider-max="100" data-slider-step="8" data-slider-value="@if($company->size) {{ '['.explode(',',$company->size)[0].','.explode(',',$company->size)[1].']' }} @else [2,50] @endif"/>
                                                       @if (Session::has('CompanyUpdateErrors'))
                                                              @if ($errors->has('size'))
                                                                <small>{{$errors->first('size')}}</small>
                                                              @endif
                                                        @endif
                                                  </div>



                                            </div>
                                            <div class="col-xl-4">
                                                <div class="submit-field">
                                                    <h5>{{ __('Founded in') }}</h5>
                                                    <input type="date" class="with-border" name="founded_at" value="{{ $company->founded_at ? $company->founded_at->format('Y-m-d') : '' }}">
                                                    @if (Session::has('CompanyUpdateErrors'))
                                                          @if ($errors->has('founded_at'))
                                                            <small>{{$errors->first('founded_at')}}</small>
                                                          @endif
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                  </li>
                                  <li>
                                <div class="row">


                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>{{ __('Location') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Type your address or pick it from the map below') }}"></i></h5>
                                                <div class="input-with-icon">
                                                    <div id="autocomplete-container">
                                                    <input id="autocomplete-input{{$company->id}}" type="text" placeholder="{{ __('Location') }}" name="address" value="{{$company->address->address}}" >
                                                        <input  type="hidden" name="city"     id="city{{$company->id}}"  value="{{$company->address->city}}" >
                                                        <input type="hidden" name="province" id="province{{$company->id}}"  value="{{$company->address->province}}" >
                                                        <input type="hidden" name="lat"      id="lat{{$company->id}}"  value="{{$company->address->lat}}" >
                                                        <input type="hidden" name="lon"      id="lon{{$company->id}}"  value="{{$company->address->lon}}" >
                                                  

                                                        @if (Session::has('CompanyUpdateErrors'))
                                                                  @if ($errors->has('address') || $errors->has('lon'))
                                                <small>{{$errors->first('address')}}</small>
                                              @endif
                                                        @endif
                                                    </div>
                                                    <i class="icon-material-outline-location-on"></i>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-xl-2">
                                            <div class="submit-field">
                                                <h5>{{ __('Zip Code') }}</h5>
                                                <input type="text" class="with-border" placeholder="{{ __('Zip Code') }}" name="zip_code" value="{{$company->address->zip}}" >
                                                @if (Session::has('CompanyUpdateErrors'))
                                                      @if ($errors->has('zip_code'))
                                                        <small>{{$errors->first('zip_code')}}</small>
                                                      @endif
                                                @endif
                                            </div>
                                        </div>

                                    <div class="col-xl-4">
                                        <div class="submit-field">
                                            <h5>{{ __('Nationality') }}</h5>
                                            <select name="country" id="countrySelect" class="selectpicker with-border updateCountrySelect" data-old-country="{{ $company->country }}" data-size="7" title="Select your nationality" data-live-search="true">
                                                <option value="Afghanistan">Afghanistan</option>
                                                <option value="Åland Islands">Åland Islands</option>
                                                <option value="Albania">Albania</option>
                                                <option value="Algeria">Algeria</option>
                                                <option value="American Samoa">American Samoa</option>
                                                <option value="Andorra">Andorra</option>
                                                <option value="Angola">Angola</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antarctica">Antarctica</option>
                                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                <option value="Argentina">Argentina</option>
                                                <option value="Armenia">Armenia</option>
                                                <option value="Aruba">Aruba</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Austria">Austria</option>
                                                <option value="Azerbaijan">Azerbaijan</option>
                                                <option value="Bahamas">Bahamas</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="Bhutan">Bhutan</option>
                                                <option value="Bolivia">Bolivia</option>
                                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                <option value="Botswana">Botswana</option>
                                                <option value="Bouvet Island">Bouvet Island</option>
                                                <option value="Brazil">Brazil</option>
                                                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                <option value="Bulgaria">Bulgaria</option>
                                                <option value="Burkina Faso">Burkina Faso</option>
                                                <option value="Burundi">Burundi</option>
                                                <option value="Cambodia">Cambodia</option>
                                                <option value="Cameroon">Cameroon</option>
                                                <option value="Canada">Canada</option>
                                                <option value="Cape Verde">Cape Verde</option>
                                                <option value="Cayman Islands">Cayman Islands</option>
                                                <option value="Central African Republic">Central African Republic</option>
                                                <option value="Chad">Chad</option>
                                                <option value="Chile">Chile</option>
                                                <option value="China">China</option>
                                                <option value="Christmas Island">Christmas Island</option>
                                                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                <option value="Colombia">Colombia</option>
                                                <option value="Comoros">Comoros</option>
                                                <option value="Congo">Congo</option>
                                                <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                <option value="Cook Islands">Cook Islands</option>
                                                <option value="Costa Rica">Costa Rica</option>
                                                <option value="Cote D'ivoire">Cote D'ivoire</option>
                                                <option value="Croatia">Croatia</option>
                                                <option value="Cuba">Cuba</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Czech Republic">Czech Republic</option>
                                                <option value="Denmark">Denmark</option>
                                                <option value="Djibouti">Djibouti</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="Dominican Republic">Dominican Republic</option>
                                                <option value="Ecuador">Ecuador</option>
                                                <option value="Egypt">Egypt</option>
                                                <option value="El Salvador">El Salvador</option>
                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                <option value="Eritrea">Eritrea</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Ethiopia">Ethiopia</option>
                                                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                <option value="Faroe Islands">Faroe Islands</option>
                                                <option value="Fiji">Fiji</option>
                                                <option value="Finland">Finland</option>
                                                <option value="France">France</option>
                                                <option value="French Guiana">French Guiana</option>
                                                <option value="French Polynesia">French Polynesia</option>
                                                <option value="French Southern Territories">French Southern Territories</option>
                                                <option value="Gabon">Gabon</option>
                                                <option value="Gambia">Gambia</option>
                                                <option value="Georgia">Georgia</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Ghana">Ghana</option>
                                                <option value="Gibraltar">Gibraltar</option>
                                                <option value="Greece">Greece</option>
                                                <option value="Greenland">Greenland</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guadeloupe">Guadeloupe</option>
                                                <option value="Guam">Guam</option>
                                                <option value="Guatemala">Guatemala</option>
                                                <option value="Guernsey">Guernsey</option>
                                                <option value="Guinea">Guinea</option>
                                                <option value="Guinea-bissau">Guinea-bissau</option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                <option value="Honduras">Honduras</option>
                                                <option value="Hong Kong">Hong Kong</option>
                                                <option value="Hungary">Hungary</option>
                                                <option value="Iceland">Iceland</option>
                                                <option value="India">India</option>
                                                <option value="Indonesia">Indonesia</option>
                                                <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                <option value="Iraq">Iraq</option>
                                                <option value="Ireland">Ireland</option>
                                                <option value="Isle of Man">Isle of Man</option>
                                                <option value="Israel">Israel</option>
                                                <option value="Italy">Italy</option>
                                                <option value="Jamaica">Jamaica</option>
                                                <option value="Japan">Japan</option>
                                                <option value="Jersey">Jersey</option>
                                                <option value="Jordan">Jordan</option>
                                                <option value="Kazakhstan">Kazakhstan</option>
                                                <option value="Kenya">Kenya</option>
                                                <option value="Kiribati">Kiribati</option>
                                                <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                                <option value="Korea, Republic of">Korea, Republic of</option>
                                                <option value="Kuwait">Kuwait</option>
                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                <option value="Latvia">Latvia</option>
                                                <option value="Lebanon">Lebanon</option>
                                                <option value="Lesotho">Lesotho</option>
                                                <option value="Liberia">Liberia</option>
                                                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                <option value="Liechtenstein">Liechtenstein</option>
                                                <option value="Lithuania">Lithuania</option>
                                                <option value="Luxembourg">Luxembourg</option>
                                                <option value="Macao">Macao</option>
                                                <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                <option value="Madagascar">Madagascar</option>
                                                <option value="Malawi">Malawi</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Maldives">Maldives</option>
                                                <option value="Mali">Mali</option>
                                                <option value="Malta">Malta</option>
                                                <option value="Marshall Islands">Marshall Islands</option>
                                                <option value="Martinique">Martinique</option>
                                                <option value="Mauritania">Mauritania</option>
                                                <option value="Mauritius">Mauritius</option>
                                                <option value="Mayotte">Mayotte</option>
                                                <option value="Mexico">Mexico</option>
                                                <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                <option value="Monaco">Monaco</option>
                                                <option value="Mongolia">Mongolia</option>
                                                <option value="Montenegro">Montenegro</option>
                                                <option value="Montserrat">Montserrat</option>
                                                <option value="Morocco">Morocco</option>
                                                <option value="Mozambique">Mozambique</option>
                                                <option value="Myanmar">Myanmar</option>
                                                <option value="Namibia">Namibia</option>
                                                <option value="Nauru">Nauru</option>
                                                <option value="Nepal">Nepal</option>
                                                <option value="Netherlands">Netherlands</option>
                                                <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                <option value="New Caledonia">New Caledonia</option>
                                                <option value="New Zealand">New Zealand</option>
                                                <option value="Nicaragua">Nicaragua</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Nigeria">Nigeria</option>
                                                <option value="Niue">Niue</option>
                                                <option value="Norfolk Island">Norfolk Island</option>
                                                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                <option value="Norway">Norway</option>
                                                <option value="Oman">Oman</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="Palau">Palau</option>
                                                <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                <option value="Panama">Panama</option>
                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                <option value="Paraguay">Paraguay</option>
                                                <option value="Peru">Peru</option>
                                                <option value="Philippines">Philippines</option>
                                                <option value="Pitcairn">Pitcairn</option>
                                                <option value="Poland">Poland</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="Puerto Rico">Puerto Rico</option>
                                                <option value="Qatar">Qatar</option>
                                                <option value="Reunion">Reunion</option>
                                                <option value="Romania">Romania</option>
                                                <option value="Russian Federation">Russian Federation</option>
                                                <option value="Rwanda">Rwanda</option>
                                                <option value="Saint Helena">Saint Helena</option>
                                                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                <option value="Saint Lucia">Saint Lucia</option>
                                                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                <option value="Samoa">Samoa</option>
                                                <option value="San Marino">San Marino</option>
                                                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                <option value="Senegal">Senegal</option>
                                                <option value="Serbia">Serbia</option>
                                                <option value="Seychelles">Seychelles</option>
                                                <option value="Sierra Leone">Sierra Leone</option>
                                                <option value="Singapore">Singapore</option>
                                                <option value="Slovakia">Slovakia</option>
                                                <option value="Slovenia">Slovenia</option>
                                                <option value="Solomon Islands">Solomon Islands</option>
                                                <option value="Somalia">Somalia</option>
                                                <option value="South Africa">South Africa</option>
                                                <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Sri Lanka">Sri Lanka</option>
                                                <option value="Sudan">Sudan</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                <option value="Swaziland">Swaziland</option>
                                                <option value="Sweden">Sweden</option>
                                                <option value="Switzerland">Switzerland</option>
                                                <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                                <option value="Tajikistan">Tajikistan</option>
                                                <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                <option value="Thailand">Thailand</option>
                                                <option value="Timor-leste">Timor-leste</option>
                                                <option value="Togo">Togo</option>
                                                <option value="Tokelau">Tokelau</option>
                                                <option value="Tonga">Tonga</option>
                                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                <option value="Tunisia">Tunisia</option>
                                                <option value="Turkey">Turkey</option>
                                                <option value="Turkmenistan">Turkmenistan</option>
                                                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                <option value="Tuvalu">Tuvalu</option>
                                                <option value="Uganda">Uganda</option>
                                                <option value="Ukraine">Ukraine</option>
                                                <option value="United Arab Emirates">United Arab Emirates</option>
                                                <option value="United Kingdom">United Kingdom</option>
                                                <option value="United States">United States</option>
                                                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                <option value="Uruguay">Uruguay</option>
                                                <option value="Uzbekistan">Uzbekistan</option>
                                                <option value="Vanuatu">Vanuatu</option>
                                                <option value="Venezuela">Venezuela</option>
                                                <option value="Viet Nam">Viet Nam</option>
                                                <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                <option value="Western Sahara">Western Sahara</option>
                                                <option value="Yemen">Yemen</option>
                                                <option value="Zambia">Zambia</option>
                                                <option value="Zimbabwe">Zimbabwe</option>
                                            </select>

                                        @if (Session::has('CompanyUpdateErrors'))
                                                @if ($errors->has('country'))
                                                    <small>{{$errors->first('country')}}</small>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                        <div class="col-xl-3">

                                            <div class="submit-field">
                                                <h5>{{ __('Phone') }}</h5>
                                                <input type="text" class="with-border" placeholder="+ (---) ___________" name="cPhone" value="{{$company->phone}}" >
                                                @if (Session::has('CompanyUpdateErrors'))
                                                      @if ($errors->has('cPhone'))
                                                        <small>{{$errors->first('cPhone')}}</small>
                                                      @endif
                                                @endif
                                            </div>

                                        </div>
                                        <div class="col-xl-3">

                                        <div class="submit-field">
                                            <h5>{{ __('Fax') }} ({{ __('Optional') }})</h5>
                                            <input type="text" class="with-border" placeholder="{{ __('Company Fax') }}" name="fax" value="{{$company->fax}}" >
                                            @if (Session::has('CompanyUpdateErrors'))
                                                  @if ($errors->has('fax'))
                                                    <small>{{$errors->first('fax')}}</small>
                                                  @endif
                                            @endif
                                        </div>

                                    </div>

                                    <div class="col-xl-6">

                                        <div class="submit-field">
                                            <h5>{{ __('Website') }} ({{ __('Optional') }})</h5>
                                            <input type="text" class="with-border" placeholder="{{ __('Company Website') }}" name="website" value="{{ $company->website}}" >
                                            @if (Session::has('CompanyUpdateErrors'))
                                                @if ($errors->has('website'))
                                                    <small>{{$errors->first('website')}}</small>
                                                @endif
                                            @endif
                                        </div>

                                    </div>


                                
                                    <div class="col-xl-12">
                                        <div class="submit-field">
                                            <h5>{{ __('Introduce your company') }}</h5>
                                            <textarea cols="30" rows="5"
                                              class="with-border" name="abt_me">{{$company->info->abt_me}}</textarea>
                                              @if (Session::has('CompanyUpdateErrors'))
                                                    @if ($errors->has('abt_me'))
                                                      <small>{{$errors->first('abt_me')}}</small>
                                                    @endif
                                              @endif
                                        </div>
                                    </div>
                                        <div class="col-xl-2">
                                        <div class="submit-field">
                                            <h5>{{ __('Socials') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Add your social accounts') }}"></i></h5>
                                            <select class="socials" id="socials{{$company->id}}" class="selectpicker with-border" data-index ="{{$company->id}}">
                                                <option value="linkedin" selected>LinkedIn</option>  
                                                <option value="facebook">Facebook</option>
                                                <option value="instagram">Instagram</option>
                                                <option value="github">Github</option>
                                                <option value="behance">Behance</option>
                                                <option value="twitter">Twitter</option>
                                                <option value="dribbble">Dribbble</option>

                                            </select>

                                        </div>
                                    </div>

                                    <div class="col-xl-4">


                                        <div class="submit-field">
                                            <h5>{{ __('Links') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Add your social accounts') }}"></i> <span style="color: red" id="socialErrors{{$company->id}}"></span></h5>

                                            <!-- Skills List -->
                                            <div class="keywords-container">
                                                <div class="keyword-input-container">

                                                    <input id="socialLinks{{$company->id}}" data-id = {{$company->id}} type="text" class="updateSocialLinks with-border" value="https://linkedin.com/"/>
                                                    <button class="socialUpdateBtn ripple-effect" data-id = {{$company->id}} type="button"><i class="icon-material-outline-add"></i></button>

                                                </div>


                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="section-headline ">
                                            <h5>{{ __('Company social accounts') }} <span style="color: green" id="socialSuccess{{$company->id}}"></span></h5>
                                        </div>
                                        <div class="row">

                                            <div class="col-xl-12 col-md-12">
                                                <ul class="list-3 color">
                                                    <li> <span class="icon-line-awesome-legal"></span> <a style="margin-left: 5px;
                                                      margin-right: 30px;" href="{{ route('showCompanyDetails', $company->getCompanyParamName()) }}">Hired</a>
                                                    </li>
                                                    <div id="companySocialLinks{{$company->id}}">
                                                        @if(count($company->socials) != 0)
                                                            @foreach($company->socials as $key => $link)
                                                                <li class="{{$link->label}}"> <span class="fa fa-{{$link->label}}"></span> <a style="margin-left: 5px;
                              margin-right: 30px;" href="{{$link->link}}">{{$link->label}}</a></li>
                                                            @endforeach
                                                        @endif

                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

<!-- 
                                    <div class="col-xl-12">

                                        <div id="singleMap{{$company->id}}" data-latitude="{{$company->address->lat}}" data-longitude="{{$company->address->lon}}"></div>

                                    </div> -->

                                </div>
                            </li>
                        </ul>
                        <div class="col-xl-12 text-center">
                          <button type="submit"  class="button ripple-effect big margin-top-30" style="font-size: 18px;padding: 13px 22px; margin-bottom: 15px;}"><i class="icon-feather-plus"></i> {{ __('Update Company') }}</button>
                        </div>
                      </form>

                    </div>
                  @endforeach

              </ul>
            </div>
          @else
            <div class="col-xl-12 text-center">
              <a href="{{route('showAddCompanyAfterRegister')}}" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> {{ __('Create your first company') }}</a>
            </div>


          @endif

        

    </div>
</div>


@if (count(Auth::user()->companies)>0)
  <script type="text/javascript">

    var company ;
    $('.socials').on('change' , function () {
 company = $(this).data('index');
                        switch($(this).children("option:selected").val()) {

                          case "linkedin":
                        $('#socialLinks'+company).val('https://linkedin.com/')

                          break;
                        case "github":
                        $('#socialLinks'+company).val('https://github.com/')

                          break;
                        case "behance":
                        $('#socialLinks'+company).val('https://behance.com/')

                          break;
                        case "facebook":
                        $('#socialLinks'+company).val('https://facebook.com/')

                          break;
                        case "dribbble":
                        $('#socialLinks'+company).val('https://dribbble.com/')

                        break;
                         case "twitter":
                        $('#socialLinks'+company).val('https://twitter.com/')

                        break;
                         case "instagram":
                        $('#socialLinks'+company).val('https://instagram.com/')

                        break;
                        }
                      })
    $('#socialLinks'+company).focusout(function() {

        updateCompanySocialLink()
    });

    $('.updateSocialLinks').focus(function() {
        var id = $(this).data('id');
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                updateCompanySocialLink(id);
                return false;
            }
        });

    });

    $('.updateSocialLinks').focusout(function() {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                $('.updateCompanyForm').submit();

            }
        });
    });

    $('.socialUpdateBtn').on('click', function () {

          updateCompanySocialLink($(this).data('id'));
    })

    function updateCompanySocialLink(company) {

        var label = $('#socials'+company).children("option:selected").val();
        var link = $('#socialLinks'+company).val();



        $.get("{{ route('apiHandleUpdateSocialLinkCompany')}}?label="+label+"&link="+link+"&company="+company).done(function (res) {
          if (res.status == 'created') {
              $('#companySocialLinks'+company).append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                  '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

          $('#socialSuccess'+company).text('Link added successfully !').fadeIn('slow').delay(2000).fadeOut();

          }else if (res.status == 'updated') {
                $('#companySocialLinks'+company+' .'+label).remove();
              $('#companySocialLinks'+company).append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                  '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

              $('#socialSuccess'+company).text('Link updated successfully !').fadeIn('slow').delay(2000).fadeOut();
          }
          else if(res.status == 400) {
              $('#socialErrors'+company).text('Social link already exists !').fadeIn('slow').delay(2000).fadeOut();
          }
          else {
              $('#socialErrors'+company).text('Something went wrong, please try again.').fadeIn('slow').delay(2000).fadeOut();
          }
        });
    }


  </script>
@endif

<script type="text/javascript">
        $('#campany').on('click',function () {

            localStorage.removeItem("companySocialLinks");
            var initlsLinks = JSON.stringify([
                {
                    label : '',
                    link : ''
                }
            ])

            localStorage.setItem("companySocialLinks",initlsLinks );

            $('.list-3').html(" ");
            $('#form').fadeIn();
            $(this).css('display', 'none');
        });

        $('#showAddFormComapny').on('click',function () {

            localStorage.removeItem("companySocialLinks");

            var initlsLinks = JSON.stringify([
                {
                    label : '',
                    link : ''
                }
            ])

            localStorage.setItem("companySocialLinks",initlsLinks );

            $('.list-3').html(" ");
            $('.toggleUpdate').hide();

            $('#form').slideToggle('slow');
            if($('#showAddFormComapny span').hasClass('add')) {
                $('#showAddFormComapny span').html('Cancel');
                $('#showAddFormComapny span').removeClass('add');
            }else {
                $('#showAddFormComapny span').html('Add New');
                $('#showAddFormComapny span').addClass('add');
            }

        });
</script>


@if (Session::has('CompanyCreateErrors'))

  <script type="text/javascript">
            $('#form').fadeIn();
            $('#campany').css('display', 'none');
          // window.location.href = '{{route('showSettings')}}' + '#form';

           $('#hideEmployerCompany').addClass('hide');
           $('#showAddFormComapny').addClass('hide');
           $(document).ready(function() {
               const addCompanyIndustry = '{{ $company->industry }}';
               if(addCompanyIndustry !== '') {
                   $('.updateTypeSelect').val(addCompanyIndustry);
               }

                const oldFoundedDate = '{{ old('founded_at') }}';
               $('#companyOldfoundedDate').val(oldFoundedDate);

           });


  </script>
@endif
@if (Session::has('CompanyCreated'))
  <script type="text/javascript">

         // window.location.href = '{{route('showSettings')}}' + '#company-created';
           localStorage.setItem("companySocialLinks",null);

  </script>
@endif

@if (count(Auth::user()->companies)>0)

    <script type="text/javascript">

        $('.company-delete').on('click',function (e) {
            e.preventDefault();
            var companyId = $(this).data('id');
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure you want to delete this company ?',
                buttons: {
                    cancel: function () {
                    },
                    somethingElse: {
                        text: 'Yes',
                        btnClass: 'button ripple-effect',
                        keys: ['enter', 'shift'],
                        action: function(){
                            deleteForm('{{route('HandleDeleteCompany')}}', {
                                companyId : companyId
                            }, 'post');
                        }
                    }
                }
            });


        });

        $('.company-edit').on('click',function (e) {
            e.preventDefault();
           var companyId =   $(this).data('company-edit');
           var type = $(this).data('type');
           var country = $(this).data('country');

            $('.updateTypeSelect').selectpicker('val',type);
            $('.updateCountrySelect').selectpicker('val',country);

          $('#company'+companyId).toggle('hide');

        });

    </script>

    <script type="text/javascript">
    @if (Session::has('CompanyUpdateErrors'))
      @if (Session::has('CompanyId'))
              $(function () {
                $('#company'+{{Session::get('CompanyId')}}).removeClass('hide');
                window.location.href = '#company'+{{Session::get('CompanyId')}};

                return false;
              })
      @endif
    @endif
    </script>
@endif

  <script type="text/javascript">


  $('#addNewCompany').on('click' , function() {
    var companySocialLinks = "";
    $.each(JSON.parse(localStorage.getItem("companySocialLinks")) , function(i,socialLink) {
        if(socialLink.label != '') {
              companySocialLinks+="," +socialLink.link;
        };
      });

        $('#companySocialLinks').val(companySocialLinks)
  })

  $('#socialLinks').focus(function() {
      $(window).keydown(function(event){
          if(event.keyCode == 13) {
              event.preventDefault();
              return false;
          }
      });

  });

  $('#socialLinks').focusout(function() {
      $(window).keydown(function(event){
          if(event.keyCode == 13) {
            var companySocialLinks = "";
            $.each(JSON.parse(localStorage.getItem("companySocialLinks")) , function(i,socialLink) {
                if(socialLink.label != '') {
                      companySocialLinks+="," +socialLink.link;
                };
              });

                $('#companySocialLinks').val(companySocialLinks)
              $('#addCompanyForm').submit();

          }
      });
  });

    $('#socials').on('change' , function () {
      switch($(this).children("option:selected").val()) {

        case "linkedin":
      $('#socialLinks').val('https://linkedin.com/')

        break;
      case "github":
      $('#socialLinks').val('https://github.com/')

        break;
      case "behance":
      $('#socialLinks').val('https://behance.com/')

        break;
      case "facebook":
      $('#socialLinks').val('https://facebook.com/')

        break;
      case "dribbble":
      $('#socialLinks').val('https://dribbble.com/')

      break;
      }
    })

    $(document).ready(function () {
      if(JSON.parse(localStorage.getItem("companySocialLinks")) == null) {
        var initlsLinks = JSON.stringify([
          {
            label : '',
            link : ''
          }
        ])

          localStorage.setItem("companySocialLinks",initlsLinks );
      }else {
        lsLinks = JSON.parse(localStorage.getItem("companySocialLinks"));
        $.each(lsLinks , function(i,socialLink) {
            if(socialLink.label != '') {
              $('.list-3').append(' <li class="' + socialLink.label + '"> <span class="fa fa-' + socialLink.label + '"></span> <a style="margin-left: 5px;\n' +
                  '    margin-right: 30px;" href="' + socialLink.link + '" target="_blank">' + socialLink.label + '</a> </li>')

            };
          });
      }

    })

        $('#socialLinks').on('keyup', function(e) {
            if ((e.keyCode == 13) && ($(this).val() !== "")) {
              addSocialLink();
            }
        });

        $('.socialAddBtn').on('click', function () {
              addSocialLink();
        })

        function addSocialLink() {

            var label = $('#socials').children("option:selected").val();
            var link = $('#socialLinks').val();
            var lsLinks = JSON.parse(localStorage.getItem("companySocialLinks"));
             var status = checkSocialLinks(label,link);

                if (status == 'create') {

                  lsLinks.push({
                    label : label,
                    link : link
                  });
                    $('.list-3').append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                        '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

                          $('#socialSuccess').text('Link added successfully !').fadeIn('slow').delay(2000).fadeOut();
                            localStorage.setItem("companySocialLinks",    JSON.stringify(lsLinks) );
                    }else if (status == 'update') {
                        $('.'+label).remove();
                      var lsLinks = JSON.parse(localStorage.getItem("companySocialLinks"));
                      $.each(lsLinks , function(i,socialLink) {
                        if(socialLink.label == label) {
                            socialLink.link = link
                        }
                      });

                    $('.list-3').append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                        '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

                    $('#socialSuccess').text('Link updated successfully !').fadeIn('slow').delay(2000).fadeOut();

                }
                else if(status == 'exist') {
                    $('#socialErrors').text('Social link already exists !').fadeIn('slow').delay(2000).fadeOut();
                }

            };


function checkSocialLinks(label,link) {
  var status ='';
    $.each(JSON.parse(localStorage.getItem("companySocialLinks")) , function(i,socialLink) {
      if(socialLink.label == label) {
        if(socialLink.link == link) {
          status =  'exist';
        }else {
          status = 'update';
        }
      }else {
        status = 'create';
      }
    });
    return status;
}

  $(document).on('click','.social-delete',function() {

    var label = $(this).data('label');

    var  lsLinks = JSON.parse(localStorage.getItem("companySocialLinks"));
      $.each(lsLinks , function(i,socialLink) {
              if (socialLink.label == label) {
                  lsLinks.splice(i, 1);
                  $('.'+label).remove();
                  break;
              }
        });
        localStorage.setItem("companySocialLinks",    JSON.stringify(lsLinks) );

  });


  </script>


<script>
    @if (count(Auth::user()->companies)>0)
        @foreach (Auth::user()->companies as $company)
    function companySwitcher_{{$company->id}}() {

        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#company-profile-{{$company->id}}-pic').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };
        $("#company-file-upload-{{$company->id}}").on('change', function () {
            readURL(this);
        });
        $("#company-upload-button-{{$company->id}}").on('click', function () {
            $("#company-file-upload-{{$company->id}}").click();
        });
    }

    companySwitcher_{{$company->id}}();
        @endforeach
    @endif

</script>
<script type="text/javascript">
    function multipleAutoComplete() {
      var city, street, place, state, code, sublocality = ' ';


        @if (count(Auth::user()->companies)>0)
          @foreach (Auth::user()->companies as $company)

          var input_{{$company->id}} = document.getElementById('autocomplete-input{{$company->id}}');
          var autocomplete_{{$company->id}} = new google.maps.places.Autocomplete(input_{{$company->id}});
          autocomplete_{{$company->id}}.addListener('place_changed', function() {
                        var placeUser = autocomplete_{{$company->id}}.getPlace();
                        var lat = placeUser.geometry.location.lat();
                        var lng = placeUser.geometry.location.lng();
                        for (var i = 0; i < placeUser.address_components.length; i++) {
                            var addressType = placeUser.address_components[i]["types"][0];
                         
                            switch (addressType) {
                                case 'route':
                                    street = placeUser.address_components[i]['long_name'];

                                    break;
                                case 'administrative_area_level_1':
                                    place = placeUser.address_components[i]['long_name'];
                                    break;
                                    case 'locality':
                                    city = placeUser.address_components[i]['long_name'];
                                    break;
                                    case 'sublocality_level_1':
                                    sublocality = placeUser.address_components[i]['long_name'];
                                    break;
                                    case 'country':
                                    country = placeUser.address_components[i]['long_name'];
                                    break;
                            }


                        }



                          $('#city{{$company->id}}').val(city);
                          $('#province{{$company->id}}').val(sublocality);
                          $('#lat{{$company->id}}').val(lat);
                          $('#lon{{$company->id}}').val(lng);

                          var myLatLng = {
                              lng: lng,
                              lat: lat,
                          };
           
                        });

        @endforeach
       @endif

    }
</script>

<script type="text/javascript">
    function initAutocomplete() {
        var city, street, place, state, code, sublocality = ' ';
        @if (Auth::user()->type === 0)
          multipleAutoComplete();
        @endif
    }
</script> 

 <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

<script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

<script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script> 
