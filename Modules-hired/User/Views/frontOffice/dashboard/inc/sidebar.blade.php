<!-- Dashboard Sidebar
================================================== -->
<div class="dashboard-sidebar">
  <div class="dashboard-sidebar-inner" data-simplebar>
    <div class="dashboard-nav-container">

      <!-- Responsive Navigation Trigger -->
      <a href="#" class="dashboard-responsive-nav-trigger">
        <span class="hamburger hamburger--collapse">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </span>
        <span class="trigger-title">{{ __('Dashboard Navigation') }}</span>
      </a>

      <!-- Navigation -->
      <div class="dashboard-nav">
        <div class="dashboard-nav-inner">

          <ul data-submenu-title="Start">
            <li ><a href="{{route('showDashboard')}}"><i class="icon-material-outline-dashboard"></i> {{ __('Dashboard') }}</a></li>

            <li><a href="{{route('showMessages')}}"><i class="icon-material-outline-question-answer"></i> {{ __('Messages') }}</a></li>
            <li><a href="{{route('showBookmarks')}}"><i class="icon-material-outline-star-border"></i> {{ __('Bookmarks') }}</a></li>
{{--
            <li><a href="{{route('showReviews')}}"><i class="icon-material-outline-rate-review"></i> Reviews</a></li>
--}}
          </ul>

          <ul data-submenu-title="Organize and Manage">

            
          @if(Auth::user()->type == 0)
            <li><a href="#"><i class="icon-material-outline-business-center"></i> {{ __('Jobs') }}</a>
              <ul>
                <li><a href="{{route('showManageJobs')}}">{{ __('Manage Jobs') }} <span class="nav-tag">{{ count(Auth::user()->jobs) }}</span></a></li>
                <li><a href="{{route('showManageCandidates')}}">{{ __('Manage Candidates') }}</a></li>
                <li><a @if ((Auth::user()->type == 0) && (count(Auth::user()->companies)== 0)) href="#" id="UserNotHaveCompany" @else href="{{route('showPostJob')}}" @endif>{{ __('Post a Job') }}</a></li>
              </ul>
            </li>
              @endif
            <li><a href="#"><i class="icon-material-outline-assignment"></i> {{ __('Tasks') }}</a>
              <ul>
                <li><a href="{{route('showManageTask')}}">{{ __('Manage Tasks') }} <span class="nav-tag">{{count(Auth::user()->tasks()->where('status',1)->get())}}</span></a></li>
                @if(Auth::user()->type == 0)

                  <li><a @if(count(Auth::user()->tasks()->where('status',2)->get()) == 0) id="userHaveNoMangeTasks" href="#" @else href="{{route('showManageInProgressTasks')}}" @endif> {{ __('Manage InProgress Tasks') }} <span class="nav-tag">{{count(Auth::user()->tasks()->where('status',2)->get())}}</span></a></li>

                @else
                   <li><a  @if(count(Auth::user()->bids()->whereHas('accepted')->get()) == 0) id="userHaveNoMangeTasks" href="#" @else href="{{route('showInProgressTasks')}}" @endif> {{ __('My InProgress Tasks') }}<span class="nav-tag">{{count(Auth::user()->bids()->whereHas('accepted')->get())}}</span></a></li>

                @endif


                <li><a @if(count(Auth::user()->tasks()->where('status',1)->get()) == 0) href="#" id="userHaveNoTasks" @else href="{{route('showManageBidders')}}" @endif> {{ __('Manage Bidders') }} </a></li>
                  <li><a href="{{route('showActiveBids')}}"> {{ __('My Active Bids') }} <span class="nav-tag">{{ count(Auth::user()->bids) - count(Auth::user()->bids()->whereHas('accepted')->get()) }}</span></a></li>
                <li><a  href="{{route('showPostTask')}}"  > {{ __('Post a Task') }}</a></li>
              </ul>
            </li>
              @if(Auth::user()->type == 1)

                  <li><a href="#"><i class="icon-line-awesome-tasks"></i> {{ __('Services') }}</a>
                      <ul>
                          <li><a href="{{route('showManageServices')}}"> {{ __('Manage Services') }} <span class="nav-tag">{{count(Auth::user()->services)}}</span></a></li>
                          <li><a href="{{ route('showManageBookings') }}" > {{ __('Manage Bookings') }} </a></li>
                          <li><a href="{{route('showPostService')}}"  > {{ __('Post a Service') }}</a></li>
                      </ul>
                  </li>
              @endif
          </ul>
          <ul data-submenu-title="Account">
            @if(Auth::user()->type == 1)
              <li><a href="{{route('showProfile')}}"><i class="icon-material-outline-face"></i> {{ __('Profile') }}</a></li>
              <li><a href="{{route('showPortfolio')}}"><i class="icon-material-outline-folder-shared"></i> {{ __('Portfolio') }}</a></li>
            @endif
              @if(Auth::user()->type == 0)
                <li><a href="{{route('showCompanies')}}"><i class="icon-material-outline-business"></i> {{ __('Companies') }}</a></li>
              @endif
            <li><a href="{{route('showCalendar')}}"><i class="icon-feather-calendar"></i> {{ __('Interviews') }}</a></li>
            <li><a href="{{route('showSettings')}}"><i class="icon-material-outline-settings"></i> {{ __('Settings') }}</a></li>
            <li><a href="{{ route('handleLogout') }}"><i class="icon-material-outline-power-settings-new"></i> {{ __('Logout') }}</a></li>
          </ul>

        </div>
      </div>
      <!-- Navigation / End -->

    </div>
  </div>
</div>
<!-- Dashboard Sidebar / End -->


<script type="text/javascript">
$('#UserNotHaveCompany').on('click',function(e) {
  e.preventDefault();

      Snackbar.show({
          text: '{{__('You must have at least one company in order to post a job.')}}',
          pos: 'bottom-right',
          showAction: false,
          actionText: "Dismiss",
          duration: 3000,
          textColor: '#fff',
          backgroundColor: '#de5959'
      });
});

$('#cantCreateService').on('click',function(e) {
  e.preventDefault();

      Snackbar.show({
          text: '{{__('You must update your profile infos to be able to post a service. ')}}'+'<strong><a style="color:white" href="{{ route('showProfile') }}">{{ __('Update now!') }}</a></strong>',
          pos: 'bottom-right',
          showAction: false,
          actionText: "Dismiss",
          duration: 5000,
          textColor: '#fff',
          backgroundColor: '#de5959'
      });
})

$('#userHaveNoTasks').on('click',function(e) {
  e.preventDefault();

    Snackbar.show({
        text: 'You have no active tasks!',
        pos: 'bottom-right',
        showAction: false,
        actionText: "Dismiss",
        duration: 3000,
        textColor: '#fff',
        backgroundColor: '#de5959'
    });
})

$('#userHaveNoMangeTasks').on('click',function(e) {
  e.preventDefault();

    Snackbar.show({
        text: '{{__("You don\'t have any task in progress")}}',
        pos: 'bottom-right',
        showAction: false,
        actionText: "Dismiss",
        duration: 3000,
        textColor: '#fff',
        backgroundColor: '#de5959'
    });
})



    @if(Session::has('NotAllowed'))
    Snackbar.show({
        text: '{{__('You must have at least one company in order to post a job.')}}',
        pos: 'bottom-right',
        showAction: false,
        actionText: "Dismiss",
        duration: 3000,
        textColor: '#fff',
        backgroundColor: '#de5959'
    });

    @endif

    $(document).ready(function () {
         var current_title = $(document).attr('title');
           $.each($('.dashboard-nav-inner ul li a'), function (i, item) {
                  if(item.text.replace(/\s+/g, '') === current_title.replace(/\s+/g, '')) {
                      $(this).parent().addClass('active');
                  }
           })
    })

</script>
