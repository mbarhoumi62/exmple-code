<!-- Dashboard Box -->
<style>
    #progress-wrp {
        border: 1px solid #0099CC;
        padding: 1px;
        position: relative;
        height: 30px;
        border-radius: 3px;
        margin: 10px;
        text-align: left;
        background: #fff;
        box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
    }

    .submit-field > .btn-group > .dropdown-toggle {
        border-right: none!important;
        border-radius: 5px 0px 0px 5px;
    }

    #progress-wrp .progress-bar {
        height: 100%;
        border-radius: 3px;
        background-color: #2a41e8;
        width: 0;
        box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
    }

    #progress-wrp .status {
        top: 3px;
        left: 40%;
        position: absolute;
        display: inline-block;
        color: #000000;
    }

    .socialAddBtn {
        position: absolute;
        top: 0;
        right: 0;
        height: 36px;
        width: 36px;
        padding: 0;
        color: #fff;
        background-color: #2a41e8;
        border-radius: 4px;
        margin: 6px;
        font-size: 19px;
        text-align: center;
        line-height: 36px;
    }

    .uploadButton {
        align-items: center;
        display: block;
    }

    .uploadButton-button-video {
        display: flex;
        align-items: center;
        justify-content: center;
        box-sizing: border-box;
        height: 44px;
        padding: 10px 18px;
        cursor: pointer;
        border-radius: 4px;
        color: #365ee8;
        background-color: transparent;
        border: 1px solid #365ee8;
        flex-direction: row;
        transition: .3s;
        margin: 0;
        outline: none;
        box-shadow: 0 3px 10px rgba(102,103,107,.1);
    }

    .uploadButton-input-video {
        opacity: 0;
        position: absolute;
        overflow: hidden;
        z-index: -1;
        pointer-events: none;
    }
    .input-with-icon-right i{
        position: absolute;
        right: 0;
        top: 0;
        color: #a0a0a0;
        text-align: center;
        line-height: 48px;
        width: 48px;
        height: 48px;
        font-size: 15px;
        background-color: #f8f8f8;
        border: 1px solid #e0e0e0;
        box-sizing: border-box;
        display: block;
        border-radius: 0px 4px 4px 0px;
    }
</style>
<div class="col-xl-12">
  <div class="dashboard-box">
    <!-- Headline -->

      <div class="headline">

      <h3 ><i class="icon-material-outline-face"></i> {{ __('My Profile') }} </h3>
          <a href="#!" class="accordionHandler ripple-effect slideProfile" style="top: 10px!important"><i class="icon-line-awesome-minus" style="color:#fff;"></i></a>

    </div>

    <div id="profileAccordion"  class="content">
      <ul class="fields-ul">


      <li id="freelancerInfos">

          <form  action="{{ route('handleUpdateFreelancerInfo') }}" method="post" >
              @csrf
        <div  class="row">

          <div class="col-xl-6">
            <div class="submit-field">
              <h5>{{ __('Tagline') }}</h5>
              <input type="text" value="{{ Auth::user()->info ? Auth::user()->info->tagline : old('tagline') }}" name="tagline" class="with-border" placeholder="iOS Expert + Node Dev">
                @if ($errors->has('tagline'))
                 <small>{{ $errors->first('tagline') }}</small>
                @endif
            </div>
          </div>

									<div class="col-xl-6">
										<div class="submit-field">
											<h5>{{ __('Nationality') }}</h5>
											<select name="country" class="selectpicker with-border countrySelect" data-size="7" title="{{ __('Select your nationality') }}" data-live-search="true">
                        <option value="Afghanistan">Afghanistan</option>
  <option value="Åland Islands">Åland Islands</option>
  <option value="Albania">Albania</option>
  <option value="Algeria">Algeria</option>
  <option value="American Samoa">American Samoa</option>
  <option value="Andorra">Andorra</option>
  <option value="Angola">Angola</option>
  <option value="Anguilla">Anguilla</option>
  <option value="Antarctica">Antarctica</option>
  <option value="Antigua and Barbuda">Antigua and Barbuda</option>
  <option value="Argentina">Argentina</option>
  <option value="Armenia">Armenia</option>
  <option value="Aruba">Aruba</option>
  <option value="Australia">Australia</option>
  <option value="Austria">Austria</option>
  <option value="Azerbaijan">Azerbaijan</option>
  <option value="Bahamas">Bahamas</option>
  <option value="Bahrain">Bahrain</option>
  <option value="Bangladesh">Bangladesh</option>
  <option value="Barbados">Barbados</option>
  <option value="Belarus">Belarus</option>
  <option value="Belgium">Belgium</option>
  <option value="Belize">Belize</option>
  <option value="Benin">Benin</option>
  <option value="Bermuda">Bermuda</option>
  <option value="Bhutan">Bhutan</option>
  <option value="Bolivia">Bolivia</option>
  <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
  <option value="Botswana">Botswana</option>
  <option value="Bouvet Island">Bouvet Island</option>
  <option value="Brazil">Brazil</option>
  <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
  <option value="Brunei Darussalam">Brunei Darussalam</option>
  <option value="Bulgaria">Bulgaria</option>
  <option value="Burkina Faso">Burkina Faso</option>
  <option value="Burundi">Burundi</option>
  <option value="Cambodia">Cambodia</option>
  <option value="Cameroon">Cameroon</option>
  <option value="Canada">Canada</option>
  <option value="Cape Verde">Cape Verde</option>
  <option value="Cayman Islands">Cayman Islands</option>
  <option value="Central African Republic">Central African Republic</option>
  <option value="Chad">Chad</option>
  <option value="Chile">Chile</option>
  <option value="China">China</option>
  <option value="Christmas Island">Christmas Island</option>
  <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
  <option value="Colombia">Colombia</option>
  <option value="Comoros">Comoros</option>
  <option value="Congo">Congo</option>
  <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
  <option value="Cook Islands">Cook Islands</option>
  <option value="Costa Rica">Costa Rica</option>
  <option value="Cote D'ivoire">Cote D'ivoire</option>
  <option value="Croatia">Croatia</option>
  <option value="Cuba">Cuba</option>
  <option value="Cyprus">Cyprus</option>
  <option value="Czech Republic">Czech Republic</option>
  <option value="Denmark">Denmark</option>
  <option value="Djibouti">Djibouti</option>
  <option value="Dominica">Dominica</option>
  <option value="Dominican Republic">Dominican Republic</option>
  <option value="Ecuador">Ecuador</option>
  <option value="Egypt">Egypt</option>
  <option value="El Salvador">El Salvador</option>
  <option value="Equatorial Guinea">Equatorial Guinea</option>
  <option value="Eritrea">Eritrea</option>
  <option value="Estonia">Estonia</option>
  <option value="Ethiopia">Ethiopia</option>
  <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
  <option value="Faroe Islands">Faroe Islands</option>
  <option value="Fiji">Fiji</option>
  <option value="Finland">Finland</option>
  <option value="France">France</option>
  <option value="French Guiana">French Guiana</option>
  <option value="French Polynesia">French Polynesia</option>
  <option value="French Southern Territories">French Southern Territories</option>
  <option value="Gabon">Gabon</option>
  <option value="Gambia">Gambia</option>
  <option value="Georgia">Georgia</option>
  <option value="Germany">Germany</option>
  <option value="Ghana">Ghana</option>
  <option value="Gibraltar">Gibraltar</option>
  <option value="Greece">Greece</option>
  <option value="Greenland">Greenland</option>
  <option value="Grenada">Grenada</option>
  <option value="Guadeloupe">Guadeloupe</option>
  <option value="Guam">Guam</option>
  <option value="Guatemala">Guatemala</option>
  <option value="Guernsey">Guernsey</option>
  <option value="Guinea">Guinea</option>
  <option value="Guinea-bissau">Guinea-bissau</option>
  <option value="Guyana">Guyana</option>
  <option value="Haiti">Haiti</option>
  <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
  <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
  <option value="Honduras">Honduras</option>
  <option value="Hong Kong">Hong Kong</option>
  <option value="Hungary">Hungary</option>
  <option value="Iceland">Iceland</option>
  <option value="India">India</option>
  <option value="Indonesia">Indonesia</option>
  <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
  <option value="Iraq">Iraq</option>
  <option value="Ireland">Ireland</option>
  <option value="Isle of Man">Isle of Man</option>
  <option value="Israel">Israel</option>
  <option value="Italy">Italy</option>
  <option value="Jamaica">Jamaica</option>
  <option value="Japan">Japan</option>
  <option value="Jersey">Jersey</option>
  <option value="Jordan">Jordan</option>
  <option value="Kazakhstan">Kazakhstan</option>
  <option value="Kenya">Kenya</option>
  <option value="Kiribati">Kiribati</option>
  <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
  <option value="Korea, Republic of">Korea, Republic of</option>
  <option value="Kuwait">Kuwait</option>
  <option value="Kyrgyzstan">Kyrgyzstan</option>
  <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
  <option value="Latvia">Latvia</option>
  <option value="Lebanon">Lebanon</option>
  <option value="Lesotho">Lesotho</option>
  <option value="Liberia">Liberia</option>
  <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
  <option value="Liechtenstein">Liechtenstein</option>
  <option value="Lithuania">Lithuania</option>
  <option value="Luxembourg">Luxembourg</option>
  <option value="Macao">Macao</option>
  <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
  <option value="Madagascar">Madagascar</option>
  <option value="Malawi">Malawi</option>
  <option value="Malaysia">Malaysia</option>
  <option value="Maldives">Maldives</option>
  <option value="Mali">Mali</option>
  <option value="Malta">Malta</option>
  <option value="Marshall Islands">Marshall Islands</option>
  <option value="Martinique">Martinique</option>
  <option value="Mauritania">Mauritania</option>
  <option value="Mauritius">Mauritius</option>
  <option value="Mayotte">Mayotte</option>
  <option value="Mexico">Mexico</option>
  <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
  <option value="Moldova, Republic of">Moldova, Republic of</option>
  <option value="Monaco">Monaco</option>
  <option value="Mongolia">Mongolia</option>
  <option value="Montenegro">Montenegro</option>
  <option value="Montserrat">Montserrat</option>
  <option value="Morocco">Morocco</option>
  <option value="Mozambique">Mozambique</option>
  <option value="Myanmar">Myanmar</option>
  <option value="Namibia">Namibia</option>
  <option value="Nauru">Nauru</option>
  <option value="Nepal">Nepal</option>
  <option value="Netherlands">Netherlands</option>
  <option value="Netherlands Antilles">Netherlands Antilles</option>
  <option value="New Caledonia">New Caledonia</option>
  <option value="New Zealand">New Zealand</option>
  <option value="Nicaragua">Nicaragua</option>
  <option value="Niger">Niger</option>
  <option value="Nigeria">Nigeria</option>
  <option value="Niue">Niue</option>
  <option value="Norfolk Island">Norfolk Island</option>
  <option value="Northern Mariana Islands">Northern Mariana Islands</option>
  <option value="Norway">Norway</option>
  <option value="Oman">Oman</option>
  <option value="Pakistan">Pakistan</option>
  <option value="Palau">Palau</option>
  <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
  <option value="Panama">Panama</option>
  <option value="Papua New Guinea">Papua New Guinea</option>
  <option value="Paraguay">Paraguay</option>
  <option value="Peru">Peru</option>
  <option value="Philippines">Philippines</option>
  <option value="Pitcairn">Pitcairn</option>
  <option value="Poland">Poland</option>
  <option value="Portugal">Portugal</option>
  <option value="Puerto Rico">Puerto Rico</option>
  <option value="Qatar">Qatar</option>
  <option value="Reunion">Reunion</option>
  <option value="Romania">Romania</option>
  <option value="Russian Federation">Russian Federation</option>
  <option value="Rwanda">Rwanda</option>
  <option value="Saint Helena">Saint Helena</option>
  <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
  <option value="Saint Lucia">Saint Lucia</option>
  <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
  <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
  <option value="Samoa">Samoa</option>
  <option value="San Marino">San Marino</option>
  <option value="Sao Tome and Principe">Sao Tome and Principe</option>
  <option value="Saudi Arabia">Saudi Arabia</option>
  <option value="Senegal">Senegal</option>
  <option value="Serbia">Serbia</option>
  <option value="Seychelles">Seychelles</option>
  <option value="Sierra Leone">Sierra Leone</option>
  <option value="Singapore">Singapore</option>
  <option value="Slovakia">Slovakia</option>
  <option value="Slovenia">Slovenia</option>
  <option value="Solomon Islands">Solomon Islands</option>
  <option value="Somalia">Somalia</option>
  <option value="South Africa">South Africa</option>
  <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
  <option value="Spain">Spain</option>
  <option value="Sri Lanka">Sri Lanka</option>
  <option value="Sudan">Sudan</option>
  <option value="Suriname">Suriname</option>
  <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
  <option value="Swaziland">Swaziland</option>
  <option value="Sweden">Sweden</option>
  <option value="Switzerland">Switzerland</option>
  <option value="Syrian Arab Republic">Syrian Arab Republic</option>
  <option value="Taiwan, Province of China">Taiwan, Province of China</option>
  <option value="Tajikistan">Tajikistan</option>
  <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
  <option value="Thailand">Thailand</option>
  <option value="Timor-leste">Timor-leste</option>
  <option value="Togo">Togo</option>
  <option value="Tokelau">Tokelau</option>
  <option value="Tonga">Tonga</option>
  <option value="Trinidad and Tobago">Trinidad and Tobago</option>
  <option value="Tunisia">Tunisia</option>
  <option value="Turkey">Turkey</option>
  <option value="Turkmenistan">Turkmenistan</option>
  <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
  <option value="Tuvalu">Tuvalu</option>
  <option value="Uganda">Uganda</option>
  <option value="Ukraine">Ukraine</option>
  <option value="United Arab Emirates">United Arab Emirates</option>
  <option value="United Kingdom">United Kingdom</option>
  <option value="United States">United States</option>
  <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
  <option value="Uruguay">Uruguay</option>
  <option value="Uzbekistan">Uzbekistan</option>
  <option value="Vanuatu">Vanuatu</option>
  <option value="Venezuela">Venezuela</option>
  <option value="Viet Nam">Viet Nam</option>
  <option value="Virgin Islands, British">Virgin Islands, British</option>
  <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
  <option value="Wallis and Futuna">Wallis and Futuna</option>
  <option value="Western Sahara">Western Sahara</option>
  <option value="Yemen">Yemen</option>
  <option value="Zambia">Zambia</option>
  <option value="Zimbabwe">Zimbabwe</option>
											</select>

										</div>
									</div>
          <div class="col-xl-12">
            <div class="submit-field">
              <h5>{{ __('Introduce Yourself') }}</h5>
              <textarea name="abt_me" cols="30" rows="5" class="with-border" placeholder="{{ __('With less than 500 characters try to introduce yourself.') }}">{{Auth::user()->info ? Auth::user()->info->abt_me : old('abt_me')}}</textarea>
                @if ($errors->has('abt_me'))
                    <small>{{ $errors->first('abt_me') }}</small>
                @endif
            </div>
          </div>


             <div class="col-xl-8">
                 <div class="submit-field">
                     <h5>{{ __('Location') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Type your full address') }}"></i></h5>
                     <div class="input-with-icon">
                         <div id="autocomplete-container">
                             <input value="{{Auth::user()->address ? Auth::user()->address->address : old('address')}}" id="autocomplete-input" type="text" placeholder="{{ __('Type your full address') }}" name="address">
                             <input value="@if(Auth::user()->address) {{Auth::user()->address->city ? Auth::user()->address->city : old('city') }} @else {{old('city')}} @endif" type="hidden" name="city"     id="city">
                             <input value="@if(Auth::user()->address) {{Auth::user()->address->province ? Auth::user()->address->province : old('province') }} @else {{old('province')}} @endif" type="hidden" name="province" id="province">
                             <input value="@if(Auth::user()->address) {{Auth::user()->address->lon ? Auth::user()->address->lat : old('lat') }} @else {{old('lat')}} @endif" type="hidden" name="lat"      id="lat">
                             <input value="@if(Auth::user()->address) {{Auth::user()->address->lat ? Auth::user()->address->lon : old('lon') }} @else {{old('lon')}} @endif" type="hidden" name="lon"      id="lon">
                         </div>
                         <i class="icon-material-outline-location-on"></i>
                     </div>
                     @if ($errors->has('address') || $errors->has('lon'))
                         <small>{{ __('Please pick your address from the suggestions!') }}</small>
                     @endif
                 </div>
             </div>
             <div class="col-xl-4">
                 <div class="submit-field">
                     <h5>{{ __('Zip Code') }}</h5>
                     <input value="{{Auth::user()->address ? Auth::user()->address->zip : old('zip')}}" type="text" name="zip" class="with-border" placeholder="{{ __('Zip Code') }}">
                     @if ($errors->has('zip'))
                         <small>{{ $errors->first('zip') }}</small>
                     @endif
                 </div>

         </div>

        </div>
              <button type="submit" class="button ripple-effect big margin-top-30">{{ __('Save Changes') }}</button>

          </form>
      </li>

    </ul>


  </div>
    </div>
  <div class="dashboard-box">
    <div class="headline">

    <h3 ><i class="icon-line-awesome-sliders"></i> {{ __('My Preferences') }} </h3>
        <a href="#!" class="accordionHandler ripple-effect slidePreferences" style="top: 10px!important"><i class="icon-material-outline-add" style="color:#fff;"></i></a>

  </div>

  <div id="preferencesAccordion" style="display: none" class="content">
    <ul class="fields-ul">
      <li>

 
      {{-- <div class="col-xl-12">
          <div class="submit-field">
              @if(count(Auth::user()->medias()->where('type',2)->get()) == 0)
                  <form action="{{route('handleUploadIntroVideo')}}" method="post" id="uploadIntroForm" enctype="multipart/form-data">
                      @csrf
                      <div class="margin-top-0">
                          <input name="introVideo" class="uploadButton-input-video" type="file" accept="video/*" id="uploadVideo"/>
                          <label class="uploadButton-button-video ripple-effect" for="uploadVideo">{{ __('Upload your introduction video') }} <i style="margin-left: 10px" class="icon-feather-video"></i></label>
                      </div>
                  </form>
              @else
                  <div style="display: flex">
                      <a href="#" class="button ripple-effect margin-top-10 watchIntroVideo" data-url="{{asset(Auth::user()->medias()->where('type',2)->first()->link)}}" data-ext="{{explode('.',Auth::user()->medias()->where('type',2)->first()->label)[1]}}" style="padding: 5px 10px; color: white">{{ __('Watch your introduction video') }} <i style="margin-left: 10px" class="icon-line-awesome-video-camera"></i></a>
                      <form action="{{route('handleUpdateIntroVideo')}}" method="post" id="updateIntoVideoForm" enctype="multipart/form-data">
                          @csrf
                          <div class="margin-top-0">
                              <input name="introVideo" class="uploadButton-input-video" type="file" accept="video/*" id="updateIntoVideo"/>
                              <label class="uploadButton-button-video ripple-effect" style="margin-left: 10px;
                  margin-top: 10px; height: 35px" for="updateIntoVideo"> <i class="icon-feather-edit"></i></label>
                          </div>
                      </form>
                  </div>
              @endif
              <span style="color: red" id="videoErrors"></span>
          </div>
      </div> --}}

         {{-- <div class="col-xl-4">
              <div class="submit-field">
                <div class="bidding-widget">
                  <span class="bidding-detail">{{ __('Set your') }} <strong>{{ __('minimal hourly rate') }} </strong> <span style="color: green" id="hourRateStatus"></span></span>

                    <div class="input-with-icon-right" style="position: relative">

                        <i style="float: right;font-style: normal;">TND</i>
                        <input id="hourRate"  class="input-text with-border margin-top-10"  type="number" min="" name="hour_rate" value="{{Auth::user()->preference->hour_rate ?? ""}}" placeholder="35" />
                        <input type="hidden" name="" id="oldHourRate" value="{{Auth::user()->preference->hour_rate ?? ""}}">
                    </div>

                </div>
              </div>
            </div> --}}
    

            <div class="row">
                <div class="col-xl-6">
                    <div class="submit-field">
                      <h5>{{ __('Skills') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Add up to 6 skills') }}"></i> <span style="color: red" id="skillsStatus"></span></h5>
              
                      <!-- Skills List -->
                      <div class="keywords-container">
                        <div class="keyword-input-container">
                          <input  type="text" class="keyword-input with-border" placeholder="e.g. Angular, Laravel"/>
                          <button id="addKeywordBtn" class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
                        </div>
                        <div class="keywords-list">
                            @foreach(Auth::user()->skills as $skill)
                                <span class='keyword'><span class='keyword-remove' data-label="{{$skill->label}}"></span><span class='keyword-text'>{{$skill->label}}</span></span>
                            @endforeach
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                    <div class="col-xl-6">
                        <div class="submit-field">
                            <h5>{{ __('Attachments') }} 
                                <span>({{ __("Manage your cover letters and cv's") }})</span>
                            </h5>
              
                            <!-- Attachments -->
                            <div id="attachments" class="attachments-container margin-top-0 margin-bottom-0">
                                   @if(count(Auth::user()->medias()->where('type',5)->get()) != 0)
                                @foreach(Auth::user()->medias()->where('type',5)->get() as $key => $file)
                                    <div class="attachment-box ripple-effect">
                                        <span>{{$file->label}}</span>
                                        <button class="remove-attachment" data-id="{{$file->id}}" data-tippy-placement="top" title="Remove"></button>
                                    </div>
                                   @endforeach
              
                                       @endif
                            </div>
                        @if(count(Auth::user()->medias()->where('type',5)->get()) < 6)
                            <!-- Upload Button -->
                            <div class="uploadButton margin-top-0">
                                <span id="uplaodSuccess" style="color: green"></span>
                                <input name="attachments" class="uploadButton-input" type="file" accept="application/pdf,application/msword" id="upload" multiple/>
                                <label class="uploadButton-button ripple-effect" for="upload">Upload Files</label>
                                <div style="width: 49%; display: none;" id="progress-wrp">
                                    <div class="progress-bar"></div>
                                    <div class="status">0%</div>
                                </div>
                                <span>{{ __('Maximum file size: 10 MB | 6 files are allowed | Only PDF or MS-word are accepted') }}</span> <span style="color:red;" id="fileErrors"></span>
              
                            </div>
              
                            @endif
                        </div>
                    </div>
            </div>

                <div class="row">
                    <div class="col-xl-8">
                            <div style="display: flex">
                                <div class="submit-field" style="flex-basis: 20%">
                                    <h5>{{__('Socials')}} {{ __('Links') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Add your social accounts') }}"></i></h5>
                                    <select id="socials" class="selectpicker with-border" title="Select Job Type" >
                                        <option value="linkedin" default selected>LinkedIn</option>
                                        <option value="github">Github</option>
                                        <option value="behance">Behance</option>
                                        <option value="facebook">Facebook</option>
                                        <option value="dribbble">Dribbble</option>
                      
                                    </select>
                      
                                </div>
                                  
                      
                                <div class="submit-field" style="flex-basis: 80%">
                                    <h5 style="visibility: hidden">{{__('Socials')}} {{ __('Links') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Add your social accounts') }}"></i></h5>
                                    <!-- Skills List -->
                                    <div class="keywords-container">
                                        <div class="keyword-input-container">
                      
                                            <input id="socialLinks" type="text" class=" with-border" value="https://linkedin.com/"/ style="border-radius:0px 5px 5px 0px">
                                            <button class="socialAddBtn ripple-effect"><i class="icon-material-outline-add"></i></button>
                      
                                        </div>
                      
                      
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                    </div>
              
                    <div class="col-xl-4">
                        <div class="section-headline ">
                            <h5>{{ __('Your social accounts') }} <span style="color: green" id="socialSuccess"></span></h5>
                        </div>
                        <div class="row">
              
                            <div class="col-xl-12 col-md-12">
                                <ul class="list-3 color">
                                    @if(Auth::user()->status == 2)
                                        <li> <span class="icon-line-awesome-legal"></span> <a style="margin-left: 5px;
              margin-right: 30px;" href="{{ route('showFreelancerDetails', Auth::user()->getFreelancerUsername()) }}">Hired</a></li>
                                        @endif
                                    @if(count(Auth::user()->socials) != 0)
                                        @foreach(Auth::user()->socials as $key => $link)
                                            <li class="{{$link->label}}"> <span class="fa fa-{{$link->label}}"></span> <a style="margin-left: 5px;
              margin-right: 30px;" href="{{$link->link}}">{{$link->label}}</a></li>
                                        @endforeach
                                        @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


</li>
  </ul>


  </div>
  </div>
</div>

<script>

        $(document).ready(function () {
            @if(Auth::user()->address)
            var uc = '{{ Auth::user()->address->country }}'
            $('.countrySelect').val(uc).change();
            @endif
        })

    $(document).on("focusout" , "#hourRate", function () {

        var InitialhourRate = $('#oldHourRate').val();
        var hourRate = $('#hourRate').val();

        if(parseInt(InitialhourRate) != parseInt(hourRate)) {

            $.get("{{ route('apiHandleSetHourRate')}}?hourRate="+hourRate).done(function (res) {
                if(res.status == 'created') {
                    $('#hourRateStatus').text('Hour rate settled').fadeIn('slow').delay(1000).fadeOut();
                    $('#oldHourRate').val(parseInt(hourRate))
                }
                else if (res.status == 'updated') {
                    $('#hourRateStatus').text('Hour rate updated').fadeIn('slow').delay(1000).fadeOut();
                    $('#oldHourRate').val(parseInt(hourRate))

                }else {
                    $('#hourRateStatus').text('Please enter a number').fadeIn('slow').delay(1000).fadeOut();
                }
            });

        }

    });


    $(".keywords-container").each(function() {
        var keywordInput = $(this).find(".keyword-input");
        var keywordsList = $(this).find(".keywords-list");

        function addKeyword() {

            $.get("{{ route('apiHandleAddSkill')}}?label="+keywordInput.val()).done(function (res) {
                if(res.status == 200) {
                    var $newKeyword = $("<span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>" + keywordInput.val() + "</span></span>");
                    keywordsList.append($newKeyword).trigger('resizeContainer');
                    keywordInput.val("");
                }else {

                    if(res.status == 401) {
                        keywordInput.val("");
                        $('#skillsStatus').text('You have reached the maximum skills !').fadeIn('slow').delay(2000).fadeOut()
                    }else if(res.status == 400) {
                        $('#skillsStatus').text('Skill already exists !').fadeIn('slow').delay(2000).fadeOut()

                    }else if (res.status == 404) {
                        $('#skillsStatus').text('Something went wrong, try again.').fadeIn('slow').delay(2000).fadeOut()
                    }

                }
            });

        }
        keywordInput.on('keyup', function(e) {
            if ((e.keyCode == 13) && (keywordInput.val() !== "")) {
                addKeyword();
            }
        });
        $('.keyword-input-button').on('click', function() {
            if ((keywordInput.val() !== "")) {
                addKeyword();
            }
        });
        $(document).on("click", ".keyword-remove", function() {
            $(this).parent().addClass('keyword-removed');
            var label = $(this).data('label');
            function removeFromMarkup() {
                $.get("{{ route('apiHandleDeleteSkill')}}?label="+label).done(function (res) {
                    if(res.status == 200) {
                        $(".keyword-removed").remove();

                    }
                });

            }
            setTimeout(removeFromMarkup, 500);
            keywordsList.css({
                'height': 'auto'
            }).height();
        });
        keywordsList.on('resizeContainer', function() {
            var heightnow = $(this).height();
            var heightfull = $(this).css({
                'max-height': 'auto',
                'height': 'auto'
            }).height();
            $(this).css({
                'height': heightnow
            }).animate({
                'height': heightfull
            }, 200);
        });
        $(window).on('resize', function() {
            keywordsList.css({
                'height': 'auto'
            }).height();
        });
        $(window).on('load', function() {
            var keywordCount = $('.keywords-list').children("span").length;
            if (keywordCount > 0) {
                keywordsList.css({
                    'height': 'auto'
                }).height();
            }
        });
    });

    var elems = '{{ count(Auth::user()->medias) - 1 }}';
    $('#upload').on('change',function () {
        $('#progress-wrp').css('display', 'block');

        var files = $(this)[0].files;

        if(files.length > 6) {

        $('#fileErrors').text('Only 6 files are allowed !').fadeIn('slow').delay(3000).fadeOut();

        } else {
            $.each(files, function (i, file) {
                var upload = new Upload(file);
                var type_reg = /^image\/(jpg|png|jpeg|bmp|gif|ico)$/;
                var size = upload.getSize();
                if(type_reg.test(upload.getType())) {
                    $('#progress-wrp').css('display', 'none');
                    $('#fileErrors').text('Only PDF or MS-word files are accepted').fadeIn('slow').delay(2000).fadeOut();
                }
                else if((size / 1048576).toFixed(2) > 10 ) {
                    $('#progress-wrp').css('display', 'none');
                    $('#fileErrors').text('Maximum file size is 10 MB').fadeIn('slow').delay(2000).fadeOut();
                }

                 else {

                upload.doUpload();
                $('#progress-wrp').css('display', 'none');

               
                elems++;

                if (elems > 5) {
                    $('.uploadButton').css('display', 'none');
                 }

                }
            })


        }


    });

    var Upload = function (file) {
        this.file = file;
    };

    Upload.prototype.getType = function() {
        return this.file.type;
    };
    Upload.prototype.getSize = function() {
        return this.file.size;
    };
    Upload.prototype.getName = function() {
        return this.file.name;
    };
    Upload.prototype.doUpload = function () {
        var that = this;
        var formData = new FormData();

        formData.append("file", this.file, this.getName());
        formData.append("upload_file", true);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '{{ route('apiHandleUploadFreelancerFiles') }}',
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener('progress', that.progressHandling, false);
                }
                return myXhr;
            },
            success: function (data) {

              $('#attachments').append('<div class="attachment-box ripple-effect"> <span>'+ data.media.label +'</span> <button class="remove-attachment" data-id="'+data.media.id+'" data-tippy-placement="top" title="Remove"></button> </div>')
              $('#uplaodSuccess').text('File uploaded successfuly').fadeIn('slow').delay(2000).fadeOut();
                $("#progress-wrp .progress-bar").css("width", "0%");
                $("#progress-wrp .status").text("0%");
            },
            error: function (error) {

            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    };

    Upload.prototype.progressHandling = function (event) {
        var percent = 0;
        var position = event.loaded || event.position;
        var total = event.total;
        var progress_bar_id = "#progress-wrp";
        if (event.lengthComputable) {
            percent = Math.ceil(position / total * 100);
        }
        // update progressbars classes so it fits your code
        $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
        $(progress_bar_id + " .status").text(percent + "%");
    };


    $('#attachments').on('click', '.attachment-box .remove-attachment' ,  function () {
            var parent = $(this).parent()
        $.get("{{ route('apiHandleDeleteFreelancerFile')}}?id="+$(this).data('id')).done(function (res) {
            if(res.status == 200) {
                parent.css('display', 'none');
                $('#uplaodSuccess').text('File removed successfuly').fadeIn('slow').delay(2000).fadeOut();
                elems--;
                $('.uploadButton').css('display', 'block');
            }
            else {
                $('#fileErrors').text('Something went wrong, please try again').fadeIn('slow').delay(2000).fadeOut();
            }
        });
    })

    $('#socials').on('change' , function () {
        switch($(this).children("option:selected").val()) {
            case "linkedin":
                $('#socialLinks').val('https://linkedin.com/')
                break;
            case "github":
                $('#socialLinks').val('https://github.com/')
                break;
            case "behance":
                $('#socialLinks').val('https://behance.com/')
                break;
            case "facebook":
                $('#socialLinks').val('https://facebook.com/')
                break;
            case "dribbble":
                $('#socialLinks').val('https://dribbble.com/')
                break;
        }
    })

    $('#socialLinks').on('keyup', function(e) {
        if ((e.keyCode == 13) && ($(this).val() !== "")) {
          addSocialLink();
        }
    });

    $('.socialAddBtn').on('click', function () {
          addSocialLink();
    })

    function addSocialLink() {

        var label = $('#socials').children("option:selected").val();
        var link = $('#socialLinks').val();

        $.get("{{ route('apiHandleAddSocialLink')}}?label="+label+"&link="+link).done(function (res) {
            if (res.status == 'created') {
                $('.list-3').append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                    '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

            $('#socialSuccess').text('Link added successfully !').fadeIn('slow').delay(2000).fadeOut();

            }else if (res.status == 'updated') {
                  $('.'+label).remove();
                $('.list-3').append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                    '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

                $('#socialSuccess').text('Link updated successfully !').fadeIn('slow').delay(2000).fadeOut();
            }
            else if(res.status == 400) {
                $('#socialErrors').text('Social link already exists !').fadeIn('slow').delay(2000).fadeOut();
            }
            else {
                $('#socialErrors').text('Something went wrong, please try again.').fadeIn('slow').delay(2000).fadeOut();
            }
        });
    }

    $('#uploadVideo').on('change' , function () {
        var size = this.files[0].size;
        if((size / 1048576).toFixed(2) > 50 ) {
            $('#videoErrors').text('Maximum video size is 50 MB').fadeIn('slow').delay(2000).fadeOut();
        } else {
            $('#uploadIntroForm').submit();

            $('.uploadButton-button-video').html('Please wait while we upload your video <span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
            $('.uploadButton-button-video').css('cursor','not-allowed');
            $(".uploadButton-button-video").attr("disabled", true);

        }
    })


    $('#updateIntoVideo').on('change' , function () {
        var size = this.files[0].size;
        if((size / 1048576).toFixed(2) > 50 ) {
            $('#videoErrors').text('Maximum video size is 50 MB').fadeIn('slow').delay(2000).fadeOut();
        } else {
            $('#updateIntoVideoForm').submit();

            $('.uploadButton-button-video').html('Please wait while we upload your video <span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
            $('.uploadButton-button-video').css('cursor','not-allowed');
            $(".uploadButton-button-video").attr("disabled", true);

        }
    })

    $('.slideProfile').on('click', function (e) {
      e.preventDefault();
        if($('.slideProfile i').hasClass('icon-material-outline-add')) {
            $('.slideProfile i').removeClass('icon-material-outline-add');
            $('.slideProfile i').addClass('icon-line-awesome-minus');
        }else {
            $('.slideProfile i').addClass('icon-material-outline-add');
            $('.slideProfile i').removeClass('icon-line-awesome-minus');
        }
        $('#profileAccordion').slideToggle('slow');
    });

    $('.slidePreferences').on('click', function (e) {
      e.preventDefault();
        if($('.slidePreferences i').hasClass('icon-material-outline-add')) {
            $('.slidePreferences i').removeClass('icon-material-outline-add');
            $('.slidePreferences i').addClass('icon-line-awesome-minus');
        }else {
            $('.slidePreferences i').addClass('icon-material-outline-add');
            $('.slidePreferences i').removeClass('icon-line-awesome-minus');
        }
        $('#preferencesAccordion').slideToggle('slow');
    })


</script>
<script type="text/javascript">
    function initAutocomplete() {
        var city, street, place, state, code, sublocality = ' ';


        var input = document.getElementById('autocomplete-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
            var placeUser = autocomplete.getPlace();
            var lat = placeUser.geometry.location.lat();
            var lng = placeUser.geometry.location.lng();
            for (var i = 0; i < placeUser.address_components.length; i++) {
                var addressType = placeUser.address_components[i]["types"][0];
                console.log(addressType);
                switch (addressType) {
                    case 'route':
                        street = placeUser.address_components[i]['long_name'];

                        break;
                    case 'administrative_area_level_1':
                        place = placeUser.address_components[i]['long_name'];
                        break;
                    case 'locality':
                        city = placeUser.address_components[i]['long_name'];
                        break;
                    case 'sublocality_level_1':
                        sublocality = placeUser.address_components[i]['long_name'];
                        break;
                    case 'country':
                        country = placeUser.address_components[i]['long_name'];
                        break;
                }


            }
            $('#city').val(city);
            $('#province').val(sublocality);
            $('#lat').val(lat);
            $('#lon').val(lng);

            var myLatLng = {
                lng: lng,
                lat: lat,
            };
            var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                zoom: 18,
                center: myLatLng,
                scrollwheel: false,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                panControl: false,
                navigationControl: false,
                streetViewControl: false,
                styles: [{
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }]
                }]
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                draggable: true,
                map: single_map,
                title: 'Your location'
            });

            google.maps.event.addListener(single_map, 'click', function(event) {



                marker.setPosition(event.latLng);
                var geocoder	= new google.maps.Geocoder();							// create a geocoder object
                var location	= new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());		// turn coordinates into an object

                geocoder.geocode({'latLng': location}, function (results, status) {


                    $('#autocomplete-input').val("");
                    $('#city').val("");
                    $('#province').val("");
                    $('#zip').val("");
                    $('#lat').val("");
                    $('#lon').val("");
                    $('#autocomplete-input').val(results[1].formatted_address);

                    if (results[0].address_components[3]) {
                        $('#city').val(results[0].address_components[3].long_name);
                    }else {
                        $('#city').val(results[0].address_components[2].long_name);
                    }

                    $('#province').val(results[0].address_components[2].long_name);
                    $('#lat').val(event.latLng.lat());
                    $('#lon').val(event.latLng.lng());
                });

            });

            if (!placeUser.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
        });

        $('.watchIntroVideo').on('click', function (e) {
            e.preventDefault();
            var url = $(this).data('url');
            var ext = $(this).data('ext');
            $.confirm({
                title: 'Intro',
                columnClass: 'col-xl-6',
                containerFluid: true,
                content: ' <video style="width: 100%" controls autoplay>\n' +
                    '                    <source src="'+ url +'" type="video/'+ext+'">\n' +
                    '                    Your browser does not support the video tag.\n' +
                    '                </video>',
                buttons: {
                    close: function () {
                    }
                }
            });
        })
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

<script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

<script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>
