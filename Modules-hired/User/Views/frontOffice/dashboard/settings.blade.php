@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Settings')])
@section('header')
@include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
    <style>

        .setting-wrapper {
            position: relative;
            width: 150px;
            height: 150px;
            border-radius: 4px;
            overflow: hidden;
            box-shadow: none;
            margin: 0 10px 30px 0;
            transition: all .3s ease
        }

        .setting-wrapper:hover {
            transform: scale(1.05);
            cursor: pointer
        }

        .setting-wrapper .setting-profile-pic {
            height: 100%;
            width: 100%;
            transition: all .3s ease;
            object-fit: cover
        }

        .setting-wrapper .setting-profile-pic:after {
            font-family: Feather-Icons;
            content: "\e9f1";
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            line-height: 120px;
            position: absolute;
            font-size: 60px;
            background: #f0f0f0;
            color: #aaa;
            text-align: center
        }

        .setting-wrapper .setting-upload-button {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%
        }

        .setting-wrapper .setting-file-upload {
            opacity: 0;
            pointer-events: none;
            position: absolute
        }


    </style>



    <div class="dashboard-container">


   @if(Auth::user()->type == 3 )

        @include('General::admin.sideBar')

    @else

        @include('User::frontOffice.dashboard.inc.sidebar')

    @endif

    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner">

            <div class="dashboard-headline">
                <h3>{{ __('Settings') }}</h3>

                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                        <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                        <li>{{ __('Settings') }}</li>
                    </ul>
                </nav>
            </div>


            <div class="row">
                  
                <div class="col-xl-12">

                    <div class="dashboard-box">

                        <div class="headline">
                            <h3><i class="icon-material-outline-account-circle"></i> {{ __('My Account') }}</h3>
                        
                        </div>

                        <div id="accountSettingsAccordion" class="content with-padding padding-bottom-0">
                            <form  action="{{route('handleUpdateAccountInfos')}}" method="post" enctype="multipart/form-data">
                                @csrf
                            <div class="row" id="account-image-row">

                                <div class="col-auto" >
                                    <div class="setting-wrapper" data-tippy-placement="bottom" title="Change Avatar">
                                        <img class="setting-profile-pic" src="{{Auth::user()->medias()->where('type',0)->first() ? asset(Auth::user()->medias()->where('type',0)->first()->link) : asset('frontOffice/images/user-avatar-placeholder.png')}}" alt="" />
                                        <div class="setting-upload-button"></div>
                                        <input name="profileImage" class="setting-file-upload" type="file" accept="image/*" />

                                    </div>
                                    @if ($errors->has('profileImage'))
                                        <small>{{ $errors->first('profileImage') }}</small>
                                    @endif
                                </div>

                                <div class="col">
                                    <div class="row">

                                        <div class="col-xl-4">
                                            <div class="submit-field">
                                                <h5>{{ __('First Name') }}</h5>
                                                <input name="firstName" type="text" class="with-border" placeholder="{{ __('First Name') }}" value="{{Auth::user()->first_name ? Auth::user()->first_name : old('firstName') }}">
                                                @if ($errors->has('firstName'))
                                                    <small>{{ $errors->first('firstName') }}</small>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-xl-4">
                                            <div class="submit-field">
                                                <h5>{{ __('Last Name') }}</h5>
                                                <input name="lastName" type="text" class="with-border" placeholder="{{ __('Last Name') }}" value="{{Auth::user()->last_name ? Auth::user()->last_name : old('lastName') }}">
                                                @if ($errors->has('lastName'))
                                                    <small>{{ $errors->first('lastName') }}</small>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-xl-4">
                                            <div class="submit-field">
                                                <h5>Birthday</h5>
                                                <input name="birthday" type="date"  value="{{ Auth::user()->birthday ? Auth::user()->birthday->format('Y-m-d') : null }}"  class="with-border">
                                                @if ($errors->has('birthday'))
                                                    <small>{{ $errors->first('birthday') }}</small>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>{{ __('Email') }}</h5>
                                                <input disabled style="opacity: 0.5;" type="text" class="with-border" value="{{Auth::user()->email}}">
                                               <small><a href="#small-dialog" class="popup-with-zoom-anim ">Update email</a></small>
                                            </div>
                                        </div>

                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>{{ __('phone') }}</h5>
                                                <input name="phone" type="text" class="with-border" placeholder="{{ __('+216 -- --- ---') }}" value="{{Auth::user()->phone ? Auth::user()->phone : old('phone') }}">
                                                @if ($errors->has('phone'))
                                                    <small>{{ $errors->first('phone') }}</small>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                                <button type="submit" name="updateAccountInfos" class="button ripple-effect big" style="margin-bottom: 10px">{{ __('Save Changes') }}</button>

                        </form>
                        </div>

                    </div>


                </div>



                <form  class="col-xl-12" id="updatePassword" action="{{route('handleUpdatePassword')}}" method="post">
                @csrf
                <!-- Dashboard Box -->

                <div >
                    <div id="test1" class="dashboard-box">

                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-material-outline-lock"></i> {{ __('Password & Security') }}</h3>
                        </div>

                        <div id="passwordResetAccordion" class="content with-padding">
                            <div class="row">
                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>{{ __('Current Password') }}</h5>
                                        <input name="current" type="password" class="with-border">

                                        @if ($errors->has('current'))
                                            <small>{{ $errors->first('current') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>{{ __('New Password') }}</h5>
                                        <input name="new" type="password" class="with-border">
                                        @if ($errors->has('new'))
                                            <small>{{ $errors->first('new') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>{{ __('Repeat New Password') }}</h5>
                                        <input name="new_confirmation" type="password" class="with-border">
                                        @if ($errors->has('new_confirmation'))
                                            <small>{{ $errors->first('new_confirmation') }}</small>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <button type="submit" name="updatePassword" class="button ripple-effect big" style="margin-bottom: -20px">{{ __('Save Changes') }}</button>

                        </div>

                    </div>

                </div>


                </form>
            </div>
            <!-- Row / End -->

            @include('frontOffice.inc.small-footer')

        </div>
    </div>
    <!-- Dashboard Content / End -->

</div>


@include('User::frontOffice.dashboard.modals.updateEmail')



<script type="text/javascript">
    function initAutocomplete() {
        var city, street, place, state, code, sublocality = ' ';
        @if (Auth::user()->type === 0)
          multipleAutoComplete();
        @endif
        var input = document.getElementById('autocomplete-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
                      var placeUser = autocomplete.getPlace();
                      var lat = placeUser.geometry.location.lat();
                      var lng = placeUser.geometry.location.lng();
                      for (var i = 0; i < placeUser.address_components.length; i++) {
                          var addressType = placeUser.address_components[i]["types"][0];
                          console.log(addressType);
                          switch (addressType) {
                              case 'route':
                                  street = placeUser.address_components[i]['long_name'];

                                  break;
                              case 'administrative_area_level_1':
                                  place = placeUser.address_components[i]['long_name'];
                                  break;
                                  case 'locality':
                                  city = placeUser.address_components[i]['long_name'];
                                  break;
                                  case 'sublocality_level_1':
                                  sublocality = placeUser.address_components[i]['long_name'];
                                  break;
                                  case 'country':
                                  country = placeUser.address_components[i]['long_name'];
                                  break;
                          }


                      }
                        $('#city').val(city);
                        $('#province').val(sublocality);
                        $('#lat').val(lat);
                        $('#lon').val(lng);

                        var myLatLng = {
                            lng: lng,
                            lat: lat,
                        };
                        var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                            zoom: 10,
                            center: myLatLng,
                            scrollwheel: false,
                            zoomControl: true,
                            mapTypeControl: false,
                            scaleControl: false,
                            panControl: false,
                            navigationControl: false,
                            streetViewControl: false,
                            styles: [{
                                "featureType": "landscape",
                                "elementType": "all",
                                "stylers": [{
                                    "color": "#f2f2f2"
                                }]
                            }]
                        });

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            draggable: true,
                            map: single_map,
                            title: 'Your location'
                        });

                        google.maps.event.addListener(single_map, 'click', function(event) {



                            marker.setPosition(event.latLng);
                            var geocoder	= new google.maps.Geocoder();							// create a geocoder object
                            var location	= new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());		// turn coordinates into an object
                           
                            geocoder.geocode({'latLng': location}, function (results, status) {


                                  $('#autocomplete-input').val("");
                                  $('#city').val("");
                                  $('#province').val("");
                                  $('#zip').val("");
                                  $('#lat').val("");
                                  $('#lon').val("");
                                  $('#autocomplete-input').val(results[1].formatted_address);

                                  if (results[0].address_components[3]) {
                                      $('#city').val(results[0].address_components[3].long_name);
                                  }else {
                                      $('#city').val(results[0].address_components[2].long_name);
                                  }

                                  $('#province').val(results[0].address_components[2].long_name);
                                  $('#lat').val(event.latLng.lat());
                                  $('#lon').val(event.latLng.lng());
                            });

                          });

                          if (!placeUser.geometry) {
                              window.alert("No details available for input: '" + place.name + "'");
                              return;
                          }
                      });
    }
</script> 

    <script>

        function settingSwitcher() {
            var readURL = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.setting-profile-pic').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            };
            $(".setting-file-upload").on('change', function() {
                readURL(this);
            });
            $(".setting-upload-button").on('click', function() {
                $(".setting-file-upload").click();
            });
        }


        settingSwitcher();



    </script>

@endsection
