@extends('frontOffice.layout',['class' => 'gray','title' =>__('Manage In Progress Tasks')])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
    <!-- Dashboard Container -->
    <div class="dashboard-container">
        <!-- Dashboard Sidebar
      ================================================== -->

    @include('User::frontOffice.dashboard.inc.sidebar')

    <!-- Dashboard Sidebar / End -->

        <!-- Dashboard Content
            ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>{{ __('Manage In Progress Tasks') }}</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                            <li>{{ __('Manage In Progress Tasks') }}</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-assignment"></i>{{ __('In Progress Tasks') }}</h3>
                            </div>

                            <div class="content">
                                <ul class="dashboard-box-list">
                                @foreach(Auth::user()->tasks()->where('status',2)->get() as $task)
                                        @php

                                            $endDate = Carbon\Carbon::parse($task->created_at)->addDays($task->accepted->bid->delivery_time);
                                            $left = $endDate->diffInDays(Carbon\Carbon::now());
                                            $leftInHours = null;
                                            if($left == 0 ) {
                                                 $leftInHours = $endDate->diffInHours(Carbon\Carbon::now());
                                            }
                                        @endphp
                                    <li>
                                        <!-- Job Listing -->
                                        <div class="job-listing width-adjustment">

                                            <!-- Job Listing Details -->
                                            <div class="job-listing-details">

                                                <!-- Details -->
                                                <div class="job-listing-description">
                                                    <a class="job-listing-title"><a href="{{ route('showTaskDetails', $task->getTaskParamName()) }}">{{$task->name}}</a>  @if($leftInHours) <span class="dashboard-status-button yellow">{{ __('Expiring') }}</span> @endif </a>
                                                    <div class="freelancer-name" style="margin-top: 0px">
                                                        <h4><i class="icon-line-awesome-user"></i> <a href="{{ route('showFreelancerDetails', $task->accepted->bid->bidder->getFreelancerUsername()) }}">{{$task->accepted->bid->bidder->getFullName()}}</a></h4>
                                                    </div>
                                                    <!-- Job Listing Footer -->
                                                    <div class="job-listing-footer">
                                                        <ul>
                                                            <li><i class="icon-material-outline-access-time"></i>   {{ $leftInHours ? $leftInHours.' Hours Left' : $left.' Days Left' }} </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Task Details -->
                                        <ul class="dashboard-task-info">
                                            <li><strong>{{__('Budget')}}</strong><span>{{ $task->accepted->bid->rate.' TND' }}</span></li>
                                        </ul>

                                        <!-- Buttons -->
                                        <div class="buttons-to-right always-visible">
                                            <a href="#small-dialog-2" class="popup-with-zoom-anim button dark ripple-effect sendDirectMessage" data-name="{{ $task->accepted->bid->bidder->getFullName() }}" data-id="{{$task->accepted->bid->bidder->id}}"><i class="icon-feather-mail"></i> {{ __('Send Message') }}</a>
                                        </div>
                                    </li>
        @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row / End -->

                @include('frontOffice.inc.small-footer')
                @include('User::frontOffice.dashboard.modals.sendDirectMessageBiddersPopup')

            </div>
        </div>
        <!-- Dashboard Content / End -->

    </div>

    <script type="text/javascript">
         $('.sendDirectMessage').on('click', function () {
          $('#bidder_name').text($(this).data('name'));
          $('#receiverId').val($(this).data('id'));
      })
    </script>

@endsection


