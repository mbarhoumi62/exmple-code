<!-- Send Direct Message Popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">


        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>{{ __('Email Update') }}</h3>
                    <span style="color: red;" id="updateEmailErrors"></span>
                </div>

                <!-- Form -->
                <form action="javascript:void(0)" method="post" id="updateEmailForm">

                    <div class="input-with-icon-left">
                        <i class="icon-material-baseline-mail-outline"></i>
                        <input  class="input-text with-border" type="text" id="updateEmail-email" class="with-border" placeholder="{{ __('New Email') }}">
                        <small id="updateEmail-emailError"></small>
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-outline-lock"></i>
                        <input placeholder="{{ __('Enter your current password') }}"  class="input-text with-border" id="updateEmail-password" type="password" class="with-border">
                        <small id="updateEmail-passwordError"></small>
                    </div>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" id="updateEmailBtn" type="submit" form="updateEmailForm">{{ __('Submit') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>

<script>
    $( "#updateEmailForm" ).submit(function( event ) {
        event.preventDefault();
        $('#updateEmailBtn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $('#updateEmailBtn').css('opacity','0.5');
        $('#updateEmailBtn').css('cursor','not-allowed');
        $("#updateEmailBtn").attr("disabled", true);
        $('#updateEmail-emailError').text('')
        $('#updateEmail-passwordError').text('')
        var data = {
            'email' : $('#updateEmail-email').val(),
            'password' : $('#updateEmail-password').val()
        }

        $.post('{{route('apiHandleUpdateEmail')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {
                    if(res.status === 200) {
                        $('#updateEmailForm').fadeOut();
                        $('#updateEmailBtn').fadeOut();
                        $('#tab').append('   <div class="notification success ">\n' +
                            '                            <p>Check your mailbox !</p>\n' +
                            '                        </div>')

                         }
                if(res.errors) {
                    $('#updateEmailBtn').html('Submit <i class="icon-material-outline-arrow-right-alt"></i>')
                    $('#updateEmailBtn').css('opacity','1');
                    $('#updateEmailBtn').css('cursor','pointer');
                    $("#updateEmailBtn").attr("disabled", false);
                    if(res.errors.email) {
                        $('#updateEmail-emailError').text(res.errors.email[0])
                    }
                    if(res.errors.password) {
                        $('#updateEmail-passwordError').text(res.errors.password[0])
                    }
                }
            })
            .fail(function (res) {
                $('#updateEmailBtn').html('Submit <i class="icon-material-outline-arrow-right-alt"></i>')
                $('#updateEmailBtn').css('opacity','1');
                $('#updateEmailBtn').css('cursor','pointer');
                $("#updateEmailBtn").attr("disabled", false);
                    $('#updateEmailErrors').text('Something went wrong, please try again !')
              })
    });
</script>





