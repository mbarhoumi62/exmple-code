@extends('frontOffice.layout',['class' => 'gray','title' => __('Post New Task')])
@section('header')
      @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
  <!-- Dashboard Container -->
<div class="dashboard-container">
  <!-- Dashboard Sidebar
================================================== -->

  @include('User::frontOffice.dashboard.inc.sidebar')

<!-- Dashboard Sidebar / End -->
    <style>
        .toggle {
            display: none;
        }
    </style>

<!-- Dashboard Content
================================================== -->
<div class="dashboard-content-container" data-simplebar>
  <div class="dashboard-content-inner" >

    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
      <h3>{{ __('Post New Task') }}</h3>

      <!-- Breadcrumbs -->
      <nav id="breadcrumbs" class="dark">
        <ul>
            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
          <li>{{ __('Post New Task') }}</li>
        </ul>
      </nav>
    </div>

    <!-- Row -->

      <form action="{{route('handleAddTask')}}" method="post" enctype="multipart/form-data">
          @csrf
    <div class="row">

      <!-- Dashboard Box -->
      <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">

          <!-- Headline -->
          @if(Auth::user()->type == 0 && (count(Auth::user()->companies) > 0))
          <div class="headline">
        <div class="row">
          <div class="col-xl-3">
            <h3><i class="icon-feather-folder-plus"></i> {{ __('Task Submission') }}</h3>
          </div>
              </div>

              <div class="row margin-top-20">                
                <div class="col-xl-12" style="display: flex">
                  <div class="switches-list">
                  <div class="switch-container">
                      <label class="switch"><input id="submitForCompany" type="checkbox" ><span class="switch-button"></span>{{ __('Submit for a company?') }}</label>
                  </div>
                  </div>
                  <div id="selectCompany" class="col-xl-6 toggle">
                    <div class="submit-field">
                        <select name="company" class="selectpicker with-border" data-size="7" title="{{ __('Select company') }}">
                            @foreach (Auth::user()->companies as $company)
                                <option value="{{$company->id}}" selected>{{ $company->name}}</option>
  
                            @endforeach
                        </select>
                    </div>
                </div>

              </div>
              </div>
          </div>
        @else

          <div class="headline">
            <h3><i class="icon-feather-folder-plus"></i> {{ __('Task Submission') }}</h3>
          </div>
        @endif

          <div class="content with-padding padding-bottom-10">
            <div class="row">

              <div class="col-xl-12">
                <div class="submit-field">
                  <h5>{{ __('Project Name') }}</h5>
                  <input name="name" type="text" value="{{ old('name') }}" class="with-border" placeholder="{{ __('e.g. build me a website') }}">
                    @if ($errors->has('name'))
                        <small>{{ $errors->first('name') }}</small>
                    @endif
                </div>
              </div>
                <div class="col-xl-6">
                <div class="submit-field">
                  <h5>{{ __('Category') }}</h5>
                  <select name="category" class="selectpicker with-border" data-size="7" title="{{ __('Select Category') }}">
                      @foreach (\App\Modules\General\Models\TaskCategory::all() as $category)
                          <option value="{{$category->id}}">{{$category->label}}</option>
                      @endforeach
                  </select>
                    @if ($errors->has('category'))
                        <small>{{ $errors->first('category') }}</small>
                    @endif
                </div>
              </div>

              <div class="col-xl-6">
                <div class="submit-field">
                  <h5>{{ __('Location') }}  <i class="help-icon" data-tippy-placement="right" title="{{ __("Leave blank if it's a remote task") }}"></i></h5>
                  <div class="input-with-icon">
                    <div id="autocomplete-container">
                        <input value="{{ old('address')}}" id="autocomplete-input" autocomplete="off" type="text" placeholder="{{ __("Leave blank if it's a remote task") }}" name="address">
                        <input value="{{ old('city') }} " type="hidden" name="city" id="city">
                        <input value="{{ old('province')}}" type="hidden" name="province" id="province">
                        <input value="{{ old('lat') }}" type="hidden" name="lat" id="lat">
                        <input value="{{ old('lon') }}" type="hidden" name="lon" id="lon">
                            @if ($errors->has('address') || $errors->has('lon'))
                                  <small>{{ __('Please pick your address from the suggestions!') }}</small>
                            @endif
                    </div>
                    <i class="icon-material-outline-location-on"></i>
                  </div>
                </div>
              </div>

              <div class="col-xl-8">
                <div class="submit-field">
                  <h5>{{ __('What is your estimated budget?') }}</h5>
                  <div class="row">
                    <div class="col-xl-6">
                      <div class="input-with-icon">
                        <input value="{{ old('min')}}" name="min" class="with-border" type="text" placeholder="{{ __('Minimum') }}">
                        <i class="currency">TND</i>
                      </div>
                        @if ($errors->has('min'))
                            <small>{{ $errors->first('min') }}</small>
                        @endif
                    </div>
                    <div class="col-xl-6">
                      <div class="input-with-icon">
                        <input value="{{ old('max')}}" name="max" class="with-border" type="text" placeholder="{{ __('Maximum') }}">
                        <i class="currency">TND</i>
                      </div>
                        @if ($errors->has('max'))
                            <small>{{ $errors->first('max') }}</small>
                        @endif
                    </div>
                  </div>
                  <div class="feedback-yes-no margin-top-15">
                    <div class="radio">
                      <input id="radio-1" name="budgetType" value="0" type="radio" checked>
                      <label for="radio-1"><span class="radio-label"></span> {{ __('Fixed Price Project') }}</label>
                    </div>

                    <div class="radio">
                      <input id="radio-2" name="budgetType" value="1" type="radio">
                      <label for="radio-2"><span class="radio-label"></span> {{ __('Hourly Project') }}</label>
                    </div>
                  </div>
                </div>
              </div>

                <div class="col-xl-12">
                <div class="submit-field">
                  <h5>{{ __('What skills are required?') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Up to 5 skills that best describe your project') }}"></i> <small id="maxSkills"></small></h5>
                  <div class="keywords-container">
                    <div class="keyword-input-container">
                      <input  type="text" id="skillsInput" class="keyword-input with-border" placeholder="{{ __('Add Skills') }}"/>
                        @if ($errors->has('skills'))
                            <small>{{ $errors->first('skills') }}</small>
                        @endif
                        <input value="{{ old('skills') }}" type="hidden" id="taskSkills" name="skills">
                      <a class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></a>
                    </div>
                      @php
                          $oldSkills = null;
                          if(old('skills'))  $oldSkills = explode(',',old('skills'));
                      @endphp
                    <div class="keywords-list">
                        @if($oldSkills)
                            @foreach($oldSkills as $skill)
                                <span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>{{ $skill }}</span></span>
                            @endforeach
                         @endif
                     </div>
                    <div class="clearfix"></div>
                  </div>

                </div>
              </div>

              <div class="col-xl-12">
                <div class="submit-field">
                  <h5>{{ __('Describe Your Project') }}</h5>
                  <textarea name="description" cols="30" rows="5" class="with-border">{{ old('description')}}</textarea>
                    @if ($errors->has('description'))
                        <small>{{ $errors->first('description') }}</small>
                    @endif
                  <div class="uploadButton margin-top-30">
                    <input name="attachments[]" class="uploadButton-input" type="file" id="upload" accept=".pdf, .csv,.xls,.xlsx, image/*" multiple/>
                    <label class="uploadButton-button ripple-effect" for="upload">{{ __('Upload Files') }}</label>

                      @if(count($errors) > 0)
                          <span style="font-size: 18px" class="uploadButton-file-name">{{ __('Please check your files, you may need to re-upload them!') }}</span>

                      @else
                      <span class="uploadButton-file-name">{{ __('Images or documents that might be helpful in describing your project') }} <small id="uploadErrors"> &nbsp;({{ __('Each file size must be less than 10 MB') }})</small></span>
                        @endif
                  </div>
                    <div id="attachments" class="attachments-container margin-top-0 margin-bottom-0">

                    </div>
                    @if ($errors->has('attachments'))
                        <small>{{ $errors->first('attachments') }}</small>
                    @endif
                    @for($i = 0; $i<3 ; $i++ )
                    @if ($errors->has('attachments.'.$i))
                        <small>{{ 'File '.($i +1 ).' :'. $errors->first('attachments.'.($i +1)).' | ' }}</small>
                    @endif
                    @endfor
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-12">
        <button type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> {{ __('Post a Task') }}</button>
      </div>

    </div>
    <!-- Row / End -->
      </form>

      @include('frontOffice.inc.small-footer')

  </div>
</div>
<!-- Dashboard Content / End -->

</div>

  <script>

      $(document).ready(function() {
          var oldCategory = '{{ old('category') }}';

          if(oldCategory !== '') {
              $('select').val(oldCategory).change();
          }
      });

      $('#skillsInput').focus(function() {
          $(window).keydown(function(event){
              if(event.keyCode == 13) {
                  event.preventDefault();
                  return false;
              }
          });

      });

      $('#skillsInput').focusout(function() {
          $(window).keydown(function(event){
              if(event.keyCode == 13) {
                  $('form').submit();

              }
          });
      });
      $(".keywords-container").each(function() {
          var skills = []  ;
          var oldSkills;

              @if( old('skills'))
                  oldSkills = '{{  old('skills') }}'
                  skills = oldSkills.split(',');
          @endif


          var keywordInput = $(this).find(".keyword-input");
          var keywordsList = $(this).find(".keywords-list");

          function addKeyword() {
              if(skills.length < 6 ) {
              var $newKeyword = $("<span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>" + keywordInput.val() + "</span></span>");
              keywordsList.append($newKeyword).trigger('resizeContainer');
                  skills.push(keywordInput.val());
                  $('#taskSkills').val(skills);
              }else {
                  $('#maxSkills').text('You can just add 6 skills !').fadeIn('slow').delay(2000).fadeOut();
              }
              keywordInput.val("");
          }
          keywordInput.on('keyup', function(e) {
              if ((e.keyCode == 13) && (keywordInput.val() !== "")) {
                  addKeyword();
              }
          });
          $('.keyword-input-button').on('click', function() {
              if ((keywordInput.val() !== "")) {
                  addKeyword();
              }
          });
          $(document).on("click", ".keyword-remove", function() {
              $(this).parent().addClass('keyword-removed');
              function removeFromMarkup() {
                  skills = $.grep(skills, function(value) {
                  return value !=  $(".keyword-removed").text();
                  });
                  $('#taskSkills').val(skills);
                  $(".keyword-removed").remove()
              }
              setTimeout(removeFromMarkup, 500);
              keywordsList.css({
                  'height': 'auto'
              }).height();
          });
          keywordsList.on('resizeContainer', function() {
              var heightnow = $(this).height();
              var heightfull = $(this).css({
                  'max-height': 'auto',
                  'height': 'auto'
              }).height();
              $(this).css({
                  'height': heightnow
              }).animate({
                  'height': heightfull
              }, 200);
          });
          $(window).on('resize', function() {
              keywordsList.css({
                  'height': 'auto'
              }).height();
          });
          $(window).on('load', function() {
              var keywordCount = $('.keywords-list').children("span").length;
              if (keywordCount > 0) {
                  keywordsList.css({
                      'height': 'auto'
                  }).height();
              }
          });
      });

      $('#upload').on('change', function () {
        $('#attachments').html('');
          var files = $(this)[0].files;
          if(files.length > 3) {

              $('#uploadErrors').text( ' Only 3 files are allowed !').fadeIn('slow').delay(3000).fadeOut();

          } else
          $.each(files, function (i, file) {

                  $('#attachments').append('<div class="attachment-box ripple-effect">\n' +
                      '\t\t\t\t\t\t\t\t\t\t\t\t\t<span>'+ file.name +'</span> \n' +

                      '\t\t\t\t\t\t\t\t\t\t\t\t</div>')

          })
      })

      $('#submitForCompany').on('change', function () {

            $('#selectCompany').hasClass('toggle') ? $('#selectCompany').removeClass('toggle') : $('#selectCompany').addClass('toggle');
      })
  </script>
  <script type="text/javascript">
      function initAutocomplete() {
          var city, street, place, state, code, sublocality = ' ';


          var input = document.getElementById('autocomplete-input');
          var autocomplete = new google.maps.places.Autocomplete(input);
          autocomplete.addListener('place_changed', function() {
              var placeUser = autocomplete.getPlace();
              var lat = placeUser.geometry.location.lat();
              var lng = placeUser.geometry.location.lng();
              for (var i = 0; i < placeUser.address_components.length; i++) {
                  var addressType = placeUser.address_components[i]["types"][0];
                  console.log(addressType);
                  switch (addressType) {
                      case 'route':
                          street = placeUser.address_components[i]['long_name'];

                          break;
                      case 'administrative_area_level_1':
                          place = placeUser.address_components[i]['long_name'];
                          break;
                      case 'locality':
                          city = placeUser.address_components[i]['long_name'];
                          break;
                      case 'sublocality_level_1':
                          sublocality = placeUser.address_components[i]['long_name'];
                          break;
                      case 'country':
                          country = placeUser.address_components[i]['long_name'];
                          break;
                  }


              }
              $('#city').val(city);
              $('#province').val(sublocality);
              $('#lat').val(lat);
              $('#lon').val(lng);

              var myLatLng = {
                  lng: lng,
                  lat: lat,
              };
              var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                  zoom: 18,
                  center: myLatLng,
                  scrollwheel: false,
                  zoomControl: true,
                  mapTypeControl: false,
                  scaleControl: false,
                  panControl: false,
                  navigationControl: false,
                  streetViewControl: false,
                  styles: [{
                      "featureType": "landscape",
                      "elementType": "all",
                      "stylers": [{
                          "color": "#f2f2f2"
                      }]
                  }]
              });

              var marker = new google.maps.Marker({
                  position: myLatLng,
                  draggable: true,
                  map: single_map,
                  title: 'Your location'
              });

              google.maps.event.addListener(single_map, 'click', function(event) {



                  marker.setPosition(event.latLng);
                  var geocoder	= new google.maps.Geocoder();							// create a geocoder object
                  var location	= new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());		// turn coordinates into an object

                  geocoder.geocode({'latLng': location}, function (results, status) {


                      $('#autocomplete-input').val("");
                      $('#city').val("");
                      $('#province').val("");
                      $('#zip').val("");
                      $('#lat').val("");
                      $('#lon').val("");
                      $('#autocomplete-input').val(results[1].formatted_address);

                      if (results[0].address_components[3]) {
                          $('#city').val(results[0].address_components[3].long_name);
                      }else {
                          $('#city').val(results[0].address_components[2].long_name);
                      }

                      $('#province').val(results[0].address_components[2].long_name);
                      $('#lat').val(event.latLng.lat());
                      $('#lon').val(event.latLng.lng());
                  });

              });

              if (!placeUser.geometry) {
                  window.alert("No details available for input: '" + place.name + "'");
                  return;
              }
          });
      }
  </script>

  <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>

  <script>


      function singleMap() {
          var myLatLng = {
              lng: $('#singleMap').data('longitude'),
              lat: $('#singleMap').data('latitude'),
          };


          var single_map = new google.maps.Map(document.getElementById('singleMap'), {
              zoom: 18,
              center: myLatLng,
              scrollwheel: false,
              zoomControl: true,
              mapTypeControl: false,
              scaleControl: false,
              panControl: false,
              navigationControl: false,
              streetViewControl: false,
              styles: [{
                  "featureType": "landscape",
                  "elementType": "all",
                  "stylers": [{
                      "color": "#f2f2f2"
                  }]
              }]
          });

          var marker = new google.maps.Marker({
              position: myLatLng,
              draggable: true,
              map: single_map,
              title: 'Your location'
          });
          var zoomControlDiv = document.createElement('div');
          var zoomControl = new ZoomControl(zoomControlDiv, single_map);


          function ZoomControl(controlDiv, single_map) {
              zoomControlDiv.index = 1;
              single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
              controlDiv.style.padding = '5px';
              var controlWrapper = document.createElement('div');
              controlDiv.appendChild(controlWrapper);
              var zoomInButton = document.createElement('div');
              zoomInButton.className = "mapzoom-in";
              controlWrapper.appendChild(zoomInButton);
              var zoomOutButton = document.createElement('div');
              zoomOutButton.className = "mapzoom-out";
              controlWrapper.appendChild(zoomOutButton);
              google.maps.event.addDomListener(zoomInButton, 'click', function() {
                  single_map.setZoom(single_map.getZoom() + 1);
              });
              google.maps.event.addDomListener(zoomOutButton, 'click', function() {
                  single_map.setZoom(single_map.getZoom() - 1);
              });
          }
          google.maps.event.addListener(marker, 'dragend', function(event) {
              document.getElementById("lat").value = event.latLng.lat();
              document.getElementById("long").value = event.latLng.lng();
          });

          google.maps.event.addListener(single_map, 'click', function(event) {



              marker.setPosition(event.latLng);
              var geocoder	= new google.maps.Geocoder();							// create a geocoder object
              var location	= new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());		// turn coordinates into an object

              geocoder.geocode({'latLng': location}, function (results, status) {


                  $('#autocomplete-input').val("");
                  $('#city').val("");
                  $('#province').val("");
                  $('#zip').val("");
                  $('#lat').val("");
                  $('#lon').val("");
                  $('#autocomplete-input').val(results[1].formatted_address);

                  if (results[0].address_components[3]) {
                      $('#city').val(results[0].address_components[3].long_name);
                  }else {
                      $('#city').val(results[0].address_components[2].long_name);
                  }

                  $('#province').val(results[0].address_components[2].long_name);
                  $('#lat').val(event.latLng.lat());
                  $('#lon').val(event.latLng.lng());
              });

          });
      }
      var single_map = document.getElementById('singleMap');
      if (typeof(single_map) != 'undefined' && single_map != null) {
          google.maps.event.addDomListener(window, 'load', singleMap);
      }

  </script>

@endsection
