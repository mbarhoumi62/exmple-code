@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Manage Jobs')])
@section('header')
      @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
  <!-- Dashboard Container -->
<div class="dashboard-container">
  <!-- Dashboard Sidebar
================================================== -->

  @include('User::frontOffice.dashboard.inc.sidebar')

<!-- Dashboard Sidebar / End -->

<!-- Dashboard Content
================================================== -->
<div class="dashboard-content-container" data-simplebar>
  <div class="dashboard-content-inner" >

    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
      <h3>{{ __('Manage Jobs') }}</h3>

      <!-- Breadcrumbs -->
      <nav id="breadcrumbs" class="dark">
        <ul>
            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
          <li>{{ __('Manage Jobs') }}</li>
        </ul>
      </nav>
    </div>

    <!-- Row -->
    <div class="row">

      <!-- Dashboard Box -->
      <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">

          <!-- Headline -->
          <div class="headline">
            <h3><i class="icon-material-outline-business-center"></i> {{ __('My Job Listings') }}</h3>
          </div>

          <div class="content">
            <ul class="dashboard-box-list">
                @if(count(Auth::user()->jobs) !=0 )
                @foreach(Auth::user()->jobs as $job)
              <li>
                <!-- Job Listing -->
                <div class="job-listing">

                  <!-- Job Listing Details -->
                  <div class="job-listing-details">


										<a href="{{ route('showCompanyDetails', $job->company->getCompanyParamName()) }}" class="job-listing-company-logo">
                      <img src="{{ $job->company->medias()->where('type',0)->first() ?  $job->company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png' }}" alt="{{$job->company->name}}">
                    </a>

                    <!-- Details -->
                    <div class="job-listing-description">
                      <h3 class="job-listing-title"><a href="{{ route('showJobDetails', $job->getJobParamTitle()) }}">{{ $job->title }}</a> <span class="dashboard-status-button green">{{ __('Published') }}</span></h3>

                      <!-- Job Listing Footer -->
                      <div class="job-listing-footer">
                        <ul>
                          <li><i class="icon-material-outline-date-range"></i> {{ __('Posted on') }} {{$job->created_at->format('d M, Y')}}</li>
                          <li><i class="icon-material-outline-date-range"></i> {{ __('Expiring on') }} {{Carbon\Carbon::parse($job->created_at)->addDays(30)->format('d M, Y')}}</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- Buttons -->
                <div class="buttons-to-right always-visible">
                  <a href="{{route('showManageCandidates' , Crypt::encrypt($job->id))}}" class="button ripple-effect"><i class="icon-material-outline-supervisor-account"></i> {{ __('Manage Candidates') }} <span class="button-info">{{ count($job->applications) }}</span></a>
                  <a href="{{route('showUpdateJob', Crypt::encrypt($job->id))}}" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
                  <a href="#" class="button gray ripple-effect ico deleteJob" data-id={{ $job->id }} title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2 "></i></a>
                </div>
              </li>
            @endforeach
          @else
          <li>
            <h3>{{ __('You have no jobs yet,') }} @if(count(Auth::user()->companies) != 0)  <a href="{{ route('showPostJob') }}">{{ __('Create one?') }}</a> @else <a href="{{ route('showAddCompanyAfterRegister') }}">{{ __('start by creating a profile for your company') }}</a> @endif</h3>
          </li>
                            @endif 
            </ul>
          </div>
        </div>
      </div>

    </div>
    <!-- Row / End -->

      @include('frontOffice.inc.small-footer')

  </div>
</div>
<!-- Dashboard Content / End -->
</div>

    <script>
      $(document).on("click", ".deleteJob", function(e) {
        e.preventDefault();
         var jobId = $(this).data('id');
          $.confirm({
              title: '{{ __('Confirm!') }}',
              content: '{{ __('Are you sure you want to delete this job?') }}',
              buttons: {
                  cancel: function () {
                  },
                  somethingElse: {
                      text: 'Yes',
                      btnClass: 'button ripple-effect',
                      keys: ['enter', 'shift'],
                      action: function(){
                        deleteForm('{{route('handleDeleteJob')}}', {
                          jobId : jobId
                        }, 'post');
                    }
                  }
              }
          });
      });

  </script>

@endsection
