@extends('frontOffice.layout',['class' => 'gray', 'title' => 'Update Task : '.$task->name])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
    <!-- Dashboard Container -->
    <div class="dashboard-container">
        <!-- Dashboard Sidebar
      ================================================== -->

    @include('User::frontOffice.dashboard.inc.sidebar')

    <!-- Dashboard Sidebar / End -->


        <!-- Dashboard Content
        ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>{{ __('Update a Task') }}</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                            <li>{{ __('Update a Task') }}</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->

                <form action="{{route('handleUpdateTask', Crypt::encrypt($task->id))}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">

                        <!-- Dashboard Box -->
                        <div class="col-xl-12">
                            <div class="dashboard-box margin-top-0">

                                <!-- Headline -->
                                <div class="headline">
                                    <h3><i class="icon-feather-folder-plus"></i> {{ __('Update') }} : {{ $task->name }} Task</h3>
                                </div>

                                <div class="content with-padding padding-bottom-10">
                                    <div class="row">

                                        <div class="col-xl-12">
                                            <div class="submit-field">
                                                <h5>{{ __('Project Name') }}</h5>
                                                <input name="name" type="text" value="{{ $task->name }}" class="with-border">
                                                @if ($errors->has('name'))
                                                    <small>{{ $errors->first('name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>{{ __('Category') }}</h5>
                                                <select name="category" class="selectpicker with-border" data-size="7" title="{{ __('Select Category') }}">
                                                    @foreach (\App\Modules\General\Models\TaskCategory::all() as $category)
                                                        <option value="{{$category->id}}">{{$category->label}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('category'))
                                                    <small>{{ $errors->first('category') }}</small>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                                <h5>{{ __('Location') }}  <i class="help-icon" data-tippy-placement="right" title="{{ __("Leave blank if it's a remote task") }}"></i></h5>
                                                <div class="input-with-icon">
                                                    <div id="autocomplete-container">
                                                        <input value="{{ $task->address ? $task->address->address : '' }}" id="autocomplete-input" type="text" placeholder="{{ __("Leave blank if it's a remote task") }}" name="address">
                                                       @if($task->address)
                                                        <input value="{{ $task->address->city }} " type="hidden" name="city" id="city">
                                                        <input value="{{ $task->address->province }}" type="hidden" name="province" id="province">
                                                        <input value="{{ $task->address->lat }}" type="hidden" name="lat" id="lat">
                                                        <input value="{{ $task->address->lon }}" type="hidden" name="lon" id="lon">
                                                        @else
                                                                  <input value="{{ old('city') }} " type="hidden" name="city" id="city">
                        <input value="{{ old('province')}}" type="hidden" name="province" id="province">
                        <input value="{{ old('lat') }}" type="hidden" name="lat" id="lat">
                        <input value="{{ old('lon') }}" type="hidden" name="lon" id="lon">   
                                                        @endif
                                                            @if ($errors->has('address') || $errors->has('lon'))
                                  <small>{{ __('Please pick your address from the suggestions!') }}</small>
                                 @endif
                                                    </div>
                                                    <i class="icon-material-outline-location-on"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-8">
                                            <div class="submit-field">
                                                <h5>{{ __('What is your estimated budget?') }}</h5>
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="input-with-icon">
                                                            <input value="{{$task->budget->min}}" name="min" class="with-border" type="text" placeholder="{{ __('Minimum') }}">
                                                            <i class="currency">TND</i>
                                                        </div>
                                                        @if ($errors->has('min'))
                                                            <small>{{ $errors->first('min') }}</small>
                                                        @endif
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="input-with-icon">
                                                            <input value="{{$task->budget->max}}" name="max" class="with-border" type="text" placeholder="{{ __('Maximum') }}">
                                                            <i class="currency">TND</i>
                                                        </div>
                                                        @if ($errors->has('max'))
                                                            <small>{{ $errors->first('max') }}</small>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="feedback-yes-no margin-top-0">
                                                    <div class="radio">
                                                        <input id="radio-1" name="budgetType" value="0" type="radio" @if($task->budget->type == 0) checked @endif>
                                                        <label for="radio-1"><span class="radio-label"></span> {{ __('Fixed Price Project') }}</label>
                                                    </div>

                                                    <div class="radio">
                                                        <input id="radio-2" name="budgetType" value="1" type="radio"  @if($task->budget->type == 1) checked @endif>
                                                        <label for="radio-2"><span class="radio-label"></span> {{ __('Hourly Project') }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-12">
                                            <div class="submit-field">
                                                <h5>{{ __('What skills are required?') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Up to 5 skills that best describe your project') }}"></i> <small id="maxSkills"></small></h5>
                                                <div class="keywords-container">
                                                    <div class="keyword-input-container">
                                                        <input  type="text" id="skillsInput" class="keyword-input with-border" placeholder="{{ __('Add Skills') }}"/>
                                                        @if ($errors->has('skills'))
                                                            <small>{{ $errors->first('skills') }}</small>
                                                        @endif

                                                        @php
                                                            $oldSkills = '';
                                                            foreach($task->skills as $skill)
                                                            $oldSkills .=$skill->label.',';
                                                        @endphp
                                                        <input value="{{  substr($oldSkills, 0, -1) }}" type="hidden" id="taskSkills" name="skills">
                                                        <a class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></a>
                                                    </div>
                                                    <div class="keywords-list">

                                                            @foreach($task->skills as $skill)
                                                                <span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>{{ $skill->label }}</span></span>
                                                            @endforeach

                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-xl-12">
                                            <div class="submit-field">
                                                <h5>{{ __('Describe Your Project') }}</h5>
                                                <textarea name="description" cols="30" rows="5" class="with-border">{{ $task->description }}</textarea>
                                                @if ($errors->has('description'))
                                                    <small>{{ $errors->first('description') }}</small>
                                                @endif
                                                <div class="uploadButton margin-top-30">
                                                    <input name="attachments[]"  class="uploadButton-input" type="file" id="upload" multiple/>
                                                    <label class="uploadButton-button ripple-effect" for="upload">{{ __('Upload Files') }}</label>

                                                    @if(count($errors) > 0)
                                                        <span style="font-size: 18px" class="uploadButton-file-name"> {{ __('Please check your files, you may need to re-upload them') }}</span>

                                                    @else
                                                        <span class="uploadButton-file-name">{{ __('Images or documents that might be helpful in describing your project') }} <small id="uploadErrors"> &nbsp;({{ __('Each file size must be less than 10 MB') }})</small></span>
                                                    @endif
                                                </div>
                                                <div id="attachments" class="attachments-container margin-top-0 margin-bottom-0">
                                                        @if(count($task->medias()->where('type',3)->get()) > 0)
                                                            @foreach($task->medias()->where('type',3)->get() as $file)
                                                        <div class="attachment-box ripple-effect">
                                                          <span>{{ $file->label }}</span>
                                                            <button class="remove-attachment" data-id="{{$file->id}}" data-tippy-placement="top" title="Remove"></button>
                                                        </div>
                                                        @endforeach
                                                            @endif
                                                </div>
                                                @if ($errors->has('attachments'))
                                                    <small>{{ $errors->first('attachments') }}</small>
                                                @endif
                                                @for($i = 0; $i<3 ; $i++ )
                                                    @if ($errors->has('attachments.'.$i))
                                                        <small>{{ 'File '.($i +1).' :'. $errors->first('attachments.'.($i +1)).' | ' }}</small>
                                                    @endif
                                                @endfor
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12">
                            <button type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> {{ __('Update Task') }}</button>
                        </div>

                    </div>
                    <!-- Row / End -->
                </form>

                @include('frontOffice.inc.small-footer')

            </div>
        </div>
        <!-- Dashboard Content / End -->

    </div>

    <script>

        $(document).ready(function() {
            const oldCategory = '{{ $task->category_id   }}';

            if(oldCategory !== '') {
                $('select').val(oldCategory).change();
            }
        });

        $('#skillsInput').focus(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

        });

        $('#skillsInput').focusout(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    $('form').submit();

                }
            });
        });
        $(".keywords-container").each(function() {
            var skills = []  ;
            skills = $('#taskSkills').val().split(',');

            var keywordInput = $(this).find(".keyword-input");
            var keywordsList = $(this).find(".keywords-list");

            function addKeyword() {
                if(skills.length < 6 ) {
                    var $newKeyword = $("<span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>" + keywordInput.val() + "</span></span>");
                    keywordsList.append($newKeyword).trigger('resizeContainer');
                    skills.push(keywordInput.val());
                    $('#taskSkills').val(skills);
                }else {
                    $('#maxSkills').text('You can just add 6 skills !').fadeIn('slow').delay(2000).fadeOut();
                }
                keywordInput.val("");
            }
            keywordInput.on('keyup', function(e) {
                if ((e.keyCode == 13) && (keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $('.keyword-input-button').on('click', function() {
                if ((keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $(document).on("click", ".keyword-remove", function() {
                $(this).parent().addClass('keyword-removed');
                function removeFromMarkup() {
                    skills = $.grep(skills, function(value) {
                        return value !=  $(".keyword-removed").text();
                    });
                    $('#taskSkills').val(skills);
                    $(".keyword-removed").remove()
                }
                setTimeout(removeFromMarkup, 500);
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            keywordsList.on('resizeContainer', function() {
                var heightnow = $(this).height();
                var heightfull = $(this).css({
                    'max-height': 'auto',
                    'height': 'auto'
                }).height();
                $(this).css({
                    'height': heightnow
                }).animate({
                    'height': heightfull
                }, 200);
            });
            $(window).on('resize', function() {
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            $(window).on('load', function() {
                var keywordCount = $('.keywords-list').children("span").length;
                if (keywordCount > 0) {
                    keywordsList.css({
                        'height': 'auto'
                    }).height();
                }
            });
        });
        var length = $("#attachments").children().length;
        $('#upload').on('change', function () {
            $('#attachments').html('');
            var files = $(this)[0].files;

            if(length > 2) {

                $('#uploadErrors').text( ' Only 3 files are allowed !').fadeIn('slow').delay(3000).fadeOut();

            } else
                $.each(files, function (i, file) {

                    $('#attachments').append('<div class="attachment-box ripple-effect">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t<span>'+ file.name +'</span> \n' +

                        '\t\t\t\t\t\t\t\t\t\t\t\t</div>')

                    length++;
                })
        })

        $('.remove-attachment').on('click' ,  function (e) {
            e.preventDefault();
            var parent = $(this).parent()
            $.get("{{ route('apiHandleDeleteFreelancerFile')}}?id="+$(this).data('id')).done(function (res) {
                if(res.status == 200) {
                    parent.css('display', 'none');
                    length--;
                }

            });
        })
    </script>
  <script type="text/javascript">
      function initAutocomplete() {
          var city, street, place, state, code, sublocality = ' ';


          var input = document.getElementById('autocomplete-input');
          var autocomplete = new google.maps.places.Autocomplete(input);
          autocomplete.addListener('place_changed', function() {
              var placeUser = autocomplete.getPlace();
              var lat = placeUser.geometry.location.lat();
              var lng = placeUser.geometry.location.lng();
              for (var i = 0; i < placeUser.address_components.length; i++) {
                  var addressType = placeUser.address_components[i]["types"][0];
                  console.log(addressType);
                  switch (addressType) {
                      case 'route':
                          street = placeUser.address_components[i]['long_name'];

                          break;
                      case 'administrative_area_level_1':
                          place = placeUser.address_components[i]['long_name'];
                          break;
                      case 'locality':
                          city = placeUser.address_components[i]['long_name'];
                          break;
                      case 'sublocality_level_1':
                          sublocality = placeUser.address_components[i]['long_name'];
                          break;
                      case 'country':
                          country = placeUser.address_components[i]['long_name'];
                          break;
                  }


              }
              $('#city').val(city);
              $('#province').val(sublocality);
              $('#lat').val(lat);
              $('#lon').val(lng);

              var myLatLng = {
                  lng: lng,
                  lat: lat,
              };
              var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                  zoom: 18,
                  center: myLatLng,
                  scrollwheel: false,
                  zoomControl: true,
                  mapTypeControl: false,
                  scaleControl: false,
                  panControl: false,
                  navigationControl: false,
                  streetViewControl: false,
                  styles: [{
                      "featureType": "landscape",
                      "elementType": "all",
                      "stylers": [{
                          "color": "#f2f2f2"
                      }]
                  }]
              });

              var marker = new google.maps.Marker({
                  position: myLatLng,
                  draggable: true,
                  map: single_map,
                  title: 'Your location'
              });

              google.maps.event.addListener(single_map, 'click', function(event) {



                  marker.setPosition(event.latLng);
                  var geocoder  = new google.maps.Geocoder();                           // create a geocoder object
                  var location  = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());       // turn coordinates into an object

                  geocoder.geocode({'latLng': location}, function (results, status) {


                      $('#autocomplete-input').val("");
                      $('#city').val("");
                      $('#province').val("");
                      $('#zip').val("");
                      $('#lat').val("");
                      $('#lon').val("");
                      $('#autocomplete-input').val(results[1].formatted_address);

                      if (results[0].address_components[3]) {
                          $('#city').val(results[0].address_components[3].long_name);
                      }else {
                          $('#city').val(results[0].address_components[2].long_name);
                      }

                      $('#province').val(results[0].address_components[2].long_name);
                      $('#lat').val(event.latLng.lat());
                      $('#lon').val(event.latLng.lng());
                  });

              });

              if (!placeUser.geometry) {
                  window.alert("No details available for input: '" + place.name + "'");
                  return;
              }
          });
      }
  </script>

  <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>
@endsection
