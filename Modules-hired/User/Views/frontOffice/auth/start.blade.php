@extends('frontOffice.layout', ['title' => __('Hired | Start')])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')


    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">

                <div class="login-register-page">

                    <div class="order-confirmation-page" style="padding-bottom: 25px!important;">
                        <div class="breathing-icon" style="font-size: 70px!important;"><i class="icon-feather-check"></i></div>
                        <h2 class="margin-top-30" style="    line-height: 3rem;">{{ __('Congratulation! Your account has been created.') }}</h2>
                        @if(Auth::user()->type == 0)
                        <p>{{ __("Let's start by creating your company") }}</p>
                        <a href="{{ route('showAddCompanyAfterRegister') }}" class="button ripple-effect-dark button-sliding-icon margin-top-30">{{ __('Create company') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                        <p>{{ __("Are you an individual?") }} <a href="{{ route('showPostTask') }}" class="margin-top-30"> {{ __('Post your first task.') }}</a></p>
                        @else
                            <p>{{ __("Let's start by creating your profile") }}</p>
                            <a href="{{ route('showFreelancerSteps') }}" class="button ripple-effect-dark button-sliding-icon margin-top-30">{{ __('Create your profile') }} <i class="icon-material-outline-arrow-right-alt"></i></a>

                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="margin-top-70"></div>

@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection
