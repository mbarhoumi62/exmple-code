@extends('frontOffice.layout', ['title' =>  __('Hired | Login')])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')


    <div class="margin-top-70"></div>

    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-3">
          
               
                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3>{{ __("Login in to proceed!") }}</h3>
                        <span>Don't have an account? <a href="{{route('showRegister')}}">{{ __('Sign Up!') }}</a></span>
                        @if ($errors->getBag('invalidCredentials')->first())
                            <small>
                                {{ __($errors->getBag('invalidCredentials')->first()) }}
                            </small>
                        @endif
                    </div>

                    <!-- Form -->
               
                             <form method="post" action="{{route('handleLogin')}}">
                        @csrf
                        <div class="input-with-icon-left">
                            <i class="icon-material-baseline-mail-outline"></i>
                            <input value="{{old('email')}}" type="text" class="input-text with-border" name="email" id="emailaddress" placeholder="{{ __('Email Address') }}" />
                            @if ($errors->has('email'))
                                <small>
                                {{ $errors->first('email') }}
                           </small>
                            @endif
                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-outline-lock"></i>
                            <input value="{{old('password')}}" type="password" class="input-text with-border" name="password" id="password" placeholder="{{ __('Password') }}" />
                            <i class="fa fa-eye-slash toggle-password" aria-hidden="true" style="right: 0; cursor: pointer;"></i>
                            @if ($errors->has('password'))
                                <small>
                                    {{ $errors->first('password') }}
                                </small>
                            @endif
                        </div>
                        <a href="{{route('showRequestResetPassword')}}" class="forgot-password">{{ __('Forgot Password?') }}</a>

                        <input type="hidden" name="redirect" value="redirect">
                    <!-- Button -->
                    <button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit" >{{ __('Log In') }} <i class="icon-material-outline-arrow-right-alt"></i></button>
                    </form>

                </div>

            </div>
        </div>
    </div>

@endsection

