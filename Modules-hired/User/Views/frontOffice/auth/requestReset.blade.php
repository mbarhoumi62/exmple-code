@extends('frontOffice.layout',['title' => __('Hired | Request Password Reset')])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')
    <style>
        #invalid-email {
            color : red;
        }
    </style>
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>{{ __('Forgot Password') }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{__('Home')}}</a></li>
                            <li>{{ __('Log In') }}</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-3">
                @if(Session::has('emailStatus'))
                    <div class="notification notice closeable">
                        <p>{{ __('Please check your mailbox to activate your account.') }}</p>
                        <a class="close"></a>
                    </div>
                @endif
                @if(Session::has('restricted'))
                    <div  class="notification notice closeable">
                        <p>{{ __(Session::get('restricted')) }}</p>
                        <a class="close"></a>
                    </div>
                @endif

                    <div id="done-notif" style="display: none" class="notification notice closeable">
                        <span id="done"></span>
                        <a class="close"></a>
                    </div>

                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3>{{ __('Enter your email to receive password reset link!') }}</h3>
                        <span id="invalid-email"></span>
                    </div>

                    <!-- Form -->
                    <form  method="post" action="javascript:void(0)" id="reset-password-form">
                        <div class="input-with-icon-left">
                            <i class="icon-material-baseline-mail-outline"></i>
                            <input type="text" autocomplete="off" class="input-text with-border" name="email" id="reset-email" placeholder="{{ __('Email Address') }}" />
                            <small id="reset-email-error"></small>
                        </div>
                        <!-- Button -->
                        <button id="reset-submit-btn" class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit" form="reset-password-form" >{{ __('Submit') }} <i class="icon-material-outline-arrow-right-alt"></i></button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="margin-top-70"></div>
    <script>
        $( "#reset-password-form" ).submit(function( event ) {
            event.preventDefault();
            $('#reset-submit-btn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
            $('#reset-submit-btn').css('opacity','0.5');
            $('#reset-submit-btn').css('cursor','not-allowed');
            $("#reset-submit-btn").attr("disabled", true);
            $('#invalid-email').text('')
            $('#reset-email-error').text('')
            $('#reset-email-error').text('')
            $('#reset-email').css('border-color','')
            $('#reset-email').css('border-color','')
            var data = {
                'email' : $('#reset-email').val(),
            }

            $.post('{{route('apiResetPassword')}}',
                {
                    '_token': $('meta[name=csrf-token]').attr('content'), data : data

                })
                .done(function (res) {
                    $('#reset-submit-btn').html('Submit <i class="icon-material-outline-arrow-right-alt"></i>')
                    $('#reset-submit-btn').css('opacity','1');
                    $('#reset-submit-btn').css('cursor','pointer');
                    $("#reset-submit-btn").attr("disabled", false);
                    if(res.errors) {
                        if(res.errors.email) {
                            $('#reset-email-error').text(res.errors.email[0])
                            $('#reset-email').css('border-color','red')
                        }
                    }else {
                        if(res.notFound) {
                            $('#invalid-email').text(res.notFound)
                        }
                        if(res.status === 200) {
                             $('#reset-email').val('');
                            $('#done-notif').css('display', 'block');
                            $('#done').html('{{ __('We have sent you an email with the reset password link.The email can take few minutes to be delivered, if you did not receive it, please try again later.') }}');

                        }
                    }
                })
                .fail(function (res) {
                    $('#reset-submit-btn').html('Register <i class="icon-material-outline-arrow-right-alt"></i>')
                    $('#reset-submit-btn').css('opacity','1');
                    $('#reset-submit-btn').css('cursor','pointer');
                    $("#reset-submit-btn").attr("disabled", false);
                    $('#invalid-email').text('Something went wrong, please try again !')
                })
        });
    </script>
@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection


