@extends('frontOffice.layout', ['title' =>  __('Hired | Login')])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')

    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>{{ __('Log In') }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{__('Home')}}</a></li>
                            <li>{{ __('Log In') }}</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-3">
              @if(Session::has('emailStatus'))
              <div class="notification notice closeable">
                <p>{{ __('Please check the your mailbox to activate your account.') }}</p>
                <a class="close"></a>
              </div>
              @endif
                  @if(Session::has('restricted'))
                      <div class="notification notice closeable">
                          <p>{{ __(Session::get('restricted')) }}</p>
                          <a class="close"></a>
                      </div>
                  @endif

                  @if(Session::has('emailUpdated'))
                      <div class="notification success closeable">
                          <p>{{ __(Session::get('emailUpdated')) }}</p>
                          <a class="close"></a>
                      </div>
                  @endif

                  @if(Session::has('emailUpdatedError'))
                      <div class="notification error closeable">
                          <p>{{ __(Session::get('emailUpdatedError')) }}</p>
                          <a class="close"></a>
                      </div>
                  @endif

                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3>{{ __("We're glad to see you again!") }}</h3>
                        <span>Don't have an account? <a href="{{route('showRegister')}}">{{ __('Sign Up!') }}</a></span>
                        @if ($errors->getBag('invalidCredentials')->first())
                            <small>
                                {{ __($errors->getBag('invalidCredentials')->first()) }}
                            </small>
                        @endif
                    </div>

                    <!-- Form -->
                    <form method="post" action="{{route('handleLogin')}}">
                        @csrf
                        <div class="input-with-icon-left">
                            <i class="icon-material-baseline-mail-outline"></i>
                            <input value="{{old('email')}}" type="text" class="input-text with-border" name="email" id="emailaddress" placeholder="{{ __('Email Address') }}" />
                            @if ($errors->has('email'))
                                <small>
                                {{ $errors->first('email') }}
                           </small>
                            @endif
                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-outline-lock"></i>
                            <input value="{{old('password')}}" type="password" class="input-text with-border" name="password" id="password" placeholder="{{ __('Password') }}" />
                           <i class="fa fa-eye-slash toggle-password" aria-hidden="true" style="right: 0; cursor: pointer;"></i>
                            @if ($errors->has('password'))
                                <small>
                                    {{ $errors->first('password') }}
                                </small>
                            @endif
                        </div>
                        <a href="{{route('showRequestResetPassword')}}" class="forgot-password">{{ __('Forgot Password?') }}</a>


                    <!-- Button -->
                    <button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit" >{{ __('Log In') }} <i class="icon-material-outline-arrow-right-alt"></i></button>
                    </form>
                    <!-- Social Login -->
                    <div class="social-login-separator"><span>{{ __('or') }}</span></div>
                    <div class="social-login-buttons">
                        <button onclick="window.location.href='{{route('handleProviderRedirect', 'facebook')}}'" class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> {{ __('Log In via Facebook') }}</button>
                        <button  onclick="window.location.href='{{route('handleProviderRedirect', 'google')}}'" class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> {{ __('Log In via Google') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="margin-top-70"></div>



@endsection



@section('footer')
    @include('frontOffice.inc.footer')
@endsection
