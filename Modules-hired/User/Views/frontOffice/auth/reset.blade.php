@extends('frontOffice.layout',['title' => __('Hired | Reset Password')])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>{{ __('Reset Password') }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{__('Home')}}</a></li>
                            <li>{{ __('Register') }}</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-3">

                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3 style="font-size: 26px;">{{ __('Password Reset') }}</h3>

                        @if ($errors->getBag('badRequest')->first())
                            <small>
                                {{ __($errors->getBag('badRequest')->first()) }}
                            </small>
                        @endif
                    </div>

                    <form method="post" action="{{route('handleResetPassword')}}" >
                        @csrf


                        <div class="input-with-icon-left register-input" title="Should be at least 6 characters long" data-tippy-placement="bottom">
                            <i class="icon-material-outline-lock"></i>
                            <input value="{{old('password')}}" type="password" class="input-text with-border" name="password"  placeholder="{{ __('New Password') }}" />
                            <i class="fa fa-eye-slash toggle-password" aria-hidden="true" style="right: 0; cursor: pointer;"></i>
                            @if ($errors->has('password'))
                                <small>
                                    {{ __($errors->first('password')) }}
                                </small>
                            @endif
                        </div>

                        <div class="input-with-icon-left register-input">
                            <i class="icon-material-outline-lock"></i>
                            <input value="{{old('password_confirmation')}}"  type="password" class="input-text with-border" name="password_confirmation"  placeholder="{{ __('Repeat Password') }}" />
                            @if ($errors->has('password_confirmation'))
                                <small>
                                    {{ __($errors->first('password_confirmation')) }}
                                </small>
                            @endif
                        </div>

                        <input type="hidden" name="id" value="{{$userId}}">

                    <!-- Button -->
                        <button class="button full-width button-sliding-icon ripple-effect margin-top-10"  type="submit" >{{ __('Submit') }} <i class="icon-material-outline-arrow-right-alt"></i></button>
                    </form>


                </div>

            </div>
        </div>
    </div>


    <!-- Spacer -->
    <div class="margin-top-70"></div>

@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection

