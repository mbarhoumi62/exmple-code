# Migration
```php
        Schema::create('locals', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name', 100);
            $table->string('image_language');
            $table->enum('status', ['enable', 'disable']);
            $table->enum('is_default', ['yes', 'no']);
            $table->timestamps();
        });
          Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->text("description")->nullable();
            $table->timestamps();
        });
           Schema::create('category_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Local::class, 'local_id')->onDelete('cascade');
            $table->foreignIdFor(Category::class)->onDelete('cascade');
            $table->string("name");
            $table->string("code")->index();
            $table->text("description")->nullable();
            $table->unique(['category_id', 'local_id']);
            $table->timestamps();
        });
```

# Models
```php
class Category extends Model
{
    use HasFactory, Trans;
    /**
     * Table Name
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * List of translated attributes.
     *
     * @var string[]
     */
    protected $translatable = ['name', 'description'];


    public function translations(): HasMany
    {
        return $this->hasMany(CategoryTranslation::class);
    }
}
```

# Trait

```php
trait Trans
{
    /**
     * Array Containt
     *
     * @var array
     */
    protected array $currentTranslate = [];


    protected string $locale;
    /**
     * Boot the translatable trait.
     *
     * @return void
     */
    public static function boot(): void
    {
        parent::boot();
        static::created(function (Model $model) {

            $model->translations()->saveMany($model->currentTranslate);
        });

        static::updating(function (Model $model) {

            $model->translations()->saveMany($model->currentTranslate);
        });
    }

    /**
     * Get a translation.
     *
     * @param string|null $locale
     * @param bool $fallback
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function translate(string $locale = null): ?Model
    {
        $locale = $locale ?: $this->getLocale();

        $translation = $this->getTranslation($locale);

        if (!$translation) {
            $translation = null;
        }

        return $translation;
    }

    /**
     * Get a translation from cache, loaded relation, or database.
     *
     * @param string $locale
     * @return Model|null
     */
    protected function getTranslation(string $locale): ?Model
    {

        if (isset($this->currentTranslate[$locale])) {
            return $this->currentTranslate[$locale];
        }

        if (!$this->exists) {
            return null;
        }

        $translation = $this->translations
            ->where('code', $locale)
            ->first();

        if ($translation) {
            $this->currentTranslate[$locale] = $translation;
        }

        return $translation;
    }

    /**
     * Get a translation or create new.
     *
     * @param string $locale
     * @param array $translation_array
     * @return Model
     */
    protected function translateOrNew(string $locale, array $translation_array): Model
    {


        if (isset($this->currentTranslate[$locale])) {
            return $this->currentTranslate[$locale];
        }
        $test = $this->translations()->where('code', $locale)->first();
        if ($test) {
            foreach ($translation_array as $key => $value) {
                $test->{$key} = $value;
            }
            return $test;
        }
        return $this->translations()->newModelInstance($translation_array);
    }


    /**
     * Set a given attribute on the model or translation.
     *
     * @param string|null $locale
     * @param array $translation
     * @return void
     */
    public function setTraduction(string $locale = null, array $translation)
    {

        $locale = $locale ?: $this->getLocale();
        $localId = $this->findLocalId($locale);
        $translation_array = [];

        foreach ($translation as $key => $value) {

            if (in_array($key, $this->getTranslatable())) {
                $translation_array[$key] = $value;
            }
        }

        $translation_array['code'] = $locale;
        $translation_array['local_id'] = $localId;
        $translation = $this->translateOrNew($locale, $translation_array);
        $this->currentTranslate[$locale] = $translation;

        return $translation;
    }


    /**
     * Get the translatable attributes array.
     *
     * @throws \Vinkla\Translator\TranslatableException
     *
     * @return array
     */
    protected function getTranslatable(): array
    {
        if (!property_exists($this, 'translatable')) {
            dd(1);
        }

        return $this->translatable;
    }


    /**
     * Delete the model from the database with its translations.
     *
     * @return bool|null
     */
    public function delete(): ?bool
    {
        if ($deleted = parent::delete()) {
            $this->translations()->delete();
        }

        return $deleted;
    }

    /**
     * Set the locale.
     *
     * @param string $locale
     *
     * @return void
     */
    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }

    /**
     * Get Default Language
     *
     *@author Mohamed Barhoumi<>

     * @return String
     */
    public function getLocale(): string
    {
        return App::getLocale();
    }

    /**
     * Undocumented function
     *
     * @param string $code
     * @return integer
     */
    protected function findLocalId(string $code): int
    {
        return Local::select('id')->whereCode($code)->first()->id;
    }


    /**
     * Get the translations relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    abstract public function translations(): HasMany;
}
```
# Test it
