<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ShippingMethod extends Model
{
    use HasFactory;
    protected $table = 'shipping_methods';
    protected $guarded = [];

    public function shippingCategory(): BelongsTo
    {
        return $this->belongsTo(ShippingCategory::class);
    }
}
