<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Supplier extends Model
{
    use HasFactory;
    /**
     * Table Name
     *
     * @var string
     */
    protected $table = 'suppliers';

    protected $guarded = [];


    public function supplierBranches(): HasMany
    {
        return $this->hasMany(Supplier::class);
    }
    public function branches(): BelongsToMany
    {
        return $this->belongsToMany(Branch::class);
    }
}
