<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Review extends Model
{
    use HasFactory;
    /**
     * Table Name
     *
     * @var string
     */
    protected $table = 'reviews';

    protected $guarded = [];

    public function reviewable(): MorphTo
    {
        return $this->morphTo();
    }
}
