<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AddressType extends Model
{
    use HasFactory;
    /**
     * Table Name
     *
     * @var string
     */
    protected $table = 'address_types';

    protected $guarded = [];

    public function supplierBranches(): HasMany
    {
        return $this->hasMany(SupplierBranch::class);
    }
}
