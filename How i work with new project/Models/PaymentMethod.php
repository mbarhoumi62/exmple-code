<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use HasFactory;
    protected $table = 'payment_methods';
    protected $guarded = [];

    const IS_ENABLE_STATUS = 'enable';
    const IS_DiSABLE_STATUS = 'disable';

    const ONLINE_GATEWAY = 'online';
    const OFFLINE_GATEWAY = 'offline';

    /**
     * Scope for enable status
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeEnable(Builder $query): Builder
    {
        return $query->where('status', self::IS_ENABLE_STATUS);
    }
    /**
     * Scope for inactive status
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDisable(Builder $query): Builder
    {
        return $query->where('status', self::IS_DiSABLE_STATUS);
    }

    /**
     * Scope for online gateway
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOnline(Builder $query): Builder
    {
        return $query->where('gateway', self::ONLINE_GATEWAY);
    }
    /**
     * Scope for offline gateway
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOffline(Builder $query): Builder
    {
        return $query->where('gateway', self::OFFLINE_GATEWAY);
    }
}
