<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class OrderDetails extends Model
{
    use HasFactory;

    /**
     * Table Name
     *
     * @var string
     */
    protected $table = 'order_details';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $guarded = [];

    public function item(): MorphTo
    {
        return $this->morphTo();
    }
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
