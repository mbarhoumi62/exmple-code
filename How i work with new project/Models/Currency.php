<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;
    protected $table = 'currencies';
    protected $guarded = [];

    const YES_IS_ACTIVE = 'yes';
    const NO_IS_NOT_ACTIVE = 'no';

    /**
     * Scope for active Currency
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeIsActive(Builder $query): Builder
    {
        return $query->where('status', self::YES_IS_ACTIVE);
    }

    /**
     * Scope for inactive Currency
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeIsNotActive(Builder $query): Builder
    {
        return  $query->where('status', self::NO_IS_NOT_ACTIVE);
    }
}
