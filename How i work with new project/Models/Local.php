<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    use HasFactory;

    const ENABLE_STATUS = 'enable';
    const DISABLE_STATUS = 'disable';
    const IS_DEFAULT = 'yes';
    const IS_NOT_DEFAULT = 'yes';

    protected $table = 'locals';
    protected $guarded = [];

    /**
     * Scope Status : Enable
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeEnable(Builder $query): Builder
    {
        return  $query->where('status', self::ENABLE_STATUS);
    }

    /**
     * Scope Status : Disable
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDisable(Builder $query): Builder
    {
        return  $query->where('status', self::DISABLE_STATUS);
    }

    /**
     * Scope Default : Yes is default
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeIsDefault(Builder $query): Builder
    {
        return  $query->where('default', self::IS_DEFAULT);
    }

    /**
     * Scope Default : No is not default
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeIsNotDefault(Builder $query): Builder
    {
        return  $query->where('default', self::IS_NOT_DEFAULT);
    }
}
