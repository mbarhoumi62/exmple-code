<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const ADMIN  = 'admin';
    const BUYER  = 'buyer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'image'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public  function scopeAdmin(Builder  $query): Builder
    {
        return  $query->where('role', 'admin');
    }

    public  function scopeBuyer(Builder  $query): Builder
    {
        return  $query->where('role', 'buyer');
    }

    public function IsAdmin(): bool
    {
        return $this->role === self::ADMIN;
    }

    public function IsBuyer(): bool
    {
        return $this->role === self::BUYER;
    }

    public function admin(): HasMany
    {
        return $this->hasMany(Admin::class);
    }

    public function buyer(): HasMany
    {
        return $this->hasMany(Buyer::class);
    }
}
