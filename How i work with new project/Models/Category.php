<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;
    protected $table = 'categories';

    protected $guarded = [];


    public function categoryType(): BelongsTo
    {
        return $this->belongsTo(CategoryType::class);
    }
    public function subcategory(): HasMany
    {
        return $this->hasMany(Category::class, 'parent_id');
    }
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
    public function taxes(): HasMany
    {
        return $this->hasMany(Tax::class);
    }

    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class);
    }

    public function supplies(): BelongsToMany
    {
        return $this->belongsToMany(Supplier::class);
    }
    public function attributes(): HasMany
    {
        return $this->hasMany(Attribute::class);
    }

    public  function scopeIsParent(Builder  $query): Builder
    {
        return  $query->whereNotNull('parent_id');
    }
    public  function scopeIsChild(Builder  $query): Builder
    {
        return  $query->whereNull('parent_id');
    }
}
