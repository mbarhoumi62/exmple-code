<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Tax extends Model
{
    use HasFactory;
    protected $table = 'taxes';
    protected $guarded = [];

    const YES_IS_ACTIVE = 'yes';
    const NO_IS_NOT_ACTIVE = 'no';


    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
    /**
     * Scope for active Tax
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeIsActive(Builder $query): Builder
    {
        return $query->where('status', self::YES_IS_ACTIVE);
    }

    /**
     * Scope for inactive Tax
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeIsNotActive(Builder $query): Builder
    {
        return  $query->where('status', self::NO_IS_NOT_ACTIVE);
    }
}
