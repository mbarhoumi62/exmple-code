<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model
{
    use HasFactory;

    /**
     * Table Name
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'shipping_date' => 'datetime',
        'approved_at' => 'datetime',
        'accepted_at' => 'datetime',

    ];

    const NORMAL_EMERGENCY = 'normal';
    const URGENT_EMERGENCY = 'urgent';

    const DRAFT_ACCEPTANCE_STATUS = 'draft';
    const WAITING_ACCEPTANCE_STATUS = 'waiting';
    const ACCEPTED_ACCEPTANCE_STATUS = 'accepted';
    const REFUSED_ACCEPTANCE_STATUS = 'refused';
    const NONE_ACCEPTANCE_STATUS = 'none';

    const WAITING_APPROVAL_STATUS = 'waiting_approved';
    const NOT_APPROVAL_STATUS = 'not_approved';
    const YES_APPROVAL_STATUS = 'approved';

    const PENDING_DELIVERY_STATUS = 'pending';
    const CONFIRMED_DELIVERY_STATUS = 'confirmed';
    const DELEVERED_DELIVERY_STATUS = 'delevered';
    const BILLED_DELIVERY_STATUS = 'billed';
    const NONE_DELIVERY_STATUS = 'none';
    const PARTIALY_DELEVRED_DELIVERY_STATUS = 'partially_delivered';

    const WAITING_PICK_UP_SHIPPING_STATUS = 'waiting_pick_up';
    const PICKED_SHIPPING_STATUS = 'picked';
    const WAITING_SHIPEMENT_SHIPPING_STATUS = 'waiting_shipment';
    const SHIIPED_SHIPPING_STATUS = 'shipped';
    const NONE_SHIPPING_STATUS = 'None';



    public function buyer(): BelongsTo
    {
        return  $this->belongsTo(Buyer::class, 'order_by');
    }

    public function approvedBy(): BelongsTo
    {
        return  $this->belongsTo(Buyer::class, 'approved_by');
    }

    public function admin(): BelongsTo
    {
        return  $this->belongsTo(Admin::class);
    }
    public function OrderDetails(): HasMany
    {
        return  $this->hasMany(OrderDetails::class);
    }
    public function address(): BelongsTo
    {
        return $this->belongsTo(Address::class);
    }

    public function IsAccepted(): bool
    {
        return $this->approved_at !== Null;
    }
    public function isApproved(): bool
    {
        return $this->accepted_at !== Null;
    }

    public function isNotApproved(): bool
    {
        return !$this->isApproved();
    }
    public function IsNotAccepted(): bool
    {
        return $this->approved_at !== Null;
    }
    /**
     * scope Order emergency level : Urgent
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderLevelUrgent(Builder $query): Builder
    {
        return $query->where('emergency_level', self::URGENT_EMERGENCY);
    }

    /**
     * scope Order emergency level : Normal
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderLevelNormal(Builder $query): Builder
    {
        return $query->where('emergency_level', self::NORMAL_EMERGENCY);
    }

    /**
     * order acceptance status : draft
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderAcceptanceDraft(Builder $query): Builder
    {
        return $query->where('acceptance_status', self::DRAFT_ACCEPTANCE_STATUS);
    }
    /**
     * order acceptance status : waiting
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderAcceptanceWaiting(Builder $query): Builder
    {
        return $query->where('acceptance_status', self::WAITING_ACCEPTANCE_STATUS);
    }
    /**
     * order acceptance status : accepted
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderAcceptanceAccepted(Builder $query): Builder
    {
        return $query->where('acceptance_status', self::ACCEPTED_ACCEPTANCE_STATUS);
    }
    /**
     * order acceptance status : refused
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderAcceptanceRefused(Builder $query): Builder
    {
        return $query->where('acceptance_status', self::REFUSED_ACCEPTANCE_STATUS);
    }
    /**
     * order acceptance status : None
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderAcceptanceNone(Builder $query): Builder
    {
        return $query->where('acceptance_status', self::NONE_ACCEPTANCE_STATUS);
    }

    /**
     * order approval status Waiting approved
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdereWaitingApproved(Builder $query): Builder
    {
        return $query->where('approval_status', self::WAITING_APPROVAL_STATUS);
    }

    /**
     * order approval status not approved
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderNotApproved(Builder $query): Builder
    {
        return $query->where('approval_status', self::NOT_APPROVAL_STATUS);
    }
    /**
     * order approval approved
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderApproved(Builder $query): Builder
    {
        return $query->where('approval_status', self::YES_APPROVAL_STATUS);
    }

    /**
     * Order delivery status : Pending
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderPending(Builder $query): Builder
    {
        return $query->where('delivery_status', self::PENDING_DELIVERY_STATUS);
    }
    /**
     * Order delivery status : Confirmed
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderConfirmed(Builder $query): Builder
    {
        return $query->where('delivery_status', self::CONFIRMED_DELIVERY_STATUS);
    }
    /**
     * Order delivery status : Delevred
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderDelevered(Builder $query): Builder
    {
        return $query->where('delivery_status', self::DELEVERED_DELIVERY_STATUS);
    }
    /**
     * Order delivery status : Billed
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderBilled(Builder $query): Builder
    {
        return $query->where('delivery_status', self::BILLED_DELIVERY_STATUS);
    }
    /**
     * Order delivery status : None
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderNoneDelivery(Builder $query): Builder
    {
        return $query->where('delivery_status', self::NONE_DELIVERY_STATUS);
    }
    /**
     * Order delivery status : Partially Delevred
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderPartialyDelivred(Builder $query): Builder
    {
        return $query->where('delivery_status', self::PARTIALY_DELEVRED_DELIVERY_STATUS);
    }

    /**
     * Order shipping status : Waiting Shipping Status
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeWaitingPickUoShipping(Builder $query): Builder
    {
        return $query->where('shipping_status', self::WAITING_PICK_UP_SHIPPING_STATUS);
    }
    /**
     * Order shipping status : Picked
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopePickedShipement(Builder $query): Builder
    {
        return $query->where('shipping_status', self::PICKED_SHIPPING_STATUS);
    }
    /**
     * Order shipping status : Waiting shipment
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeWaitingShipement(Builder $query): Builder
    {
        return $query->where('shipping_status', self::WAITING_SHIPEMENT_SHIPPING_STATUS);
    }
    /**
     * Order shipping status : shipped
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeShipped(Builder $query): Builder
    {
        return $query->where('shipping_status', self::SHIIPED_SHIPPING_STATUS);
    }
    /**
     * Order shipping status : shipped
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeNoneShipping(Builder $query): Builder
    {
        return $query->where('shipping_status', self::NONE_SHIPPING_STATUS);
    }
}
