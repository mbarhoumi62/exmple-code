<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SupplierBranch extends Model
{
    use HasFactory;
    /**
     * Table Name
     *
     * @var string
     */
    protected $table = 'supplier_branches';

    protected $guarded = [];

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }
    public function AdressTypes(): BelongsTo
    {
        return $this->belongsTo(AddressType::class);
    }

    public function addresses(): BelongsTo
    {
        return $this->belongsTo(Address::class);
    }
}
