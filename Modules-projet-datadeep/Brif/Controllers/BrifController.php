<?php

namespace App\Modules\Brif\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Brif\Models\General;


use Illuminate\Support\Facades\Input;

class BrifController extends Controller
{


  public function handleAddBriefClient()
  {


    $data = Input::All();


    $business = General::create([
           'nom' => $data['nom'] ?? null,
           'address' => $data['address']?? null,
           'email' => $data['email']?? null,
           'phone' => $data['phone']?? null,
           'fax' => $data['fax']?? null,
           'activity'=> $data['activity']?? null,
         ]);

           // create personal
    $business->personal()->create([
      'nom' => $data['contact-nom'] ?? null,
      'position' => $data['contact-position'] ?? null,
      'email' => $data['contact-email'] ?? null,
      'phone' => $data['contact-phone'] ?? null,
    ]);


          // create portfolio
    $business->portfolio()->create([
        'service' => $data['service'] ?? null,
        'va' => $data['va'] ?? null,
        'values' => $data['values'] ?? null,
        'audience' => $data['audience'] ?? null,
        'market' => $data['market'] ?? null,
    ]);

    // Community Management
    $business->management()->create([
      'existent_accounts'    =>  isset($data['existent_accounts']) ? serialize($data['existent_accounts']) : null,
      'needed_accounts'      =>  isset($data['needed_accounts']) ? serialize($data['needed_accounts']) : null,
      'other_accounts'       =>  $data['other_accounts'] ?? null,
      'max_posts'            =>  $data['max_posts'] ?? null,
      'post_types'           =>  isset($data['post_types']) ? serialize($data['post_types']) : null,
      'other_types'          =>  $data['other_types'] ?? null,
      'preferred_supports'   =>  isset($data['preferred_supports']) ? serialize($data['preferred_supports']) : null,
      'sponsored'            =>  $data['sponsored'] ?? null,
      'sponsor_goal'         =>  $data['sponsor_goal'] ?? null,
      'contract_delay'       =>  $data['contract_delay'] ?? null,
      'reporting_delay'      =>  $data['reporting_delay'] ?? null ,
    ]);


    //  Shooting

    $business->shooting()->create([
      'type' => isset($data['typeImg']) ? serialize($data['typeImg']) : null,
      'photo_types' => isset($data['photo_types']) ? serialize($data['photo_types']) : null,
      'categories' => isset($data['categories']) ? serialize($data['categories']) : null,
      'number' => $data['number'] ?? null,
      'theme' => $data['theme'] ?? null,
      'length' => $data['length'] ?? null,
    ]);

      // Website Developement

      $business->development()->create([
        'have'   => $data['have'] ?? null,
        'create'   => $data['create'] ?? null,
        'moderate'   => $data['moderate'] ?? null,
        'url'   => $data['url'] ?? null,
        'techs'   => $data['techs'] ?? null,
        'type'   => isset($data['typeSite']) ? serialize($data['typeSite']) : null,
        'domain'   => $data['domain'] ?? null,
        'hosting'   => $data['hosting'] ?? null,
        'domain_name'   => $data['domain_name'] ?? null,
        'hosting_name'   => $data['hosting_name'] ?? null,
        'pages'   => isset($data['pages']) ?  serialize($data['pages']) : null,
        'languages'   => isset($data['languages']) ? serialize($data['languages']) : null,
        'content'   => $data['content'] ?? null,
        'content_type'   => isset($data['content_type']) ? serialize($data['content_type']) : null,
        'theme'  => isset($data['themeSite']) ? serialize($data['themeSite']) : null,
        'other_languages' => $data['other_languages'] ?? null,
        'other_site_type' => $data['other_site_type'] ?? null,
        'other_pages' => $data['other_pages'] ?? null,
      ]);

        // graphic

        $business->graphic()->create([
            'logo' => $data['logo'] ?? null,
            'cards' => $data['cards'] ?? null,
            'flyers' => $data['flyers'] ?? null,
            'catalogue' => $data['catalogue'] ?? null,
            'chart' => $data['chart'] ?? null,
            'ppt' => $data['ppt'] ?? null,
            'affiche' => $data['affiche'] ?? null,
        ]);



          return view('General::resume',[
            'business' => $business
          ]);

  }




}
