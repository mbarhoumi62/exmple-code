<?php

namespace App\Modules\Brif\Models;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'approvals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start',
        'end',
        'approved_by',
        'business_id'
    ];

    protected $dates = [
        'start', 'end'
    ];

    public function business() {

        return $this->belongsTo('App\Modules\Brif\Models\General');
    }

}
