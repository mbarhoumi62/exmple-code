<?php

namespace App\Modules\Brif\Models;

use Illuminate\Database\Eloquent\Model;

class Graphic extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'graphics';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'logo',
        'cards',
        'flyers',
        'catalogue',
        'chart',
        'ppt',
        'affiche',
        'business_id'
    ];



    public function business() {

        return $this->belongsTo('App\Modules\Brif\Models\General');
    }


        public function getLogoAttribute($value)
        {
            $answer = '';
            if ($value == 1) {
                $answer = 'Oui';
            }else if($value == 2) {
                  $answer = 'Peut être';
            }else {
              $answer = 'Nom';
            }
            return $answer;
        }

        public function getCardsAttribute($value)
        {
            $answer = '';
            if ($value == 1) {
                $answer = 'Oui';
            }else if($value == 2) {
                  $answer = 'Peut être';
            }else {
              $answer = 'Nom';
            }
            return $answer;
        }


      public function getFlyersAttribute($value)
      {
          $answer = '';
          if ($value == 1) {
              $answer = 'Oui';
          }else if($value == 2) {
                $answer = 'Peut être';
          }else {
            $answer = 'Nom';
          }
          return $answer;
      }

      public function getChartAttribute($value)
      {
          $answer = '';
          if ($value == 1) {
              $answer = 'Oui';
          }else if($value == 2) {
                $answer = 'Peut être';
          }else {
            $answer = 'Nom';
          }
          return $answer;
      }

      public function getPptAttribute($value)
      {
          $answer = '';
          if ($value == 1) {
              $answer = 'Oui';
          }else if($value == 2) {
                $answer = 'Peut être';
          }else {
            $answer = 'Nom';
          }
          return $answer;
      }

      public function getCatalogueAttribute($value)
      {
          $answer = '';
          if ($value == 1) {
              $answer = 'Oui';
          }else if($value == 2) {
                $answer = 'Peut être';
          }else {
            $answer = 'Nom';
          }
          return $answer;
      }
}
