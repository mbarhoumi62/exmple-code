<?php

namespace App\Modules\Brif\Models;

use Illuminate\Database\Eloquent\Model;

class Management extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'managements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'existent_accounts',
        'needed_accounts',
        'other_accounts',
        'max_posts',
        'post_types',
        'other_types',
        'preferred_supports',
        'sponsored',
        'sponsored_supports',
        'sponsor_goal',
        'contract_delay',
        'reporting_delay',
        'business_id'
    ];



    public function business() {

        return $this->belongsTo('App\Modules\Brif\Models\General');
    }

    public function getSponsoredAttribute($value)
    {


        $answer = '';
        if ($value == 1) {
            $answer = 'Oui';
        }else if($value == 2) {
              $answer = 'Peut être';
        }else {
          $answer = 'Nom';
        }
        return $answer;
    }

}
