<?php

namespace App\Modules\Brif\Models;

use Illuminate\Database\Eloquent\Model;

class General extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'general_infos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref',
        'nom',
        'address',
        'email',
        'phone',
        'fax',
        'activity'
    ];



    public function apporval() {

        return $this->hasOne('App\Modules\Brif\Models\Approval','business_id');
    }


    public function development() {

        return $this->hasOne('App\Modules\Brif\Models\Development','business_id');
    }

    public function graphic() {

        return $this->hasOne('App\Modules\Brif\Models\Graphic','business_id');
    }

    public function management() {

        return $this->hasOne('App\Modules\Brif\Models\Management','business_id');
    }

    public function personal() {

        return $this->hasOne('App\Modules\Brif\Models\Personal','business_id');
    }

    public function portfolio() {

        return $this->hasOne('App\Modules\Brif\Models\Portfolio','business_id');
    }

    public function shooting() {

        return $this->hasOne('App\Modules\Brif\Models\Shooting','business_id');
    }



}
