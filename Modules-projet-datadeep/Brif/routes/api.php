<?php

Route::group(['module' => 'Brif', 'middleware' => ['api'], 'namespace' => 'App\Modules\Brif\Controllers'], function() {

    Route::resource('Brif', 'BrifController');

});
