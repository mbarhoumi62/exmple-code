<?php

namespace App\Modules\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function articles()
    {
        return $this->belongsToMany(
            'App\Modules\Blog\Models\Article',
            'article_tags',
            'tag_id',
            'article_id'
        )->withTimestamps();
    }
}
