<?php

namespace App\Modules\Blog\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'image',
        'email',
        'status',
        'type',
        'password',
        'validation',
        'provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            'App\Modules\Blog\Models\Role',
            'user_roles',
            'user_id',
            'role_id'
        )->withTimestamps();
    }

    public function assignRole($role)
    {
        if (is_string($role)) {
            $role = Role::where('title', $role)->get();
        }
        if (!$role) return false;
        $this->roles()->attach($role);
    }

    public function articels(){
        return  $this->hasMany('App\Modules\Blog\Models\Article');
    }

    public function comments(){
        return  $this->hasMany('App\Modules\Blog\Models\Comment');
    }


    public function clicks(){
        return  $this->hasMany('App\Modules\Blog\Models\Click');
    }



}
