<?php

namespace App\Modules\Blog\Controllers;

use App\Modules\Blog\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Blog\Models\Role;
use App\Modules\Blog\Models\User;
use App\Modules\Blog\Models\Article;
use App\Modules\Blog\Models\Category;
use App\Modules\Blog\Models\Click;
use App\Modules\Blog\Models\Comment;
use App\Modules\General\Models\Student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Analytics;
use Spatie\Analytics\Period;
use Image;

class BlogController extends Controller
{


    public function showDashboard(){


      $browsers = Analytics::fetchTopBrowsers(Period::days(30));

      $country = Analytics::performQuery(Period::days(30),'ga:sessions',  ['dimensions'=>'ga:country,ga:latitude,ga:longitude','sort'=>'-ga:sessions']);

      $newUsers = Analytics::fetchUserTypes(Period::days(7));

      $userLanguage = Analytics::performQuery(Period::years(1),'ga:users',  ['dimensions'=>'ga:language']);

      $users = Analytics::performQuery(Period::years(1),'ga:users');

      $usersTypeMonthly = Analytics::performQuery(Period::days(30),'ga:users', ['dimensions'=>'ga:userType']);

      $deviceCategory = Analytics::performQuery(Period::years(1),'ga:users',  ['dimensions'=>'ga:deviceCategory']);

      $sources = Analytics::performQuery(Period::years(1),'ga:users,ga:sessions,ga:bounceRate,ga:avgSessionDuration',  ['dimensions'=>'ga:fullReferrer']);

      $countries= collect($country['rows'] ?? [])->map(function (array $dateRow) {
          return [
              'country'    =>  $dateRow[0],
              'latitude'   =>(int) $dateRow[1],
              'longitude'  =>(int) $dateRow[2],
              'sessions'   => (int) $dateRow[3],
          ];
      });

      $totalUsers = collect($users['rows'] ?? [])->map(function (array $dateRow) {
          return [
              'total' =>  $dateRow[0]
          ];
      });

      $usersType= collect($usersTypeMonthly['rows'] ?? [])->map(function (array $dateRow) use($totalUsers)  {
          return [
              'visitorType'       =>  $dateRow[0] ,
              'nombres'           =>  (int) $dateRow[1],
              'percentage'        =>  (int) $dateRow[1] * 100 /  $totalUsers['0']['total'],
          ];
      });

      $userLanguages  = collect($userLanguage['rows'] ?? [])->map(function (array $dateRow ) use ($totalUsers) {

          return [
            'language'   =>  $dateRow[0],
            'users'      =>  $dateRow[1],
            'percentage' => ($dateRow[1] * 100) / $totalUsers['0']['total']
          ];
      });

      $userDeviceCategory= collect($deviceCategory['rows'] ?? [])->map(function (array $dateRow ) use ($totalUsers) {

          return [
            'devices'      =>  $dateRow[0],
            'users'        =>  $dateRow[1],
            'percentage'   => ($dateRow[1] * 100) / $totalUsers['0']['total']
          ];
      });

      $traffics = collect($sources['rows'] ?? [])->map(function (array $dateRow ) {

          return [
            'referrer'           => $dateRow[0],
            'users'              => $dateRow[1],
            'sessions'           => $dateRow[2],
            'bounceRate'         => $dateRow[3],
            'avgSessionDuration' => $dateRow[4],
          ];
      });


      $activeVisitors    = Analytics::getAnalyticsService()->data_realtime->get('ga:'.env('ANALYTICS_VIEW_ID'), 'rt:activeVisitors')->totalsForAllResults['rt:activeVisitors'];

      $visitorPagesViews = Analytics::fetchTotalVisitorsAndPageViews(Period::days(14));

      $dates      = $visitorPagesViews->pluck('date');

      $visitors   = $visitorPagesViews->pluck('visitors');

      $pageViews  = $visitorPagesViews->pluck('pageViews');


        return view('Blog::backOffice.analytics.analytics',[
          'activeVisitors'          => $activeVisitors,
          'countries'               => $countries,
          'userLanguages'           => $userLanguages,
          'dates'                   => $dates,
          'visitors'                => $visitors,
          'pageViews'               => $pageViews,
          'userDeviceCategory'      => $userDeviceCategory,
          'traffics'                => $traffics,
          'browsers'                => $browsers,
          'usersType'               => $usersType,
          'pageviews'               => Analytics::performQuery(Period::days(30),'ga:pageviews')['rows'][0][0]
        ]);
    }



    public function getActiveUser()
    {
              $activeVisitors = Analytics::getAnalyticsService()->data_realtime->get('ga:'.env('ANALYTICS_VIEW_ID'), 'rt:activeVisitors')->totalsForAllResults['rt:activeVisitors'];

             return response()->json($activeVisitors);
    }

    public function showDashboardLogin(){

        return view('Blog::backOffice.login');
    }

    public function handleDashboardLogin(){

        $data = Input::all();

        $credentials = [
            'email' => $data['email'],
            'password' => $data['password'],
        ];


        if (Auth::attempt($credentials)) {
            $user= Auth::user();

            if ($user->status === 0) {

                Auth::logout();
                Session::flash('message', 'Account not activated !');
                Session::flash('alert-class', 'alert-danger');
                return back();

            } elseif ($user->status === 1) {

                Auth::login($user);
                return redirect()->route('showDashboard');

            } else {

                Auth::logout();
                Session::flash('message', 'Account banned !');
                Session::flash('alert-class', 'alert-danger');
                return back();
            }
            }

            Session::flash('message', 'Please verify your credenetials !');
            Session::flash('alert-class', 'alert-danger');
            return back();
    }

    public function handleDashboardLogout()
    {
        Session::flush();
        Auth::logout();
        // return redirect(route('showDashboardLogin'));
        return back();
    }

    public function showUsers(){

        return view('Blog::backOffice.users.index',[
            'registred' => User::where('status',1)->get(),
            'unverified' => User::where('status',0)->get(),
            'banned' => User::where('status',2)->get()
        ]);
    }

    public function showUser($id){

        $user = User::find($id);

        if(!$user){
            Session::flash('message','User not found !');
            Session::flash('alert-class', 'alert-warning');
            return back();
        }

        return view('Blog::backOffice.users.show',[
            'user' => $user
        ]);
    }

    public function handleBanUser($id){
        $user = User::find($id);

        if(!$user){
            Session::flash('message','User not found !');
            Session::flash('alert-class', 'alert-warning');
            return back();
        }

        $user->update([
            'status' => 2
        ]);
        Session::flash('message','User'.$user->first_name .' '.$user->last_name.' is now banned !');
        Session::flash('alert-class', 'alert-success');
        return back();

    }

    public function handleUnbanUser($id){
        $user = User::find($id);

        if(!$user){
            Session::flash('message','User not found !');
            Session::flash('alert-class', 'alert-warning');
            return back();
        }

        $user->update([
            'status' => 1
        ]);
        Session::flash('message','User'.$user->first_name .' '.$user->last_name.' is now unbanned !');
        Session::flash('alert-class', 'alert-success');
        return back();
    }

    public function showArticles()
    {
        return view('Blog::backOffice.articles.index',[
            'articles' => Article::all()
        ]);
    }

    public function showAddArticle(){

        return view('Blog::backOffice.articles.add',[
            'tags' =>  Tag::pluck('name')->toArray(),
            'categories' => Category::all()
        ]);
    }

    public function handleAddArticle(){
        $data = Input::all();

        //TODO validation

        $content = $data['content'];

        $dom = new \domdocument();
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');

        foreach($images as $k => $img){

            $dataImg = $img->getattribute('src');

            list($type, $dataImg) = explode(';', $dataImg);
            list(, $dataImg)      = explode(',', $dataImg);

            $dataImg = base64_decode($dataImg);
            $image_name= time().$k.'.png';
            $path = public_path('storage/articles/content') .'/'. $image_name;

            file_put_contents($path, $dataImg);

            $img->removeattribute('src');
            $img->setattribute('src', url('/storage/articles/content').'/'.$image_name);
        }

        $content = $dom->savehtml();

        $file = $data['image'];
        $imagePath = 'storage/articles/images/';
        $filename = 'articleImage' . Auth::user()->id . '.' . time() . $file->getClientOriginalExtension();
        $file->move(public_path($imagePath), $filename);

        $article = Article::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'content' => $content,
            'image' => $imagePath . '' . $filename,
            'status' => ($data['status'] || $data['status'] != 'off') ? 1 : 0,
            'user_id' => Auth::id(),
            'category_id' => $data['category']
        ]);

        $tags = explode(',', $data['tags']);

        foreach ($tags as $tag) {

        $article->addTag($tag);

        }

        return redirect()->route('showArticles');


    }

    public function showEditArticle($id) {

        $article = Article::find($id);
        //TODO test if not exist
        return view('Blog::backOffice.articles.edit',[
            'article' => $article,
            'tags' => Tag::pluck('name')->toArray(),
            'categories' => Category::all()
        ]);
    }

    public function handleEditArticle($id){

         $data = Input::all();

        //TODO validation

        $content = $data['content'];

        $dom = new \domdocument();
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');

        foreach($images as $k => $img){
            $dataImg = $img->getattribute('src');

            list($type, $dataImg) = explode(';', $dataImg);
            list(, $dataImg)      = explode(',', $dataImg);

            $dataImg = base64_decode($dataImg);
            $image_name= time().$k.'.png';
            $path = public_path('storage/articles/content') .'/'. $image_name;

            file_put_contents($path, $dataImg);

            $img->removeattribute('src');
            $img->setattribute('src', url('/storage/articles/content').'/'.$image_name);
        }

        $content = $dom->savehtml();

        $file = $data['image'];
        $imagePath = 'storage/articles/images/';
        $filename = 'articleImage' . Auth::user()->id . '.' . time() . $file->getClientOriginalExtension();
        $file->move(public_path($imagePath), $filename);

        $article =  Article::find($id);
        //TODO check if not exist

        $article->update([
            'title' => $data['title'],
            'description' => $data['description'],
            'content' => $content,
            'image' => $imagePath . '' . $filename,
            'status' => ($data['status'] || $data['status'] != 'off') ? 1 : 0,
            'user_id' => Auth::id(),
            'category_id' => $data['category']
        ]);

        $tags = explode(',', $data['tags']);

        foreach ($tags as $tag) {

        $article->addTag($tag);

        }

        return redirect()->route('showArticles');

    }

    public function showArticle($articelId) {

        $id = explode('_',$articel);

        if(count($id) <2){
           //not found
            return back();
        }

        $article = Forum::find($id[1]);

        if(!$article){
            //notfound
            return back();
        }

        return view('General::details',[
            'article' => $article
        ]);
    }




    public function showManagementStudentsInscription() {
      return view('Blog::backOffice.managementStudentsInscription',[
          'studentsDev' => Student::where([
            ['status', '=', 0],
            ['formation_type', '=', 1]
          ])->get(),
          'studentsDevAccepted' => Student::where([
            ['status', '=', 1],
            ['formation_type', '=', 1]
          ])->get(),
          'studentsDevRefused' => Student::where([
            ['status', '=', 0],
            ['formation_type', '=', 2]
          ])->get(),
          'studentsMarketing' => Student::where([
            ['status', '=', 0],
            ['formation_type', '=', 0]
          ])->get(),

      ]);
    }

    public function apiHundleManagmentStudent(Request $request) {

      $studentId  = $request->get('studentId');
      $status     = $request->get('status');
      $student = Student::find($studentId);

      if (!$student) {
          return back(); //Some thing wont wrong
      }

      $student->status = $status;

      $student->save();

      if ($status == 1) {
          // TODO: Send Accepted Email
          return response()->json(['status',200]); //Some thing wont wrong
      }else {
        // TODO: Send Refuse Email
        return response()->json(['status',200]); //Some thing wont wrong
      }

        return back();

    }




}
