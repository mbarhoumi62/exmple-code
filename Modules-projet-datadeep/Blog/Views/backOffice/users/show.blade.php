@extends('backOffice.layout')
@section('content')

    <div class="row mailbox-wrapper">
        <div class="col-md-4">

            <div class="panel-layout">
                <div class="panel-box">

                    <div class="panel-content image-box">
                        {{--<div class="ribbon">--}}
                            {{--<div class="bg-primary">Ribbon</div>--}}
                        {{--</div>--}}
                        <div class="image-content font-white">

                            <div class="meta-box meta-box-bottom">
                                <img src="" alt="" class="meta-image img-bordered img-circle">
                                <h3 class="meta-heading">{{$user->first_name}}</h3>
                                <h4 class="meta-subheading">{{$user->email}}</h4>
                            </div>

                        </div>
                        <img width="100%" src="{{asset('backOffice/images/unknown.png')}}" alt="">

                    </div>
                    <div class="panel-content pad15A bg-white radius-bottom-all-4">
<p>Registred with</p>
                        <div class="clear profile-box">
                            <ul class="nav nav-pills nav-justified">

                                @if(!$user->provider)
                                <li>
                                    <a class="btn btn-sm bg-google">
                                <span class="glyph-icon icon-separator">
                                    <i class="glyph-icon icon-linecons-mail"></i>
                                </span>
                                        <span class="button-content">
                                    Email
                                </span>
                                    </a>
                                </li>
                                @else
                                <li>
                                    <a class="btn btn-sm bg-facebook">
                                <span class="glyph-icon icon-separator">
                                    <i class="glyph-icon icon-facebook"></i>
                                </span>
                                        <span class="button-content">
                                    Facebook
                                </span>
                                    </a>
                                </li>
                                    @endif

                            </ul>
                        </div>
                        <div class="mrg15T mrg15B"></div>
                        <p   class="font-gray">
<h5>Account Type</h5>                            <h2    >
                              {{($user->type == 0)? 'Individual' : 'Entreprise'}}
                            </h2>
                        </p>
                    </div>
                </div>
            </div>



        </div>
        <div class="col-md-8">



            <div class="example-box-wrapper">
                <ul class="list-group row list-group-icons">
                    <li class="col-md-4 active">
                        <a href="#tab-example-4" data-toggle="tab" class="list-group-item">
                            <i class="glyph-icon font-red icon-bullhorn"></i>
                            Personal Information
                        </a>
                    </li>
                    <li class="col-md-4">
                        <a href="#tab-example-1" data-toggle="tab" class="list-group-item">
                            <i class="glyph-icon icon-dashboard"></i>
                            Visited Articles
                        </a>
                    </li>
                    <li class="col-md-4">
                        <a href="#tab-example-2" data-toggle="tab" class="list-group-item">
                            <i class="glyph-icon font-primary icon-comment"></i>
                            Comments
                        </a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade" id="tab-example-1">
                        <div class="panel">
                            <div class="panel-body">
                                <ul class="todo-box">

                                @if(count($user->clicks) == 0)
                                    <h3>This user visited no article yet !</h3>
                                
                                @endif
                                   
                                   @foreach($user->clicks->groupBy('article_id') as $click)
                                    <li class="border-red">
                                        <label for="todo-1">{{$click->first()->article->title}}</label>
                                        <span class="bs-label bg-red  float-right" title="Times">{{count($click)}}</span>
                                   
                                    </li>
                                    @endforeach
   
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="tab-example-2">
                        <div class="content-box mrg15B">
                            <h3 class="content-box-header clearfix">
                                Recent Comments
                                <div class="font-size-11 float-right">
                                    <a href="#" title="">
                                        <i class="glyph-icon mrg5R opacity-hover icon-plus"></i>
                                    </a>
                                    <a href="#" title="">
                                        <i class="glyph-icon opacity-hover icon-cog"></i>
                                    </a>
                                </div>
                            </h3>

                            <div class="content-box-wrapper text-center clearfix">
                                <div class="timeline-box timeline-box-right">
                                @if(count($user->comments) == 0)
                                    <h3>This user has no comments yet !</h3>
                                    @endif
                                  @foreach($user->comments as $comment)
                                    <div class="tl-row">
                                        <div class="tl-item">
                                            <div class="tl-icon bg-yellow">
                                                <i class="glyph-icon icon-comment"></i>
                                            </div>
                                            <div class="popover left">
                                                <div class="arrow"></div>
                                                <div class="popover-content">
                                                    <div class="tl-label bs-label label-success">{{$comment->article->title}}</div>
                                                    <p class="tl-content">{{$comment->content}}</p>
                                                    <div class="tl-time">
                                                        <i class="glyph-icon icon-clock-o"></i>
                                                        {{$comment->created_at->diffForHumans()}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                      @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-example-3">
                        <div class="row">
                            <div class="col-md-3">
                                <ul class="list-group">
                                    <li class="mrg10B">
                                        <a href="#faq-tab-1" data-toggle="tab" class="list-group-item">
                                            How to get paid
                                            <i class="glyph-icon icon-angle-right mrg0A"></i>
                                        </a>
                                    </li>
                                    <li class="mrg10B">
                                        <a href="#faq-tab-2" data-toggle="tab" class="list-group-item">
                                            ThemeForest related
                                            <i class="glyph-icon font-green icon-angle-right mrg0A"></i>
                                        </a>
                                    </li>
                                    <li class="mrg10B">
                                        <a href="#faq-tab-3" data-toggle="tab" class="list-group-item">
                                            Common questions
                                            <i class="glyph-icon icon-angle-right mrg0A"></i>
                                        </a>
                                    </li>
                                    <li class="mrg10B">
                                        <a href="#faq-tab-4" data-toggle="tab" class="list-group-item">
                                            Terms of service
                                            <i class="glyph-icon icon-angle-right mrg0A"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in pad0A" id="faq-tab-1">
                                        <div class="panel-group" id="accordion5">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion5" href="#collapseOne">
                                                            Collapsible Group Item #1
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion5" href="#collapseTwo">
                                                            Collapsible Group Item #2
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion5" href="#collapseThree">
                                                            Collapsible Group Item #3
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade pad0A" id="faq-tab-2">
                                        <div class="panel-group" id="accordion1">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
                                                            Collapsible Group Item #1
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne1" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1">
                                                            Collapsible Group Item #2
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo1" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseThree1">
                                                            Collapsible Group Item #3
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree1" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade pad0A" id="faq-tab-3">
                                        <div class="panel-group" id="accordion2">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2">
                                                            Collapsible Group Item #1 
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne2" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2">
                                                            Collapsible Group Item #2
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseThree2">
                                                            Collapsible Group Item #3
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree2" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade pad0A" id="faq-tab-4">
                                        <div class="panel-group" id="accordion3">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne4">
                                                            Collapsible Group Item #1
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne4" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo4">
                                                            Collapsible Group Item #2
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo4" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree4">
                                                            Collapsible Group Item #3
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree4" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane pad0A fade active in" id="tab-example-4">
                        <div class="content-box">
                            <form class="form-horizontal pad15L pad15R bordered-row">
                                <div class="form-group remove-border">
                                    <label class="col-sm-3 control-label">First Name:</label>
                                    <div class="col-sm-6">
                                        <h2>{{$user->first_name}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Last Name:</label>
                                    <div class="col-sm-6">
<h2>                                        {{$user->last_name}}
</h2>                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email:</label>
                                    <div class="col-sm-6">
                                        <h2>{{$user->email}}</h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Phone:</label>
                                    <div class="col-sm-6">
                                        <h2>{{$user->phone}} 54 008 911</h2>
                                    </div>
                                </div>


                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection