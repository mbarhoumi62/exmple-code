@extends('backOffice.layout')
@section('content')

            <div class="container">


                <script type="text/javascript">
                    /* Datatables basic */

                    $(document).ready(function() {
                        $('#datatable-example').dataTable();
                    });

                    /* Datatables hide columns */

                    $(document).ready(function() {
                        var table = $('#datatable-hide-columns').DataTable( {
                            "scrollY": "300px",
                            "paging": false
                        } );

                        $('#datatable-hide-columns_filter').hide();

                        $('a.toggle-vis').on( 'click', function (e) {
                            e.preventDefault();

                            // Get the column API object
                            var column = table.column( $(this).attr('data-column') );

                            // Toggle the visibility
                            column.visible( ! column.visible() );
                        } );
                    } );

                    $(document).ready(function() {
                        $('.dataTables_filter input').attr("placeholder", "Search...");
                    });

                </script>

                <div id="page-title">
                    <h2>Users Management</h2>
                    <br>
                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif
                    <a class="btn btn-default" data-toggle="modal" data-target="#bannedUsers" title="Details">Show Banned Users
                    </a>
                </div>

     <div class="row">
         <div class="col-md-7">
             <div class="panel">
                 <div class="panel-body">
                     <h3 class="title-hero">
                         Registred Users
                     </h3>
                     <div class="example-box-wrapper">

                         <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
                             <thead>
                             <tr>
                                 <th>Name</th>
                                 <th>Email</th>
                                 <th>Actions</th>

                             </tr>
                             </thead>
                             <tbody>
                             @foreach($registred as $user)
                                 <tr>
                                     <td>{{$user->first_name.' '.$user->last_name}}</td>
                                     <td>{{$user->email}}</td>
                                     <td>    <a class="btn btn-default" href="{{ route('showUser',$user->id) }}" title="Details">
                                             <i class="glyph-icon icon-eye"></i>
                                         </a>
                                         <a class="btn btn-danger" href="{{ route('handleBanUser',$user->id) }}" title="Ban User">
                                             <i class="glyph-icon icon-close"></i>
                                         </a></td>

                                 </tr>
                             @endforeach

                             </tbody>
                         </table>
                     </div></div></div>

         </div>
         <div class="col-md-5">
             <div class="row">
                 <div class="panel">
                     <div class="panel-body">
                         <h3 class="title-hero">
                             Unactivated Accounts
                         </h3>
                         <div class="example-box-wrapper">

                             <table id="datatable-hide-columns" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                 <thead>
                                 <tr>
                                     <th>Email</th>
                                     <th>Actions</th>

                                 </tr>
                                 </thead>

                                 <tfoot>
                                 <tr>
                                     <th>Email</th>
                                     <th>Actions</th>

                                 </tr>
                                 </tfoot>

                                 <tbody>
                                 @foreach($unverified as $user)
                                 <tr>
                                     <td>{{$user->email}}</td>
                                     <td>    <a class="btn btn-default" href="{{ route('showUser',$user->id) }}" title="Details">
                                             <i class="glyph-icon icon-eye"></i>
                                         </a>
                                         </td>

                                 </tr>
                                 @endforeach
                                 </tbody>
                             </table>
                         </div>
                     </div>
                 </div>


             </div>



         </div>
         </div>
     </div>

            </div>


@include('Blog::backOffice.users.Modals.bannedUsers')

@endsection
