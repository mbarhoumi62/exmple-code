@extends('backOffice.layout')
@section('content')

    <div class="container">

        <script type="text/javascript">

            /* Datatables basic */

            $(document).ready(function() {
                $('#datatable-example').dataTable();
            });

            /* Datatables hide columns */

            $(document).ready(function() {
                var table = $('#datatable-hide-columns').DataTable( {
                    "scrollY": "300px",
                    "paging": false
                } );

                $('#datatable-hide-columns_filter').hide();

                $('a.toggle-vis').on( 'click', function (e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column( $(this).attr('data-column') );

                    // Toggle the visibility
                    column.visible( ! column.visible() );
                } );
            } );

            /* Datatable row highlight */

            $(document).ready(function() {
                var table = $('#datatable-row-highlight').DataTable();

                $('#datatable-row-highlight tbody').on( 'click', 'tr', function () {
                    $(this).toggleClass('tr-selected');
                } );
            });



            $(document).ready(function() {
                $('.dataTables_filter input').attr("placeholder", "Search...");
            });

        </script>
    <div id="page-title">
        <h2>Articles Management</h2>
        <br>
        @if(Session::has('message'))
            <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ Session::get('message') }}</strong>
            </div>
        @endif
        <a class="btn btn-default"  title="add new user">Add New Article
        </a>
    </div>
    <div class="panel">
        <div class="panel-body">

            <div class="example-box-wrapper">

                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datatable-example">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Publisher</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $article)
                    <tr class="odd gradeX">
                        <td>{{$article->title}}</td>
                        <td>{{$article->description}}</td>
                        <td><span style="cursor: pointer" data-id="{{$article->id}}" @if($article->status == 0)  class="badge badge-danger status" @else  class="badge badge-success status"@endif>{{($article->status == 0)? 'Unpublished' : 'Publiched'}}</span>
                        </td>
                        <td class="center"> {{$article->author->first_name}}</td>
                        <td style="width: 95px;" class="center">
                            <a class="btn btn-primary" href="" title="Edit">
                                <i class="glyph-icon icon-pencil"></i>
                            </a>
                            <a class="btn btn-danger" href="" title="Delete">
                                <i class="glyph-icon icon-trash"></i>
                            </a>
                        </td>
                    </tr>
                        @endforeach

                    </tbody>
                </table>
            </div></div>
    </div>
    </div>
    <script>
        $('.status').on('click', function () {
            alert($(this).data('id'));
        })
    </script>
@endsection