
<style media="screen">
#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset .form-card {
    background: white;
    border: 0 none;
    border-radius: 0px;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    padding: 20px 40px 30px 40px;
    box-sizing: border-box;
    width: 94%;
    margin: 0 3% 20px 3%;
    position: relative
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform fieldset .form-card {
    text-align: left;
    color: #9E9E9E
}

#msform input,
#msform textarea {
    padding: 0px 8px 4px 8px;
    border: none;
    border-bottom: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: none;
    font-weight: bold;
    border-bottom: 2px solid rgba(255, 214, 0, 1);;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: rgba(255, 214, 0, 1);;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
}

#msform .action-button:hover,
#msform .action-button:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px rgba(255, 214, 0, 1);
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px #616161
}

select.list-dt {
    border: none;
    outline: 0;
    border-bottom: 1px solid #ccc;
    padding: 2px 5px 3px 5px;
    margin: 2px
}

select.list-dt:focus {
    border-bottom: 2px solid rgba(255, 214, 0, 1);
}

.card {
    z-index: 0;
    border: none;
    border-radius: 0.5rem;
    position: relative
}

.fs-title {
    font-size: 19px;
    color: #2C3E50;
    margin-bottom: 10px;
    font-weight: bold;
    text-align: left
}

#progressbar {
    padding-left: 70px;
    margin-bottom: 30px;
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #000000
}

#progressbar li {
    list-style-type: none;
    font-size: 12px;
    width: 25%;
    float: left;
    position: relative
}

#progressbar #account:before {
    font-family: FontAwesome;
    content: "\f023"
}

#progressbar #personal:before {
    font-family: FontAwesome;
    content: "\f007"
}

#progressbar #payment:before {
    font-family: FontAwesome;
    content: "\f09d"
}

#progressbar #confirm:before {
    font-family: FontAwesome;
    content: "\f00c"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 18px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '' !important;
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: rgba(255, 214, 0, 1);
}

.radio-group {
    position: relative;
    margin-bottom: 25px
}

.radio {
    display: inline-block;
    width: 204;
    height: 104;
    border-radius: 0;
    background: lightblue;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    box-sizing: border-box;
    cursor: pointer;
    margin: 8px 2px
}

.radio:hover {
    box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
}

.radio.selected {
    box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
}

.fit-image {
    width: 100%;
    object-fit: cover
}

.errors{
  margin-top: -24px;
    font-size: 10px;
    color: red;
}

</style>
<div class="modal fade" id="myModalIntegration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-smll" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgba(255, 214, 0, 0.7);color: black;">
                <h2 class="modal-title" id="myModalLabel" style="margin: 0 auto;">Subscribe.</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>


            </div>
            <div class="modal-body">
              <div class="row align-items-center justify-content-center">
                  <div class="col-12 col-md-12 col-lg-12">

                      <div class="form-container form-container-transparent form-container-white" >
                          <div class="form-desc  white-section">

                          </div>
                          <form id="msform">
                              <!-- progressbar -->
                              <ul id="progressbar">
                                  <li class="active" id="account"><strong>Courses</strong></li>
                                  <li id="personal"><strong>Personal</strong></li>

                                  <li id="confirm"><strong>Finish</strong></li>
                              </ul> <!-- fieldsets -->
                              <fieldset class="integration">
                                  <div class="form-card">
                                    <div class="center-vh dir-col">
                                        <ul class="item-features">
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Customising Templates
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Social Media
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Medias Content Integartion
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Contact Us Feature
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Dynamic Pages + blog
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>100% Seo friendly
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Content Management
                                          </li>

                                          <li class="diseabled">
                                              <i class="icon ion-checkmark"></i>Google Analytics
                                          </li>

                                          <li class="diseabled">
                                              <i class="icon ion-checkmark"></i>Client Assistance
                                          </li>

                                        </ul>
                                    </div>
                                  </div> <input type="button" name="next" class="next action-button" value="Next Step" />
                              </fieldset>
                              <fieldset class="integration">
                                  <div class="form-card">
                                      <h2 class="fs-title">Personal Information</h2>
                                      <input type="text" id="i_first_name" placeholder="First Name" />
                                      <div id="i_first_name_errors" class="errors">

                                      </div>
                                      <input type="text" id="i_last_name" placeholder="Last Name" />
                                      <div id="i_last_name_errors" class="errors">

                                      </div>
                                      <input type="text" id="i_phone" placeholder="Contact No." />
                                      <div id="i_phone_errors" class="errors">

                                      </div>
                                      <input type="email" id="i_email" placeholder="Email." />
                                      <div id="i_email_errors" class="errors">

                                      </div>
                                  </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                              </fieldset>

                              <fieldset class="integration">
                                  <div class="form-card">
                                      <h2 class="fs-title text-center">Success !</h2> <br><br>
                                      <div class="row justify-content-center">
                                          <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                      </div> <br><br>
                                      <div class="row justify-content-center">
                                          <div class="col-7 text-center">
                                              <h5>You Have Successfully Signed Up</h5>
                                          </div>
                                      </div>
                                  </div>
                              </fieldset>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
