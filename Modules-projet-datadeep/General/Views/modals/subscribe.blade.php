
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-smll" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgba(255, 214, 0, 0.7);color: black;">
                <h2 class="modal-title" id="myModalLabel" style="margin: 0 auto;">Subscribe.</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>


            </div>
            <div class="modal-body">
              <div class="row align-items-center justify-content-center">
                  <div class="col-12 col-md-12 col-lg-12">

                      <div class="form-container form-container-transparent form-container-white" >
                          <div class="form-desc  white-section">

                              <p class="invite anim-3 opacity-75">Be the first to hear about the latest releases. Join the
                                  DataDeep email list.</p>
                          </div>
                          <div class="form-input  anim-4">
                              <div id="form-input" class="form-group form-success-gone show-form-input">
                                  <label for="reg-email">Email</label>
                                  <input id="reg-email" name="email" class="form-control-line form-control-white" type="email"  placeholder="your@email.address" required />
                              </div>
                              <div class="form-group mb-0 text-center">
                                  <div id="message-input">
                                      <p class="hide-form-input">Your email has been registred, thank you.</p>
                                  </div>
                                  <div id="msg">

                                  </div>
                                  <button id="submit-email" class="button1 show-form-input" name="submit_email">Subscribe
                                  </button>




                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
