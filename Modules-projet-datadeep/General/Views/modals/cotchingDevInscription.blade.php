
<style media="screen">
#wform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#wform fieldset .form-card {
    background: white;
    border: 0 none;
    border-radius: 0px;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    padding: 20px 40px 30px 40px;
    box-sizing: border-box;
    width: 94%;
    margin: 0 3% 20px 3%;
    position: relative
}

#wform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

#wform fieldset:not(:first-of-type) {
    display: none
}

#wform fieldset .form-card {
    text-align: left;
    color: #9E9E9E
}

#wform input,
#wform textarea,
#wform select {
    padding: 0px 8px 4px 8px;
    border: none;
    border-bottom: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    font-size: 16px;
    letter-spacing: 1px
}

#wform input:focus,
#wform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: none;
    font-weight: bold;
    border-bottom: 2px solid rgba(255, 214, 0, 1);;
    outline-width: 0
}

#wform .action-button {
    width: 100px;
    background: rgba(255, 214, 0, 1);;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
}

#wform .action-button:hover,
#wform .action-button:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px rgba(255, 214, 0, 1);
}

#wform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
}

#wform .action-button-previous:hover,
#wform .action-button-previous:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px #616161
}

select.list-dt {
    border: none;
    outline: 0;
    border-bottom: 1px solid #ccc;
    padding: 2px 5px 3px 5px;
    margin: 2px
}

select.list-dt:focus {
    border-bottom: 2px solid rgba(255, 214, 0, 1);
}

.card {
    z-index: 0;
    border: none;
    border-radius: 0.5rem;
    position: relative
}

.fs-title {
    font-size: 19px;
    color: #2C3E50;
    margin-bottom: 10px;
    font-weight: bold;
    text-align: left
}

#wprogressbar {
    padding-left: 0px;
    margin-bottom: 30px;
    overflow: hidden;
    color: lightgrey
}

#wprogressbar .active {
    color: #000000
}

#wprogressbar li {
    list-style-type: none;
    font-size: 12px;
    width: 25%;
    float: left;
    position: relative
}

#wprogressbar #waccount:before {
    font-family: FontAwesome;
    content: "\f023"
}

#wprogressbar #wwconfirm:before {
    font-family: FontAwesome;
    content: "\f007"
}

#wprogressbar #payment:before {
    font-family: FontAwesome;
    content: "\f09d"
}

#wprogressbar #confirm:before {
    font-family: FontAwesome;
    content: "\f00c"
}

#wprogressbar #wwskills:before {
    font-family: FontAwesome;
    content: "\f013"
}

#wprogressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 18px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#wprogressbar li:after {
    content: '' !important;
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#wprogressbar li.active:before,
#wprogressbar li.active:after {
    background: rgba(255, 214, 0, 1);
}

.radio-group {
    position: relative;
    margin-bottom: 25px
}

.radio {
    display: inline-block;
    width: 204;
    height: 104;
    border-radius: 0;
    background: lightblue;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    box-sizing: border-box;
    cursor: pointer;
    margin: 8px 2px
}

.radio:hover {
    box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
}

.radio.selected {
    box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
}

.fit-image {
    width: 100%;
    object-fit: cover
}

.errors{
  margin-top: -24px;
    font-size: 10px;
    color: red;
}

</style>
<div class="modal fade" id="myModalDevelopemnt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-smll" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgba(255, 214, 0, 0.7);color: black;">
                <h2 class="modal-title" id="myModalLabel" style="margin: 0 auto;">Subscribe.</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>


            </div>
            <div class="modal-body">
              <div class="row align-items-center justify-content-center">
                  <div class="col-12 col-md-12 col-lg-12">

                      <div class="form-container form-container-transparent form-container-white" >
                          <div class="form-desc  white-section">

                          </div>
                          <form id="wform">
                              <!-- wprogressbar -->
                              <ul id="wprogressbar">
                                  <li class="active" id="waccount"><strong>Courses</strong></li>
                                  <li id="wwconfirm"><strong>Personal</strong></li>
                                  <li id="wwskills"><strong>Skills</strong></li>

                                  <li id="confirm"><strong>Finish</strong></li>
                              </ul> <!-- fieldsets -->
                              <fieldset class="fieldset">
                                  <div class="form-card">
                                    <div class="center-vh dir-col">
                                        <ul class="item-features">
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Customising Templates
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Social Media
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Medias Content Integartion
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Contact Us Feature
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Dynamic Pages + blog
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>100% Seo friendly
                                          </li>
                                          <li class="ok">
                                              <i class="icon ion-checkmark"></i>Content Management
                                          </li>

                                          <li class="diseabled">
                                              <i class="icon ion-checkmark"></i>Google Analytics
                                          </li>

                                          <li class="diseabled">
                                              <i class="icon ion-checkmark"></i>Client Assistance
                                          </li>

                                        </ul>
                                    </div>
                                  </div> <input type="button" name="next" class="wnext action-button" value="Next Step" />
                              </fieldset>
                              <fieldset class="fieldset">
                                  <div class="form-card">
                                      <h2 class="fs-title">Personal Information</h2>
                                      <input type="text" id="w_first_name" placeholder="Full Name" />
                                      <div id="w_first_name_errors" class="errors">

                                      </div>
                                      <input type="text" id="w_age" placeholder="Age" />
                                      <div id="w_age_errors" class="errors">

                                      </div>
                                      <input type="text" id="w_phone" placeholder="Contact No." />
                                      <div id="w_phone_errors" class="errors">
                                      </div>
                                      <input type="email" id="w_email" placeholder="Email." />
                                      <div id="w_email_errors" class="errors">

                                      </div>
                                      <input type="address" id="w_address" placeholder="Address" />
                                      <div id="w_address_errors" class="errors"> </div>

                                      <select class="" name=""  id="w_profession">
                                        <option value="select">select</option>
                                        <option value="select">select1</option>
                                        <option value="select">select2</option>
                                      </select>
                                  </div> <input type="button" name="previous" class="wprevious action-button-previous" value="Previous" /> <input type="button" name="next" class="wnext action-button" value="Next Step" />
                              </fieldset>

                              <fieldset class="fieldset">
                                  <div class="form-card">
                                      <h2 class="fs-title">Witch languages you know ?</h2>
                                      <input type="hidden" id="studentId" value="">
                                      <div>
                                        <input type="checkbox" class="skills" name="scales"
                                               checked style="margin-bottom:0px;width:15%" value="css">
                                        <label for="css">Css</label>
                                      </div>

                                      <div>
                                        <input type="checkbox" class="skills" name="skills" style="margin-bottom:0px;width:15%" value="html">
                                        <label for="html">Html</label>
                                      </div>

                                      <div>
                                        <input type="checkbox" class="skills" name="skills" style="margin-bottom:0px;width:15%" value="javascript">
                                        <label for="javascript">Javascript</label>
                                      </div>

                                      <div>
                                        <input type="checkbox" class="skills" name="skills" style="margin-bottom:0px;width:15%" value="react">
                                        <label for="mysql">React</label>
                                      </div>

                                      <div>
                                        <input type="checkbox" class="skills" name="skills" style="margin-bottom:0px;width:15%" value="mysql">
                                        <label for="mysql">Mysql</label>
                                      </div>

                                      <div>
                                        <input type="checkbox" class="skills" name="skills" style="margin-bottom:0px;width:15%" value="php">
                                        <label for="php">Php</label>
                                      </div>

                                      <div>
                                        <input type="checkbox" class="skills" name="skills" style="margin-bottom:0px;width:15%" value="nodejs">
                                        <label for="nodejs">Nodejs</label>
                                      </div>

                                      <div>
                                        <input type="checkbox" class="skills" name="skills" style="margin-bottom:0px;width:15%" value="nothing">
                                        <label for="nodejs">Nothings</label>
                                      </div>
                                  </div> <input type="button" name="previous" class="wprevious action-button-previous" value="Previous" /> <input type="button" name="next" class="wnext action-button" value="Next Step" />
                              </fieldset>

                              <fieldset class="fieldset">
                                  <div class="form-card">
                                      <h2 class="fs-title text-center">Success !</h2> <br><br>
                                      <div class="row justify-content-center">
                                          <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                      </div> <br><br>
                                      <div class="row justify-content-center">
                                          <div class="col-7 text-center">
                                              <h5>You Have Successfully Signed Up</h5>
                                          </div>
                                      </div>
                                  </div>
                              </fieldset>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
