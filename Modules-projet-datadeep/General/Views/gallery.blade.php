@extends('frontOffice.dipper.layoutBlog')
@section('title')
      <title>Blog</title>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/blog/slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/blog/slick/slick-theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/blog/HoverEffect/demo.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('frontOffice/blog/HoverEffect/set1.css')}}" />
<link rel="stylesheet" href="{{asset('frontOffice/blog/css/styles.css')}}">
<link rel="stylesheet" href="{{asset('dipper/css/personalizeBlog.css')}}">

<style media="screen">

.hide{
  display: none;
}

.message{
  position: absolute;
left: 5vw;
top: 25vh;
color: black;
}

.message img{
  width: 25%;
}
</style>



@endsection


@section('content')



<div class="page-cover">

    {{-- <div class="cover-bg bg-img" data-image-src="{{asset('dipper/img/about.jpg')}}"></div> --}}

<div id="particles-js" class="cover-bg pos-abs full-size bg-color"></div>
</div>

<main class="page-main page-fullpage main-anim" id="itempage">


    <div class="section section-twoside fp-auto-height-responsive" data-section="projects">

        <div class="section-wrapper twoside" style="text-align:center;margin-bottom:20px;">


          <div id="loader" class="hide">
            <div id="page-loader" class="page-loader"  style="background: transparent !important">
                <div>
                  <div class="icon ion-spin"></div>
                </div>
              </div>
          </div>



            <div class="grid" id="articles">
                @foreach($articles as $article)
                <figure class="effect-ruby {{$article->category->name}} articles">
                    <img src="{{asset($article->image)}}" alt="{{$article->title}}" style="height: 360px;" />
                    <figcaption>
                        <h3><span>{{$article->title}}</span></h3>
                        <p >{{$article->description}}</p>
                        <a href="{{route('showArticle',htmlentities(urlencode(str_replace(' ', '-', $article->title))).'_'.$article->id)}}">View more</a>
                    </figcaption>
                </figure>
                @endforeach


            </div>

        </div>

        <div class="btn-more-article">
            <a href="#" id="show-more-article" data-offset='1' data-category='all'> Show More Article </a>
        </div>

    </div>

</main>

<script src="{{asset('frontOffice/blog/js/jquery.min.js')}}"></script>

<script src="{{ asset('frontOffice/blog/js/isotope.pkgd.min.js') }}"></script>


<script src="{{ asset('frontOffice/blog/js/functions.js')}} "></script>
@endsection

@section('js')

<script src="{{asset('frontOffice/blog/slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>
@include('General::inc.galleryScript')

@endsection
