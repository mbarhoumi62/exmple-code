@extends('frontOffice.dipper.layout')
@section('title')
      <title>Data Deep - Homepage</title>
@endsection
@section('css')
  <link rel="stylesheet" href="{{asset('dipper/css/personalizeIndex.css')}}">
@endsection
@section('content')

  <style>
  .btn-courses{
    margin-top:15px ;
    margin-bottom: 20px ;
    background-color: black;
    color: white ;
    border: 2px solid white ;
    padding: 5px 30px ;
  }

.btn-courses:focus{

  color: black ;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
      background-color: white;
        border: 2px solid black ;
}
  .btn-courses:hover{
    color: black ;
    border: 2px solid black ;
        background-color: white;
  }
  .flip-card {
    background-color: transparent;
    width: 300px;
    height: 200px;
    perspective: 1000px;
  }

  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transform-style: preserve-3d;
    -webkit-transform-style: preserve-3d;
    transition: transform 0.6s;
    -moz-transition:  transform 0.6s; /* Firefox 4 */
   -webkit-transition:  transform 0.6s; /* Safari and Chrome */
   -o-transition:  transform 0.6s; /* Opera */
   -ms-transition:  transform 0.6s; /* Explorer 10 */
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  }

  .flip-card:hover .flip-card-inner {
    transform: rotateY(180deg);
  }

  .flip-card-front, .flip-card-back {
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility:hidden;
    -moz-backface-visibility:hidden;
    -ms-backface-visibility:hidden;
    -o-backface-visibility:hidden;
    backface-visibility:hidden;
  }

  .flip-card-front {
    /* background-color: #bbb; */
    padding:30px 10px;
    color: black;
  }

  .flip-card-back {

    padding: 30px 10px;
    background-color: #ffd600;
    color: black;
    transform: rotateY(180deg);
  }

  .flip-card-back p{
    margin:0;
    text-transform: uppercase;
  }

  </style>
<div class="page-cover">

    {{-- <div class="cover-bg bg-img" data-image-src="{{asset('dipper/img/data-deep-website_0035_Hue_Saturation-1.jpg')}}"></div> --}}

<div id="particles-js" class="cover-bg pos-abs full-size bg-color" data-bgcolor="rgba(255,255,255,1)"></div>
</div>


<main class="page-main page-fullpage main-anim" id="mainpage">

    <div  class="section section-home fullscreen-md fp-auto-height-responsive main-home" data-section="home" id="home-section">



        <div class="container container-padding-left">


            <div class="row row-position show-image">
                <div class="col-md-11 col-padding-left">

                    <img src="{{asset('dipper/img/222.png')}}" alt="datadeep" class="img-width">
                    <div class="index-title">
                    <h1><span id="title"></span></h1>
                    </div>

                </div>
                <div class="col-md-1">

                </div>
            </div>

            <div id="res-img" class="row row-position hide-image" >
                <div class="col-md-12 col-padding-left">

                    <img src="{{asset('dipper/img/bcg_image_res.png')}}" alt="datadeep" class="img-width">
                </div>

            </div>
        </div>




        <div class="section-wrapper center-vh dir-col anim">
            <div class="section-content reduced text-center">
                <div class="row font-test">

                  <div class="section-home-title">
                      <p>Home</p>
                  </div>



                    <a href="#about" class="button left" id="left"><span>About us</span><img id="arrow-left"  src="{{asset('dipper/img/arrow-left-icon-black.png')}}" alt="">
                    </a>
                    <a href="#services" class="button right" id="right"><span>Services</span><img id="arrow-right"  src="{{asset('dipper/img/arrow-right-icon-black.png')}}" alt="">
                    </a>

                    <div class="hide-image">
                        <a href="#services" class="button1" id="down"><span>Services</span> </a>
                          <br>
                        <img id="arrow-down-mobile"  src="{{asset('dipper/img/arrow-down-icon-black.png')}}" alt="" style="width:10px;margin-top: 10px;">
                    </div>


                    <div class="section-title text-center">
                        <h5 class="title-bg">Home</h5>
                    </div>

                </div>
            </div>

        </div>

    </div>


    <div style="background: linear-gradient(90deg, rgba(255,255,255,0.8) 0%, rgba(255,214,0,0.7) 80%);" class="section section-description fp-auto-height-responsive about-section" data-section="about">

        <div class="section-wrapper center-vh dir-col anim">

            <div class="section-title text-center">
                <h5 class="title-bg">About</h5>
                <h2 style="color:black" class="display-4 display-title anim-2 mb-32">About Us</h2>
            </div>

            <div class="section-content reduced center-vh dir-col">

                <div class="col-12 col-sm-12 center-vh">
                    <div class="section-content anim translateUp">
                        <div class="images text-center">
                            <div class="img-frame-legend-alpha">
                                <div>
                                    <img class="img shadow" src="{{asset('dipper/img/pc.png')}}" alt="Image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8 col-sm-8 about-description shadow" style="max-width: 82%;">
                    <div class="section-content anim">

                        <h4 class="text-center">Who Are we ?</h4>
                        <p style="text-align:center;">We are a creative, futurists, visionary team of designers, marketers, strategists and developers.
we don't sell a product. We Identify our clients needs. We build a custom solutions, helping them to stand out, achieve their goals, succeed, grow and look really good on the web. </p>
                    </div>
                </div>
            </div>



        </div>

    </div>


    <div class="section section-list-feature fp-auto-height-responsive " data-section="services">

        <div class="section-wrapper twoside center-vh dir-col anim">

            <div class="section-title text-center white-section">
                <h2 class="title-bg" >Services</h2>
                <h2 class="display-4 display-title anim-2 mb-32">Services</h2>
            </div>


            <div class="item row justify-content-between fade-1">

                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-6">

                    <div class="section-content reduced">

                        <div class="media">
                              <div class="media-body text-center tool">


                              {{-- <i class="icon icon ion-code-working m-auto"></i> --}}
                              <i class="fa fa-code fa-3x" style="color:black;"></i>
                              <h4 class="servicesTitles">Development</h4>

                              <div class="flip-card">
                                  <div class="flip-card-inner">
                                      <div class="flip-card-front">
                                          <p class="servicesDesc">Coding is the backbone of every digital strategy. that's why we don't just do it but we do it right. </p>
                                      </div>
                                      <div class="flip-card-back">

                                        <p>websites</p>

                                        <p>mobile apps</p>

                                      </div>
                                  </div>
                              </div>

                          </div>
                        </div>

                        <div class="media">
                            <div class="media-body text-center tool " style="margin-top: 15px;">


                              {{-- <i class="icon ion-paintbrush m-auto"></i> --}}

                               <i class="fa fa-pen-nib fa-3x" style="color:black;"></i>
                              <h4 class="servicesTitles" >WEB DESIGN</h4>

                              <div class="flip-card">
                                  <div class="flip-card-inner">
                                      <div class="flip-card-front">
                                        <p class="servicesDesc">Our design it's your way to communicate and attract your audience and our manner of offering to them the best customer experiences. </p>
                                      </div>
                                      <div class="flip-card-back">
                                        <p>ui design for web/app</p>
                                        <p>ux design for web/app</p>
                                        <p>landing page design</p>
                                        <p>  customized template:</p>
                                      </div>
                                  </div>
                              </div>


                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-6">

                    <div class="section-content reduced">

                        <div class="media">
                            <div class="media-body text-center tool">

                              <i class="fas fa-chart-area fa-3x" style="color:black;"></i>

                              <h4 class="servicesTitles" >Digital Marketing</h4>

                              <div class="flip-card">
                                  <div class="flip-card-inner">
                                      <div class="flip-card-front">
                                          <p class="servicesDesc">We make sure that your brand exist in the world wide web by creating a customized web strategy. </p>
                                      </div>
                                      <div class="flip-card-back">

                                          <p>SEO "Search Engine Optimisation"</p>
                                          <p>SMA "Social Media Advertising"</p>
                                          <p>ASO "App Store Optimization"</p>

                                      </div>
                                  </div>
                              </div>


                            </div>
                        </div>

                        <div class="media">
                            <div class="media-body text-center tool" style="margin-top: 15px;">




                              <i class="fa fa-pencil-ruler fa-3x" style="color:black;"></i>

                                <h4 class="servicesTitles" >GRAPHIC DESIGN</h4>

                                <div class="flip-card">
                                    <div class="flip-card-inner">
                                        <div class="flip-card-front">
                                            <p class="servicesDesc">We make sure that your brand exist in the world wide web by creating a customized web strategy. </p>
                                        </div>
                                        <div class="flip-card-back">


                                                  <p>Branding</p>


                                                  <p>LOGO</p>


                                                  <p> prints, catalog, flyers design</p>


                                                  <p>packaging design</p>


                                                  <p>ppt presentation design</p>


                                                  <p> social media content</p>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



        </div>

    </div>



    <div style="background: linear-gradient(90deg, rgba(255,255,255,0.8) 0%, rgba(255,214,0,0.7) 80%);"  class="section section-pricelist " data-section="pricelist">

        <div class="section-wrapper center-vh dir-col anim">

            <div class="section-title text-center white-section">
                <h5 class="title-bg">Pricing</h5>
                <h2 class="display-4 display-title anim-2 mb-32">Packages</h2>
            </div>

            <div class="section-content anim text-center fullwidth">

                <div class="price-table">
                  <div class="container" style="width: 100%;margin-left: auto;margin-right: auto;">
                    <div class="row ">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4">

                            <div class="price-item price-item-secondary shadow mb-4 sr-up-td4">
                                <h4 class="item-name">Bronze</h4>
                                {{-- <div class="item-price"><span class="price-small bold">$</span> <span class="price-big">0</span><span class="price-small">.00</span></div> --}}
                                <div class="center-vh dir-col">
                                    <ul class="item-features">
                                        <li class="ok">
                                            <i class="icon ion-checkmark"></i>Integrating Templates
                                        </li>
                                        <li class="ok">
                                            <i class="icon ion-checkmark"></i>Social Media
                                        </li>
                                        <li class="ok">
                                            <i class="icon ion-checkmark"></i>Medias Content Integartion
                                        </li>
                                        <li class="ok">
                                            <i class="icon ion-checkmark"></i>Contact Us Feature
                                        </li>
                                        <li class="ok">
                                            <i class="icon ion-checkmark"></i>Static Pages
                                        </li>

                                        <li class="diseabled">
                                            <i class="icon ion-checkmark"></i>100% Seo friendly
                                        </li>
                                        <li class="diseabled">
                                            <i class="icon ion-checkmark"></i>Content Management
                                        </li>
                                        <li class="diseabled">
                                            <i class="icon ion-checkmark"></i>Google Analytics
                                        </li>

                                        <li class="diseabled">
                                            <i class="icon ion-checkmark"></i>Client Assistance
                                        </li>



                                    </ul>
                                </div>
                                <div class="item-footer">
                                    <a class="btn btn-outline-perso" href="#contact/message">
                                        Contact Us
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4">

                            <div class="price-item featured price-item-primary shadow mb-4 sr-up-td2">
                                <h4 class="item-name">Silver</h4>
                                {{-- <div class="item-price"><span class="price-small bold">$</span> <span class="price-big">59</span><span class="price-small">.99</span></div> --}}
                                <div class="center-vh dir-col">
                                    <ul class="item-features">
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Customising Templates
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Social Media
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Medias Content Integartion
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Contact Us Feature
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Dynamic Pages + blog
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>100% Seo friendly
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Content Management
                                      </li>

                                      <li class="diseabled">
                                          <i class="icon ion-checkmark"></i>Google Analytics
                                      </li>

                                      <li class="diseabled">
                                          <i class="icon ion-checkmark"></i>Client Assistance
                                      </li>

                                    </ul>
                                </div>
                                <div class="item-footer">
                                    <a class="btn btn-primary" href="#contact/message">
                                        Contact Us
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4">

                            <div class="price-item price-item-secondary shadow mb-4 sr-up-td4">
                                <h4 class="item-name">Gold</h4>
                                {{-- <div class="item-price"><span class="price-small bold">$</span> <span class="price-big">99</span><span class="price-small">.99</span></div> --}}
                                <div class="center-vh dir-col">
                                    <ul class="item-features">
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>UX/UI Design
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Social Media
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Medias Content Integartion
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Contact Us Feature
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Dynamic Pages + blog
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>100% Seo friendly
                                      </li>
                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Content Management
                                      </li>

                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Google Analytics
                                      </li>

                                      <li class="ok">
                                          <i class="icon ion-checkmark"></i>Client Assistance
                                      </li>
                                    </ul>
                                </div>
                                <div class="item-footer">
                                    <a class="btn btn-outline-perso" href="#contact/message">
                                        Contact Us
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                  </div>
                </div>
            </div>


        </div>

    </div>


    <div class="section section-twoside fp-auto-height-responsive " data-section="portfolio">

        <div class="section-wrapper twoside">

            <div class="section-title text-center  white-section">
                <h5 class="title-bg ">Portfolio</h5>
                <h2 class="display-4 display-title mb-32">Portfolio</h2>
            </div>

            <div class="slider-wrapper carousel-swiper-beta carousel-smalls carousel-swiper-beta-demo">

                <div class="slider-container swiper-container">
                    <ul class="item-list swiper-wrapper">


                        <li class="slide-item swiper-slide">
                            <div class="item-wrapper">
                                <div class="illustr">
                                    <img src="{{('dipper/img/bag-min-dataDeep.jpg')}}" alt="datadeep LEMON COKE" class="img">
                                </div>
                                <a class="legend l-portfolio" href="item.html#project_url" style="text-align:center">
                                    <h2 class="display-4" style="font-size:2rem">Design</h2>
                                    <p>Bag design for swag mood style</p>
                                </a>
                            </div>
                        </li>

                        <li class="slide-item swiper-slide">
                            <div class="item-wrapper">
                                <div class="illustr">
                                    <img src="{{asset('dipper/img/Ice-cream-dataDeep.jpg')}}" alt="datadeep POOKIE APP" class="img">
                                </div>
                                <a class="legend l-portfolio" href="item.html#project_url" style="text-align:center">
                                    <h2 class="display-4" style="font-size:2rem">Design</h2>
                                    <p>Logo and packaging design for a delicious  ice cream</p>
                                </a>
                            </div>
                        </li>

                        <li class="slide-item swiper-slide">
                            <div class="item-wrapper">
                                <div class="illustr">
                                    <img src="{{asset('dipper/img/pookie-project-dataDeep.jpg')}}" alt="datadeep POOKIE APP" class="img">
                                </div>
                                <a class="legend l-portfolio" href="item.html#project_url" style="text-align:center">
                                    <h2 class="display-4" style="font-size:2rem">Design</h2>
                                    <p> Website design for kids</p>
                                </a>
                            </div>
                        </li>
                        <li class="slide-item swiper-slide">
                            <div class="item-wrapper">
                                <div class="illustr">
                                    <img src="{{asset('dipper/img/soda-min-daraDeep.jpg')}}" alt="datadeep POOKIE APP" class="img">
                                </div>
                                <a class="legend l-portfolio" href="item.html#project_url" style="text-align:center">
                                    <h2 class="display-4" style="font-size:2rem">Design</h2>
                                    <p>A fresh and cool packaging and logo design</p>
                                </a>
                            </div>
                        </li>
                      </ul>
                </div>

                <div class="items-pagination bar"></div>

                <div class="items-button bottom fit items-button-prev" style="color: white;">
                    <a class="btn btn-transp-arrow btn-primary icon-left" href="#">
                        <span class="fa fa-chevron-circle-left fa-3x"></span>
                    </a>
                </div>
                <div class="items-button bottom fit items-button-next" style="color: white;">
                    <a class="btn btn-transp-arrow btn-primary" href="#">
                        <span class="fa fa-chevron-circle-right fa-3x"></span>
                    </a>
                </div>
            </div>



        </div>

    </div>

    <div  class="section section-twoside fp-auto-height-responsive " data-section="cotching">

        <div class="section-wrapper twoside center-vh dir-col">

            <div class="section-title text-center white-section">

                <h5 class="title-bg">Courses</h5>

                <h2 class="display-4 display-title mb-32">Our Courses</h2>
                <p>ACCESS THE BEST TRAINING</p>
            </div>


            <div class="item row justify-content-between">


                <div class="col-sm-12 col-md-12 col-lg-6 center-vh">
                    <div class="section-content anim translateUp  text-center">
                        <div class="images text-center">
                            <div class="img-frame-normal">
                                <div class="img-1 shadow">
                                    <div>
                                        <img class="img" src="{{asset('storage/articles/images/articleImage1.1574331645.jpg')}}" alt="Image">
                                    </div>
                                    {{-- <div class="legend text-left pos-abs">
                                      <h5 class="text-center">Test 1</h5>


                                        <div class="btns-action text-center" style="position: absolute;bottom: 0;width: 100%;/*! border: 1px solid red; */">
                                            <a class="btn btn-outline-white" href="#">
                                                <span class="text">Show Details</span>
                                            </a>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                          <button type="button" class="btn btn-courses" id="integration-details" data-formation = "0" > Learn Web Design</button>

                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-6 center-vh">
                    <div class="section-content anim translateUp  text-center">
                        <div class="images text-center">
                            <div class="img-frame-normal">
                                <div class="img-1 shadow">
                                    <div>
                                        <img class="img" src="{{asset('storage/articles/images/articleImage1.1574331645.jpg')}}" alt="Image">
                                    </div>
                                    {{-- <div class="legend text-left pos-abs">
                                      <h5 class="text-center">Test 1</h5>


                                        <div class="btns-action text-center" style="position: absolute;bottom: 0;width: 100%;/*! border: 1px solid red; */">
                                            <a class="btn btn-outline-white" href="#">
                                                <span class="text">Show Details</span>
                                            </a>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    <button type="button" class="btn btn-courses" id="developemnt-details" data-formation = "1" >Learn Webflow</button>
                    </div>
                </div>


            </div>


        </div>

    </div>

    <div style="background: linear-gradient(90deg, rgba(255,255,255,0.8) 0%, rgba(255,214,0,0.7) 80%);"  class="section section-twoside fp-auto-height-responsive " data-section="blog">

        <div class="section-wrapper twoside center-vh dir-col">

            <div class="section-title text-center white-section">
                <h5 class="title-bg">Blog</h5>
                <h2 class="display-4 display-title mb-32">Blog</h2>
            </div>


            <div class="item row justify-content-between">

              @foreach ($articles as $article)
                <div class="col-sm-12 col-md-12 col-lg-6 center-vh">
                    <div class="section-content anim translateUp">
                        <div class="images text-center">
                            <div class="img-frame-normal">
                                <div class="img-1 shadow">
                                    <div>
                                        <img class="img" src="{{asset($article->image)}}" alt="Image">
                                    </div>
                                    <div class="legend text-left pos-abs">
                                      <h5 class="text-center">{{$article->title}}</h5>


                                        <div class="btns-action text-center" style="position: absolute;bottom: 0;width: 100%;/*! border: 1px solid red; */">
                                            <a class="btn btn-outline-white" href="{{route('showArticle',htmlentities(urlencode(str_replace(' ', '-', $article->title))).'_'.$article->id)}}">
                                                <span class="text">Read More</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              @endforeach



            </div>
                  <a href="{{route('showGallery')}}" class="button1">See More</a>

        </div>

    </div>




    <div  class="section section-contact fp-auto-height-responsive no-slide-arrows " data-section="contact">

        <div class="slide" id="information" data-anchor="information">

            <div class="section-wrapper">

                <div class="section-title text-center">
                    <h5 class="title-bg">Contact</h5>
                </div>
                <div class="row">
                    <div class="col-12 col-md-12">

                        <div class="section-content anim text-center">

                            <div class="title-desc">
                                <div class="anim-2 white-section">
                                    <h2 class="display-4 display-title">Let's Talk</h2>
                                    <p>It would be great to hear about your new projects</p>
                                </div>
                                <div class="address-container anim-3">
                                    <div class="row">
                                        <div class="col-12 col-md-12 col-lg-4 white-section">
                                            <h4><img src="{{asset('dipper/icons/tel.png')}}" alt=""></h4>
                                            <p> (+216) 31 583 559 | (+216) 53 676 920 </p>
                                        </div>
                                        <div class="col-12 col-md-12 col-lg-4 white-section">
                                              <h4><img src="{{asset('dipper/icons/mail.png')}}" alt=""></h4>
                                            <p>
                                                <a href="#" style="color: black">Contact
                                                    @DataDeep.tn</a>
                                            </p>
                                        </div>
                                        <div class="col-12 col-md-12 col-lg-4 white-section">
                                            <h4><img src="{{asset('dipper/icons/adresse.png')}}" alt=""></h4>
                                            <p>
                                                Centre Amine 4th Floor , 7 Ahmed khabthani
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="btns-action anim-4 mt-3">

                                <a class="button1" href="#contact/message">
                                    <span class="txt">Send Message</span>
                                    <i class="icon fas fa-arrow-right ml-2"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                    </div>
                </div>
            </div>

        </div>


        <div class="slide" id="message" data-anchor="message">

            <div class="section-wrapper center-vh dir-col">
                <div class="row justify-content-between">
                    <div class="col-12 col-md-12 col-lg-12">

                        <div class="section-content anim text-center">

                            <div class="title-desc">
                                <div class="white-section">
                                    <h2 class="display-4 display-title">Write Message</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-12">

                        <div class="section-content anim text-left">

                            <div class="">
                                <div class="form-container form-container-card">

                                    <form class="send_message_form message form" method="post" action="#" id="message_form">
                                        <div class="form-group name">
                                            <label for="mes-name">Your Name</label>
                                            <input id="mes-name" name="name" type="text" placeholder="" class="form-control-line form-success-clean" required>
                                        </div>
                                        <div class="form-group email">
                                            <label for="mes-email">Your Email</label>
                                            <input id="mes-email" type="email" placeholder="" name="email" class="form-control-line form-success-clean" required>
                                        </div>
                                        <div class="form-group email">
                                            <label for="mes-email">Your Company (*)</label>
                                            <input type="text" placeholder="" id="company" class="form-control-line form-success-clean">
                                        </div>
                                        <div class="form-group email">
                                            <label for="mes-email">Service (*)</label>
                                              <select class="form-control-line form-success-clean" id="service">
                                                <option selected disabled value="">What service you are interested in ?</option>
                                                <option value="Digital Marketing">Digital Marketing</option>
                                                <option value="Graphic Desig">Graphic Design</option>
                                                <option value="UI/UX Design">UI/UX Design</option>
                                                  <option value="Web/Mobile Apps Developement">Web/Mobile Apps Developemnt</option>
                                              </select>
                                        </div>
                                        <div class="form-group no-border">
                                            <label for="mes-text">Message</label>
                                            <textarea placeholder="Your projects, your questions, your comments ..." id="mes-text" name="message" class="form-control form-control-outline thick form-success-clean" required></textarea>
                                            <div>
                                                <p id="sendMsg" style="color : green!important" class="message-ok invisible form-text-feedback form-success-visible">
                                                    Your message has been sent, thank you.</p>
                                            </div>
                                        </div>
                                        <div class="btns">
                                            <button id="submitMsg" class="button1 btn-full " name="">
                                                <i class="icon ion-paper-airplane mr-2"></i>
                                                <span class="">Send</span>
                                                <span class="arrow-icon"></span>
                                            </button>
                                            <a class="button1 text-center" href="#contact/information" style="border:none;">
                                                <span class="txt"><i class="fas fa-arrow-left mr-2"></i> Back To Contact</span>
                                            </a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</main>
@include('General::modals.subscribe')
@include('General::modals.cotchingDevInscription')
@include('General::modals.cotchingIntegrationInscription')
@endsection

@section('js')
  @include('General::inc.indexScript')
  @include('General::inc.inscriptionDevScript')
  @include('General::inc.inscriptionIntegrationScript')



@endsection
