<script type="text/javascript">
      $('#left').mouseover(function () {

        var styles = {backgroundColor: "black",
                    color: "white"
                  };


          $('#left').css(styles);

        var base_url ='{!! url('/') !!}'

        var img = base_url+'/dipper/img/arrow-left-icon-white.png';
         $("#arrow-left").attr('src',img);
      });

      $('#left').mouseleave(function () {

        var styles = {backgroundColor: "white",
                    color: "black"

                  };


        $('#left').css(styles);

        var base_url ='{!! url('/') !!}'

        var img = base_url+'/dipper/img/arrow-left-icon-black.png';
            $("#arrow-left").attr('src',img);

      });
</script>


<script type="text/javascript">
      $('#right').mouseover(function () {

        var styles = {backgroundColor: "black",
                    color: "white"
                  };


          $('#right').css(styles);

        var base_url ='{!! url('/') !!}'

        var img = base_url+'/dipper/img/arrow-right-icon-white.png';
         $("#arrow-right").attr('src',img);
      });

      $('#right').mouseleave(function () {

        var styles = {backgroundColor: "white",
                    color: "black"

                  };


        $('#right').css(styles);

        var base_url ='{!! url('/') !!}'

        var img = base_url+'/dipper/img/arrow-right-icon-black.png';
            $("#arrow-right").attr('src',img);

      });
</script>

<script>

  var typed = new Typed('#title', {
  strings: ['WEB APP', 'WEB SITE', 'IDENTITY', 'STRATEGY', 'FUTURE'],
    typeSpeed: 100,
    backSpeed: 100,
    loop: true

  });

</script>



<script type="text/javascript">

$('#submitMsg').on('click' , function () {
      var data = {
          'email' : $('#mes-email').val(),
          'name' : $('#mes-name').val(),
          'message' : $('#mes-text').val(),
          'company' : $('#company').val(),
          'service' : $('#service').val()
      }
      $.post('{{route('apiSendMessage')}}',
          {
              '_token': $('meta[name=csrf-token]').attr('content'), data : data

          })
          .done(function (res) {

              if(res['status'] == 400){
          }else {
            $('#sendMsg').text('Smething went wrong, please try again !');
            $('#sendMsg').css('color', 'red!important');

          }

      })

    })

</script>

<script type="text/javascript">

$('#submit-email').on('click' , function () {
      var data = {
          'email' : $('#reg-email').val()

      }

        $('#msg').html("");
        $('#msg').removeClass('error');
      $.post('{{route('apiHandleUserSubscribe')}}',
          {
              '_token': $('meta[name=csrf-token]').attr('content'), data : data

          })
          .done(function (res) {

               $('#reg-email').val(" ");

                  if (res.status == 200) {

                    $('#form-input').hide();
                    $('#submit-email').hide();

                    $('#message-input p').addClass('suc').removeClass('hide-form-input');

                    setTimeout(function() {$('#myModal').modal('hide');}, 1000);
                  }else if (res.status == 201) {
                    $('#msg').addClass('error');
                    $('#msg').html(' You\'re Already Subscribed!');
                    setTimeout(function() {$('#myModal').modal('hide');}, 1000);
                  }else {


                    $('#msg').addClass('error');
                    $('#msg').html(res.errors.email[0]);
                  }





      })

    })

</script>
