<script type="text/javascript">

var formation
$('#developemnt-details').on('click',function () {
 formation = $(this).data('formation');
    $('#myModalDevelopemnt').modal('show');
})

  $(document).ready(function() {

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;

    $(".wnext").click(function() {


      current_fs = $(this).parent();
      next_fs = $(this).parent().next();
      console.log($("fieldset.fieldset").index(next_fs));



      if ($("fieldset.fieldset").index(next_fs) == 2) {
        var data = {
            'full_name' : $('#w_first_name').val(),
            'age' : $('#w_age').val(),
            'phone' : $('#w_phone').val(),
            'email' : $('#w_email').val(),
            'address' : $('#w_address').val(),
            'profession' : $('#w_profession').val(),
            'formation_type' :formation
        }
            $('.errors').val('');
        $.post('{{route('apiCotchingDevInscription')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {
              console.log(res);
              if (res.errors) {
                $.each(res.errors, function( index, value ) {

                  $('#w_'+index+'_errors').text(value);
                  $('#w_'+index+'_errors').css('color', 'red!important');

                    });
              }
              if (res.status == 200) {
                $('#studentId').val(res.studentId);
                $("#wprogressbar li").eq($("fieldset.fieldset").index(next_fs)).addClass("active");
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                  opacity: 0
                }, {
                  step: function(now) {
                      // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                      'display': 'none',
                      'position': 'relative'
                    });
                    next_fs.css({
                      'opacity': opacity
                    });
                  },
                  duration: 600
                });
              }

        })
      }else if ($("fieldset.fieldset").index(next_fs) == 3) {
        var skills = [];
              $(':checkbox:checked').each(function(i){
              skills[i] = $(this).val();
              });
                var data = {
                  'skills' : skills,
                  'studentId' : $('#studentId').val()
                }

                $.post('{{route('apiCotchingStudentSkills')}}',
                    {
                        '_token': $('meta[name=csrf-token]').attr('content'), data : data

                    })
                    .done(function (res) {
                      console.log(res);
                      if (res.status == 401) {
                            console.log('some Thing went wrong');
                      }
                      if (res.status == 200) {
                        $("#wprogressbar li").eq($("fieldset.fieldset").index(next_fs)).addClass("active");
                        next_fs.show();
                        //hide the current fieldset with style
                        current_fs.animate({
                          opacity: 0
                        }, {
                          step: function(now) {
                              // for making fielset appear animation
                            opacity = 1 - now;

                            current_fs.css({
                              'display': 'none',
                              'position': 'relative'
                            });
                            next_fs.css({
                              'opacity': opacity
                            });
                          },
                          duration: 600
                        });
                      }

                })
            }else {
        $("#wprogressbar li").eq($("fieldset.fieldset").index(next_fs)).addClass("active");
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
          opacity: 0
        }, {
          step: function(now) {
              // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
              'display': 'none',
              'position': 'relative'
            });
            next_fs.css({
              'opacity': opacity
            });
          },
          duration: 600
        });
      }
    });

    $(".wprevious").click(function() {

      current_fs = $(this).parent();
      previous_fs = $(this).parent().prev();

      //Remove class active
      $("#wprogressbar li").eq($("fieldset.fieldset").index(current_fs)).removeClass("active");

      //show the previous fieldset
      previous_fs.show();

      //hide the current fieldset with style
      current_fs.animate({
        opacity: 0
      }, {
        step: function(now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            'display': 'none',
            'position': 'relative'
          });
          previous_fs.css({
            'opacity': opacity
          });
        },
        duration: 600
      });
    });

    $('.radio-group .radio').click(function() {
      $(this).parent().find('.radio').removeClass('selected');
      $(this).addClass('selected');
    });

    $(".submit").click(function() {
      return false;
    })

  });
</script>
wnext
