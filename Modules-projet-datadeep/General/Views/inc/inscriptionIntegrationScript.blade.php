<script type="text/javascript">

var formation
$('#integration-details').on('click',function () {
 formation = $(this).data('formation');
    $('#myModalIntegration').modal('show');
})

  $(document).ready(function() {

    var current_fs, next_fs, previous_fs; //fieldset.integrations
    var opacity;

    $(".next").click(function() {


      current_fs = $(this).parent();
      next_fs = $(this).parent().next();
      console.log($("fieldset.integration").index(next_fs));
      //Add Class Active
      console.log($("fieldset.integration").index(next_fs),'int');


      if ($("fieldset.integration").index(next_fs) == 2) {
        var data = {
            'first_name' : $('#i_first_name').val(),
            'last_name' : $('#i_last_name').val(),
            'phone' : $('#i_phone').val(),
            'email' : $('#i_email').val(),
            'formation' : formation
        }
        console.log(data);
        $.post('{{route('apiCotchingIntegrationInscription')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {
              console.log(res);
              if (res.errors) {
                $.each(res.errors, function( index, value ) {
                  console.log(index);
                  $('#i_'+index+'_errors').text(value);
                  $('#i_'+index+'_errors').css('color', 'red!important');
                  console.log($('#i_'+index+'_errors').text())
                    });
              }
              if (res.status == 200) {
                $("#progressbar li").eq($("fieldset.integration").index(next_fs)).addClass("active");
                next_fs.show();
                //hide the current fieldset.integration with style
                current_fs.animate({
                  opacity: 0
                }, {
                  step: function(now) {
                      // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                      'display': 'none',
                      'position': 'relative'
                    });
                    next_fs.css({
                      'opacity': opacity
                    });
                  },
                  duration: 600
                });
              }

        })
      }else {
        $("#progressbar li").eq($("fieldset.integration").index(next_fs)).addClass("active");
        next_fs.show();
        //hide the current fieldset.integration with style
        current_fs.animate({
          opacity: 0
        }, {
          step: function(now) {
              // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
              'display': 'none',
              'position': 'relative'
            });
            next_fs.css({
              'opacity': opacity
            });
          },
          duration: 600
        });
      }
    });

    $(".previous").click(function() {

      current_fs = $(this).parent();
      previous_fs = $(this).parent().prev();

      //Remove class active
      $("#progressbar li").eq($("fieldset.integration").index(current_fs)).removeClass("active");

      //show the previous fieldset.integration
      previous_fs.show();

      //hide the current fieldset.integration with style
      current_fs.animate({
        opacity: 0
      }, {
        step: function(now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            'display': 'none',
            'position': 'relative'
          });
          previous_fs.css({
            'opacity': opacity
          });
        },
        duration: 600
      });
    });

    $('.radio-group .radio').click(function() {
      $(this).parent().find('.radio').removeClass('selected');
      $(this).addClass('selected');
    });

    $(".submit").click(function() {
      return false;
    })

  });
</script>
