<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'contacts';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'email',
    'message',
    'service',
    'company',
    'user_id'
  ];

}
