<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model {

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'subscriptions';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'email'
  ];

  protected $casts = [
      'tags' => 'array'
  ];




}
