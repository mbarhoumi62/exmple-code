<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model {

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'skills';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'label',
    'student_id'
  ];

  public function student()
  {
        return  $this->hasOne('App\Modules\General\Models\Student','id','student_id');
  }


}
