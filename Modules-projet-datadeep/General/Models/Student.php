<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model {

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'students';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'full_name',
    'email',
    'age',
    'address',
    'phone',
    'profession',
    'status' ,
    'formation_type',
    'user_id'
  ];

  public function skills()
  {
    return  $this->hasMany('App\Modules\General\Models\Skill');
  }

}
