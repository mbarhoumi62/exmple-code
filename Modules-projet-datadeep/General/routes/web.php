<?php

Route::group(['module' => 'General', 'middleware' => ['web'], 'namespace' => 'App\Modules\General\Controllers'], function() {

    Route::get('/user/social/{provider}', 'GeneralController@handleProviderRedirect')->name('handleProviderRedirect');
    Route::get('/user/social/{provider}/callback', 'GeneralController@handleProviderCallback')->name('handleProviderCallback');

    Route::get('/','GeneralController@showHome')->name('showHome');
    Route::get('/blog','GeneralController@showGallery')->name('showGallery');
    Route::get('/test','GeneralController@test')->name('test');


    Route::get('blog/article/{articleId}','GeneralController@showArticle')->name('showArticle');




    Route::post('comment','GeneralController@apiHandleAddComment')->name('apiHandleAddComment');
    Route::post('more-comment','GeneralController@apiHandleShowMoreComment')->name('apiHandleShowMoreComment');
    Route::post('more-replies','GeneralController@apiHandleGetCommentReplies')->name('apiHandleGetCommentReplies');
    Route::post('more-article','GeneralController@apiHandleShowMoreArticle')->name('apiHandleShowMoreArticle');

    Route::post('subscribe','GeneralController@apiHandleUserSubscribe')->name('apiHandleUserSubscribe');

    Route::post('integration-inscription','GeneralController@apiCotchingIntegrationInscription')->name('apiCotchingIntegrationInscription');
    Route::post('dev-inscription','GeneralController@apiCotchingDevInscription')->name('apiCotchingDevInscription');
    Route::post('student-skills','GeneralController@apiCotchingStudentSkills')->name('apiCotchingStudentSkills');
    Route::post('filter','GeneralController@apiHandleGategoryFilter')->name('apiHandleGategoryFilter');
    Route::post('message','GeneralController@apiSendMessage')->name('apiSendMessage');
    Route::get('mail','GeneralController@showEmailTest')->name('showEmailTest');


    Route::get('client_brief','GeneralController@showBriefClient')->name('showBriefClient');
    // Route::get('resume/','GeneralController@showResumeClient')->name('showResumeClient');
});
